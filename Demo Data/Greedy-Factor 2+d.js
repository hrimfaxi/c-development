/*
	Demonstrates approximation factor o the Greedy algorithm.
	The curves consist of two overlaid sections that can be
	repeated arbitrarily.

	The resulting free-space diagram shows a regular pattern 
	of circles and dumb-bell-like structures. It can be extended
	to the right and to the top arbitrarily.

	Thre greedy algorithm will choose components at the bottom
	and at the right, while the optimal choice are components on
	the diagonal.

	If x is the number of repetitions, then we have:

	k_greedy = 4x-2
	k_opt = 2x

	(n=m=5x+1, 1/2 <= epsilon < 1);

	The appromixation factor 2-1/x converges to 2 for growing x.
*/

function criss(p) {
	P.M(0,-1).M(-p,-p).M(p,p).M(0,+1);
	P.M(-1,0).M(-p,-p).M(p,p).M(+1,0);
	P.Z().lineStyle(SOLID);	//	back to start
}

function cross(p) {
	Q.M(-1,0).M(-p,p).M(p,-p).M(+1,0);
	Q.M(0,+1).M(-p,p).M(p,-p).M(0,-1);
	Q.Z().lineStyle(SOLID);	//	back to start
}

var x = 6;
P.M(+0.5,-0.5);
Q.M(-0.5,-0.5);

P.setDefaultLineStyle(NONE);
Q.setDefaultLineStyle(NONE);

var s = 0.25/(x-1);
for(var i=x-1; i >= 0; --i) {
	criss(i*s);
	cross(i*s);
}

P.outerLineStyle(SOLID);
Q.outerLineStyle(SOLID);

//	scale for better visualisation
//P.scale(100);
//Q.scale(100);




