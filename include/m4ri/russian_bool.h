/**
 * \file russian_boolean.h
 * \brief M4RI and M4RM over Boolean.
 *
 * \author Gregory Bard <bard@fordham.edu>
 * \author Martin Albrecht <martinralbrecht@googlemail.com>
 * \author Peter Schäfer <pete.schaefer@gmail.com>
 *
 * \note For reference see Gregory Bard; Accelerating Cryptanalysis with
 * the Method of Four Russians; 2006;
 * http://eprint.iacr.org/2006/251.pdf
 */

#ifndef M4RI_RUSSIAN_BOOL_H
#define M4RI_RUSSIAN_BOOL_H

 /*******************************************************************
 *
 *                 M4RI:  Linear Algebra over GF(2)
 *
 *    Copyright (C) 2007, 2008 Gregory Bard <bard@fordham.edu>
 *    Copyright (C) 2008-2010 Martin Albrecht <martinralbrecht@googlemail.com>
 *
 *  Distributed under the terms of the GNU General Public License (GPL)
 *  version 2 or higher.
 *
 *    This code is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    General Public License for more details.
 *
 *  The full text of the GPL is available at:
 *
 *                  http://www.gnu.org/licenses/
 *
 ********************************************************************/

#include <m4ri/brilliantrussian.h>
#include <m4ri/mzd_bool.h>

/**
 * \brief Matrix multiplication using Konrod's method, i.e. compute C
 * such that C == AB. 
 *
 * This is the convenient wrapper function, please see _mzd_bool_mul_m4rm
 * for authors and implementation details.
 *
 * Use Boolean addtion (OR).
 *
 * \param C Preallocated product matrix, may be NULL for automatic creation.
 * \param A Input matrix A
 * \param B Input matrix B
 * \param k M4RI parameter, may be 0 for auto-choose.
 *
 *
 * \return Pointer to C.
 */

mzd_t *mzd_bool_mul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int blocksize, int threads);


/**
 * \brief Matrix multiplication using Konrod's method, i.e. compute C
 * such that C == AB.
 *
 * This is the convenient wrapper function, please see _mzd_bool_mul_m4rm
 * for authors and implementation details.
 *
 * Assumes A and B, two symmetrical, upper triangular matrices.
 *
 * \param C Preallocated product matrix, may be NULL for automatic creation.
 * \param A Input upper triangular matrix A,
 * \param B Input upper triangular matrix B,
 * \param k M4RI parameter, may be 0 for auto-choose.
 * \param blocksize size of work-set, choose carefully. may be 0 for auto-choose.
 * \param number of parallel threads
 *
 * \return Pointer to C.
 */

mzd_t *mzd_bool_mul_uptri(mzd_t *C, mzd_t const *A, mzd_t const *B, 
							int k, int blocksize, int threads);

/**
* calculate optimal values for k and blocksize
*
* \param concurrency number of parallel matrix operations (ideally 1)
*/

void mzd_optimal_parameters(
        const mzd_t *A, const mzd_t *B,
	    int l2_cache_size, int l3_cache_size,
	    int *k, int *blocksize);


/**
 * Set C to C + AB using Konrod's method.
 *
 * This is the convenient wrapper function, please see _mzd_bool_mul_m4rm
 * for authors and implementation details.
 *
 * Use Boolean addtion (OR).
 *
 * \param C Preallocated product matrix, may be NULL for zero matrix.
 * \param A Input matrix A
 * \param B Input matrix B
 * \param k M4RI parameter, may be 0 for auto-choose.
 *
 *
 * \return Pointer to C.
 */

mzd_t *mzd_bool_addmul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int blocksize, int threads);

/**
 * \brief Matrix multiplication using Konrod's method, i.e. compute C such
 * that C == AB.
 *
 * Use Boolean addtion (OR).
 *
 * This is the actual implementation.
 * 
 * This function is described in Martin Albrecht, Gregory Bard and
 * William Hart; Efficient Multiplication of Dense Matrices over
 * GF(2); pre-print available at http://arxiv.org/abs/0811.1714
 *
 * \param C Preallocated product matrix.
 * \param A Input matrix A
 * \param B Input matrix B
 * \param k M4RI parameter, may be 0 for auto-choose.
 * \param clear clear the matrix C first
 *
 * \author Martin Albrecht -- initial implementation
 * \author William Hart -- block matrix implementation, use of several
 * Gray code tables, general speed-ups
 *
 *
 * \return Pointer to C.
 */

mzd_t *_mzd_bool_mul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int clear, int blocksize, int threads);
mzd_t *_mzd_bool_mul_uptri(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int clear, int blocksize, int threads);


#endif // M4RI_RUSSIAN_BOOL_H
