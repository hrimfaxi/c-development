/**
 * \file mzd_bool.h
 * \brief Dense matrices over Boolean represented as a bit field.
 *
 * \author Gregory Bard <bard@fordham.edu>
 * \author Martin Albrecht <martinralbrecht+m4ri@googlemail.com>
 * \author Carlo Wood <carlo@alinoe.com>
 * \author Peter Schäfer <pete.schaefer@gmail.com>
 */

#ifndef M4RI_MZD_BOOL
#define M4RI_MZD_BOOL

/*******************************************************************
*
*                M4RI: Linear Algebra over GF(2)
*
*    Copyright (C) 2007, 2008 Gregory Bard <bard@fordham.edu>
*    Copyright (C) 2008-2013 Martin Albrecht <M.R.Albrecht@rhul.ac.uk>
*    Copyright (C) 2011 Carlo Wood <carlo@alinoe.com>
*
*  Distributed under the terms of the GNU General Public License (GPL)
*  version 2 or higher.
*
*    This code is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*  The full text of the GPL is available at:
*
*                  http://www.gnu.org/licenses/
*
********************************************************************/

#include <m4ri/mzd.h>

/**
 * \brief OR n bits from values to M starting a position (x,y).
 *
 * \param M Source matrix.
 * \param x Starting row.
 * \param y Starting column.
 * \param n Number of bits (<= m4ri_radix);
 * \param values Word with values;
 */

static inline void mzd_or_bits(mzd_t const *M, rci_t const x, rci_t const y, int const n, word values) {
    assert(n <= m4ri_radix);
    int const spot   = y % m4ri_radix;
    wi_t const block = y / m4ri_radix;
    M->rows[x][block] |= values << spot;
    int const space = m4ri_radix - spot;
    if (n > space) {
        M->rows[x][block + 1] |= values >> space;
    }
}

/**
 * \brief Add the rows sourcerow and destrow and stores the total in the row
 * destrow, but only begins at the column coloffset.
 * Use Boolean addition (OR).
 *
 * \param M Matrix
 * \param dstrow Index of target row
 * \param srcrow Index of source row
 * \param coloffset Start column (0 <= coloffset < M->ncols)
 *
 * \warning This function expects that there is at least one word worth of work.
 */

static inline void mzd_bool_row_add_offset(mzd_t *M, rci_t dstrow, rci_t srcrow, rci_t coloffset) {
  assert(dstrow < M->nrows && srcrow < M->nrows && coloffset < M->ncols);
  wi_t const startblock = coloffset / m4ri_radix;
  wi_t wide             = M->width - startblock;
  word *src             = M->rows[srcrow] + startblock;
  word *dst             = M->rows[dstrow] + startblock;
  word const mask_begin = __M4RI_RIGHT_BITMASK(m4ri_radix - coloffset % m4ri_radix);
  word const mask_end   = M->high_bitmask;

  *dst++ |= *src++ & mask_begin;
  --wide;

#if __M4RI_HAVE_SSE2
  int not_aligned = __M4RI_ALIGNMENT(src, 16) != 0; /* 0: Aligned, 1: Not aligned */
  if (wide > not_aligned + 1)                       /* Speed up for small matrices */
  {
    if (not_aligned) {
      *dst++ |= *src++;
      --wide;
    }
    /* Now wide > 1 */
    __m128i *__src     = (__m128i *)src;
    __m128i *__dst     = (__m128i *)dst;
    __m128i *const eof = (__m128i *)((unsigned long)(src + wide) & ~0xFUL);
    do {
      __m128i xmm1 = _mm_or_si128(*__dst, *__src);
      *__dst++     = xmm1;
    } while (++__src < eof);
    src  = (word *)__src;
    dst  = (word *)__dst;
    wide = ((sizeof(word) * wide) % 16) / sizeof(word);
  }
#endif
  wi_t i = -1;
  while (++i < wide) {
    dst[i] |= src[i];
  }
  /*
   * Revert possibly non-zero excess bits.
   * Note that i == wide here, and wide can be 0.
   * But really, src[wide - 1] is M->rows[srcrow][M->width - 1] ;)
   * We use i - 1 here to let the compiler know these are the same addresses
   * that we last accessed, in the previous loop.
   */
  dst[i - 1] |= src[i - 1] & ~mask_end;

  __M4RI_DD_ROW(M, dstrow);
}

/**
 * \brief Add the rows sourcerow and destrow and stores the total in
 * the row destrow.
 *  Use Boolean addition (OR).
 *
 * \param M Matrix
 * \param sourcerow Index of source row
 * \param destrow Index of target row
 *
 * \note this can be done much faster with mzd_combine.
 */

void mzd_bool_row_add(mzd_t *M, rci_t const sourcerow, rci_t const destrow);

/**
 * \brief Naive cubic matrix multiplication.
 * That is, compute C such that C == AB.
 *
 *  Use Boolean addition (OR).
 *
 * \param C Preallocated product matrix, may be NULL for automatic creation.
 * \param A Input matrix A.
 * \param B Input matrix B.
 *
 * \note Normally, if you will multiply several times by b, it is
 * smarter to calculate bT yourself, and keep it, and then use the
 * function called _mzd_mul_naive
 *
 */
mzd_t *mzd_bool_mul_naive(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

/**
 * \brief Naive cubic matrix multiplication and addition
 * That is, compute C such that C == C + AB.
 *
 *  Use Boolean addition (OR).
 *
 * \param C Preallocated product matrix.
 * \param A Input matrix A.
 * \param B Input matrix B.
 *
 * \note Normally, if you will multiply several times by b, it is
 * smarter to calculate bT yourself, and keep it, and then use the
 * function called _mzd_mul_naive
 */

mzd_t *mzd_bool_addmul_naive(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

/**
 * \brief Naive cubic matrix multiplication with the pre-transposed B.
 * That is, compute C such that C == AB^t.
 *
 *  Use Boolean addition (OR).
 *
 * \param C Preallocated product matrix.
 * \param A Input matrix A.
 * \param B Pre-transposed input matrix B.
 * \param clear Whether to clear C before accumulating AB
 */

mzd_t *_mzd_bool_mul_naive(mzd_t *C, mzd_t const *A, mzd_t const *B, int const clear, int threads);

/**
 * \brief Matrix multiplication optimized for v*A where v is a vector.
 *
 *  Use Boolean addition (OR).
 *
 * \param C Preallocated product matrix.
 * \param v Input matrix v.
 * \param A Input matrix A.
 * \param clear If set clear C first, otherwise add result to C.
 *
 */
mzd_t *_mzd_bool_mul_va(mzd_t *C, mzd_t const *v, mzd_t const *A, int const clear);

/**
 * \brief Set C = A+B.
 *
 *  Use Boolean addition (OR).
 *
 * C is also returned. If C is NULL then a new matrix is created which
 * must be freed by mzd_free.
 *
 * \param C Preallocated sum matrix, may be NULL for automatic creation.
 * \param A Matrix
 * \param B Matrix
 */

mzd_t *mzd_bool_add(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

/**
 * \brief Same as mzd_bool_add but without any checks on the input.
 *
 *  Use Boolean addition (OR).
 *
 * \param C Preallocated sum matrix, may be NULL for automatic creation.
 * \param A Matrix
 * \param B Matrix
 */

mzd_t *_mzd_bool_add(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

/**
 * \brief Set C = A & B. (Boolean AND)
 *
 * C is also returned. If C is NULL then a new matrix is created which
 * must be freed by mzd_free.
 *
 * \param C Preallocated sum matrix, may be NULL for automatic creation.
 * \param A Matrix
 * \param B Matrix
 */

mzd_t *mzd_bool_and(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

mzd_t *mzd_bool_and_uptri(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

/**
 * \brief Same as mzd_bool_and but without any checks on the input.
 *
 * \param C Preallocated sum matrix, may be NULL for automatic creation.
 * \param A Matrix
 * \param B Matrix
 */

mzd_t *_mzd_bool_and(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

mzd_t *_mzd_bool_and_uptri(mzd_t *C, mzd_t const *A, mzd_t const *B, int threads);

/**
 * \brief a_row[a_startblock:] += b_row[b_startblock:] for offset 0
 *
 *  Use Boolean addition (OR).
 *
 * Adds a_row of A, starting with a_startblock to the end, to
 * b_row of B, starting with b_startblock to the end. This gets stored
 * in A, in a_row, starting with a_startblock.
 *
 * \param A destination matrix
 * \param a_row destination row for matrix C
 * \param a_startblock starting block to work on in matrix C
 * \param B source matrix
 * \param b_row source row for matrix B
 * \param b_startblock starting block to work on in matrix B
 *
 */

static inline void mzd_bool_or_even_in_place(mzd_t *A,       rci_t const a_row, wi_t const a_startblock,
                                             mzd_t const *B, rci_t const b_row, wi_t const b_startblock) {

  wi_t wide = A->width - a_startblock - 1;

  word *a = A->rows[a_row] + a_startblock;
  word *b = B->rows[b_row] + b_startblock;

#if __M4RI_HAVE_SSE2
  if (wide > 2) {
    /** check alignments **/
    if (__M4RI_ALIGNMENT(a, 16)) {
      *a++ |= *b++;
      wide--;
    }

    if (__M4RI_ALIGNMENT(a, 16) == 0 && __M4RI_ALIGNMENT(b, 16) == 0) {
      __m128i *a128      = (__m128i *)a;
      __m128i *b128      = (__m128i *)b;
      const __m128i *eof = (__m128i *)((uint64_t)(a + wide) & ~0xFULL);

      do {
        *a128 = _mm_or_si128(*a128, *b128);
        ++b128;
        ++a128;
      } while (a128 < eof);

      a    = (word *)a128;
      b    = (word *)b128;
      wide = ((sizeof(word) * wide) % 16) / sizeof(word);
    }
  }
#endif  // __M4RI_HAVE_SSE2

  if (wide > 0) {
    wi_t n = (wide + 7) / 8;
    switch (wide % 8) {
    case 0: do { *(a++) |= *(b++);
    case 7:      *(a++) |= *(b++);
    case 6:      *(a++) |= *(b++);
    case 5:      *(a++) |= *(b++);
    case 4:      *(a++) |= *(b++);
    case 3:      *(a++) |= *(b++);
    case 2:      *(a++) |= *(b++);
    case 1:      *(a++) |= *(b++);
    } while (--n > 0);
    }
  }

  *a |= *b & A->high_bitmask;

  __M4RI_DD_MZD(A);
}

/**
 * \brief c_row[c_startblock:] = a_row[a_startblock:] + b_row[b_startblock:] for offset 0
 *
 *  Use Boolean addition (OR).
 *
 * Adds a_row of A, starting with a_startblock to the end, to
 * b_row of B, starting with b_startblock to the end. This gets stored
 * in C, in c_row, starting with c_startblock.
 *
 * \param C destination matrix
 * \param c_row destination row for matrix C
 * \param c_startblock starting block to work on in matrix C
 * \param A source matrix
 * \param a_row source row for matrix A
 * \param a_startblock starting block to work on in matrix A
 * \param B source matrix
 * \param b_row source row for matrix B
 * \param b_startblock starting block to work on in matrix B
 *
 */

static inline void mzd_bool_or_even(mzd_t *C,       rci_t const c_row, wi_t const c_startblock,
                                    mzd_t const *A, rci_t const a_row, wi_t const a_startblock, 
                                    mzd_t const *B, rci_t const b_row, wi_t const b_startblock) {

  wi_t wide = A->width - a_startblock - 1;
  word *a   = A->rows[a_row] + a_startblock;
  word *b   = B->rows[b_row] + b_startblock;
  word *c   = C->rows[c_row] + c_startblock;

#if __M4RI_HAVE_SSE2
  if (wide > 2) {
    /** check alignments **/
    if (__M4RI_ALIGNMENT(a, 16)) {
      *c++ = *b++ | *a++;
      wide--;
    }

    if ((__M4RI_ALIGNMENT(b, 16) | __M4RI_ALIGNMENT(c, 16)) == 0) {
      __m128i *a128      = (__m128i *)a;
      __m128i *b128      = (__m128i *)b;
      __m128i *c128      = (__m128i *)c;
      const __m128i *eof = (__m128i *)((uint64_t)(a + wide) & ~0xFULL);

      do {
        *c128 = _mm_or_si128(*a128, *b128);
        ++c128;
        ++b128;
        ++a128;
      } while (a128 < eof);

      a    = (word *)a128;
      b    = (word *)b128;
      c    = (word *)c128;
      wide = ((sizeof(word) * wide) % 16) / sizeof(word);
    }
  }
#endif  // __M4RI_HAVE_SSE2

  if (wide > 0) {
    wi_t n = (wide + 7) / 8;
    switch (wide % 8) {
    case 0: do { *(c++) = *(a++) | *(b++);
    case 7:      *(c++) = *(a++) | *(b++);
    case 6:      *(c++) = *(a++) | *(b++);
    case 5:      *(c++) = *(a++) | *(b++);
    case 4:      *(c++) = *(a++) | *(b++);
    case 3:      *(c++) = *(a++) | *(b++);
    case 2:      *(c++) = *(a++) | *(b++);
    case 1:      *(c++) = *(a++) | *(b++);
    } while (--n > 0);
    }
  }
  *c = ((*a | *b) & C->high_bitmask);

  __M4RI_DD_MZD(C);
}

/**
 * \brief c_row[c_startblock:] = a_row[a_startblock:] & b_row[b_startblock:] for offset 0
 *
 *  Use Boolean addition (OR).
 *
 * Adds a_row of A, starting with a_startblock to the end, to
 * b_row of B, starting with b_startblock to the end. This gets stored
 * in C, in c_row, starting with c_startblock.
 *
 * \param C destination matrix
 * \param c_row destination row for matrix C
 * \param c_startblock starting block to work on in matrix C
 * \param A source matrix
 * \param a_row source row for matrix A
 * \param a_startblock starting block to work on in matrix A
 * \param B source matrix
 * \param b_row source row for matrix B
 * \param b_startblock starting block to work on in matrix B
 *
 */

static inline void mzd_bool_and_even(mzd_t *C,       rci_t const c_row, wi_t const c_startblock,
                                    mzd_t const *A, rci_t const a_row, wi_t const a_startblock,
                                    mzd_t const *B, rci_t const b_row, wi_t const b_startblock) {

  wi_t wide = A->width - a_startblock - 1;
  word *a   = A->rows[a_row] + a_startblock;
  word *b   = B->rows[b_row] + b_startblock;
  word *c   = C->rows[c_row] + c_startblock;

#if __M4RI_HAVE_SSE2
  if (wide > 2) {
    /** check alignments **/
    if (__M4RI_ALIGNMENT(a, 16)) {
      *c++ = *b++ & *a++;
      wide--;
    }

    if ((__M4RI_ALIGNMENT(b, 16) | __M4RI_ALIGNMENT(c, 16)) == 0) {
      __m128i *a128      = (__m128i *)a;
      __m128i *b128      = (__m128i *)b;
      __m128i *c128      = (__m128i *)c;
      const __m128i *eof = (__m128i *)((uint64_t)(a + wide) & ~0xFULL);

      do {
        *c128 = _mm_and_si128(*a128, *b128);
        ++c128;
        ++b128;
        ++a128;
      } while (a128 < eof);

      a    = (word *)a128;
      b    = (word *)b128;
      c    = (word *)c128;
      wide = ((sizeof(word) * wide) % 16) / sizeof(word);
    }
  }
#endif  // __M4RI_HAVE_SSE2

  if (wide > 0) {
    wi_t n = (wide + 7) / 8;
    switch (wide % 8) {
    case 0: do { *(c++) = *(a++) & *(b++);
    case 7:      *(c++) = *(a++) & *(b++);
    case 6:      *(c++) = *(a++) & *(b++);
    case 5:      *(c++) = *(a++) & *(b++);
    case 4:      *(c++) = *(a++) & *(b++);
    case 3:      *(c++) = *(a++) & *(b++);
    case 2:      *(c++) = *(a++) & *(b++);
    case 1:      *(c++) = *(a++) & *(b++);
    } while (--n > 0);
    }
  }
  *c = ((*a & *b) & C->high_bitmask);

  __M4RI_DD_MZD(C);
}

/**
 * \brief row3[col3:] = row1[col1:] + row2[col2:]
 *
 *  Use Boolean addition (OR).
 *
 * Adds row1 of SC1, starting with startblock1 to the end, to
 * row2 of SC2, starting with startblock2 to the end. This gets stored
 * in DST, in row3, starting with startblock3.
 *
 * \param C destination matrix
 * \param c_row destination row for matrix dst
 * \param c_startblock starting block to work on in matrix dst
 * \param A source matrix
 * \param a_row source row for matrix sc1
 * \param a_startblock starting block to work on in matrix sc1
 * \param B source matrix
 * \param b_row source row for matrix sc2
 * \param b_startblock starting block to work on in matrix sc2
 *
 */
static inline void mzd_bool_or(mzd_t *C,       rci_t const c_row, wi_t const c_startblock,
                               mzd_t const *A, rci_t const a_row, wi_t const a_startblock, 
                               mzd_t const *B, rci_t const b_row, wi_t const b_startblock) {

  if ((C == A) & (a_row == c_row) & (a_startblock == c_startblock)) {
    mzd_bool_or_even_in_place(C, c_row, c_startblock, B, b_row, b_startblock);
  } else {
    mzd_bool_or_even(C, c_row, c_startblock, A, a_row, a_startblock, B, b_row, b_startblock);
  }
  return;
}

#endif  // M4RI_MZD_BOOL
