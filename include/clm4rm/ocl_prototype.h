#ifndef OCL_PROTOTYPE_H
#define OCL_PROTOTYPE_H

#include <clm4rm.h>

#if defined(__cplusplus) && !defined(_MSC_VER)
extern "C" {
#endif
#include <m4ri/mzd.h>
#if defined(__cplusplus) && !defined(_MSC_VER)
}
#endif

/*
 *  C prototype for M4R matrix multiplication
 */

mzd_t *proto_bool_mul_m4rm(mzd_t *Cm, mzd_t const *Am, mzd_t const *Bm, int k);

void proto_mul_m4rm(
		gpuword *C, const gpuword *A, const gpuword *B, /*gpuword * T,*/ int k,
	int A_nrows, int A_ncols, int B_ncols);


mzd_t *proto_bool_mul_cubic(mzd_t *Cm, mzd_t const *Am, mzd_t const *Bm, int);

void proto_mul_cubic(gpuword *C, const gpuword *A, const gpuword *B, /*gpuword * T,*/
					int A_nrows, int A_ncols, int B_ncols);

int nblocks(mzd_t const* A);
int adjust_k(int k, rci_t A_nrows);

#endif // OCL_PROTOTYPE_H
