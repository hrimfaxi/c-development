/**
 *  M4RM on the GPU
 */

#ifndef CLM4RM_H
#define CLM4RM_H
/**
 * @file clm4rm.h
 * Perform Boolean matrix arithmetics on the GPU.
 * Based on OpenCL.
 *
 * Kernels are provided for arithmetic, like bitwise operation, multiplication, etc.
 *
 * Methods are provided for allocating and copying data from CPU memory to GPU memory
 * and back.
 */

// Includes the normal OpenCL C header
#if defined(__APPLE__) || defined(__MACOSX)
# include <OpenCL/opencl.h>
#else
# include <CL/opencl.h>
#endif

//  host data structures from M4RI
//  row-major 64-bit unsigned integers
#if defined(__cplusplus) && !defined(_MSC_VER)
extern "C" {
#endif
#include <m4ri/mzd.h>
#if defined(__cplusplus) && !defined(_MSC_VER)
}
#endif

/**
 * @name basic definitions
 * @{
 */

//!  word size. for compatibility with GPU memory layout, we operate on 32 bit words.
#define clm4rm_radix 32

/**
* OpenCL data can be stored as Buffer object or Image objects,
* the latter being supposedly faster (really?). Within a kernel,
* an image buffer can be only read-only or write-only, with is
* alright with us.
*
* Turns out that the different is actually marginal.
* Note: texture memory if limited (about 2G on a Tesla V100).
* Global buffer memory is not.
*/
#define IMAGE2D 0
/**
 * Use shared GPU memory to buffer tiles of the matrix.
 */
#define BUFFERED 1
/**
  * max. tile parameter
  * (the actual size of a tile is 32*m*n)
  */
#define MAX_TILE_M 6

//! word size of GPU data (32 bits)
typedef uint32_t gpuword;
//! tow-dimensional size; used for various OpenCL parameters
typedef size_t size2_t[2];

//! integer division with rounding to a multiple of y
#define CEILDIV(x,y)    (((x)+(y)-1)/(y))

//! integer division with rounding
#define FLOOR(x,y)      ((y)*((x)/(y)))

//! integer division by number of bits per word
#define CEILCOLS(i)	    CEILDIV(i,clm4rm_radix)

#define POW2(i)		    (((gpuword)1)<<(i))

/** @} */

/**
 * @name matrix storage
 * @{
 */

/**
 * @brief OpenCL boolean matrix data structure.
 *  Data is arranged in 32 bit words.
 *
 *  Words are stored in column-major order
 *  (as to optimise GPU memory access patterns).
 *
 *  Keeps two copies of matrix data: one in CPU memory
 *  and one in GPU memory.
 *  Methods are provided to move data back and forth.
 */
struct clmatrix_t {
    rci_t nrows;        //!< Number of rows
    rci_t padded_rows;  //!< Number of rows padded to a multiple of 32
    rci_t ncols;        //!< Number of columns.
    rci_t padded_cols;  //!< Number of columns padded to a multiple of 64
    rci_t width;        //!< Number of words with valid bits: width = ceil(ncols / m4ri_radix) */
    /*
     * Offset in words between rows.
     *
     * rowstride = ((width & 1) == 0) ? width : width + 1;
     * where width is the width of the underlying non-windowed matrix.
     * @deprecated not used anymore
     */
    //wi_t rowstride;

    gpuword* local_data;//!< matrix data in CPU memory
    cl_mem data; //!< handle to GPU data (32-bit unsigned integers)

    //  TODO indicators for Triangular matrices ?
};
typedef struct clmatrix_t clmatrix_t;
#define DATA_BYTES(m)       ( (m)->padded_rows * (m)->width * sizeof(gpuword) )

/**
 * @brief calculate the number of padded rows
 * @param nrows actual matrix rows
 * @param padding desired padding (32, or 64)
 * @return number of padded rows
 */
int padded_rows(int nrows, int padding);

/**
 * @brief create a column-major copy from an mzd_t matrix
 * @param dest destination data in clmatrix format
 * @param src input data in M4RI format
 * @param padded_rows number of words (padded)
 * @return pointer to CPU matrix data
 */
gpuword*	copy_matrix_data(gpuword* dest, const mzd_t* src, int padded_rows);

/**
 * @brief copy back a colum--major matrix
 * @param dest destination data in M4RI format
 * @param src input data
 * @param padded_rows number of rows (padded)
 */
void		copy_back_matrix_data(mzd_t* dest, const gpuword* src, int padded_rows);

/** @} */

/**
 * @name global variables
 * @{
 */

//! latest OpenCL result code. CL_SUCCESS indicates no error.
extern cl_int clm4rm_error;

//! max. size of a work group
extern size_t max_group_size;
//!  max. number of items in each dimension
extern size_t max_items[3];

//!	size of shared memory in bytes
extern size_t shared_mem_bytes;
//!	size of shared memory in (32bit) words
extern size_t shared_mem_words;
//!	size of allocated memory in bytes
extern size_t heap_size, allocated_size;
//!	max. object allocation size
extern size_t max_object_size;

/**
 * @brief load OpenCL kernels and set up parameters
 * @param cl_kernel_directory location on disk where the kernel source code files  (*.cl) are stored
 * @param ctx OpenCL context
 * @param device OpenCL device
 * @return OpenCL error code. 0 means no error.
 */
cl_int clm4rm_setup(const char* cl_kernel_directory,
                    cl_context ctx, cl_device_id device);
/**
 * @brief release OpenCL resources
 * @param ctx OpenCL context
 * @param device OpenCL device
 */
void clm4rm_tear_down(cl_context ctx, cl_device_id device);

/** @} */


/**
 * @name event handling
 * @{
 */

#define MAX_EVENTS 6

/**
 * @brief a list of cl_events; used by clm4rm_conditions
 * to keep track of schedules jobs in the OpenCL queue.
 */
struct clm4rm_event_list {
    cl_uint count;  //!< current number of events
    cl_event events[MAX_EVENTS];    //!< array of OpenCL events
};
typedef struct clm4rm_event_list clm4rm_event_list;
/**
 * @brief reset events list
 * @param list a list of OpenCL events
 */
void init_events(clm4rm_event_list* list);
/**
 * @brief release events
 * @param list a list of OpenCL events
 */
void release_events(clm4rm_event_list* list);
/**
 * @brief append tow lists
 * @param a a list of OpenCL events
 * @param b another list of OpenCL events
 */
void merge_events(clm4rm_event_list* a, clm4rm_event_list* b);

/**
 * 	@brief Manages OpenCL event dependencies;
 * 	necessary when the queue is out-of-order;
 * 	dependencies must be established through cl_event
 */
struct clm4rm_conditions {
    //!<	pre-conditions and post-conditions
	clm4rm_event_list event_lists [2];
    //! @brief pre-conditions: an operation is scheduled when all pre-conditions are met
    clm4rm_event_list *pre;
    //! @brief post-conditions: conditions after an operation finishes.
    //! post-conditions may act as pre-conditioins for the next operation.
    clm4rm_event_list *post;
};
typedef struct clm4rm_conditions clm4rm_conditions;
/**
 * @brief reset conditions list
 * @param cond a list of pre- and post-conditions
 */
void init_conditions(clm4rm_conditions* cond);
/**
 * @brief release conditions list
 * @param cond a list of pre- and post-conditions
 */
void release_conditions(clm4rm_conditions* cond);
/**
 * @brief called when the pre-conditions are met.
 * The post-conditions become new pre-conditioins.
 * @param cond a list of pre- and post-conditions
 */
void join_conditions(clm4rm_conditions* cond);
/**
 * @brief merge pre-conditions into one list
 * @param a a list of pre- and post-conditions
 * @param b another list of pre- and post-conditions
 */
void merge_conditions(clm4rm_conditions* a, clm4rm_conditions* b);

/**
 * @param cond a list of pre- and post-conditions
 * @return number of pre-conditioins
 */
cl_uint pre_count(clm4rm_conditions* cond);
/**
 * @param cond  a list of pre- and post-conditions
 * @return pointer to list of pre-conditions
 */
cl_event* pre_events(clm4rm_conditions* cond);
/**
 * @brief reserve one post-condition event
 * @param cond  a list of pre- and post-conditions
 * @return pointer to reserved event
 */
cl_event* push_event(clm4rm_conditions* cond);
/**
 * @param cond a list of pre- and post-conditions
 * @return pointer to last reserved event
 */
cl_event* pushed_event(clm4rm_conditions* cond);

/** @} */

/**
 * @name matrix operations
 * @{
 */

/**
 * @brief create an empty matrix
 * @param rows number of rows
 * @param cols number of columns
 * @param rowpadding pad rows to multiples of 32, or 64
 * @param read_only 1 if the GPU memory buffer should be read only
 * @param ctx OpenCL context
 * @return a newly allocated matrix structure.
 *  Both, CPU memory and GPU memory are allocated.
 */
clmatrix_t* clm4rm_create(rci_t rows, rci_t cols, int rowpadding,
                    int read_only, cl_context ctx);
/**
 * @brief ceate a copy from a matrix in M4RI format
 * @param host_matrix matrix data in M4RI format
 * @param rowpadding desired padding
 * @param read_only if 1, create a read-only buffer in GPU memory
 * @param ctx OpenCL context
 * @return a newly allocated matrix structure.
 *  Both, CPU memory and GPU memory are allocated and filled with data.
 */
clmatrix_t* clm4rm_copy(const mzd_t* host_matrix, int rowpadding,
                    int read_only, cl_context ctx);

//? clmatrix_t* clm4rm_copy(clmatrix_t* gpu_matrix);

/**
 * @brief Fill a matrix with zero data.
 * The operation is scheduled for asynchronous execution of the GPU.
 * The function returns immediately. Use post-condition events to wait
 * for the execution of the operation.
 * @param gpu_matrix a matrix structure
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 */
void clm4rm_zero_fill(clmatrix_t* gpu_matrix,
                    cl_command_queue  queue, clm4rm_conditions* cond);

/**
 * @brief Copy matrix data from host memory to GPU.
 * The operation is scheduled for asynchronous execution of the GPU.
 * The function returns immediately. Use post-condition events to wait
 * for the execution of the operation.
 * @param gpu_matrix a matrix structure
 * @param host_matrix  matrix data in M4RI format
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 */
void clm4rm_write(clmatrix_t* gpu_matrix, const mzd_t* host_matrix,
                    cl_command_queue queue, clm4rm_conditions* cond);



/**
 * @brief copy matrix from gpu memory to host
 * @param host_matrix matrix data in M4RI format; if nullptr, allocate a new one
 * @param gpu_matrix a matrix structure
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 * @return pointer to a matrix structure inf M4RI format
 */
mzd_t* clm4rm_read(mzd_t* host_matrix, clmatrix_t* gpu_matrix, 
                    cl_command_queue queue, clm4rm_conditions* cond);

/**
 * @brief release memory (CPU and GPU)
 * @param gpu_matrix a matrix structure
 */
void clm4rm_free(clmatrix_t* gpu_matrix);

/**
 * @brief Boolean matrix multiplication on the GPU using the method of the Four Russians.
 * C := A * B
 *
 * The function returns immediately.
 * The operation is scheduled for asynchronous execution of the GPU.
 * Use post-condition events to wait for the execution of the operation.
 *
 * @param C a matrix structure; receives the resutl
 * @param A a matrix structure
 * @param B a matrix structure
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 */
void clm4rm_mul(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
                        cl_command_queue queue, clm4rm_conditions* cond);
/**
 * @brief Boolean matrix multiplication on the GPU using nested loops.
 * C := A*B
 *
 * The function returns immediately.
 * The operation is scheduled for asynchronous execution of the GPU.
 * Use post-condition events to wait for the execution of the operation.
 *
 * @param C a matrix structure; receives the resutl
 * @param A a matrix structure
 * @param B a matrix structure
 * @param max_tile max. size of tiles
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 */
void clcubic_mul(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
					 	size2_t max_tile,
                        cl_command_queue queue, clm4rm_conditions* cond);

/**
 * @brief Boolean matrix multiplication on the GPU using nested loops.
 * C := A*B
 * Assumes matrixes to be upper triangular
 *
 * The function returns immediately.
 * The operation is scheduled for asynchronous execution of the GPU.
 * Use post-condition events to wait for the execution of the operation.
 *
 * @param C a matrix structure; receives the resutl
 * @param A an upper triangular matrix structure
 * @param B an upper triangular matrix structure
 * @param max_tile max. size of tiles
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 */
void clutri_mul(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
						size2_t max_tile,
                        cl_command_queue queue, clm4rm_conditions* cond);

/*
 * @brief C := C + A*B
 * @deprecated not used anymore
 */
/*cl_event clm4rm_addmul(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
                       cl_command_queue queue);*/

/**
 * @brief concatenate two matrices
 * @deprecated not used anymore
 */
void clm4rm_stack(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
                      cl_command_queue queue, clm4rm_conditions* cond);

/**
 * @brief concatenate two matrices
 * @deprecated not used anymore
 */
void clm4rm_concat(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
                       cl_command_queue queue, clm4rm_conditions* cond);

/**
 * @brief perform element-wise logical disjunction (OR)
 * @deprecated not used anymore
 */
void clm4rm_or(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
                   cl_command_queue queue, clm4rm_conditions* cond);

/**
 * @brief perform element-wise logical conjunction (AND).
 * For each entry, compute
 *  C_ij := A_ij & B_ij.
 *  All input matrices must have the same size.
 * @param C a Boolean matrix; holds the result on return
 * @param A an input Boolean matrix
 * @param B an input Boolean matrix
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 */
void clm4rm_and(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
                    cl_command_queue queue, clm4rm_conditions* cond);
//	TODO
//cl_event clutri_and(clmatrix_t* C, clmatrix_t* A, clmatrix_t* B,
//					 cl_command_queue queue, int wait_for_it);


//  @returns
/**
 * @brief find a non-zero entry on the diagonal of a matrix.
 * Return the column/row of the first non-zero entry, or -1 if all entries are zero.
 * The operation does not immediately return a result.
 * It performs asynchronously. Use the post-conditions variables to wait for
 * the execution of the operation, then use clm4rm_query_result to retrieve the actual result.
 * @param M a matrix
 * @param ctx OpenCL context
 * @param queue OpenCL command queue
 * @param cond keeps track of pre-conditions and newly created post-conditions
 * @return a memory buffer that holds exactly one integer. It will eventually hold the result.
 */
cl_mem clm4rm_query_diagonal(clmatrix_t* M,
							cl_context ctx, cl_command_queue queue,
                            clm4rm_conditions* cond);
/**
 * @brief examine the result of a previous call to clm4rm_query_diagonal
 * @param result_buffer buffer that holds one integer. Was returned by clm4rm_query_diagonal.
 * Will be released.
 * @param queue OpenCL command queue
 * @param cond  keeps track of pre-conditions and newly created post-conditions
 * @return the column/row of the first non-zero entry, or -1 if all entries are zero.
 */
int clm4rm_query_result(cl_mem result_buffer,
                        cl_command_queue queue,
                        clm4rm_conditions* cond);

/* @} */


#endif //CLM4RM_H
