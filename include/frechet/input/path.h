#ifndef PATH_H
#define PATH_H

#include <QObject>
#include <QTransform>
#include <QPolygonF>

#include <string>
#include <grid.h>



/**
 * @brief Represents a polygonal curve.
 *
 *  Used when constructing curves from input data.
 *  Has methods for assembling and modifying the curve point by point.
 *
 *  This class appears in the JavaScript interface,
 *  so many methods are exposed as Qt slots. All methods marked as Q_INVOKABLE
 *  can be referred to from JavaScript. All methods return 'this' as result,
 *  allowing for a fluent API.
 *  @see https://bitbucket.org/hrimfaxi/c-development/wiki/ScriptFiles.md
 *
 *  Note that this class has to live in the global namespace, rather than in
 *  the frechet::input namespace.
 *
 * @author Peter Schäfer
 */
class Path : public QObject {
    Q_OBJECT
private:
    //! @brief the list of vertexes
    QPolygonF points;
    //! @brief used during assembly of a path: the next "move" operation
    std::string opcode;
    //! @brief turtle_angle current direction of the turtle
    double turtle_angle;

    //! @brief default style for grid disaplay
    //! @see setDefaultLineStyle
    LineStyle defaultLineStyle;
    //! @brief line styles for grid disaplay
    //! @see lineStyle
    LineStyleVector lineStyles;

public:
   /*
    *  All methods that are marked as Q_INVOKABLE
    *  are exposed to JavaScript.
    * */

    /**
     * @brief default constructor
     * @param parent owning QObject, optional
     */
    Q_INVOKABLE
    Path(QObject* parent=0);

    /**
     * @brief copy constructor
     * @param that object to copy from
     */
    Q_INVOKABLE
    Path(const Path& that);
//    Path(const Path&& that) : points(that.points), rel(that.rel) { }

    /**
     * @brief copy constructor
     * @param that object to copy from
     */
    Q_INVOKABLE
    Path(const Path* that);
//    Path(const Path&& that) : points(that.points), rel(that.rel) { }

    /**
     * @brief constructor from SVG input string
     * @param svg an svg-encoded polygonal path
     * @param forceAbsolute assume all commands are absolute movements (no relative movements)
     * @param parent owning QObject, optional
     * @see SVG specification https://www.w3.org/TR/SVG/paths.html
     */
    Q_INVOKABLE
    Path(QString svg, bool forceAbsolute=false, QObject* parent=0);

    /**
     * @brief static constructor from SVG input string
     * @param str an svg-encoded polygonal path
     * @param forceAbsolute assume all commands are absolute movements (no relative movements)
     * @see SVG specification https://www.w3.org/TR/SVG/paths.html
     * @return a Path object, constructed from SVG input
     */
    static Path svgPath(QString str, bool forceAbsolute=false);
    /**
     * @brief static constructor from IPE input string
     * @param str an IPE-encoded polygonal curve
     * @return a Path object, constructed from IPE input
     */
    static Path ipePath(QString str);

    /**
     * @brief convert to a QPolygonF object, ready for processing by the Frechet distance algorithms
     * @param precision limit numerical precision (optional, default is most precise)
     * @return a polygonal curve with absolute vertex data
     */
    const QPolygonF& toCurve(int precision=0);

    /**
     * @brief assignment operator
     * @param that object to copy from
     * @return this object, containing a copy of the input
     */
    Q_INVOKABLE
    Path& operator= (const Path& that);
 //   Path& operator= (const Path&&);

    /**
     * @brief append operator
     * @param that object to copy from
     * @return this object, after appending a copy of the input
     */
    Q_INVOKABLE
    Path& operator+= (const Path& that);

    /**
     * @brief append operator with SVG input string
     * @param svg an svg-encoded polygonal path
     * @see SVG specification https://www.w3.org/TR/SVG/paths.html
     * @return this object, after appending a copy of the input
     */
    Q_INVOKABLE
    Path& operator+= (QString svg);

public slots:
    //! \return number of vertexes in the polygonal curve
    int size() const { return points.size(); }

    /*
     * All public slots are exposed to JavaScript.
     * Cool. But works only with Path*.
     **/
    /**
     * @brief appends a copy of another Path
     * @param that object to copy from
     * @return this object, after appending a copy of the input
     */
    Path* appendPath(Path* that) { (*this) += (*that); return this; }
    /**
     * @brief appends a copy of an SVG path
     * @param svg
     * @see SVG specification https://www.w3.org/TR/SVG/paths.html
     * @return this object, after appending a copy of the input
     */
    Path* appendString(QString svg) { (*this) += svg; return this; }

    /*
     * append points
     * absolute:
     */
    /**
     * @brief append a new vertex at an absolute position
     * @param p next vertex
     * @return this object, after appending one more vertex
     */
    Path* M(QPointF p) { append(p,'M'); return this; }
    /**
     * @brief append a new vertex at an absolute position
     * @param x x-coordinate of next vertex
     * @param y y-coordinate of next vertex
     * @return this object, after appending one more vertex
     */
    Path* M(double x, double y) { return M(QPointF(x,y)); }
    /**
     * @brief horizontal move to an absolute position
     * @param h x-coordinate of next vertex
     * @return this object, after appending one more vertex
     */
    Path* H(double h) { append(QPointF(h,0),'H'); return this; }
    /**
     * @brief vertical move to an absolute position
     * @param v y-coordinate of next vertex
     * @return this object, after appending one more vertex
     */
    Path* V(double v) { append(QPointF(0,v),'V'); return this; }
    /**
     * @brief return to the first vertex, creating a closed curve
     * @return this object, after appending one more vertex
     */
    Path* close();
    /**
     * @brief return to the first vertex, creating a closed curve
     * @return this object, after appending one more vertex
     */
    Path* Z();
    /**
     * @brief remove the last vertex
     * @return this object, after remove one vertex
     */
    Path* undo();
    /**
     * @brief make sure the curve is closed
     * @return this object, appending one more vertex, if necessary
     */
    Path* ensureClosed();

    /*
     * append points
     * relative:
     */
    /**
     * @brief append a new vertex, relative to the current position
     * @param p offset to next vertex
     * @return this object, after appending one more vertex
     */
    Path* m(QPointF p) { append(p,'m'); return this; }
    /**
     * @brief append a new vertex, relative to the current position
     * @param x x-offset to next vertex
     * @param y y-offset to next vertex
     * @return this object, after appending one more vertex
     */
    Path* m(double x, double y) { return m(QPointF(x,y)); }
    /**
     * @brief horizontal move, relative to the current position
     * @param h horizontal offset
     * @return this object, after appending one more vertex
     */
    Path* h(double h) { append(QPointF(h,0),'h'); return this; }
    /**
     * @brief vertical move, relative to the current position
     * @param v vertical offset
     * @return this object, after appending one more vertex
     */
    Path* v(double v) { append(QPointF(0,v),'v'); return this; }
    /**
     * @brief append a new vertex, given by polar coordinates
     * @param angle direction of movement
     * @param distance distance from current position
     * @return this object, after appending one more vertex
     */
    Path* polar(double angle,double distance);

    /*
     * Transforms
     */
    /**
     * @brief apply a transformation to all vertexes
     * @param tf an affine transformation
     * @return this object, after applying the transformation
     */
    Path* map(QTransform tf);
    /**
     * @brief apply a transformation to all vertexes
     * @param mx an affine transformation
     * @return this object, after applying the transformation
     */
    Path* map(QMatrix mx);

    /**
     * @brief scale the polygonal curve
     * @param scale scaling factor
     * @return this object, after applying the scaling
     */
    Path* scale(double scale);
    /**
     * @brief scale the polygonal curve
     * @param scalex horizontal scaling factor
     * @param scaley vertital scaling factor
     * @return this object, after applying the scaling
     */
    Path* scale(double scalex, double scaley);
    /**
     * @brief rotate the curve
     * @param angle rotation angle (in degrees)
     * @return this object, after applying the rotation
     */
    Path* rotate(double angle);

    /**
     * @brief translate the curve
     * @param offset translation offset
     * @return this object, after applying the translation
     */
    Path* translate(QPointF offset);
    /**
     * @brief translate the curve
     * @param dx horizontal translation offset
     * @param dy vertical translation offset
     * @return this object, after applying the translation
     */
    Path* translate(double dx, double dy);
    /**
     * @brief mirror the curve about the x-axis
     * @return this object, after applying the mirror transformation
     */
    Path* mirrorx();
    /**
     * @brief mirror the curve about the y-axis
     * @return this object, after applying the mirror transformation
     */
    Path* mirrory();
    /**
     * @brief revert the order of vertexes
     * @return this object, after reverting the order of vertexes
     */
    Path* revert();

    /*
     *  Turtle Graphics
     * */
    /**
     * @brief turn the turtle's head to the left
     * @param angle in degrees
     * @return this object
     */
    Path* left(double angle);
    /**
     * @brief turn the turtle's head to the right
     * @param angle in degrees
     * @return this object
     */
    Path* right(double angle);
    /**
     * @brief turn the turtle's head to a given angle
     * @param angle new direction
     * @return this object
     */
    Path* reset(double angle=0.0);
    /**
     * @brief append a new vertex, relative to the current position
     * @param distance distance to move
     * @return this object, after appending a new vertex
     */
    Path* forward(double distance);

    /**
     * @brief change the style of grid lines
     * @see LineStyle
     */
    void lineStyle(int);
    /**
     * @brief change the default style of grid lines
     * @see LineStyle
     */
    void setDefaultLineStyle(int);
    /**
     * @brief set the style of outermost grid lines
     * @see LineStyle
     */
    void outerLineStyle(int);

public:
    /*
     * String parsing
     */
    //! @brief parse an SVG input string
    void parseSvg(QString str, bool forceAbsolute=false);
    //! @brief pare an IPE input string
    void parseIpe(QString str);
    //! @return current default grid line style
    LineStyle getDefaultLineStyle() const { return defaultLineStyle; }
    //! @return line styles for drawing the grid
    const LineStyleVector& getLineStyles() const { return lineStyles; }

private:
    //! append a point to the curve
    //! @param p new point
    //! @param opcode path operation (move etc.)
    void append(QPointF p, char opcode);
    //! replace all relative movements by absolute positions
    void absolutize();
    //! set the numerical precision for input data (default is unlimited)
    void setPrecision(int precision);
    //! remove all duplicate points    
    void removeDuplicates();
   // void concat(QBitArray& a, const QBitArray& b);
};

#endif // PATH_H
