#ifndef INPUTREADER_H
#define INPUTREADER_H


#include <QAbstractMessageHandler>
#include <QXmlItem>
#include <QTransform>

#include <types.h>
#include <datapath.h>
#include <path.h>

namespace frechet { namespace input {

/**
 * @brief The InputReader class reads input data from a file.
 *
 *  XML filfes like:
 *  -   SVG files
 *  -    IPE files
 *  -   (KML Google Earth not implemented)
 *  -   (CSV files not implemented)
 *
 *  - JavaScript files (.js,.jscript)
 *
 * As well as from (g)zipped files
 *      KMZ (=zipped kml)
 *      SVGZ (=gzipped svg)
 *
 * @author Peter Schäfer
 */
class InputReader {
//    Q_OBJECT ?
private:
    //! result values; expected to contain 2 result
    QVector<Path> results;
    //! parse error
    QString error_message;    
    bool is_svg; //!< is it an SVG file?
    bool is_ipe; //!< is it an IPE file?
    bool is_kml; //!< not implemented
    bool is_script; //!< is it a JavaScript file

public:
    //! @return result data: 2 polygonal curves
    QVector<Path>& getResults() { return results; }

    //! @brief clear results
    void clear();
    //! @brief read polygonal curves from a file
    //! @param path location of inpupt data within the file
    void readAll(const DataPath& path);

    //! @return true if the input is an SVG file
    bool isSvg() const { return is_svg; }
    //! @return true if the input is an IPE file
    bool isIpe() const { return is_ipe; }
    //! @return true if the input is an SVG file
    bool isScript() const { return is_script; }

    //! @return error message during reading a file
    QString errorMessage() { return error_message; }

private:
    //! @brief read polygonal curves from a file
    //! @param device input stream
    //! @param fileSuffix file name suffix determines the file type
    //! @param queryString path into XML structure
    void readAll(QIODevice* device, QString fileSuffix, QString queryString);
    //! @brief read polygonal curves from an XML file
    //! @param device input stream
    //! @param fileSuffix file name suffix determines the file type
    //! @param xmlPath path into XML structure
    void readXml(QIODevice* device, QString fileSuffix, QString xmlPath);
    //! @brief read a polygonal curve from an SVG string
    //! @param text SVG data string
    //! @param tf apply transform (optional)
    //! @return a polygonal curve
    Path readSvgPath(QString text, QTransform tf = QTransform());
    //! @brief read a polygonal curve from an IPE string
    //! @param text IPE data string
    //! @param tf apply transform (optional)
    //! @return a polygonal curve
    Path readIpePath(QString text, QTransform tf = QTransform());
    //! @brief read a JavaScript file
    //! @param program JavaScript code
    //! @return true if successful
    bool readScript(QString program);

    //! @param suffix file name suffix
    //! @return is it a ZIP file ?
    static bool isZipFile(QString suffix);
    //! @param suffix file name suffix
    //! @return is it a GZIP file ?
    static bool isGzipFile(QString suffix);
    //! @param suffix file name suffix
    //! @return is it an XML file ?
    static bool isXmlFile(QString suffix);
    //! @param suffix file name suffix
    //! @return is it a CSV file ?
    static bool isCsvFile(QString suffix);
    //! @param suffix file name suffix
    //! @return is it an SVG file ?
    static bool isSvgFile(QString suffix);
    //! @param suffix file name suffix
    //! @return is it an IPE file ?
    static bool isIpeFile(QString suffix);
    //! @param suffix file name suffix
    //! @return is it a JavaScript file ?
    static bool isScriptFile(QString suffix);

    //! @brief handles XML parse errors; used for debugging only
    class XmlMessageHandler: public QAbstractMessageHandler {
        void handleMessage(QtMsgType type,
                           const QString &description,
                           const QUrl &identifier,
                           const QSourceLocation &sourceLocation);
    };
    /**
     * @brief read a transformation matrix from XML
     * @param item XML item that contains the transformation matrix
     * @param namepool implements an XML namespace
     * @return an affine transformation
     */
    QTransform readTransformationMatrix(QXmlItem item, QXmlNamePool namepool);
};


} }   //  namespace frechet::input
#endif // INPUTREADER_H
