#ifndef DATAPATH_H
#define DATAPATH_H

#include <QFileInfo>

namespace frechet { namespace input {

typedef std::pair<int,int> IndexInterval;

/**
 * @brief location of input data in an XML file
 *
 * Inpupt data (SVG,IPE) contains XML data. The location
 * of the paths we ant to read consists of:
 *  - file path
 *  - file within zipped file (optional)
 *  - XQuery path to actual poly data (for xml based files)
 *
 * String format
 *      file path [! zip file path] [? xml path] [ [min,max] interva]]
 *
 * @author Peter Schäfer
 */
class DataPath {
private:    
    QFileInfo file; //!< file
    QFileInfo archFile; //!< file inside zip (not used)
    QString selector; //!< XQuery path into XML structure
    IndexInterval intval; //!< which entries to read

    static const char ZIP_SEPARATOR = '!';
    static const char XML_SEPARATOR = '?';
    static const char INTVAL_SEPARATOR = '[';

public:
    //! default xml path for svg files (= all path elements and the attribute "d")
    static const QString SVG_QUERY;
    //! default xml path for ipe files
    static const QString IPE_QUERY;
    //! Google earth files. Not implemented.
    static const QString KML_QUERY;

    //! name filters for file input dialogs
    static const QStringList INPUT_FILTERS;

    //! @return path to file
    QFileInfo   getFile() const      { return file; }
    //! @return path inside zip file (not used)
    QFileInfo   getArchFile()  const  { return archFile; }
    //! @return path to XML structure
    QString     getSelector() const   { return selector; }
    //! @return number of entries to read
    IndexInterval    getInterval() const  { return intval; }

    //! @brief constructor from string
    //! @param arg path encoded as string
    DataPath(QString arg);
};

} }  //  namespace frechet::input

#endif // DATAPATH_H
