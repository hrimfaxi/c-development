#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QRunnable>
#include <QMutex>
#include <QThreadPool>

#include <iostream>
#include <freespace.h>

namespace frechet { namespace app {

class WorkerJobHandle;

/**
 * @brief thrown by long-runner tasks
 *
 * Long-runner tasks regularly poll for user interruption.
 * The preferred way of stopping a long-runner task is throwing
 * this exception.
 *
 * This class contains no data but it indicates that a user interruption
 * took place.
 * 
 * @see WorkerJob
 */
class InterruptedException : public std::exception {

};

/**
 *  @brief Background job to run long-runner tasks in a seperate thread.
 *
 *  Implements QRunnable and is scheduled in a global QThreadPool.
 *
 *  Long-running tasks include the algorithms for simple polygons,
 *  and the k-Fréchet "brute-force" algorithms.
 *
 *  Jobs can be cancelled by setting the cancelled flag.
 *
 *  When the WorkerJob object is passed to QThreadPool, the thread
 *  pool takes ownership of the object. We must exert some care
 *  when accessing a pointer to WorkerJob.
 *  WorkerJobHandle takes care of that.
 *
 * @author Peter Schäfer
 */
class WorkerJob : public QRunnable
{
private:
    friend class WorkerJobHandle;
    //! reference to thread-safe handle
    WorkerJobHandle* handle;
public:
    //! cancellation flag
    /** This flag is set by the application when the user requests
        the interruption of a long-running taks (usually by clicking the 'Stop' button).

        Algorithm implementations are expected to poll this flag regularly.
        When the flag is set, an InterruptedException is thrown
        and caught by the run() method.

        Note that the variable is declared volatile because it will be accessed
        from different threads.
     */
    volatile bool cancelRequested;

    //! abstract method for starting the job
    virtual void runJob()=0;
    //! abstract method that is called after the job was interrupted.
    //! Implementors can use this method to clean up resources.
    virtual void afterInterrupted()=0;

    //! abstract method that performs the long-running task
    virtual void run() override;
};

/**
 *  @brief Thread safe access to a WorkerJob.
 *
 *  Is automatically reset when the Job finishes.
 *
 *  @author Peter Schäfer
 */
class WorkerJobHandle
{
private:
    //! mutex protects read/write access.
    QMutex mut;
    //! the actual Worker Job object.
    WorkerJob* job;
public:
    //! default constructor with no job
    WorkerJobHandle() : mut(), job(NULL) { }
    //! @brief start a new job
    //! @param new_job pointer to job
    void startJob(WorkerJob* new_job);
    //! request the job to be cancelled
    void cancelJob();
    //! @brief release control of the WorkerJob object
    //! @param ajob a worker job
    void invalidate(WorkerJob* ajob);
    //! cancel all remaining tasks and shut down the global QThreaPool
    void shutDown();
};



} } //  namespaces

#endif // WORKERTHREAD_H
