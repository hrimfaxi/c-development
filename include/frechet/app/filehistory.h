#ifndef FILEHISTORY_H
#define FILEHISTORY_H

#include <QSettings>
#include <QDateTime>
#include <QMenu>

namespace frechet { namespace app {
/**
 * @brief an entry in the recently opened file list
 */
struct FileEntry
{
public:
    //! absolute path to file
    QString path;
    //! unique identifier issued by FileHistory
    size_t id;
    //! time when the file was last used
    QDateTime used;

    /**
     * @brief default constructor
     * @param apath absolute path to file
     * @param id unique identifier issued by FileHistory
     * @param adate time when the file was last used
     */
    FileEntry(QString apath,
              size_t id,
              QDateTime adate = QDateTime::currentDateTime());

    /**
     * @brief compare files by their last-used date
     * @param that another file
     * @return true, if last-used date is *later* than 'that'
     */
    bool operator< (const FileEntry& that) const;
};

/**
 * @brief keeps track of recently used files.
 *
 * This list appears in the File / Open Recent menu.
 * It is stored to disk as part of the application preferences.
 *
 * @author Peter Schäfer
 */
class FileHistory : public QObject
{
    Q_OBJECT
private:
    //! list of file IDs; sorted by LRU
    QList<FileEntry> files;
    //! group identifier if preferences
    QString groupName;
    //! next identifier
    size_t next_id;
    //! pointer to File/Open Recent menu
    QMenu* menu;
signals:
    /**
     * @brief raised when a file is selected from the "Open Recent" menu
     */
    void open(QString);

public:
    //! empty constructor
    FileHistory();
    /**
     * @brief initialize file list and menu
     * @param agroup group identifier in preferences
     */
    void init(QString agroup);
    /**
     * @brief read settings from application preferences
     * @param settings application preferences
     */
    void restore(QSettings& settings);

    /**
     * @brief insert a new file into the list
     * @param path absolute file path
     * @param date last-used date (defaults to current date)
     * @return a unique identifier (for the lifetime of the application)
     */
    size_t insert(QString path, QDateTime date = QDateTime::currentDateTime());
    /**
     * @return last file ID
     */
    size_t lastId() const;
    /**
     * @return last file name
     */
    QString lastFile() const;
    /**
     * @brief attach sub-menu
     * @param recent QAction that holds the File/Open Recent menu
     */
    void attachMenu(QAction* recent);

    /**
     * @brief begin reading or updating file specific settings
     * @param settings application preferences
     * @param id file identifier (as issued by insert())
     * @param forWrite if true, file settings will be updated
     */
    void beginFile(QSettings& settings, size_t id, bool forWrite=false);
    /**
     * @brief end updating file specific settings
     * @param settings application preferences
     */
    void endFile(QSettings& settings);

public slots:
    //! @brief called when an item from the Open Recent menu is selected
    void onMenuSelected();

private:
    //! @brief find file by name
    //! @param path complete file name and path
    //! @return index of file in list (or -1 if not found)
    int indexOf(QString path);
    //! @brief find file by ID
    //! @param id file ID
    //! @return index of file in list (or -1 if not found)
    int indexOf(size_t id);

    //! @brief create a new menu item in File/Open Recent
    //! @param entry an entry in file history
    //! @param position index in menu
    //! @return a newly create menu action
    QAction* createMenuItem(const FileEntry& entry, int position);
    //! @brief move a menu item
    //! @param from old index
    //! @param to new index
    void moveMenuItem(int from, int to);

};

} } // namespaec

#endif // FILEHISTORY_H
