#ifndef FRECHETVIEWAPPLICATION_H
#define FRECHETVIEWAPPLICATION_H

#include <QApplication>
#include <QFileSystemWatcher>
#include <exception>

#include <data/interval.h>
#include <input/datapath.h>
#include <input/inputreader.h>

#include <view/mainwindow.h>
#include <poly/algorithm.h>
#include <filehistory.h>
#include <workerthread.h>

namespace frechet { namespace app {

using namespace frechet;
using namespace view;
using namespace reach;
using namespace poly;
using namespace k;
using namespace input;
/**
 * @brief the application singleton
 *
 * Manages all central concerns of the Qt application
 *
 * FrechetViewApplication is responsible for setting up
 * the GUI, opening files, starting and stopping algorithms,
 * managing data structures, and some more...
 *
 * Usually such a class would be derived from QGuiApplication.
 * We have chosen another approach, with QApplication being a separate
 * object. This allows to run FrechetViewApplication either with a GUI,
 * or head-less as a command-line application.
 *
 * @see main.cpp
 *
 * @author Peter Schäfer
 */
class FrechetViewApplication : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief we have three Fréchet Distance algorithms
     */
    enum Algorithm {
        //!  compute "classical" Fréchet distance
        //!  between two polygonal curves
        ALGORITHM_CURVE=0,
        //!  compute Fréchet distance between simple polygons
        ALGORITHM_POLYGON=1,
        //!  compute k-Fréchet distance between two curves
        ALGORITHM_K_FRECHET=2
    };

    /**
     * @brief GPU support (for headless mode only)
     */
    enum TriState {
        NO,     //!< no GPU
        YES,    //!< GPU required
        DEFAULT //!< default = use GPU, if available
    };

protected:
    //! singleton instance
    static FrechetViewApplication* frechetViewApp;

    //! Open Recent file history
    FileHistory history;
    //! reference to current File
    size_t currentId;

    //! reads files from disk
    InputReader reader;
    //! pointer to input data within an xml file
    DataPath path;
    //! keeps track of changed files on disk.
    //! If the current file changes on disk, it will be automatically reloaded.
    QFileSystemWatcher* fileWatch;

    Curve P; //!< first input curve
    Curve Q; //!< second input curve
    //! manages the free-space grid
    Grid::ptr grid;
    //! the free-space diagram
    FreeSpace::ptr freeSpace;
    //!  selects current Algorithm
    Algorithm _currentAlgorithm;
    //!  the feasible path (if available)
    FSPath::ptr fspath;
    //!  the k-Frechet algorithm (if available)
    k::kAlgorithm::ptr kAlgorithm;
    //!  the algorithm for simple polygons and curves (if available)
    poly::Algorithm::ptr polyAlgorithm;
    //! long-running job.
    //! some algorithms take long and are delegated to a background thread.
    WorkerJobHandle bgJob;
    //! maximum value for epsilon
    double epsMax;
    //! the main window
    MainWindow* window;

public:
    //  note: int &   is essential !!
    //  otherwise QApplication references a stack location !!
    /**
     * @brief default constructor
     * @param exec_path path to executable file
     * @param withGui set up GUI, or run head-less
     */
    FrechetViewApplication(QString exec_path, bool withGui);
    //! @brief desctructor; closes all resources and windows
    ~FrechetViewApplication();

    /**
     * @brief initialise the GUI application and open a file (optional)
     * @param file file to open (optional, may be empty)
     */
    void init(QString file);

    /**
     * @brief run the command line interface
     * @param inputFile file to process
     * @param algo select an algorithm
     * @param topo parameter to poly algorithm: process matrixes in topological order, saving memory
     * @param accuracy parameter to approximation algorithms
     * @param epsilon epsilon for the decision variant
     * @throw std::exception if there is an error in the input, or parameters are not correct
     * @return 0 if successful
     */
    int commandLineInterface(QString inputFile, Algorithm algo, bool topo,
                            double accuracy, double epsilon) throw(std::exception);

    /**
     * @return the currently selected algorithm
     */
    Algorithm currentAlgorithm() const          { return _currentAlgorithm; }
    /**
     * @brief test current algorithm
     * @param alg an algorithm
     * @return true if current algorithm is equal to 'alg'
     */
    bool currentAlgorithm(Algorithm alg) const  { return _currentAlgorithm==alg; }
    /**
     * @brief change current algorithm
     * @param alg new algorithm
     */
    void setCurrentAlgorithm(Algorithm alg);
    /**
     * @return a handle to the current background job, or nullptr
     */
    WorkerJobHandle* backgroundJob() { return &bgJob; }

signals:
    /**
     * @brief raised when a new file is opened. Triggers changes in all parts of the GUI.
     * @param file file name
     */
    void fileOpened(QString file);
    /**
     * @brief raised when another algorithm is selected. Triggers changes in all parts of the GUI.
     * @param algo int algorithm (of type Algorithm)
     */
    void algorithmChanged(int algo);

    //  epsilon changes are triggered by ControlPanel

public slots:
    /**
     * @brief initialises the GUI
     * @param firstTime show user hint when no file is opened (optional)
     */
    void open(bool firstTime=false);
    /**
     * @brief initialises the GUI and opens a file
     * @param filePath absolute path to file
     */
    void open(QString filePath);
    /**
     * @return singleton instance
     */
    static FrechetViewApplication* instance() {
        return frechetViewApp;
    }
    /**
     * @brief quit the application, closing all resources and windows
     */
    void quit() {
        windowClosed(window);
        QApplication::quit();
    }
    /**
     * @brief called when the main window is closed by the user
     */
    void windowClosed(QMainWindow*)
    {
        QSettings settings;
        if (currentId) saveFileSettings(settings,currentId);
        window->saveSettings(settings);
    }

    //! @return the free-space grid
    Grid::ptr       getGrid()               { return grid; }
    //! @return the free-space diagram
    FreeSpace::ptr  getFreeSpace()          { return freeSpace; }
    //! @return the feasible path (if available)
    FSPath::ptr     getFSPath()             { return fspath; }
    //! @return the k-Frechet algorith (if available)
    kAlgorithm::ptr getKAlgorithm()         { return kAlgorithm; }
    //! @return the poly-algorithm (if available)
    poly::Algorithm::ptr getPolyAlgorithm() { return polyAlgorithm; }

    //! @return the first input curve
    Curve&         getP()                  { return P; }
    //! @return the second input curve
    Curve&         getQ()                  { return Q; }

    //! @brief stops running algorithm(s)
    void resetAlgorithms();

    //! @brief opens the "About" dialog
    void showAboutDialog();
    //! @brief creates an application icon on the users desktop
    void createDesktopEntry();
    //! @brief opens the input file in an editor
    void editInputFile();

    //! @brief starts, or stops the decision algorithm for simple polygons
    void startStopDecidePolygon();
    //! @brief starts, or stops the optimisation algorithm for curves
    void startStopOptimiseCurve();
    //! @brief starts, or stops the approximation algorithm for curves
    void startStopApproximateCurve();
    //! @brief starts, or stops the optimisation algorithm for simple polygons
    void startStopOptimisePolygon();
    //! @brief starts, or stops the approximation algorithm for simple polygons
    void startStopApproximatePolygon();

    //! @brief starts the brute-force k-Frechet algorithm
    void startKBruteForce();
    //! @brief stops long-running algorithm (if necessary)
    void cancelBackgroundJob();

private:
    //! path to executable file
    QString execPath;
    /**
     * @brief save file specific settings to preferences
     * @param settings application preferences
     * @param histId ID in file history
     */
    void saveFileSettings(QSettings& settings, size_t histId);
    /**
     * @brief read file specific settings from preferences
     * @param settings application preferences
     * @param histId ID in file history
     */
    void restoreFileSettings(QSettings& settings, size_t histId);
    /**
     * @brief show a short hint when the application is opened for the very first time
     * @return true if successful
     */
    bool showFirstTimeHint();
    /**
     * @brief set up geometric data structures, like triangulations etc.
     * @param topo
     * @return true if successful
     */
    bool setupCurves(bool topo=false);

    //! @brief start or stop decision algorithm for simple polygons
    //! @param epsilon
    void startStopDecidePolygon(double epsilon);
    //! @brief start or stop algorithm for curves
    //! @param approx if > 0, run approximation algorithm. if ==0, run optimisation algorithm.
    void startStopOptimiseCurve(double approx);
    //! @brief start or stop algorithm for simple polygons
    //! @param approx if > 0, run approximation algorithm. if ==0, run optimisation algorithm.
    void startStopOptimisePolygon(double approx);

    //! @brief create a desktop icon on a Linux system (or try at least..)
    void installLinuxDesktopIcons(QSettings&);
    //! @brief create a desktop file on a Linux system (or try at least..)
    void installLinuxDesktopEntries(QSettings&);
    //! @brief find location of desktpo file
    QString linuxDesktopFile();
    //! for presentation mode only
    bool populateBookmarks();

    /**
     * @brief prepare the application for command line mode
     * @param inputFile input file
     * @param algo algorithm to perform
     */
    void cli_setup(QString inputFile, Algorithm algo);
    /**
     * @brief run the k-Frechet brute-force algorithm in headless mode
     * @param epsilon
     * @return optimal k, or < 0 if no solution is found
     */
    int cli_decideKFrechet(double epsilon);

    //! @brief copy data to a file on disk
    static void copyContents(QByteArray contents, QString path);
    //! @brief copy files on disk
    static void copyFile(QString src, QString dest);
};

} } //  namespace

#endif // FRECHETVIEWAPPLICATION_H
