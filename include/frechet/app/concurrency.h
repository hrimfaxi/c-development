
#ifndef FRECHET_VIEW_CONCURRENCY_H
#define FRECHET_VIEW_CONCURRENCY_H

#include <tbb/task_scheduler_init.h>
#include <tbb/tbb.h>
#include <chrono>
#include <clm4rm.h>

namespace frechet { namespace app {
/**
 * @brief a singleton class managing concurrency settings for the application.
 *
 * It controls the number of parallel threads (for use by TBB functions).
 *
 * It sets up OpenCL contexts for use by a GPU device.
 *
 * Also acts as a factory for poly::Algorithm
 *
 * @author Peter Schäfer
 */
class ConcurrencyContext
{
private:
    /**
     * @brief controls the number of parallel threads
     * that are used by TBB functions.
     */
    tbb::task_scheduler_init* tbb_context;
    //! number of physical cores (if known)
    int num_cores;
    //! number of logical threads (always known)
    int num_threads;

    //! OpenCL platform identifier
	cl_platform_id platform = nullptr;
    //! OpenCL platform identifier
    cl_context ctx;
    //! OpenCL device
	cl_device_id device = nullptr;
    //! OpenCL command queue
	cl_command_queue queue = nullptr;
    //! last OpenCL event
	cl_event event = nullptr;
    //! last OpenCL return code
	cl_int clerr;

    //! name of OpenCL device
	std::string device_name;
    //! number of GPU units (if known)
	cl_uint gpu_units;
    //! maximum tile size for Boolean matrix multiplication
	size2_t max_tile;

public:
    //! @brief default constructor; does no initialisation
    ConcurrencyContext();
    //! @brief destructor; releases all OpenCL resources
    ~ConcurrencyContext();
    //! singleton instance
	static ConcurrencyContext instance;

    /**
     * @brief set up TBB thread count
     * @param max_threads maximum number of thread
     */
	void setup(int max_threads);
    /**
     * @brief set up OpenCL context
     * @param max_tile maximum tile size for Boolean matrix multiplication
     * @return true, if OpenCL initialisation was succesfull.
     *  false, if OpenCL is not available.
     */
	bool setupGpu(size2_t max_tile);
    /**
     * @brief release all OpenCL resources.
     */
	void close();

    //! @return number of logical threads
	static int countThreads();
    //! @return number of physical cores (not always accurate!)
	static int countCores();
    //! @return identifier for current thread
	static tbb::tbb_thread::id currentThread();

    //! @return true if GPU support is available
	static bool hasGpuSupport();

    //! @return name of the GPU device (if available)
	static std::string gpuName();
    //! @return number of GPU units (if available)
	static cl_uint countGpuUnits();

    //! @return the OpenCL context
	static cl_context clContext();
    //! @return the OpenCL command queue
    static cl_command_queue clQueue();

	/**
     * @brief set tile size for cubic matrix multiplication.
	 * Tiles adapt to local memory and/or compute units.
	 * This is the upper limit; usually {4, 4..8}.
	 */
	static void maxMaxtrixTile(size2_t&);

    //	System info: number of physical cores
    //static int countCpuCores();
    //	System info: size (in bytes) of L1,2,3 cache
    /**
     * @brief size of CPU cache memory
     * @param level cache level (L1,L2,L3)
     * @return size of cache memory in bytes
     */
	static int cacheSize(int level);	

private:
    //! @brief retrieve CPU cache sizes
	void findCacheSizes();
    //! @brief retrieve CPU factory info
	static void cpuid(int leaf, unsigned int regs[4]);
    //! @brief retrieve CPU factory info
	static void cpuid(int leaf, int level, unsigned int regs[4]);
    //! @brief look up directory containing OpenCL source files ("kernels")
	static const char* findKernelDirectory(const char* subdir);
};

//  Debug timers
/**
 * @brief clock with high resolution; used for performing benchmark measurements
 */
typedef std::chrono::high_resolution_clock Clock;
/**
 * @brief timestamp with high resolution
 */
typedef Clock::time_point time_point;

/**
 * @brief start a new benchmark timer and push it to stack
 */
void pushTimer();
/**
 * @brief clock benchmark
 * @param label text info printed to std-out
 * @param do_print print value to std-out
 * @return elapsed time since starting the last timer
 */
time_point printTimer(std::string label, bool do_print = true);
/**
 * @brief clock benchmark and remove from stack
 * @param label text info printed to std-out
 * @param do_print print value to std-out
 * @return elapsed time since starting the last timer
 */
time_point popTimer(std::string label, bool do_print = true);
/**
 * @brief clock benchmark and print elapsed time
 * @param label text info printed to std-out
 * @return elapsed time since starting the last timer
 */
time_point printDebugTimer(std::string label);
/**
 * @brief clock benchmark and remove from stack
 * @param label text info printed to std-out
 * @return elapsed time since starting the last timer
 */
time_point popDebugTimer(std::string label);

} }  //namespace frechet

#endif //FRECHET_VIEW_CONCURRENCY_H
