#ifndef ANIMATION_H
#define ANIMATION_H

#include <QDialog>
#include <QGraphicsLineItem>
#include <QPropertyAnimation>

class QMainWindow;
class QGraphicsScene;
class QGraphicsView;
class QGraphicsSvgItem;
class QAbstractAnimation;

namespace frechet { namespace free { class GridAxis; } }

namespace frechet { namespace view {

class MainWindow;
class AnimatedLineItem;
class AnimatedGroup;

class AnimationDialog: public QDialog
{
private:
    QGraphicsView* gview;
    QGraphicsScene* scene;    
    QAbstractAnimation* animation;    
public:
    static const QPen PEN_P,PEN_Q,LEASH_PEN,PROJ_PEN;
    static const QFont TEXT_FONT;
public:
    AnimationDialog(QMainWindow* parent);
    ~AnimationDialog() {}

    void activate();

    void resetAnimation(bool fspath);
    void resetFSAnimation(double startValue, double endValue, int duration);
    void resetProjectionAnimation(QPointF x);
    void startStopAnimation();
    void stopAnimation();

private:
    void setupUI(QRect);
    double gridLength(QPolygonF) const;
    double gridLength() const;
    //QAnimation* createAnimation(QPolygonF);
    static void setTransparent(QWidget* widget);

    QAbstractAnimation* createManDogAnimation();
    QAbstractAnimation* createKangarooAnimation();
    QAbstractAnimation* createAxesAnimation();
    QAbstractAnimation* create1AxisAnimation(bool horizontal);
    QAbstractAnimation* createKangarooAnimation(AnimatedGroup* group, QPointF* points, int count, int duration);
    QAbstractAnimation* createKangarooJump(AnimatedGroup* group, QPointF from, QPointF to, int duration);

    QPointF remapCurvePoint(QPointF) const;
    QPointF remapGridPoint(QPointF p) const;

    QPropertyAnimation *create1LineAnimation(bool horizontal, int i, int j, const QPen &pen);
};

class FeasiblePathAnimation: public QPropertyAnimation
{
private:
    bool wrap_x,wrap_y;
    double max_x,max_y;
public:
    FeasiblePathAnimation(QObject *parent = nullptr);
    virtual QVariant interpolated(const QVariant &from, const QVariant &to, qreal progress) const override;
};

/**
 * For animating QGraphicsItems, we need to derived from
 * QObject and provide properties.
 *
 * @brief The AnimatedLineItem class
 */
class AnimatedLineItem: public QObject, public QGraphicsLineItem
{
    Q_OBJECT
    Q_PROPERTY(QLineF line READ line WRITE setLine)
    //  TODO provide properties t1 \in [0..n], t2 \in [0..m] and map them to locations in P,Q
};

class AnimatedEllipseItem: public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF center READ center WRITE setCenter)
    Q_PROPERTY(QRectF rect READ rect WRITE setRect)

public slots:
    QPointF center() const          {
        return rect().center();
    }
    void setCenter(QPointF p)       {
        QRectF r = rect();
        r.moveCenter(p);
        setRect(r);
    }
};

class AnimatedGroup: public QObject
{
    Q_OBJECT
protected:
    MainWindow* mainWindow;
    QGraphicsView* gview;

    AnimatedGroup(MainWindow* amainWindow, QGraphicsView* agview)
        : mainWindow(amainWindow), gview(agview)
    { }

    QPointF remapCurvePoint(QPolygonF, double offset) const;
    QPointF remapGridPoint(QPointF p) const;
};

class ManDogGroup: public AnimatedGroup
{
    Q_OBJECT
    Q_PROPERTY(QPointF location READ location WRITE setLocation)
public:
    ManDogGroup(MainWindow* amainWindow,
                  QGraphicsView* agview, bool dog_image);

    QPointF location() const;
    void setLocation(QPointF p);

private:

    //static const QPen INDICATOR_PEN;
    //  location on feasible-path
    //  x-coordinate maps to P, y-coordinate maps to Q
    QPointF loc;
    //  the leash
    QGraphicsLineItem* leash;
    QGraphicsSvgItem *man, *dog;
    QPointF dog_offset;
    //  TODO man, dog, path indicator
};

class ProjectionGroup: public AnimatedGroup
{
    Q_OBJECT
private:
    QPointF xx;
    AnimatedEllipseItem *x, *p, *q;
    AnimatedLineItem *h, *v, *d;
    QGraphicsTextItem *text;
public:
    ProjectionGroup(MainWindow* amainWindow, QGraphicsView* agview, QPointF xx);
public slots:
    void showHV();
    void showPQ();
    void showD();
    void showText();
public:
    QAbstractAnimation*  createAxisProjection(bool horizontal, int duration);
    QAbstractAnimation*  createMovePoint(bool horizontal, int duration);
private:
    AnimatedEllipseItem* createPoint(QPointF, int radius, QPen);
    AnimatedLineItem* createLine(QLineF, QPen);
};

} } // namespace

#endif // ANIMATION_H
