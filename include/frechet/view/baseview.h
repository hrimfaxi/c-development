#ifndef BASEVIEW_H
#define BASEVIEW_H

#include <QFrame>
#include <QGraphicsView>
#include <QLabel>
#include <QToolButton>
#include <QSlider>
#include <QSettings>
#include <QAbstractGraphicsShapeItem>
#include <QGestureEvent>
//#include <poly/types.h>

namespace frechet { namespace view {

class BaseView;
class GraphicsHoverLineItem;

/**
 * @brief a QGraphicsView with additional support for mouse and gesture events.
 * re-implements a couple of event handling routines.
 */
class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    /**
     * @brief default constructor
     * @param view enclosing widget
     */
    GraphicsView(BaseView *view);

protected:
    bool event(QEvent*) override;
    void mousePressEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent*) override;
    void keyPressEvent(QKeyEvent*) override;
    bool gestureEvent(QGestureEvent*);
    void mouseMoveEvent(QMouseEvent *event);

    //! parent widget
    BaseView *view;
    //! initial rotation angle
    int rot0;
};

/**
 * @brief perform a single step on a slider
 * @param factor scale factor
 */
void singleStep(QAbstractSlider*, double factor);
/**
 * @brief perform a page step on a slider
 * @param factor scale factor
 */
void pageStep(QAbstractSlider*, double factor);

/**
 * @param slider a slider widget
 * @return a slider value normalized to the interval [0..1]
 */
double normalizedValue(QAbstractSlider* slider);
/**
 * @brief update slider with a normalized value
 * @param slider a slider widget
 * @param nval normalized value in the interval [0..1]
 */
void setNormalizedValue(QAbstractSlider* slider, double nval);

/**
 * @brief base class for view widgets.
 *
 * Each view widgets displays a QGraphicsScene (curves, or freespace diagram).
 * It has controls for zooming, rotation, scrolling.
 * Supports zooming with the mouse wheel and with gestures.
 *
 * @author Peter Schäfer
 */
class BaseView : public QFrame
{
    Q_OBJECT
public:
    /**
     * @brief constructore with parent and title
     * @param parent parent widget (optional)
     * @param title window title (optional)
     * @param showRotate show rotation control ?
     */
    BaseView(QWidget *parent = 0, QString title="", bool showRotate=true);

    //! @return the embedded QGraphicsView
    QGraphicsView *view() const;

    //! @brief called when the mouse hovers over a polygon segment;
    //! implemented by derived classes.
    //! @param item a mouse sensitive polygonal segement
    virtual void segmentSelected(GraphicsHoverLineItem *item) {}

public slots:
    //! @brief zoom in; connected to the zomm controls
    //! @param level zoom step (-1==default)
    void zoomIn(int level = -1);
    //! @brief connected to the zomm controls
    //! @param level zoom step (-1==default)
    void zoomOut(int level = -1);

    /**
     * @brief store view settings to application prefs
     * @param settings application preferences container
     * @param group section in preferences
     */
    virtual void saveSettings(QSettings& settings, QString group);
    /**
     * @brief load view settings to application prefs
     * @param settings application preferences container
     * @param group section in preferences
     */
    virtual void restoreSettings(QSettings& settings, QString group);

    /**
     * @brief flip graphics scene vertically
     * @param flip true, if scene should be flipped
     */
    void flipVertical(bool flip);

    /**
     * @brief handles a mouse move event and detects mouse sensitve polygon segments
     * @param event a mouse event
     */
    void mouseMoveEvent(QMouseEvent *event);

    //! @brief map scene coordinates to window coordinate
    //! @param p a point in graphics scene coordinates
    //! @return the point in window coordinates
    QPoint mapSceneToGlobal(QPointF p) const;

protected slots:
    //! @brief reset zoom to default
    void resetView();
    //! @brief enabled reset button
    void setResetButtonEnabled();
    //! @brief set up transformation matrix to account for zoom,rotate,scroll
    void setupMatrix();
    //! @brief print the contents of the graphics scene
    void print();
    //! @brief save the contents of the graphics scene to disk (as PDF, or SVG)
    void saveAs();
    //! @brief rotate the scene to the left
    //! @param angle in degrees
    void rotateLeft(double angle=10);
    //! @brief rotate the scene to the right
    //! @param angle in degrees
    void rotateRight(double angle=10);

    //! @return current rotation angle
    int rotation() const;
    //! @brief update rotation angle
    //! @param angle in degrees
    void setRotation(double angle);

protected:
    //! pen for drawing selected polygon segment
    static const QPen PEN_SELECT;

    friend class GraphicsView;
    //static const int ZOOM_DEFAULT_VALUE = 250;
    //! what this?
    static const int ROTATE_DEFAULT_VALUE = 0;
    //! file name filters for save dialog
    static const QStringList OUTPUT_FILTERS;
    //! the embedded QGraphicsView
    QGraphicsView *graphicsView;
    //! scene that is displayed by graphicsView
    QGraphicsScene* scene;
    //! true if the scene should be flipped vertically
    bool isFlipped;
    //! zoom step for mouse wheel
    int zoomStepMouse;
    //! default zoom step
    int zoomStepDefault;

    //! the reset button
    QToolButton *resetButton;
    //! the slider control for zoom
    QSlider *zoomSlider;
    //! the slider control for rotate
    QSlider *rotateSlider;
    //! the currently selected mouse sensitve polyon segment (or nullptr)
    GraphicsHoverLineItem *selected_item;

    //! @brief handles window resizes; adjusts controls to new window size
    void resizeEvent(QResizeEvent *) override;

    //! @brief save content of scene to a PDF file
    //! @param file name of pdf file
    void saveAsPdf(QString file);
    //! @brief save content of scene to an SVG file
    //! @param file name of svg file
    void saveAsSvg(QString file);

    //! @brief clean up graphics scene items after free-space is modified
    //! implemented by FreeSpaceView.
    virtual void dropUnusedItems() { };
    //! @brief render the scene into a QPainter
    //! used for printing and saving to disk
    //! @param painter a graphics environment that saves data to disk, or sends it to a printer
    //! @param target target frame, should enclose the scene
    virtual void render(QPainter* painter, QRectF target=QRectF());

    //! @return basic transformation matrix (takes care of zoom,rotate,scroll)
    QMatrix baseMatrix();

    //! @brief update the pen width of a graphics item
    //! @param item a graphics item
    //! @param width new pen width
    static void setPenWidth(QGraphicsItem* item, double width);    
    //! @brief add a line segment to a QPainterPath
    //! @param ppath painter path
    //! @param line a line segment
    static void addLine(QPainterPath& ppath, const QLineF& line);
    //! @brief add a polygon curve segment to a QPainterPath
    //! @param ppath painter path
    //! @param poly a polygon curve
    static void addPolygon(QPainterPath& ppath, const QPolygonF& poly);
    //! @brief add a dot to a QPainterPath
    //! @param ppath painter path
    //! @param point location
    //! @param diameter diameter of dot
    static void addPoint(QPainterPath& ppath, const QPointF& point, double diameter=1.0);
};

/**
 * @brief a QGraphicsLitem that can handle mouse hover events.
 * It is used to highlight mouse sensitve polygon segments.
 *
 * Each line segment has a "logical" identifier within a polygonal curve,
 * or feasible paht.
 */
class GraphicsHoverLineItem : public QGraphicsLineItem
{
public:
    //! @brief location of the polygon segment
    enum Location { None, //!< used for segments that are not mouse sensitive
                    P,  //!< a boundary segment of P
                    Q,  //!< a boundary segment of Q
                    Pdiag,  //!< a diagonal of P
                    Qdiag,  //!< a diagonal of Q
                    FS      //!< a segment of the feasible paht (displayed in the free-space diagram)
                  };
    //! location on screen
    const Location loc;
    /**
     * @brief parameters loc,a, and b identify a segment.
     * - loc==P,Q,Pdiag,Qdiag: a,b are vertex indexes into P,Q.
     * - loc==FS: a is an index into the feasible path.
     */
    const int a, b;

    /**
     * @brief default constructor
     * @param line a line
     * @param loc location on screen
     * @param a segment identifier
     * @param b segment identifier
     * @param group group to add to
     */
    GraphicsHoverLineItem(QLineF line,
                          Location loc, int a, int b,
                          QGraphicsItemGroup* group);
    //! @brief update identifiers
    void update(int a, int b);
    /**
     * @brief check if the mouse is close to the line segment
     * @param scene_pos mouse position in scene coordinates
     * @param distance max. distance to accept as hover
     * @return true if the mouse is close to the line segment
     */
    bool is_close_to(QPointF scene_pos, int distance) const;
};

} }  //  namespace frechet
#endif // BASEVIEW_H
