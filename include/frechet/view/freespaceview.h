#ifndef FREESPACEVIEW_H
#define FREESPACEVIEW_H

#include <baseview.h>
#include <grid.h>
#include <freespace.h>
#include <fs_path.h>
#include <k_frechet/kalgorithm.h>
#include <palette.h>
#include <intervalview.h>

#include <array2d.h>
#include <QPolygonF>
#include <QGraphicsItemGroup>

namespace frechet { namespace view {

class FreeSpaceView;
/**
 * @brief displays one cell of the free-space diagram.
 *
 * A cell is either empty, completely covered, or displays a section
 * of an ellipse.
 *
 * Content color is variable (k-Frechet uses colors for components).
 *
 * Optionally, reachable intervals are shown at the border of the cell.
 *
 * Optionally, the bounding rectangle of the content area is shown.
 *
 * Ellipses are implemented as a circle object, that is applied an affine transformation
 * (see FreeSpaceView::createEllipseTransform).
 * The resulting ellipse is clipped at the bounds of each cell.
 *
 * Degenerate ellipses, which become excessively large, are replaced by an approximated
 * polygon (i.e. two parallel lines). The same is true if the ellipse covers the cell completely.
 *
 * @author Peter Schäfer
 */
class CellView : public QGraphicsItemGroup
{
private:
    //! column index in free-space grid
    int i;
    //! row index in free-space grid
    int j;
    //! pointer to parent
    FreeSpaceView* parent;

    //! bounding rectangle
    QRectF cellBounds;
    //! shape used for clipping ellipses
    QPainterPath clipShape;

    //! content area may be a polygon (created on demand)
    QGraphicsPolygonItem* _poly;   
    //! content area may be a (clipped) ellipse (created on demand).
    //! this is a circle with an affine transform
    QGraphicsEllipseItem* _ellipse;
    //! true if an ellipse is to be shown
    bool useEllipse;
    //! boundary lines of the content area
    QGraphicsLineItem* _bounds[4];

    /**
     * @brief display mode; what is displayed?
     * Flags can be combined (except Poly and Ellipse).
     */
    enum What {
        MASK    = 0x00f,//!<  bit mask
        NOTHING = 0x00, //!<  empty cell. display nothing
        POLY    = 0x01, //!<  display a polygon
        ELLIPSE = 0x02, //!<  display a clipped ellipse

        BOUNDS = 0x10,  //!<  display bounding rect

        ELLIPSE_AND_BOUNDS = ELLIPSE|BOUNDS,    //!< display ellipse plus bounding rect
        POLY_AND_BOUNDS = POLY|BOUNDS           //!< display polygon plus bounding rect
    } what;

public:
    //! an array of cell views
    typedef data::Array2D<CellView*> Matrix;
    //! pointer to an array of cell views
    typedef boost::shared_ptr<Matrix> MatrixPtr;    

    /**
     * @brief default constructor
     * @param parent parent widget
     * @param i column index in free-space grid
     * @param j row index in free-space grid
     */
    CellView(FreeSpaceView* parent, int i, int j);
    //! @brief destructor
    virtual ~CellView();

    /**
     * @brief update drawing colors
     * @param areaColor color for content area
     * @param borderColor color for borders (by default is equal to areaColor)
     */
    void setColor(QColor areaColor, QColor borderColor=QColor());

    /**
     * @brief update display
     * @param eps new value of epsilon
     * @param showBounds show bounding rect?
     * @param bounds content area bounding rectangle
     */
    void update(double eps, bool showBounds, QLineF bounds[4]);

    //! @return the clip shape
    virtual QPainterPath shape() const {
        return clipShape;
    }
    //! @return the bounding rect of the cell
    virtual QRectF boundingRect() const {
        return cellBounds;
    }
    //! @brief release graphics items that are not currently displayed
    //! @return true if items were released
    bool dropUnusedItems();

private:
    friend class FreeSpaceView;
    //! @brief determines when a hige ellipse is replaced by two parallel lines
    static const double DET_MINIMUM;
    //! @brief what's this?
    static const double MAX_DIAMETER;

    //void addLine(QLineF,int i);
    /**
     * @brief create a polygon that draws the content area
     * @param i grid column
     * @param j grid row
     * @param thin when parallel lines are close
     * @return a closed polyg
     */
    QPolygonF createPolygon(int i, int j, bool* thin);

    //! @return graphics item for drawing a polygon (created on demand)
    QGraphicsPolygonItem* getPoly();
    //! @return graphics item for drawing an ellipse (created on demand)
    QGraphicsEllipseItem* getEllipse();
    //! @return one line of the bounding rectangle (or nullptr)
    //! @param i 0 through 3
    QGraphicsLineItem* getBounds(int i);
    //! @brief update bounding rectangle
    void setBounds(QLineF bounds[4]);

    //! @brief update content
    //! @param what what should be displayed?
    void dodraw(What what);
    //! @brief release an unused item
    //! @param item handle to graphics item
    void dropUnusedItem(QGraphicsItem** item);

    //! @brief update brush (fill) color of a graphics item
    //! @param item a graphics item
    //! @param col new brush color
    static void setBrushColor(QAbstractGraphicsShapeItem* item, QColor col);
    //! @brief update pen (edge) color of a graphics item
    //! @param item a graphics item
    //! @param col new pen color
    static void setPenColor(QAbstractGraphicsShapeItem* item, QColor col);
    //! @brief update pen color of several graphics item
    //! @param bounds array of graphics items
    //! @param len number of items
    //! @param col new pen color
    static void setBoundsPenColor(QGraphicsLineItem** bounds, int len, QColor col);
    //! @brief update pen style (dotted,solid,...) of a graphics item
    //! @param item a graphics item
    //! @param style new style
    static void setPenStyle(QAbstractGraphicsShapeItem* item, Qt::PenStyle style);
    //! @brief update pen style and pen width of a graphics item
    //! @param item a graphics item
    //! @param style new style
    //! @param width new pen width
    static void setPenStyleWidth(QAbstractGraphicsShapeItem* item, Qt::PenStyle style, float width);

};
/**
 * @brief display the free-space diagram.
 *
 * It is made up of a line grid and an array of CellView objects.
 * Cell contents adjusts automatically at changes of epsilon.
 * When epsilon changes, FreeSpaceView executes the decision variant of the curve algorithm.
 *
 * When the k-Frechet algorithm is selected, connected components are color-coded.
 * Component boundaries can be shown as intervals below the free-space diagram.
 *
 * Optionally, a feasible path is displayed as one (or two) polygonal chains.
 * Line segments of the feasible path are mouse hover sensitive.
 *
 * Optionally, reachable intervals are displayed at the cell edges (along the grid lines).
 *
 * @author Peter Schäfer
 */
class FreeSpaceView : public BaseView
{
    Q_OBJECT
public:
    //! pens for drawing grid lines with different styles
    //! @see LineStyle
    QPen GRID_PEN [4];
    QPen AREA_PEN;      //!< pen for content area
    QPen BOUNDS_PEN;    //!< pen for bounding rectangles
    QPen PATH_PEN;      //!< pen for feasible path
    static const QBrush AREA_BRUSH1;    //!< brush (fill color) for content area
    static const QBrush AREA_BRUSH2;    //!< brush (fill color) for content area
    static const double TF_MAX_ARG;     //!< max. value for affine transforms
    static const QColor LIGHT_GRAY;     //!< used for disabled components (k-Frechet)
    static const QColor AREA_COLOR;     //!< color for content area (except k-Frechet)

    /**
     * @brief default constructor
     * @param parent paren widget (optional)
     */
    FreeSpaceView(QWidget *parent = 0);
    //! @brief destructor; releases all items
    virtual ~FreeSpaceView();

    //! @brief create the grid lines and an array of CellView objects
    void populateScene();

    //! @return true if bounding rectangles are displayed
    bool showBounds() const { return _showBounds; }

    //! @brief release graphics items that are currently not drawn
    //! implements empty method from super-class
    virtual void dropUnusedItems() override;
    //! @brief called when the mouse hovers over a sensitve line item
    virtual void segmentSelected(GraphicsHoverLineItem *item) override;

signals:
    //! @brief raised when after decision variant of the curve algorithm
    //! @param yes if there is a feasible path for the curve algorithm
    void curveFinished(bool yes);
    //! @brief raised when the mouse moves over a highlighted segment
    //! This signal triggers the display of associated line items.
    //! @param loc location on screen
    //! @param a identifier
    //! @param b identifier
    //! @see GraphicsHoverLineItem
    void hiliteSegment(int loc, int a, int b);

public slots:
    /**
     * @brief update free-space diagram when epsilon changes.
     * If the curve algorithm is selected, compute a feasible path, too.
     * All other algorithms must be triggered by the user.
     * @param epsilon new value of epsilon
     */
    void calculateFreeSpace(double epsilon);
    /**
     * @brief update display of bounding rects
     * @param show whether to show bounding rects
     */
    void showBounds(bool show);

    /**
     * @brief show the result of the k-Frechet greedy algorithms.
     * Highlights all components that are part of the solution,
     * other components are grayed.
     */
    void showGreedyResult();
    /**
     * @brief show the optimal result of the k-Frechet (brute-force) algorithms.
     * Highlights all components that are part of the solution,
     * other components are grayed.
     */
    void showOptimalResult();
    /**
     * @brief hide results of the k-Frechet algorithms.
     * Remove gray-out, display all components in their original color.
     */
    void hideResult();
    /**
     * @brief show or hide feasible path
     * @param show whether to show the feasible path
     */
    void showPath(bool show);

    /**
     * @brief highlight a segment of the feasible path
     * @param loc location on screen
     * @param a identifier
     * @param b identifier
     * @see GraphicsHoverLineItem
     */
    void onHiliteSegment(int loc, int a, int b);
    /**
     * @brief updatee free-space diagram when a new algorithm is selected.
     * For example, for the k-Frechet algorithm, components are color-coded.
     * @param alg next algorithm
     * @see FrechetViewApplication::Algorithm
     */
    void onAlgorithmChanged(int alg);
    /**
     * @brief hilite on point inf the diagram
     * (used for animation only)
     * @param p a point
     */
    void hilitePoint(QPointF p);

    /**
     * @brief store settings to application preferences
     * @param settings application preferences
     * @param group section where to store settings
     */
    virtual void saveSettings(QSettings& settings, QString group);
    /**
     * @brief load settings from application preferences
     * @param settings application preferences
     * @param group section where to store settings
     */
    virtual void restoreSettings(QSettings& settings, QString group);

private:
    friend class CellView;
    //! true, if there is a feasible path
    bool path_ok;
    //! true, if bounding rectangle are shown
    bool _showBounds;
    //! a palette of colors, used to color-code components
    //! for the k-Frechet algorithm
    Palette componentPalette;

    //! free space areas for all cells (i,j)
    CellView::MatrixPtr cells;

    //! k-Frechet: show component intervals below and to the left
    IntervalView* intervalView[2];
    //! the feasible path
    QGraphicsItemGroup *pathView;
    //! highlighted line segment (or nullptr)
    QGraphicsPathItem *select;

    //! @brief get a free-space cell view
    //! @param i grid column
    //! @param j grid row
    //! @return cell view
    CellView* getCellView(int i, int j);

    //! @brief update free-space view in response to a change of epsilon
    //! @param eps new value of epsilon
    void updateView(double eps);

    //! @brief update bounding rectangles
    void updateBounds();

    //! @brief update component intervals (k-Frechet only)
    void updateIntervals();

    //! @brief show the result of a k-Frechet algorithm
    //! @param resultSet a set of highlighted components
    void showResult(const data::BitSet* resultSet);

    //! @brief create a boundary rectangle for a cell
    //! @param i grid column
    //! @param j grid row
    //! @param result holds the boundary lines on return
    void createBoundsRect(int i, int j, QLineF result[4]);

    //! @brief create a set of line segments for reachability intervals
    //! @param i grid column
    //! @param j grid row
    //! @param result holds the reachability intervals on return
    void createReachabilityRect(int i, int j, QLineF result[4]);

    //! @brief calculates one boundary line
    //! @param a start of line in free-space coordinates
    //! @param b end of line in free-space coordinates
    //! @param i grid column
    //! @param j grid row
    //! @return a line to draw in the free-space diagram
    QLineF boundsLine(QPointF a, QPointF b, int i, int j);

    //! @brief compute the transformation for drawing an ellipse
    //! @param i grid column
    //! @param j grid row
    //! @return transformation
    QTransform createEllipseTransform(int i, int j);

    //! @brief validity test for a transformation. If transform parameters become excessively large,
    //! the ellipse is replaced by an approximating polygon.
    //! @param tf an affine transformation
    //! @return true if the transform is valid
    static bool validTransform(const QTransform& tf);

    //! @brief validity test for a transformation
    //! @param x entry in a transformation matrix
    //! @return true, if x <= TF_MAX_ARG
    static bool validTransformArg(double x);

    //! \brief add a grid line to the graphics scene
    //! \param line grid line
    //! \param style drawing style
    void addGridLine(QLineF line, LineStyle style);

    /**
    * @brief highlight a line segment
    * @param loc location on screen
    * @param a identifier
    * @param b identifier
    * @see GraphicsHoverLineItem
    */
    void doHiliteSegment(GraphicsHoverLineItem::Location loc, int a, int b);
};

} }  // namespace frechet::view

#endif // FREESPACEVIEW_H
