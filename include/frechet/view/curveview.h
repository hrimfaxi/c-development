#ifndef CURVEVIEW_H
#define CURVEVIEW_H

#include <baseview.h>
#include <QAbstractGraphicsShapeItem>
#include <poly/algorithm.h>

namespace frechet { namespace view {

/**
 * @brief displays input curves P and Q.
 *
 * Curves can be drawn separated for better readability.
 *
 * If the algorithm for simple polygons is appplied, c-diagonals and t-diagonals are displayed.
 *
 * Polygon edges and diagonals are mouse sensitive. When the mouse hovers over and edge (or diagonal)
 * the corresponding edges of the feasible path are highlighted.
 *
 * @author Peter Schäfer
 */
class CurveView : public BaseView
{
    Q_OBJECT
public:
    /**
     * @brief default constructor with parent
     * @param parent parent widget (optional)
     */
    CurveView(QWidget *parent = 0);

    /**
     * @brief set up the graphics scene with polygonal curves
     * @param P the first input curve
     * @param Q the second input curve
     * @param alg the algorithm for simple polygons
     */
    void populateScene(const Curve& P, const Curve& Q, frechet::poly::Algorithm::ptr alg);

    /**
     * @brief store settings to application preferences
     * @param settings application preferences
     * @param group section where to store settings
     */
    virtual void saveSettings(QSettings& settings, QString group);
    /**
     * @brief load settings from application preferences
     * @param settings application preferences
     * @param group section where to store settings
     */
    virtual void restoreSettings(QSettings& settings, QString group);

    /**
     * @return true if curves are displayed beneath each other
     */
    bool separateCurves() const { return _separate; }
    /**
     * @brief called when the mouse hovers over a sensitive line segment.
     * Updates the corresponding line segments.
     * @param item the line segment
     */
    virtual void segmentSelected(GraphicsHoverLineItem *item) override;

signals:
    /**
     * @brief raised when a mouse sensitive line segment is highlighted.
     * Parameters identify the location of the line segment.
     * @param loc location on screen
     * @param a identifier
     * @param b identifier
     * @see GraphicsHoverLineItem
     */
    void hiliteSegment(int loc, int a, int b);

public slots:
    /**
     * @brief update the display to show curves separately, or not
     * @param sep true if the curves should be displayed separately
     */
    void setSeparateCurves(bool sep);
    /**
     * @brief called when the user changes the current algorithm.
     * Update the curve display accordingly (e.g. show, or hide diagonals).
     * @param alg current algorithm
     * @see FrechetViewAlgorithm::Algorithm
     */
    void onAlgorithmChanged(int alg);
    /**
     * @brief called when a line segment should be highlighted
     * @param loc location on screen
     * @param a identifier
     * @param b identifier
     * @see GraphicsHoverLineItem
     */
    void onHiliteSegment(int loc, int a, int b);

private:
    /**
     * @brief highlight a line segment
     * @param loc location on screen
     * @param a identifier
     * @param b identifier
     * @see GraphicsHoverLineItem
     */
    void doHiliteSegment(GraphicsHoverLineItem::Location loc, int a, int b);

    static const QPen PEN_A;                //!< pen for drawing the curve P
    static const QPen PEN_B;                //!< pen for drawing the curve Q
    static const QPen PEN_C_DIAGS;          //!< pen for drawing the c-diagonals in P
    static const QPen PEN_T_DIAGS;          //!< pen for drawing the t-diagonals in P
    static const QPen PEN_PASSIVE_DIAGS;    //!< pen for drawing the diagonals in Q
    //! distance when curves are displayed separately
    static const double SEPARATOR;
    //! draw curves separately?
    bool _separate;

    QGraphicsItemGroup *polygon_a;  //!< graphics items for (boundary of) curve P
    QGraphicsItemGroup *polygon_b;  //!< graphics items for (boundary of) curve Q
    QGraphicsItemGroup *diagonals_a;  //!< graphics items for diagonals of curve P
    QGraphicsItemGroup *diagonals_b;  //!< graphics items for diagonals of curve Q
    //! boundary rectangle for both curves
    QRectF polygon_bounds;

    QGraphicsPathItem *select_a;    //!< highlited items for curve P
    QGraphicsPathItem *select_b;    //!< highlited items for curve Q

    /**
     * @brief add a polygon curve to the graphics scene
     * @param poly the polygon curve
     * @param pen pen for drawing the curve
     * @param hover hover location
     * @return a group of graphics items
     */
    QGraphicsItemGroup* addPolygonToScene(const Curve& poly, QPen pen, GraphicsHoverLineItem::Location hover);
    /**
     * @brief add a number of line segments to the graphics scene
     * @param group group to add the items to
     * @param d a number of polygon edges
     * @param exclude set of segments to exclude
     * @param with_edges include outer edges?
     * @param pen pen for drawing
     * @param hover hover location
     * @return a group of graphics items
     */
    QGraphicsItemGroup* addSegmentsToScene(QGraphicsItemGroup* group,
                            frechet::poly::Triangulation::Edge_range d,
                            const frechet::poly::Segments& exclude,
                            bool with_edges,
                            QPen pen, GraphicsHoverLineItem::Location hover);
    /**
     * @brief add a number of line segments to the graphics scene
     * @param group group to add the items to
     * @param d a number of polygon edges
     * @param with_edges include outer edges?
     * @param pen pen for drawing
     * @param P polygonal curve
     * @param hover hover location
     * @return a group of graphics items
     */
    QGraphicsItemGroup* addSegmentsToScene(QGraphicsItemGroup* group,
                            const frechet::poly::Segments& d,
                            bool with_edges,
                            QPen pen,
                            const Curve& P, GraphicsHoverLineItem::Location hover);
    /**
     * @brief create a line segment graphics item
     * @param line the line segment
     * @param pen pen for drawing
     * @param seg the associated polygon segment
     * @param group p group to add the items to
     * @param hover hover location
     * @return a graphics item
     */
    QGraphicsLineItem* createLineItem(QLineF line, QPen pen,
                                  frechet::poly::Segment seg,
                                  QGraphicsItemGroup* group, GraphicsHoverLineItem::Location hover);

    friend class GraphicsHoverLineItem;
};

} }  //  namespace frechet::view
#endif // CURVEVIEW_H
