#ifndef PALETTE_H
#define PALETTE_H

#include <QMap>
#include <QColor>

namespace frechet { namespace view {
/**
 * @brief a color map.
 *
 * Colors are chosen randomly.
 *
 * Used for the k-Frechet algorithm to color-code components.
 *
 * @author Peter Schäfer
 */
class Palette
{
private:
    //! maps component IDs to random colors
    typedef QMap<size_t,QColor> ColorMap;
    //! the map
    ColorMap map;

    //! @return a new random color
    QColor randomColor();

    //! @brief get color for a component; chose a new random color, if necessary
    //! @param key component ID
    //! @return the color for the key
    QColor nextColor(size_t key);

    //! @brief make the color a darker saturation
    //! @param col modified on return
    void stronger(QColor& col) {
        if (col.hslSaturationF() < 0.5)
            addLightness(col,+0.5);
    }

    //! @brief make the color a lighter saturation
    //! @param col modified on return
    void weaker(QColor& col) {
        if (col.hslSaturationF() > 0.5)
            addLightness(col,-0.5);
    }

    //! @brief modify the lightness of a color
    //! @param col modified on return
    //! @param diff difference in lightness
    void addLightness(QColor& col, double diff) {
        col.setHslF(col.hslHueF(), col.hslSaturationF()+diff, col.lightnessF(), col.alphaF());
    }

public:
    //! @brief constructor; creates an empty palette
    Palette();

    //! @brief look up a color
    //! @param key component ID
    //! @return associated color; chosen randomly
    QColor operator[] (size_t key);

    //! @brief map an integer key to a color
    //! @param x key value
    //! @param sat saturation
    //! @param light lightness
    //! @return a color
    static QColor toColor(int x, int sat=220, int light=200);

    //! @brief remove all entries from the map
    void clear();
};

} } // namespace

#endif // PALETTE_H
