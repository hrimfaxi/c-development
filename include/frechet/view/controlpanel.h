#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QLineEdit>
#include <QSlider>
#include <QDoubleValidator>
#include <QSettings>

#include <types.h>
#include <interval.h>
#include <k_frechet/kalgorithm.h>

#include "ui_controlpanel.h"

namespace frechet { namespace view {
/**
 * @brief Control Panel.
 *
 * Contains controls for modifying epsilon, and for running algorithms.
 *
 * @author Peter Schäfer
 */
class ControlPanel: public QWidget
{
    Q_OBJECT     
    Q_PROPERTY(double epsilon READ getEpsilon WRITE setEpsilonWithNotify)

public:
    /**
     * @brief default constructor
     * @param parent parent widget (optional)
     */
    ControlPanel(QWidget* parent=0);

    //! @return the current value of epsilon
    double getEpsilon() { return eps; }
    //  @see FrechetViewApplication::Algorithm

    /**
     * @brief store settings to application preferences
     * @param settings application preferences
     * @param group section where to store settings
     */
    void saveSettings(QSettings& settings, QString group);
    /**
     * @brief load settings from application preferences
     * @param settings application preferences
     * @param group section where to store settings
     */
    void restoreSettings(QSettings& settings, QString group);

signals:
    /**
     * @brief raised when the value of epsilon changes
     * (e.g. when the user operates the slider control).
     * This signal triggers a number of subsequent actions (e.g. updating the free-space diagram).
     * @param eps new value of epsilon
     */
    void epsilonChanged(double eps);

    /**
     * @brief raised when the user clicks the "Show" button in the k-Frechet panel.
     * Updates the free-space diagram to show the results of the "greedy" algorithm.
     */
    void showGreedyResult();
    /**
     * @brief raised when the user clicks the "Show" button in the k-Frechet panel.
     * Updates the free-space diagram to show the results of the "brute force" algorithm.
     */
    void showOptimalResult();
    /**
     * @brief raised when the user clicks the "Hide" button in the k-Frechet panel.
     * Resets the free-space diagram to default.
     */
    void hideResult();

public slots:
    //! @brief not used anymore...
    void lockInput(bool) { /*TODO*/ }

    /**
     * @brief called after the user has edited the value of epsilon.
     * Triggers epsilonChanged(), which in turn triggers a number of update operations.
     */
    void onEdit();
    /**
     * @brief called when the user moves the slider control for epsilon.
     * Triggers epsilonChanged(), which in turn triggers a number of update operations.
     * @param value new value of epsilon
     */
    void onSlider(int value);
    /**
     * @brief called when the user selects a algorithm panel.
     * Calls on FrechetViewApplication to update the current algorithm.
     * @param algorithm
     */
    void onAlgorithmChanged(int algorithm);

    /**
     * @brief called when the user clicks the "Show"  button in the "greedy" section.
     * Triggers showGreedyResult(), which in turn triggers an update to the Free-Space diagram.
     */
    void onGreedyButton();
    /**
     * @brief called when the user clicks the "Show"  button in the "brute-force" section.
     * Triggers showOptimalResult(), which in turn triggers an update to the Free-Space diagram.
     */
    void onBFButton();

    /**
     * @brief updates the value of epsilon
     * @param eps new value of epsilon
     * @param notify if true, raise signals to update other parts of the GUI.
     *  if false, update silently.
     */
    void setEpsilon(double eps, bool notify);
    /**
     * @brief update the value of epsilon without raising signals
     * @param eps new value of epsilon
     */
    void setEpsilonWithoutNotify(double eps) { setEpsilon(eps,false);}
    /**
     * @brief update the value of epsilon and raise signals
     * @param eps new value of epsilon
     */
    void setEpsilonWithNotify(double eps)    { setEpsilon(eps,true);}
    /**
     * @brief update the maximum value of epsilon; adjusts the slider control
     * @param eps_max maximum value for epsilon
     */
    void setEpsilonMax(double eps_max);

    //! @brief update the k-Frechet panel to show the latest results of the k-Frechet algorithm
    void updateResults();
    //! @brief reset the k-Frechet panel
    void clearResults();

    //! @brief update the "Polygon" pabel to show the lastest results of the poly-algorithm
    void updatePolyResult();

    //! @brief called when the user clicks the "Show Bounds" button
    void onShowBounds(bool);

    //! @brief called when the curve algorithm finishes; update the "curve" panel to show latest results
    void onCurveFinished(bool);

private:
    //! @brief update results of the k-Frechet greedy algorithm
    //! @param kalg results of the greedy algorithm
    void updateGreedyResult(const k::kAlgorithm::Greedy& kalg);
    //! @brief update results of the k-Frechet greedy algorithm
    //! @param k1 results of the greedy algorithm
    //! @param k2 results of the greedy algorithm
    void updateGreedyResult(k::kAlgorithm::Result k1, k::kAlgorithm::Result k2);

    //! @brief update results of the k-Frechet brute-force algorithm
    //! @param bfres results of the brute-force algorithm
    void updateOptimalResult(const k::kAlgorithm::BruteForce& bfres);
    //! @brief update results of the k-Frechet brute-force algorithm
    //! @param kres results of the brute-force algorithm
    void updateOptimalResult(k::kAlgorithm::Result kres);

private:
    //! number of decimals in the epsilon control
    static const int DECIMALS;

    //! the current value of epsilon
    double eps;
    //! locale used for number formatting
    QLocale locale;
    //! validator for epsilon input
    QDoubleValidator* epsValidator;
    //! step size for epsilon slider control
    double eps_step;

    //!  UI elements (auto-generated by Qt)
    Ui::ControlPanel ui;

    //! @brief format a number for display, with appropriate locale
    //! @param x a number
    //! @return string representation
    QString toString(double x);
    //! @brief parse a number input
    //! @param text string input
    //! @return a number
    double toDouble(QString text);

    //! @brief map an epsilon value to a slider location
    //! @param x a number
    //! @return location on slider
    int toSliderValue(double x);
    //! @brief map a slider location to a value of epsilon
    //! @param i slider location
    //! @return epsilon value
    double fromSliderValue(int i);

    /**
     * @brief show a status icon in the control panel
     * @param label widget that contains the icon
     * @param status algorithm status
     * @see kAlgorith::Result
     */
    void showIcon(QLabel* label, int status);
    /**
     * @brief show a status icon in the control panel
     * @param label widget that contains the icon
     * @param text status text
     * @param tooltip tooltip (mouse over) text
     */
    void setIcon(QLabel* label, QString text, QString tooltip="");

    //! displays a rotatin icon (during a long-running algorithm)
    static QMovie* loaderMovie;
    //! label that display the rotating icon
    QLabel* movieLabel;
    //! latest result of the decision algorithm for curves
    bool curve_was_ok;

    //! texts to display for algorithm results
    static QString POLY_STATUS [];

protected:
    void wheelEvent(QWheelEvent *) override;
    void keyPressEvent(QKeyEvent *event) override;
};
/**
 * @brief perform a single step on a slider
 * @param factor scale factor
 */
void singleStep(QAbstractSlider*, double factor);
/**
 * @brief perform a page step on a slider
 * @param factor scale factor
 */
void pageStep(QAbstractSlider*, double factor);

} } // namespace

#endif // CONTROLPANEL_H
