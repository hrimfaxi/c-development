#ifndef INTERVALVIEW_H
#define INTERVALVIEW_H

#include <QGraphicsItemGroup>
#include <QQueue>

#include <vector>
#include <boost/icl/interval_set.hpp>

#include <palette.h>
#include <grid.h>
#include <components.h>
#include <k_frechet/kalgorithm.h>

#include <QLineF>
#include <QPen>

namespace frechet {

using namespace free;
using namespace data;

namespace view {

/**
 *  @brief Display k-Frechet intervals, i.e. projections of connected components
 *  to the domain axes.
 *
 *  Overlapping intervals are stacked.
 *
 *  @author Peter Schäfer
 */
class IntervalView : public QGraphicsItemGroup
{
public:
    //!  @brief orientation: horizontal or vertical
    enum Orientation { HORIZONTAL, VERTICAL } orientation;
    //! height of a row
    double ROW_HEIGHT;

private:
    //! set of intervals
    typedef boost::icl::interval_set<double> boost_ivalset;
    //! what's this?
    typedef boost::icl::continuous_interval<double> boost_ival;

    //!  coordinate grid
    Grid::ptr grid;
    //!  stack of intervals
    std::vector<boost_ivalset> stack;
    //!  stack watermark
    int stack_watermark;
    //!  color palette
    Palette* palette;
    //!  pool of unused QGraphicsItems
    QQueue<QGraphicsLineItem*> itemPool;
    //!  pen for drawing lines
    QPen LINE_PEN;

public:
    /**
     * @brief constructor
     * @param grid free-space grid
     * @param orient orientation of intervals (horizonral or vertical)
     * @param palette a color palette
     * @param pen_width pen width
     * @param parent parent item
     */
    IntervalView(Grid::ptr grid,
                 Orientation orient,
                 Palette* palette,
                 double pen_width,
                 QGraphicsItem* parent=0);
    //! @brief remove all intervals
    void clear();

    /**
     * @brief add an interval
     * @param ival interval
     * @param component associated component; determines color
     */
    void add(const Interval& ival, size_t component);
    /**
     * @brief add an interval
     * @param mval an interval and a component ID
     */
    void add(const k::MappedInterval& mval);
    /**
     * @brief add a set of intervals
     * @param mvals a set of intervals and associated component IDs
     */
    void addAll(const k::IntervalMap& mvals);

    /**
     * @brief highlight result set of a k-Frechet algorithm.
     * All intervals that are not part of the result set are grayed.
     * @param resultSet set of component IDs.
     */
    void showResult(const frechet::data::BitSet* resultSet);

    /**
     * @return width/height that is needed to display the stack of intervals
     */
    double extent() const {
        return (stack_watermark+1) * ROW_HEIGHT;
    }

private:
    /**
     * @brief insert an interval
     * @param ival interval
     * @return line that represents the interval
     */
    QLineF insert(const boost_ival& ival);
    /**
     * @brief place a new interval in a free space
     * @param ival interval
     * @return stack row
     */
    int findRow(const boost_ival& ival);

    /**
     * @brief create a line item
     * @return new item
     */
    QGraphicsLineItem* createItem();
    /**
     * @brief release item
     */
    void releaseItem(QGraphicsItem*);

    /**
     * @brief update ttem color
     * @param item a line item
     * @param col new color
     */
    void setItemColor(QGraphicsLineItem* item, QColor col);
};

} } // namespace

#endif // INTERVALVIEW_H
