#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <data/types.h>
#include <poly/types.h>
#include <poly/triangulation.h>
#include <freespace.h>
#include <fs_path.h>
#include <structure.h>
#include <graph_m4ri.h>
#include <boost/smart_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <set>
#include <shortest_paths.h>
#include <data/numeric.h>
#include <tbb/concurrent_unordered_map.h>

class TriangulationTestSuite;

namespace frechet { namespace poly {

class PolygonUtilities;
class PolygonWorkerJob;
/**
 * @brief base class for algorithm for simple polygons.
 *
 *  This class performs all necessary steps to compute
 *  the algorithm for simple polygons (decision variant,
 *  and optimisation variant)
 *
 *  -# performs convex decomposition and triangulation of polygons.
 *  -# computes valid placements (using the shortest-path-tree algorithm of Guibas et al.)
 *  -# computes reachability graphs
 *  -# computes a solution to the algorithm of Buchin et al.
 *
 *  The current status of the algorithm is tracked and
 *  reported to the application logic. Other parts of the application
 *  (notably the GUI) connect to signals.
 *
 *  Input data are meant to be:
 *  - closed curves. Open curves are handled by decideCurve() instead.
 *  - simple polygons. There must be no self-intersections.
 *  - not convex. Convex curves are handled by decideCurve() instead.
 *  - oriented counter-clockwise. This condition can be easily repaired.
 *
 *  For open curves and convex curves we apply the much simpler algorithm
 *  of Alt and Godau (which is, for convenience, implemented in the same class).
 *
 * @author Peter Schäfer
 */
class Algorithm : public QObject {
    //  Qt wiring:
    Q_OBJECT
signals:
    //!  @brief emitted whenever the status of the algorithm changes.
    void statusChanged();
    //!  @brief emitted when a feasible path has been found
    //!  @param valid true if there is a valid feasible path.
    //!     The path can be retrieved from FrechetViewApplication
    void pathUpdated(bool valid);
    //!  @brief emitted when the optimisation (or approximation) algorithm finds a result
    //!  @param epsilon result of the optimisation (or approximation) algorithm
    void optimisationResult(double epsilon);

public slots:
    //! @brief reset the status of the algorithm
    //! @param stopping indicates that the reset happened in response to user interruption
    void resetStatus(bool stopping=false);

public:
    /**
     * @brief indicates the current status of the algorithm.
     * Negative values indicate that the algorithm is currently computing.
     * Positive values indicate the result and also conditions of input data.
     */
    enum Status : int8_t {
        RUNNING = -2,   //!< the algorithm is currently computing (decision variant, or optimisation variant)

        RUNNING_DECIDE_POLY=-8,     //!< the algorithm is currently computing the decision variant for simple polygons
        RUNNING_OPT_POLY=-7,        //!< the algorithm is currently computing the optimisation variant for simple polygons
        RUNNING_APPROX_POLY=-6,     //!< the algorithm is currently computing the approximation variant for simple polygons
        RUNNING_DECIDE_CURVE=-5,    //!< the algorithm is currently computing the decision variant for curves
        RUNNING_OPT_CURVE=-4,       //!< the algorithm is currently computing the optimisation variant for curves
        RUNNING_APPROX_CURVE=-3,    //!< the algorithm is currently computing the approximation variant for curves

        YES=0,  //!< positive result of the decision version
        NO,     //!< negative result of the decision version

        //! initial status: the algorithm is not yet ready to execute
        NOT_SET_UP,
        //! input data have been successfully processed, the algorithm is now ready to execute
        SET_UP,
        //! input data are faulty. P is an empty curve.
        P_EMPTY,
        //! input data are faulty. Q is an empty curve.
        Q_EMPTY,
        //! input data are faulty. P is not a closed curve.
        P_NOT_CLOSED,
        //! input data are faulty. Q is not a closed curve.
        Q_NOT_CLOSED,
        //! input data are faulty. P is not a simple polygon.
        P_NOT_SIMPLE,
        //! input data are faulty. Q is not a simple polygon.
        Q_NOT_SIMPLE,
        //! input data are faulty. P is not a oriented counter-clockwise.
        P_NOT_COUNTER_CLOCKWISE,
        //! input data are faulty. Q is not a oriented counter-clockwise.
        Q_NOT_COUNTER_CLOCKWISE,
        //! input data are faulty. P is convex.
        P_CONVEX,
        //! input data are faulty. Q is convex.
        Q_CONVEX,
    };

    //!  the result of the decision algorithm is a reachability Graph containing a feasible path
    typedef frechet::reach::Graph::ptr GraphPtr;

protected:
#ifdef QT_DEBUG
    //! used for unit testing
    friend class ::TriangulationTestSuite;
#endif
    //! current status of the algorithm
    enum Status _status;
    //! initial status of the algorithm (before or after processing input data)
    enum Status initial_status;

    /**
     * @brief stores essential information about an input curve.
     * This data is processed once for each input curve.
     */
    struct CurveData {
        //! P, resp. Q. Must be a closed curve, start and endpoint are duplicated.
        Curve curve;
        //! simplified polygon C' (containing only the end points of c-diagonals)
        Partition partition;
        //! a list of c-diagonals
        Segments _c_diagonals;
        //! all end points of diagonals
        Polygon _d_points;
        //! simplified triangulation for P, complete triangulation for Q
        Triangulation triangulation;
        //!  complete triangulation for P -- only needed for visualisation; not for the algorithm as such
        Triangulation* display_triangulation;
        //!  reflex vertices (for Q only)
        Polygon reflex;
        //! helper class for polygon analysis
        PolygonUtilities* utils;

        //! empty constructor
        CurveData();
        //! destructor; release all memory
        ~CurveData();

        //!  @brief number of vertices = number of edges; start/end point is counted once (other than QPolygonF!!)
        int n() const { return curve.size()-1; }
        //!  @brief number of diagonal end points
        int l() const { return _d_points.size(); }
        //!  @brief number of c-diagonals
        int k() const { return _c_diagonals.size(); }
        //!  @brief map a diagonal end-point to a polygon vertex
        //!  @param i index into [0..l) modulo l
        //!  @return index of vertex in input curve
        int d(int i) const {
            i = (i + l()) % l();
            return _d_points[i];
        }
        //!  @return number of convex partitions (components)
        int partitions() const { return partition.size(); }

        //! @brief calculate the c-diagonals
        //! @param result holds the c-diagonals on return
        //! @return the number of c-diagonals
        int calculate_c_diagonals(Segments& result);
        //! @brief calculate the d-points (end points of the diagonals)
        //! @return number of d-points
        int calculate_d_points();
    } *P, *Q;

    //!  current Free-Space diagram (passed to the decision variant)
    FreeSpace::ptr fs;

    //typedef boost::unordered_map<int,GraphPtr> GraphMap;
    //!  hash map for reachability graphs
    typedef tbb::concurrent_unordered_map<int,GraphPtr> PlacementMap;
    //! list of valid placements
    typedef std::vector<reach::Placements> PlacementVector;

    //!  list of placements (for each end point of diagonals)
    PlacementVector _placements;
    //!  map of valid placements (as adjacency matrices, for each c-diagonal)
    PlacementMap _validPlacements;

    //! map free-space interval to nodes of the reachability graph
    reach::GraphModel::ptr graphModel;
	
    //! used to store intermediate (memo-ized) reachability graphs
	typedef tbb::concurrent_unordered_map<int, reach::Graph::ptr> GraphMap;

    //! a map of memo-ized Combined Reachability Graphs
	GraphMap CRG_map;

    /**
     * @brief get valid placements for a given pair of diagonal end points;
     * compute them, if necssary. Subsequent calls retrieve the memo-ized result.
     * @param di a diagonal endpoint
     * @param dj anotehr diagonal endpoint
     * @return valid placements
     */
    GraphPtr getValidPlacements(int di, int dj);
    /**
     * @brief get a Reachability Graph;
     * compute it, if necessary. Subsequent calls retrieve the memo-ized result.
     * @param i diagonal point index
     * @return a reachability graph
     */
    GraphPtr calculate_RG(int i);
    /**
     * @brief hash key for memo-ized maps
     * @param di a diagonal endpoint
     * @param dj another diagonal endpoint
     * @return a hash key for a pair of diagonal end points
     */
    int mapKey(int di, int dj);

    //! Free-Space used during binary searches (optimisation variant)
	FreeSpace::ptr local_fs;

    //! a list of critical values
    typedef std::vector<double> CriticalValueList;
    //! the list of critical values (optimisation variant)
    CriticalValueList criticalValues;

public:
    //! (smart) pointer to an Algorithm object
    typedef boost::shared_ptr<Algorithm> ptr;

    //! @brief default constructor
    Algorithm();
    //! @brief destructor; releases all memory
    virtual ~Algorithm();

    //! @brief current status of the algorithm
    Status status() const { return _status; }

    //! @brief check if we can execute the decision variant on simple polygons (Buchin et al.'s algorithm)
    bool canDecidePoly() const      { return _status==SET_UP; }
    //! @brief check if we can execute the optimisation variant on simple polygons (Buchin et al.'s algorithm)
    bool canOptimisePoly() const    { return _status==SET_UP || _status==YES || _status==NO; }
    //! @brief check if we can execute the optimisation variant on curves (Alt and Godau's algorithm)
    bool canOptimiseCurve() const   { return _status==SET_UP || _status==YES || _status==NO || _status>=P_NOT_CLOSED; }

    //! @brief the c-diagonals
    const Segments& c_diagonals() const;

    //! @brief check, whether a diagonal is a c-diagonal
    //! @param di index of a diagonal endpoint
    //! @param dj index of another diagonal endpoint
    bool is_c_diagonal(int di, int dj) const;
    //! @brief check, whether a diagonal is a c-diagonal
    //! @param s a diagonal segment
    bool is_c_diagonal(Segment s) const;

    //! @brief the triangulation of P
    Triangulation& PTriangulation() { return P->triangulation; }
    //! @brief the triangulation of Q
    Triangulation& QTriangulation() { return Q->triangulation; }
    //! @brief a complete triangulation for P (only needed for visualisation
    Triangulation& displayPTriangulation() { return *P->display_triangulation; }

    //! @brief the first input curve P
    const Curve& Pcurve() const { return P->curve; }
    //! @brief the first input curve Q
    const Curve& Qcurve() const { return Q->curve; }

    //! @brief helper object for polygon analysis
    //! @param primary for P or for Q?
    PolygonUtilities* polygonUtilities(bool primary) {
        return (primary ? P:Q)->utils;
    }

    /**
     * @brief Initialize input curves and perform some basic sanity checks
     * @param aP first input curve
     * @param aQ second input curve
     * @param fixOrientation if true, clockwise oriented curves
     * are reversed ("repaired") to become counter-clockwise.
     * The subsequent algorithm required counter-clockwise oriented curves.
     * @return status of input processinig
     */
    Status setup(Curve& aP, Curve& aQ, bool fixOrientation=false);

    /**
     * @brief decompose polygons into convex parts
     * @return  true if P was chosen as primary curve,
     *          false if Q was chosen (application should adapt)
     */
    bool decompose(bool optimal);
    /**
     * @brief swap input curves.
     * The algorithm by Buchin et al. chooses the smalled
     * convex decompositioin as P.
     */
    void swap();
    /**
     * @brief compute triangulations of input curves
     * @pre decompose() must have creted a convex decomposition
     */
    virtual void triangulate();

    /*
     *  update with new epsilon & free-space
     *  @return true if there is a solution
     *
     *  return feasible path
     */
    //typedef ResultPtr (*decideFunction) (FreeSpace::ptr, reach::FSPath::ptr, double, bool);
    /**
     * @brief execute the decision variant for simple polygons (Buchin et al.'s algorithm)
     * @pre status must be SET_UP, i.e. input curves must be correct
     * @pre a free-space diagram must have been computed
     * @param fs free-space diagram
     * @param path holds a feasible path on return
     * @param epsilon free-space epsilon
     * @param update_status if true, update the status variable
     * @return the resulting reachability graph, or nullptr if there is no solution
     */
    GraphPtr decidePolygon( FreeSpace::ptr fs,
                            reach::FSPath::ptr path,
                            double epsilon,
                            bool update_status);

    /**
     * @brief execute the optimisaion variant for simple polygons. (Buchin et al.'s algorithm)
     * The result value is broadcast by signal optimisationResult(double epsilon).
     * @pre status must be SET_UP, i.e. input curves must be correct
     * @param path holds a feasible path on return
     * @param approximation if ==0, execute the optimisation variant.
     * If >0, execute the approximation variant with given approximation tolerance.
     * @return result epsilon, or 0.0 if there is no  result
     */
    double optimisePolygon( reach::FSPath::ptr path,
                            double approximation=0.0);

    /**
     * @brief execute the decision variant for curves.  (Alt and Godau's algorithm)
     * @pre a free-space diagram must have been computed
     * @pre status must be SET_UP or > NOT_CLOSED
     * @param fs free-space diagram
     * @param path holds a feasible path on return
     * @param ignored (not used)
     * @param update_status if true, update the status variable
     * @return a reference to the starting interval of the path, or nullptr if there is no solution
     */
    reach::Pointer decideCurve( FreeSpace::ptr fs,
                                reach::FSPath::ptr path,
                                double ignored,
                                bool update_status);

    /**
     * @brief execute the optimisation variant for curves.  (Alt and Godau's algorithm)
     * @pre status must be SET_UP or > NOT_CLOSED
     * @param approximation if ==0, execute the optimisation variant.
     * If >0, execute the approximation variant with given approximation tolerance.
     * @return result epsilon, or 0.0 if there is no  result
     * @return result epsilon, or 0.0 if there is no  result
     */
    double optimiseCurve(double approximation=0.0);

    /**
     * @brief pick a point from a free-space interval.
     * Helper function for creating feasible paths.
     * @param j a free-space interval
     * @param m number of columns in free-space (optional)
     * @return a point in the interval
     */
    static double pickIntervalPoint(const Interval& j, int m=INT_MAX);

//    static int findFeasibleStart(GraphPtr G);

protected:
    //! @brief calculate all valid placements
    //! @param epsilon free-space epsilon
    virtual void calculateValidPlacements(double epsilon)=0;
    //! @brief calculate all initial Reachability Graphs
    //! and fill ("warm up") the memoisation map
    virtual void calculateReachabilityGraphs()=0;

    //! @brief the main loop of Buchin et al's decision algorithm.
    //! @return resulting CRG, or nullptr if there is no solution.
    virtual GraphPtr findFeasiblePath()=0;

    //! @brief compute an adjacancy matrix of valid placements
    //! for one pair of diagonal end points
    //! @param di a diagonal end point
    //! @param dj another diagonal end point
    //! @return adjacancy matrix of valid placements
    GraphPtr createValidPlacementGraph(int di, int dj);
    //! @brief calculate valid placements for a pair of diagonal end points
    //! @param spt implements the shortest-paths-tree algorithm of Guibas et al.
    //! @param epsilon free-space epsilon
    //! @param di a diagonal end point
    //! @param dj another diagonal end point
    void calculateValidPlacements(
            poly::PolygonShortestPathsFS& spt,
            double epsilon, int di, int dj);

    //! @brief a diagonal edge of the triangulation of P
    //! @param i a diagonal end point index
    Triangulation::Edge diagonalEdge(int i) const;

    //! @brief number of diagonal end points
    int l() const { return P->l(); }
    //! @brief number of c-diagonals
    int k() const { return P->k(); }
    //!  @brief map a diagonal end-point to a vertex in P
    //!  @param i index into [0..l) modulo l
    //!  @return index of vertex in P
    int d(int i) const { return P->d(i); }

    //!  @brief number of convex partitions (components)
    int partitions() const { return P->partitions(); }

    //! @brief calculate free-space placements
    //! @param i free-space column
    //! @param result holds the placements on return
    void calculatePlacements(int i, frechet::reach::Placements& result);

    /**
     * @brief get a memoised Combined Reachabilty Graph.
     * This is one of the steps in Buchin et al.'s decision algorithm.
     * @param i first free-space column
     * @param j last free-space column
     * @param e triangulation edge that is currently examined
     * @return a combined reachability graph, computed if necessary
     */
	virtual reach::Graph::ptr CRG(int i, int j, Triangulation::Edge e = Triangulation::Edge())=0;
    /**
     * @brief compute an initial Reachabilty Graph.
     * @param i free-space column
     * @return a combined reachability graph
     */
	virtual reach::Graph::ptr create_RG(int i)=0;
    /**
     * @brief compute a Combined Reachabilty Graph.
     * @param i first free-space column
     * @param j last free-space column
     * @param e triangulation edge that is currently examined
     * @return a newly craeted Combined Reachability Graph
     */
	virtual reach::Graph::ptr create_CRG(int i, int j, Triangulation::Edge e)=0;

    /**
     * @brief perform the MERGE operation on two Reachability Graphs
     * @param A a (combined) reachability graph
     * @param B another (combined) reachability graph
     * @return result of MERGE(A,B)
     */
	virtual reach::Graph::ptr MERGE_inner(const reach::Graph::ptr  A, const reach::Graph::ptr  B)=0;
    /**
     * @brief perform the final MERGE operation on two Reachability Graphs
     * @param A  a reachability graph
     * @param B another (combined) reachability graph
     * @return result of MERGE(A,B,A)
     */
	virtual reach::Graph::ptr MERGE_outer(const reach::Graph::ptr  A, const reach::Graph::ptr  B)=0;
    /**
     * @brief perform the COMBINE operation, apply valid placements
     * @param A a (combined) reachability graph; updated on return
     * @param di diagonal end point
     * @param dj diagonal end point
     */
	virtual void COMBINE(reach::Graph::ptr  A, int di, int dj)=0;

    //! @brief set current status
    Status setStatus(Status);
    //! @brief set initial status (after pre-processing input data)
    Status setInitialStatus(Status);
    //! @brief release memory (meoisation maps, etc.)
    virtual void cleanup();

    /**
     * @brief compute a feasible path from the result of (Buchin et al.'s) decision algorithm
     * @param path holds the feasible path on return
     * @param result result  of the decision algorithm
     * @param epsilon free-space epsilon
     */
    void updatePath(reach::FSPath::ptr path, GraphPtr result, double epsilon);
    /**
     * @brief compute a feasible path from the result of (Alt and Godau's) decision algorithm
     * @param path holds the feasible path on return
     * @param startPointer start point of feasible path in reachability structure
     * @param epsilon free-space epsilon
     */
    void updatePath(reach::FSPath::ptr path, reach::Pointer startPointer, double epsilon);
public:
    /**
     * @brief compute a set of critical values for the optimisation variant
     * @param a if true, compute critical values of type (a)
     * @param b if true, compute critical values of type (b)
     * @param c if true, compute critical values of type (c)
     * @param d if true, compute critical values for diagonals and reflex vertices
     *  (Buchin's optimisation algorithm only)
     */
    void collectCriticalValues(bool a, bool b, bool c, bool d);
    /**
     * @brief get the set of current critical values
     * @pre collectCriticalValues() must have computed a set of critical values
     */
    virtual CriticalValueList& currentCriticalValueList();

protected:
    //! @brief compute critical values of type (a)
    void collectCriticalValues_a();
    //! @brief compute critical values of type (b)
    //! @param p a point in P
    //! @param Q the second curve Q
    void collectCriticalValues_b(const Point& p, const Curve& Q);
    //! @brief compute critical values of type (c)
    //! @param pseg a line segment in P
    //! @param Q the second curve Q
    void collectCriticalValues_c(const QLineF& pseg, const Curve& Q);
    //! @brief compute critical values for diagonals and reflex vertices
    //! @param diagonal a diagonal in P
    void collectCriticalValues_d(const QLineF& diagonal);
    //  TODO only i

    //! @brief store a critical value. Duplicates are accepted but removed later.
    //! @param x a critical value
    virtual void collectCriticalValue(const accurate_float& x);

    //! @brief compute critical values of type (b)
    //! @param P first input curve P
    //! @param Q second curve Q
    virtual void collectCriticalValues_b(const Curve& P, const Curve& Q)=0;
    //! @brief compute critical values of type (c)
    //! @param P first input curve P
    //! @param Q second curve Q
    virtual void collectCriticalValues_c(const Curve& P, const Curve& Q)=0;
    //! @brief compute critical values for diagonals and reflex vertices
    virtual void collectCriticalValues_d()=0;
    //! @brief hook for post-processing of critical values
    virtual void combineCriticalValues()=0;
    //! @brief remove duplicates from a sorted list of critical values
    //! @param vector a list of critical values
    //! @pre list must be sorted
    void removeDuplicates(CriticalValueList& vector);

    /**
     * @brief perform a binary search on a list of critical values.
     * For each step of the binary search, perform the decision variant.
     * @param values list of critical values
     * @param lo lower index (inclusive)
     * @param hi upper index (exclusive)
     * @param decideFunction decision function
     * @tparam R result type of the decision function
     *  (either a reachability Graph, or an interval of the reachability structure)
     * @param result holds the result on return
     * @return result index, or -1 if there is no solution
     */
    template<typename R>
    int binarySearch(const std::vector<double>& values, int lo, int hi,
            R (Algorithm::* decideFunction) (FreeSpace::ptr, reach::FSPath::ptr, double, bool),
            R& result);

    /**
     * @brief perform a nested interval search.
     * For each step of the search, perform the decision variant.
     * @param lower_bound lower bound for epsilon
     * @param upper_bound upper bound for epsilon
     * @param precision desired result tolerance (smallest interval)
     * @param decideFunction decision function
     * @tparam R result type of the decision function
     *    (either a reachability Graph, or an interval of the reachability structure)
     * @param result holds the result on return
     * @return approximate result epsilon, or 0.0 if there is not solution
     */
    template<typename R>
    double intervalSearch(
            double lower_bound, double upper_bound, double precision,
            R (Algorithm::* decideFunction) (FreeSpace::ptr, reach::FSPath::ptr, double, bool),
            R& result);

    /**
     * @brief compute the distance between a bisector and a polygon segment
     * @param tolerance for testig line intersections: allow some tolerance for
     *  (better find too many critical values, than find too little!)
     * @param p1x
     * @param p1y
     * @param p2x
     * @param p2y
     * @param q1x
     * @param q1y
     * @param q2x
     * @param q2y
     * @tparam Float floating point type for intermediate results (float,double,long double,...)
     * @return distance between bisector and a polygon segment
     */
    template<typename Float>
    static Float bisector_distance(
            const Float& p1x, const Float& p1y,
            const Float& p2x, const Float& p2y,
            const Float& q1x, const Float& q1y,
            const Float& q2x, const Float& q2y,
            const Float& tolerance);

    /**
     * @brief compute the distance between a polygon segment and a point
     * @param segment segment in P
     * @param q1 point in Q
     * @tparam Float floating point type for intermediate results (float,double,long double,...)
     * @return distance between segment and point
     */
    template<typename Float>
    static Float segment_distance(const QLineF& segment, const Point& q1);

    /**
     * @brief compute the distance between a bisector and a polygon segment
     * @param q1 a point in Q
     * @param q2 anotehr point in Q
     * @param segment segment in P
     * @param tolerance for testig line intersections: allow some tolerance for
     *  (better find too many critical values, than find too little!)
     * @tparam Float floating point type for intermediate results (float,double,long double,...)
     * @return distance between bisector and a polygon segment
     */
    template<typename Float>
    static Float bisector_distance(
            const Point& q1, const Point& q2,
            const QLineF& segment,
            Float tolerance=0.0);

    //! @brief background worker job
    friend class frechet::poly::PolygonWorkerJob;
    //! indicates user interruption
    volatile bool* cancelFlag;
    //! @brief poll cancelFlag
    //! @throw InterruptedException if the user requested an interruption
    void testCancelFlag();
};


/**
 * @brief factory method for creating a new algorithm object.
 * Choose the appropriate implementation based on availability
 * of multi-core and GPGPU processors.
 * @param topo use implementaion with topological sorting
 * @return a newly created instance of an algorithm
 */
Algorithm::ptr newPolyAlgorithm(bool topo);

} } //  namespace frechet::poly
#endif // ALGORITHM_H
