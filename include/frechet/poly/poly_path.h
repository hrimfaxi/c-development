#ifndef POLY_PATH_H
#define POLY_PATH_H

#include <fs_path.h>
#include <grid.h>
#include <graph_m4ri.h>
#include <triangulation.h>
#include <utility>

namespace frechet { namespace poly {

using namespace reach;

/**
 * @brief a feasible-path for simple polygons.
 *
 * Adds to the base class:
 * - feasible path is constructed from a series of smaller sections ("fix points")
 * - these fix points are extracted from the solution to the algorithm ("Combined Reachability Graphs")
 * - find shortest paths in P an Q (mapped to diagonals)
 *
 * @author Peter Schäfer
 */
class PolygonFSPath : public FSPath
{
private:
    //! model for mapping free-space intervals to reachability graph nodes
    GraphModel::ptr gmodel;
public:
    /**
     * @brief constructor with free-space
     * @param fs the underlying free-space
     */
    PolygonFSPath(FreeSpace::ptr fs);

    /**
     * @brief compute a feasible path from the
     * results of the simple polygon algorithm
     * @param result a reachability graph
     * @param epsilon free-space parameter epsilon
     */
    void update(
            const Graph::ptr result,
            double epsilon);

    /**
     * @brief given a diagonal in Q,
     * compute the mapped shortest-path in P.
     * @param qa a vertex in Q
     * @param qb a vertex in Q
     * @param tri triangulation of P
     * @return shortest path that maps to the diagonal
     */
    Curve findShortestPathP(int qa, int qb, Triangulation& tri);
    /**
     * @brief given a diagonal in P,
     * compute the mapped shortest-path in Q
     *
     * To map a polygon edge to a sequence in Q,
     *  use Grid::mapToPoint(mapQ(p1),mapQ(p2)) instead.
     *
     * @param pa a vertex in P
     * @param pb a vertex in P
     * @param tri triangulation of Q
     * @return shortest path that maps to the diagonal
     */
    Curve findShortestPathQ(int pa, int pb, Triangulation& tri);

private:

    /**
     * @brief add a fix point to the feasible path
     * @param pi vertex index in P
     * @param qi vertex index in Q
     */
    void addFixPoint(int pi, int qi);
    /**
     * @brief add a fix point to the feasible path
     * @param p point in free-space
     */
    void addFixPoint(Point p);
    /**
      @brief Drill down, starting from the solution Graph,
      replacing diagonals step by step.
      Until finally we have the outer border curves.
      @param res solution to simple polygons algorithm
    */
    void drillDown(Graph::ptr res);
    /**
     * @brief given a merged reachability Graph,
     * drill down to the original graphs and find
     * the fixed points of the feasible path
     * @param M a reachability graph that is the transitive
     * closusre of thow other Graphs
     * @param k1 left start node
     * @param k2 right node
     */
    void drillDown(Graph::ptr M, int k1, int k2);

	typedef std::pair<int, int> IndexPair;
    /**
     * @brief given the solution of the simple polygon algorithm,
     * drill down to the original graphs and find
     * the fixed points of the feasible path.
     * The solution is the transitive closure G = A*B*A
     * @param A reachability graph
     * @param i start node of feasible path
     * @param B combined reachability graph
     * @return two connecting nodes
     */
	IndexPair findSolutionConnectingPoints(Graph::ptr A, int i, Graph::ptr B) const;
    /**
     * @brief given two reachability graphs,
     * drill down to the original graphs and find
     * the fixed points of the feasible path.
     * @param A combined reachability graph
     * @param ia exit node in A
     * @param B combined reachability graph
     * @param ib entry node in B
     * @return the connecting node of a feasible path
     */
    int findConnectingPoint(Graph::ptr A, int ia,
                            Graph::ptr B, int ib) const;
    /**
     * @brief map a node of a reachability graph
     * to the associated free-space interval
     * @param o horizontal or vertical
     * @param p node index
     * @return the associated free-space interval
     */
    Interval mapInterval(Orientation o, int p) const;
    /**
     * @brief map a node of a reachability graph
     * to a point in the free-space interval.
     * @param o horizontal or vertical
     * @param p node index
     * @return a point in the associated free-space interval
     */
    double mapPoint(Orientation o, int p) const;

    /**
     * @brief for debugging only; test the consistency of a feasible path.
     * It must be monotone in both x- and y-coordinates
     * @param fix fix points of the feasible path
     * @return true if the curve is monotone
     */
    static bool is_consistent(Curve fix);
};

} }     //  namespace frechet::poly

#endif // POLY_PATH_H
