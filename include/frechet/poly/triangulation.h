#ifndef TRIANGULATION_H
#define TRIANGULATION_H

#include <data/types.h>
#include <poly/types.h>
#include <graph_m4ri.h>
#include <vector>

//! include CGAL solely from headers. No CGAL libraries are required.
# define CGAL_HEADER_ONLY 1
//! we don't need GMP support for CGAL
# define CGAL_NO_GMP 1
#include <CGAL/Triangulation_data_structure_2.h>

namespace frechet { namespace poly {

using namespace data;

/**
 * @brief Dummy class acting as a default for template arguments
 * (required by CGAL::Triangulation_data_structure_2)
 */
struct DummyTDS {
    typedef int  Face_handle;
    typedef int  Vertex_handle;
};

/**
 *  @brief base class for vertexes in a Triangulation Data Structure.
 *
 *  Vertexes are derived from TDS vertex class, but carry additional information, namely
 *  - a location in the plane
 *  - references into a polygon chain
 *  - aux. data for the PolygonShortestPaths algorithm, like free-space intervals
 *
 * @tparam TDS a Triangulation Data Structure
 * @author Peter Schäfer
 */
template <class TDS=DummyTDS>
class Vertex_base:
        public Point,
        public CGAL::Triangulation_ds_vertex_base_2<TDS>
{
public:
    /**
     * @brief template re-bind to resolve cyclic dependcies
     */
    template <class TDS2>
    struct Rebind_TDS { typedef Vertex_base<TDS2> Other; };

    /**
     * @brief empty constructore; creates an invalid vertex
     */
    Vertex_base();
    /**
     * @brief constructor from polygon vertex
     * @param p one of the polygon's vertexes
     * @param i index of polygon vertex
     */
    Vertex_base(const Point& p, int i);
    /**
     * @brief constructor from polygon edge
     * @param p a point on the edge of a polygon curve
     * @param i index of neighbor polygon vertex
     * @param offset offset into polygon curve
     */
    Vertex_base(const Point& p, int i, double offset);
    /**
      * @brief destructor (empty, actually)
      */
    ~Vertex_base();

    /**
     * @brief assign a point in the plane
     * @param p a point
     */
    void setPoint(const Point& p);
    /**
     * @brief reset target info (used by PolygonShortestPaths algorithm)
     */
    void clearTarget() { target=nullptr; }

    /**
     * @brief copy properties of derived class (anything but base class info)
     * @param that object to copy from
     * @return reference to this, after assignment
     */
    Vertex_base& setData(const Vertex_base& that);
    /**
     * @brief set properties of derived class (anything but base class info)
     * @param p a point in the plane
     * @param pindex index of polygon vertex (optional)
     * @param dindex index of diagonal vertex (optional)
     * @param offset offset into polygon curve
     * @return reference to this, after assignment
     */
    Vertex_base& setData(const Point& p, int pindex, int dindex, double offset);

    //!  Index of polygon vertex (if this object refers to a polygon vertex; invalid for extra points)
    int pindex;
    //!  Index of diagonal vertex (points into CurveData::d_points); invalid for other points
    int dindex;
    //!  offset on polygon edge. For polygon vertexes, there is   offset==pindex
    //!  for intermediate points on the edges, there is i <= offset <= i+1
    double offset;

    /**
      * @name Aux. data used by PolygonShortestPaths
      *
      * The algorithm for shortest-paths stores extra info at the
      * triangulation vertexes for:
      * - creating a shortes path tree
      * - creating a free-space diagram
      * @{
      */

    //!  backlink for PolygonShortestPaths algorithm.
    //!  (prev,this) describes an edge in the shortest-paths tree
    Vertex_base* prev;
    /**
     *  - for every target point of a placement:
     *  - for every point, when it becomes part of a shortest path:
     */
    const frechet::reach::Placement* target;
    /** @{
     *  @brief Free-Space intervals L,B,R (usually part of a FreeSpace object)
     *  L,B,R is calculated from [Guibas-start diagonal d] x [prev..this]
      */
    Interval L,B,R;
    /** @} */
    /** @{
     *  @brief Reachable Free-Space intervals LR,BR,RR (usually part of an FSPath object)
     */
    Interval LR,BR,RR;
    /** @} */

    /** @} group*/

    /**
     * @return edge in shortest-path tree
     */
    QLineF treeEdge() const {
        if (!prev)
            return QLineF();
        else
            return QLineF(*prev,*this);
    }

    /**
     * @brief debug assertion
     * @param that objet to compare with
     * @return true if both vertexes are considered equal
     */
    bool assertEquals(const Vertex_base& that);
};


/**
 * @brief Wrapper for CGAL::Triangulation_Data_Structure
 * https://doc.cgal.org/latest/TDS_2/index.html
 *
 *  The Triangulation is a double-linked list of vertexes and faces.
 *  It allows for efficient traversal over neighboring vertexes, faces, or edges.
 *
 *  Edge objects are note stored but identified by a face and opposite vertexes.
 *
 *  We use the basic TDS structure with addition information stored on the vertexes.
 *
 *  TDS is spherical by concept. Our polygons are flat, of course.
 *  We introduce an artifical vertex acting as opposite to all outer
 *  edges of the triangulation.
 *
 *  If you think about projecting our polygons onto a sphere,
 *  this would be the south-pole. It is indicated by a special vertex identifier
 *  (polygon vertices being identified by non-negative indexes).
 *
 *  Additional edges connect the south pole to each vertex of the polygon.
 *
 *  Initially, the vertexes of the triangulation are identical to the polygon
 *  vertexes (plus the south-pole).
 *  The algorithm for computing Valid Placements adds (and removes) more vertexes.
 *
 * @author Peter Schäfer
 */
class Triangulation
{
private:
    /**
     * @brief Triangulation Data Structure.
     * Instantiates CGAL::Triangulation_data_structure_2
     * with our customized Vertex_base class.
     */
    typedef CGAL::Triangulation_data_structure_2<
                    Vertex_base<>,
                    CGAL::Triangulation_ds_face_base_2<> >
            TDS;

    //!  polygon size, number of vertexes in polyong
    int poly_n;
    //!  CGAL Triangulation Data Structure
    TDS tds;
    //!  Keeps track of Vertex handles
    typedef std::vector<TDS::Vertex_handle> VertexMap;
    VertexMap vertices;    
    typedef std::map<Segment,TDS::Edge> NeighborMap;
    //!  Keeps track of diagonals and their neighborhood during construction
    NeighborMap neighbors;

    //! a special vertex that completes the TDS structure
    TDS::Vertex_handle south_pole;

public:
    //! @brief a vertex of the triangulation
    typedef Vertex_base<TDS> Vertex;
    //! @brief handle to a vertex (handles are managed by TDS base class)
    typedef TDS::Vertex_handle Vertex_handle;
    //! @brief handle to a triangulation face (handles are managed by TDS base class)
    typedef TDS::Face_handle Face_handle;
    //! @brief an edge of the triangulation
    //! either an edge of the polygon, or a diagonal, or an articial "outer" edge connected to the south-pole
    typedef TDS::Edge Edge;
    //! @brief a circulating iterator
    typedef TDS::Face_circulator Face_circulator;

    //! @brief a pair of vertexes
    typedef std::pair<Vertex_handle,Vertex_handle> VertexPair;
    //! @brief a pair of edges
    typedef std::pair<Edge,Edge> EdgePair;
    //! @brief a pair of faces
    typedef std::pair<Face_handle,Face_handle>  FacePair;

    /**
     * @brief edge iterator.
     * Traverses all edges of the triangulation.
     * Derives from TDS::Edge_iterator and supplies
     * some additional information.
     */
    class Edge_iterator: public TDS::Edge_iterator
    {
        //  Edge = std::pair<Face_handle,int>
    public:
        /**
         * @brief default constructor
         * @param i an edge iterator (base class)
         */
        Edge_iterator(TDS::Edge_iterator i);

        //! @return a handle to the first vertex of the edge
        Vertex_handle vertex1() const;
        //! @return a handle to the second vertex of the edge
        Vertex_handle vertex2() const;

        //! @return an embedding of the edge in the plane
        QLineF line() const;
        //! @return a Segment object, encoding the vertex indices
        Segment segment() const;

        //! @return true if the current edge is an artifical
        //! edge between a polygon vertex and the south-pole
        bool is_outer_edge() const;
        //! @return true if the current edge is an edge of the polygon curve
        bool is_polygon_edge() const;
        //! @return true if the current edge is a diagonal of the polygon
        bool is_diagonal() const;
    };
    //! @brief a range of edges
    typedef std::pair<Edge_iterator,Edge_iterator> Edge_range;

public:
    /**
     * @brief default constructor
     * @param capacity initial number of vertexes (optional)
     */
    Triangulation(int capacity=-1);
    /**
     * @brief copy constructor
     * @param that objet to copy from
     */
    Triangulation(const Triangulation& that);
    /**
      * @brief destructor
      */
	~Triangulation();

    /**
     * @brief set number of polygon vertexes
     * @param n number of polygon vertexes
     */
    void setPolygonSize(int n);

    /**
     * @return number of vertexes in triangulation
     * (not including south-pole)
     */
    int size() const;
    /**
     * @brief reset number of vertexes in triangulation
     */
    void resize(int sz);

    /**
     * @return numer of vertexes in triangulation
     * (including south-pole)
     */
    int countVertexes() const { return (int)tds.number_of_vertices(); }
    /**
     * @return number of faces in triangulation
     */
    int countFaces() const { return (int)tds.number_of_faces(); }

    /**
     * @param i index of vertex
     * @return a handle to a triangulation vertex
     */
    Vertex_handle operator[] (int i);
    /**
     * @param i index of vertex
     * @return an immutable handle to a triangulation vertex
     */
    const Vertex_handle operator[] (int i) const;

    //! @return iterator to the egdes in the triangulation
    Edge_iterator   edges_begin() const     { return Edge_iterator(tds.edges_begin()); }
    //! @return iterator beyond the end of egdes in the triangulation
    Edge_iterator   edges_end() const       { return Edge_iterator(tds.edges_end()); }
    //! @return a pair of iterators
    Edge_range      edges() const           { return Edge_range(edges_begin(),edges_end()); }

    //! @return iterator to the vertexes in the triangulation
    TDS::Vertex_iterator vertices_begin() const     { return tds.vertices_begin(); }
    //! @return iterator beyond the end of vertexes in the triangulation
    TDS::Vertex_iterator vertices_end() const       { return tds.vertices_end(); }

    /** @return the two faces that are adjacent to the segment (i,j) */
    FacePair adjacentFaces(Segment s) const;

    /** @param e an edge in the triangulation
     * @return the two edges that are adjacemt to a given egde */
    EdgePair adjacentEdges(Edge e) const;
    /**
     * @param e an edge in the triangulation
     * @param i1 a vertex
     * @param i2 a vertex
     * @return the two edges that are adjacemt to a given egde
     */
    EdgePair adjacentEdges(Edge e, int i1, int i2) const;

    /**
     * @brief flip the direction of an edge.
     * Keep in mind that Edge object are desribed as a Face + opposite vertex.
     * For each edge, there are two valid interpretations (with opposite faces).
     * @param e reference to an edge object
     */
    void mirror(Edge& e) const;
    /**
     * @brief flip the direction of two edges.
     * @param e2 a pair of Edge objects
     */
    void mirror(EdgePair& e2) const;

    /**
     * @brief a circular (never-ending) iterator that traverses
     * all faces incident to a given vertex.
     * @param v a vertex in the triangulation
     * @return a circular iterator
     */
    Face_circulator incidentFaces(Vertex_handle v) const;
    /**
     * @brief the second vertex opposite to an edge.
     * Keep in mind that Edge object are desribed as a Face + opposite vertex.
     * @param e an edge in the triangulation
     * @return the second vertex opposite to the given edge.
     */
    Vertex_handle oppositeVertex(Edge e) const;
    /**
     * @brief given two vertexes, find the third vertex to complement a triangle
     * @param a a triangulation vertex
     * @param b adjacent triangulation vertex
     * @return third vertex, undefined if the vertexes are not adjacent
     */
    int thirdVertex(int a, int b) const;

    /**
     * @brief construct an edge from two vertexes
     * @param i a triangulation vertex
     * @param j adjacent triangulation vertex
     * @return an Edge of the triangulation, undefined if the vertexes are not adjacent
     */
    Edge edge(int i, int j) const;
    /**
     * @param e an edge in the triangulation
     * @return the two vertexes adjacent to an edge
     */
    VertexPair incidentVertexes(Edge e) const;

    //! @brief a pair of vertex handles
    typedef std::pair<Vertex_handle,Vertex_handle>  TwoVertices;
    /**
     * @param s a segment (edge or diagonal) of the polygon
     * @return the two vertices that are opposite a polygon segment
     */
    TwoVertices adjacentVertices(Segment s) const;
    /**
     * @brief adjacentVertices
     * @param v a polygon vertex
     * @return the two adjacent vertices
     */
    TwoVertices adjacentVertices(int v) const;

    /**
     * @brief create a new triangulation vertex
     * @param p a point in the plane
     * @param i index of polygon vertex (optional)
     * @return a handle to a newly created triangulation vertex
     */
    Vertex_handle createVertex(const Point& p, int i);

    /**
     * @param e a triangulation edge
     * @return the associated polygon Segment (polygon vertex indexes)
     */
    static Segment segment(Edge e);
    /**
     * @param e a triangulation edge
     * @return a handle to the first vertex of the edge
     */
    static Vertex_handle vertex1(Edge e);
    /**
     * @param e a triangulation edge
     * @return a handle to the second vertex of the edge
     */
    static Vertex_handle vertex2(Edge e);

    /**
     * @brief insert a new face.
     * Caller is responsible for keeping the triangulation in a
     * consistent state. For instance, all faces must be oriented
     * counter-clockwise.
     * @param a first vertex
     * @param b second vertex
     * @param c third vertex
     */
    void createFace(int a, int b, int c);

    /**
     * @brief after all faces have been added, create the complementary
     *  "outer" edges (connected to the south-pole).
     */
    void complement();
    /**
     * @return true if all neighboring faces have been inserted
     */
    bool isComplete() const { return neighbors.empty() && tds.is_valid(); }

    /**
     * @name vertex and edge types
     * @{
     */
    /**
     * @param v a triangulation vertex
     * @return true if v is the "south-pole" vertex
     */
    static bool is_south_pole(Vertex_handle v);
    /**
     * @param f a triangulation face
     * @return true if f is an artifical face adjacent to the south-pole
     */
    static bool is_outer_face(Face_handle f);
    /**
     * @param f a triangulation face
     * @return true if f is an proper face of the triangulation
     */
    static bool is_inner_face(Face_handle f) ;
    /**
     * @param e a triangulation edge
     * @return true if e is an artifical edge adjacent to the south-pole
     */
    static bool is_outer_edge(Edge e);
    /**
     * @param e a triangulation edge
     * @return true if e is an edge of the polygon
     */
    static bool is_polygon_edge(Edge e);
    /**
     * @param e a triangulation edge
     * @return true if e is a diagonal in the polygon
     */
    static bool is_diagonal(Edge e);

    /**
     * @param f  a triangulation face
     * @return true if the face is oriented counter clock-wise
     */
    bool is_ccw_face(Face_handle f);

    /** @} group */

    /**
     * @name methods for computing valid placements
     * @{
     */

    /**
     * @brief split an edge into two
     * @param x offset on polygon
     * @return a handle to a newly created triangulation vertex
     */
    Vertex_handle insertVertexOnEdge(double x);
    /**
     * @brief remove a vertex
     * @param index index of vertex, previously created by insertVertexOnEdge.
     * Undefined if the vertex was not create by insertVertexOnEdge.
     */
    void removeVertexFromEdge(int index);

    /**
     * @brief add vertexes for computing valid placements
     * @param p a set of valid placements
     */
    void addTargetVertices(const frechet::reach::Placements& p);
    /**
     * @brief add a vertex for computing valid placements
     * @param p a valid placement
     * @return handle to a newly created triangulation vertex
     */
    Vertex_handle addTargetVertex(const frechet::reach::Placement* p);
    /**
     * @brief add a triangulation vertex on the edge of the polygon
     *  (does not correspond to a polygon vertex)
     * @param w offset on polygon
     * @return  handle to a newly created triangulation vertex
     */
    Vertex_handle addVertex(double w);
    /**
     * @brief clear targets for shortest-path tree search
     */
    void clearTargets();    
    /**
     * @brief used during shortest-paths computation:
     * reset shortest-path tree information on all vertexes
     */
    void clearPredecessors();

    /** @} group */

    /**
     * @brief equality assertion for debugging
     * @param that triangulation to compare with
     * @return true if both triangulations are equal
     */
    bool assertEquals(const Triangulation& that) const;

private:
    /**
     * @brief insert a new triangulation vertex into the map
     * @param v handle to a newly created vertex
     * @param x offset on polygon
     * @param p point in the plane
     */
    void addVertex(Vertex_handle v, double x, Point p);
    /**
     * @brief keep track of neighboring faces during construction
     * of the triangulation.
     * @param s a polygon segment (vertex indexes)
     * @param e a triangulation edge
     */
    void linkNeighbors(Segment s, Edge e);
    /**
     * @brief given two vertexes and a face, find the third vertex of the triangle
     * @param f a triangulation face
     * @param v1 first vertex
     * @param v2 second vertex
     * @return third vertex that makes up the triangle
     */
    static Vertex_handle third_vertex(Face_handle f, Vertex_handle v1, Vertex_handle v2);
    /**
     * @brief given two vertexes and a face, find the third vertex of the triangle
     * @param f a triangulation face
     * @param s two vertexes
     * @return third vertex that makes up the triangle
     */
    Vertex_handle third_vertex(Face_handle f, Segment s);
};

/**
 * @name print operators for debugging
 * @{
 */
std::ostream& operator<< (std::ostream& out, const Triangulation& tri);
std::ostream& operator<< (std::ostream& out, const Triangulation::Vertex_handle& v);
std::ostream& operator<< (std::ostream& out, const Triangulation::Face_handle& f);
std::ostream& operator<< (std::ostream& out, const Triangulation::Edge& e);
std::ostream& operator<< (std::ostream& out, const Triangulation::Edge_iterator& e);
/** @} group */

/*
 * Some template implementations
 * */

template <class TDS>
void Vertex_base<TDS>::setPoint(const Point& p) {
    Point::operator=(p);
}

template <class TDS>
Vertex_base<TDS>& Vertex_base<TDS>::setData(const Vertex_base& that) {
    //  copy _except_ TDS
    Point::operator=(that);
    this->pindex = that.pindex;
    this->dindex = that.dindex;
    this->offset = that.offset;

    this->prev = that.prev;
    this->target = that.target;
    this->L=that.L;
    this->B=that.B;
    this->R=that.R;

    this->LR=that.LR;
    this->BR=that.BR;
    this->RR=that.RR;
    return *this;
}

template <class TDS>
Vertex_base<TDS>& Vertex_base<TDS>::setData(const Point& p, int pindex, int dindex, double offset) {
    Point::operator=(p);
    this->pindex = pindex;
    this->dindex = dindex;
    this->offset = offset;

    this->prev = nullptr;
    this->target = nullptr;
    this->L.clear();
    this->B.clear();
    this->R.clear();

    this->LR.clear();
    this->BR.clear();
    this->RR.clear();
    return *this;
}


} }

#endif // TRIANGULATION_H
