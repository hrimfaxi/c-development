#ifndef SHORTEST_PATHS_H
#define SHORTEST_PATHS_H

#include <data/types.h>
#include <triangulation.h>
#include <double_queue.h>
#include <graph_m4ri.h>

#ifdef QT_DEBUG
class TriangulationTestSuite;
#endif

namespace frechet { namespace poly {

/**
 * @brief compute Shortest-Paths-Tree on a polygon
 *
 *  Given a Polygon, a Triangulation and a set of points:
 *  find all shortest paths from one point to all other points.
 *
 *  This is a slight extension of an algorithm by Guibas, Hershberger et al.
 *  Based on a "funnel" algorithm by Lee and Preparata.
 *
 *  Search structure is not a finger tree, but a specialized double-ended-queue
 *  (because it is much easier to implement).
 *
 *  The class provides some abstract functions that derived classes can override
 *  to implement custom activities. (this is where the Free-Space will be updated)
 *
 *  @author Peter Schäfer
 */
class PolygonShortestPaths
{
public:
    typedef Triangulation::Vertex_handle Vertex_handle;
    typedef Triangulation::Face_handle Face_handle;
    typedef Triangulation::Face_circulator Face_circulator;
    typedef Triangulation::Edge Edge;
    typedef DoubleEndedQueue<Vertex_handle> Funnel;

protected:
    //!  the Triangulation
    Triangulation& tri;
    //!  the Funnel
    Funnel funnel;

public:
    /**
     * @brief default constructor
     * @param atri a triangulation of a polygon
     */
    PolygonShortestPaths(Triangulation& atri);

    /**
     * @return the underlying triangulation
     */
    Triangulation& triangulation() const { return tri; }

    /**
     * @brief traverse the polygon and report found paths
     * @param start_point starting point of the algorithm
     */
    void findShortestPaths(Vertex_handle start_point);

    /**
     * @brief compute the shortest path between two
     * points on the boundary of the polygon
     * @param p1 offset of first point
     * @param p2 offset of first point
     * @return the curve representing the shortest path
     * between p1 and p2
     */
    Curve find1ShortestPath(double p1, double p2);

    /**
     * @brief callback-function when a new shortest-path has been found.
     * derived classes hook in to perform additional computations
     */
    virtual void shortestPath() {}
    /**
     * @brief callback-function when backtrcking
     * derived classes hook in to perform additional computations
     */
    virtual void undo() {}

    /**
     * @brief assertion for unit testing: is the Funnel in a consistent state?
     */
    void assertFunnel() const;
    /**
     * @brief for debugging only: access to funnel
     * @return the double-ended queue use by the algorithm
     */
    const Funnel& getFunnel() const { return funnel; }

protected:
    /**
     * @brief compute the shortest-paths tree
     * @param start_point start point of the algorithm
     * @param f start face
     */
    void search(Vertex_handle start_point, Face_handle f);
    /**
     * @brief compute the shortest-paths tree
     * @param e start edge
     */
    void search(Edge e);

    /**
     * @brief called whenever a ne segment is added to
     *  the shortest path tree; hook for derived classes
     * @param prev first vertex of the segment
     * @param v second vertex of the segment
     * @return true if the tree segment has changed
     */
    virtual bool updateSPSegment(Vertex_handle prev, Vertex_handle v);

    /**
     * @brief find a tangent from a point to the funnel
     * @param v next point to consider
     * @return index in funnel, relative to apex
     */
    int findTangent(Vertex_handle v) const;
    /**
     * @brief find a tangent from a point to the left part of the funnel
     * @param t1 first point in left part
     * @param t2 last point in left part
     * @param v next point to consider
     * @return index in funnel, or 0
     */
    int findTangentLeft(int t1, int t2, Vertex_handle v) const;
    /**
     * @brief find a tangent from a point to the right part of the funnel
     * @param t1 first point in right part
     * @param t2 last point in right part
     * @param v next point to consider
     * @return index in funnel, or 0
     */
    int findTangentRight(int t1, int t2, Vertex_handle v) const;
    /**
     * @brief tangent test
     * @param t index in funnel
     * @param v next point to consider
     * @return true if the line through v and t is a tangent
     */
    bool isTangent(int t, Vertex_handle v) const;

public:
    /**
     * @brief half-plane test.
     *
     *  Given a line segment [ab],
     *  is p on the left or the right of the segment?
     *
     * @param a first end of line segment
     * @param b second end of line segment
     * @param p point to test for
     * @tparam Float floating point type for intermediate
     *  computation (could be float,double,long double,...)
     * @return < 0 if p is on the left side,
     *      > 0 if p is on the right side,
     *      0 if p is colinear to [ab]
     *  (assuming a y-down coordinate system!)
     */
    template<typename Float>
    static int halfplaneTest(Vertex_handle a, Vertex_handle b, Vertex_handle p);
};

/**
 * @brief Guibas' Algorithm with additional Free-Space computation.
 *
 * Whenever a new point is added to the SP tree, we calculate
 * (incrementally) Free-Space reachability.
 *
 * For that purpose, Vertex_base contains additional data members
 * to store free-space and reachability information.
 *
 * Some points are marked as "Target" points. When they are reached,
 * we have discovered a "valid placmenent" which is recorded in a
 * rechability graph.
 *
 *  @author Peter Schäfer
 */
class PolygonShortestPathsFS : public PolygonShortestPaths
{
private:
    //! source placement and
    const frechet::reach::Placement* source;
    //! diagonal
    QLineF d;
    //! free-space parameter epsilon
    double epsilon;
    //! result: holds the valid placements on return
    frechet::reach::Graph::ptr validPlacements;

public:
    /**
     * @brief default constructor
     * @param atri the underlying triangulation
     */
    PolygonShortestPathsFS(Triangulation& atri);

    void findShortestPaths(Vertex_handle start_point,
                            const frechet::reach::Placement* asource,
                            QLineF d, double epsilon,
                            frechet::reach::Graph::ptr validPlacements);

protected:
    /**
     * @brief set up reachability info for a triangulation vertex
     * @param v a vertex in the triangulation
     */
    void initReachability(Vertex_handle v);
    /**
     * @brief update free-space and reachability info,
     * called when a vertex is updated in the shortest-paths tree
     * @param v a vertex in the triangulation
     */
    void propagateReachability(Vertex_handle v);

    /**
     * @brief hook function that is called by the shortest-tree search
     * whenever a new segment is added to the tree.
     * We test whether the new vertex is a 'target' vertex, and if so,
     * update its free-space data. Valid placements are updated.
     * @param prev first vertex of the segment
     * @param v second vertex of the segment
     * @return true if the tree segment has changed
     */
    virtual bool updateSPSegment(Vertex_handle prev, Vertex_handle v) override;

public:
#ifdef QT_DEBUG
    //! for unit testing only
    friend class ::TriangulationTestSuite;
#endif    
    typedef void (*assert_hook1) (PolygonShortestPathsFS*, Vertex_handle v, bool reachable);
    typedef void (*assert_hook2) (PolygonShortestPathsFS*, const frechet::reach::Placement*, const frechet::reach::Placement*);
    //! unit test and debugging hook function; called when a target vertex is found
    static assert_hook1 after_target_found;
    //! unit test and debugging hook function; called when a valid placement is updated
    static assert_hook2 after_placement_added;
};

} }     //  namespace frechet::poly


template<typename Float>
int frechet::poly::PolygonShortestPaths::halfplaneTest(Vertex_handle a, Vertex_handle b, Vertex_handle p)
{
    //  for extra vertexes on a polygon edges, we can detect colinearity from their offset
    double x1 = floor(a->offset);
    double x2 = lower_floor(b->offset);
    if ((x1==x2) && (p->offset >= x1) && (p->offset <= (x1+1)))
        return 0;
    //  otherwise: calculate from geometry
    Float ax = a->x();
    Float ay = a->y();
    Float bx = b->x();
    Float by = b->y();
    Float px = p->x();
    Float py = p->y();

    Float A = (py-ay) * (bx-ax);
    Float B = (px-ax) * (by-ay);
    if (A < B) return -1;
    if (A > B) return +1;
    return 0;
}

#endif // SHORTEST_PATHS_H
