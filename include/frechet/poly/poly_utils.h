#ifndef POLYGON_H
#define POLYGON_H

#include <data/types.h>
#include <poly/types.h>
#include <poly/triangulation.h>

#include <iterator>
#include <boost/unordered_map.hpp>

//! include CGAL solely from headers. No CGAL libraries are required.
# define CGAL_HEADER_ONLY 1
//! we don't need GMP support for CGAL
# define CGAL_NO_GMP 1
//#define CGAL_NO_PRECONDITIONS 1
//#define CGAL_NO_POSTCONDITIONS 1
#include <CGAL/Polygon_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/partition_2.h>

namespace frechet { namespace poly {

//! CGAL computation kernel; predicates are exact,
//! constructions are bound to have roundoff errors (we can't avoid those anyway)
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;

/**
 * @brief a polygon vertex to be used with CGAL::Polygon_2.
 * Derives from CGAL::Point_2 but is convertible to an from QPoint
 * (which is used troughout the rest of the program).
 * Also stores the index of the polygon vertex.
 *
 * Please note that CGAL uses a coordinate system different from Qt.
 * - Qt assumes (0,0) on the TOP left of the drawing plane
 * - CGAL assumes (0,0) on the BOTTOM left of the plane
 *
 * In most cases this doesn't matter much, but sometimes you have to
 * know this (e.g. when deciding if a polygon is oriented clock-wise
 * or not).
 *
 * @author Peter Schäfer
 */
class CgalPoint: public CGAL::Point_2<Kernel>
{
public:
    typedef CGAL::Point_2<Kernel> super;
    //! index of polygon vertex (-1 if not available)
    int i;
    //! used by PolgonUtilities::simplify to mark vertexes for deletion
    mutable bool erase;

    //! @brief empty constructor
    CgalPoint();

    /**
     * @brief default constructor from point
     * @param p point in the plane.
     *  assumes Qt coordinate plane (0,0 on TOP left)
     * @param i index of polygon vertex (optional)
     */
    CgalPoint(const QPointF& p, int i=-1);
    //
    /**
     * @brief default constructor from coordinates.
     *  assumes CGAL coordinate plane (0,0 on BOTTOM left).
     * @param x x-coordinate of point
     * @param y y-coordinate of point
     * @param i index of polygon vertex (optional)
     */
    CgalPoint(double x, double y, int i=-1);

    /**
     * @brief assignment operator from QPointF
     * @param p a point
     * @return reference to this object, after assignment
     */
    CgalPoint& operator= (const QPointF& p);

    /**
     * @brief conversion operator to QPointF
     */
    operator QPointF() const;
};

/**
 * @brief type traits for CgalPolygon.
 * Defines the type for Points.
 */
struct PolygonTraits: public Kernel {
    //!  replace Point_2 with our custom type
    typedef frechet::poly::CgalPoint Point_2;
};

//!  CGAL::Polygon_2 built upon custom type
typedef CGAL::Polygon_2<PolygonTraits> CgalPolygon;
//!  a list of CgalPolygons
typedef std::list<CgalPolygon> CgalPolygonList;

/**
 * @brief type traits for a polygon partition
 * Defines the type for Points and Polygons
 */
struct PartitionTraits: public CGAL::Partition_traits_2<PolygonTraits> {
    //!  replace Point_2 with our custom type
    typedef frechet::poly::CgalPoint Point_2;
    //!  replace Polygon_2 with our custom type
    typedef frechet::poly::CgalPolygon Polygon_2;
};

/**
 * @brief a helper class for polygon analysis.
 *
 * Contains test properties for polygons: is a polygon
 * convex, clock-wise oriented, simple (not self-intersecting) ?
 *
 * Performs convex decomposition of polygons
 * and fan-triangulation of convex parts.
 *
 * @author Peter Schäfer
 */
class PolygonUtilities
{
private:
    //! the polygon under examination
    CgalPolygon cgp;
    //! list of convex parts. created by decompose().
    CgalPolygonList convex;
public:
    /**
     * @brief choice of decomposition algorithm
     */
    enum DecompositionAlgorithm {
        APPROX_GREENE,  //!< Greene's approximation algorithm
        APPROX_HERTEL,  //!< Hertel and Mehlhorn's approximation algorithm
        OPTIMAL_GREENE  //!< Greene's optimal algorithm
    };
public:
    /**
     * @brief empty constructor
     */
    PolygonUtilities();
    /**
     * @brief default constructor with polygon
     * @param p an input polygon
     */
    PolygonUtilities(const Curve& p);

    /**
     * @name Basic Polygon properties
     * @{
     */

    //bool is_closed() const;
    /**
     * @return true if the polygon is simple,
     * i.e. if is contains no self-intersections
     */
    bool is_simple() const;
    /**
     * @return true if the polygon is convex
     */
    bool is_convex() const;
    /**
     * @return true if the polygon is oriented counter-clockwise.
     * fals if it is oriented clockwise.
     */
    bool is_counter_clockwise();

    //int vertexes() const;
    //int edges() const;

    //void revert();

    /** @} group */

    //  @return numer of convex parts
    /**
     * @brief compute a convex decomposition of the polygon.
     * Store results in the member variable 'convex'.
     * @param algo decomposition algorithm to use
     * @param verify if true, verify that results are convex
     * @return the number of convex parts
     */
    int decompose(DecompositionAlgorithm algo, bool verify);

    /**
     * @brief compute a list of sub-polygons, one for each convex part.
     * This is mainly copying data to a more convenient format.
     * @param partition holds the sub-polygons on return
     * @pre decompose() must have computed a convex decomposition
     * @return the number of convex parts
     */
    int partition(Partition& partition) const;

    /**
     * @brief Contract polygon chains to a single edge.
     * Kepp only c-diagonal and contracted edges.
     * (see Buchin et al. for explanation)
     * @pre decompose() must have computed a convex decomposition
     * @return the number of remaining parts
     */
    int simplify();

    /**
     * @brief find all reflex (concave) vertices.
     * @pre assumes the polygon is oriented counter-clockwise
     * @param poly a polygon
     * @param tolerance tolerance for half-plane test.
     * May be use to compensate round-off errors.
     */
    void findReflexVertices(Polygon& poly, double tolerance=0.0);

    //  erstellt ein Fan-Triangulierung mehrerer (konvexer!) Polygone
    /**
     * @brief compute a fan-triangulation from a list of convex parts.
     * Each convex sub-polygon is triangulated using a simple fan-like approach.
     * (see Buchin et al.)
     * @pre decompose() must have computed a convex decomposition
     * @param tri holds the triangulation data structure on return
     */
    void fanTriangulate(Triangulation& tri) const;


private:
    /**
     * @brief debug assertion
     * @return true if the vertex indexes are properly sorted
     */
    bool assertStrictlySorted(const CgalPolygon&);
    /**
     * @brief create a diagonal in a convex partition
     * @param u a vertex index
     * @param v another vertex index
     * @param n numer of vertexes in polygon
     * @param partition holds a list sub-polygons
     * @return true if (u,v) is a diagonal. If so, it was added to partition.
     */
    static bool create1Diagonal(int u, int v, int n, Partition& partition);

    /**
     * @brief Contract polygon chains to a single edge.
     * Kepp only c-diagonal and contracted edges.
     * @param poly a CGAL polygon structure
     * @return the number of remaining parts
     */
    bool simplify(CgalPolygon& poly);
    /**
     * @brief compute a fan-triangulation for one convex part
     * @param poly a CGAL polygon structure
     * @param tri holds the triangulation data on return
     */
    void fanTriangulate(const CgalPolygon& poly, Triangulation& tri) const;
    /**
     * @brief test whether a vertex is a reflex (concave) vertex
     * @param p iterator to a vertex
     * @param precision tolerance for half-plane test.
     * May be use to compensate round-off errors.
     * @return true if the vertex is considered to be a reflex vertex
     */
    static bool is_reflex_vertex(const CgalPolygon::Vertex_circulator& p, double precision=0.0);
};

//! @brief print operator (for debugging)
std::ostream& operator<< (std::ostream& out, const frechet::poly::CgalPolygon& poly);
//! @brief print operator (for debugging)
std::ostream& operator<< (std::ostream& out, const frechet::poly::CgalPolygonList& polys);


}} //   namespace frechet::poly


#endif // POLYGON_H
