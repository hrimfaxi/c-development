#ifndef PARALLEL_H
#define PARALLEL_H

#include <algorithm.h>
#include <data/matrix_pool.h>

#include <tbb/tbb.h>
#include <tbb/combinable.h>
#include <tbb/enumerable_thread_specific.h>
#include <tbb/flow_graph.h>
#include <tbb/task_group.h>
#include <frechet/app/concurrency.h>

namespace frechet { namespace poly {

using namespace reach;


/**
 * @brief Sequential implementation for a single processor core.
 *
 * @author Peter Schäfer
 */
class AlgorithmSingleCore : public Algorithm {
    /*
     *  CRG is an Array of Graph::ptr
     * */
public:
    AlgorithmSingleCore();
	
protected:
	virtual reach::Graph::ptr CRG(int i, int j, Triangulation::Edge e = Triangulation::Edge());
	virtual reach::Graph::ptr create_RG(int i);
	virtual reach::Graph::ptr create_CRG(int i, int j, Triangulation::Edge e);

	virtual reach::Graph::ptr MERGE_inner(const reach::Graph::ptr  A, const reach::Graph::ptr  B);
	virtual reach::Graph::ptr MERGE_outer(const reach::Graph::ptr  A, const reach::Graph::ptr  B);
	virtual void COMBINE(reach::Graph::ptr  A, int di, int dj);


    virtual void calculateReachabilityGraphs() override;
    virtual void calculateValidPlacements(double epsilon) override;
    virtual GraphPtr findFeasiblePath() override;
    virtual bool isSolution(GraphPtr G);

    virtual void collectCriticalValues_b(const Curve& P, const Curve& Q) override;
    virtual void collectCriticalValues_c(const Curve& P, const Curve& Q) override;
    virtual void collectCriticalValues_d() override;
    virtual void combineCriticalValues() override;

    //! @brief  debug assertion; check if the CRGs are correctyl aligned
    bool assertReachabilityGraphs();
};

/**
 * @brief Multi-Core implementaion.
 * Uses Intel TBB to perform some parallel computation.
 *
 * The number of parallel threads is controlled by ConcurrencyContext.
 *
 * - computation of free-space intervals is done in parallel
 * - computation of initial reachability graphs is done in parallel
 * - computation of critical values is done in parallel
 *  thread-local copies of critical values are hold
 *  and finally reduced to a single list
 *
 * - computation of valid placements is done in parallel
 *  to avoid conncurrency issues, the algorithm for shortest-paths
 *  trees operates on *thread-local* copies
 *
 * - MERGE and COMBINE operations on reachability graphs
 *  (multiplication of adjacancy matrices)
 *  are parallelized
 *
 * - if GPGPU support is available, matrix multiplications are
 *   delegated to the GPU. This is handled transparently by
 *   frechet::reach::Graph factory methods.
 *
 *
 *  Parallelisation of the decision-variant main loop was considered but discarded.
 *  See AlgorithmTopoSort for more information.
 *
 *  In the decision variant for curves (Alt and Godau's algorithm):
 *  computation of reachability structures (recursion) is parallelized.
 *
 * @author Peter Schäfer
 */
class AlgorithmMultiCore : public AlgorithmSingleCore {
private:
    typedef tbb::enumerable_thread_specific<Triangulation*> LocalTriangulation;
    //!  thread-local copies of Q->triangulation
    static LocalTriangulation qtri;
    //!  vector of c-diagonals
    std::vector<Segment> c_diagonals_vector;

    typedef tbb::combinable<CriticalValueList> LocalCriticalValueList;
    //!  thread-local copies of critical values
    static LocalCriticalValueList localCriticalValues;

	typedef tbb::enumerable_thread_specific<PolygonShortestPathsFS*> LocalSPT;
    //!  thread-local copies for PolygonShortestPathsFS
    static LocalSPT spt;

public:
    //! @brief default constructor
    AlgorithmMultiCore();
    //! @brief destructor
	virtual ~AlgorithmMultiCore();

    /**
     * @brief re-implements base class method;
     * stores thread-local copies of Q's triangulation
     */
    virtual void triangulate() override;

protected:
    virtual void calculateReachabilityGraphs() override;
    virtual void calculateValidPlacements(double epsilon) override;
    //virtual GraphPtr findFeasiblePath() override;

    //! @brief thread-local copy of critical values
    virtual CriticalValueList& currentCriticalValueList();
    virtual void collectCriticalValues_b(const Curve& P, const Curve& Q) override;
    virtual void collectCriticalValues_c(const Curve& P, const Curve& Q) override;
    virtual void collectCriticalValues_d() override;

    //! @brief reduce thread-local lists to a global list, then sort it
    virtual void combineCriticalValues() override;

    //! @brief reduce two thread-local lists of critical values;
    //! the lists are simply appended
    //! @param a a list
    //! @param b another list
    //! @return reference to a, after appending b
    static const CriticalValueList& combine(CriticalValueList& a, const CriticalValueList& b);
    //! @brief release thread-local variables
	void clear_qtri();
};


/**
 * @brief implementation with a sorted call-graph.
 *
 * The main loop of Buchin's decision algorithm consists of recursive function calls.
 * We create a call-graph of the recursive function calls.
 * Then the call-graph is sorted topologically from bottom to top.
 * See frechet::reach::Graph how the Graph objects double as placeholders for call-graphs nodes.
 *
 * Each level of the sorted call-graph consists of independent tasks.
 * Call dependencies exist only between one level and the next level.
 *
 * This would allow us to distribute the tasks on one level to multiple processor cores.
 * The idea was discarded later because, unfortunately, it interferes with matrix multiplications.
 *
 * It turns out that matrix multiplications are fine-tuned to make best usage
 * of processor cache memory. Running two multiplications in parallel severely hampers cache performance.
 * That's why the approach had to be dropped: the main loop is still sequential,
 * matrix multiplications are parallelized in inner loops.
 *
 * However, the call-graph implementation survived because it has one more advantage.
 * After a level of tasks is finished, unneeded data (from lower levels) can be released from the memoisation maps.
 * Thus, this implementation can be used to reduce memory usage, which is indeed an issue
 * with GPGPU memory.
 *
 * It can be turned on with the command line switch "--gpu --large".
 */
class AlgorithmTopoSort : public AlgorithmMultiCore {
protected:
    //!	@brief list of tasks, sorted by b-level
    //!	b-level 0 = leaves = RG's
	typedef std::list<Graph::ptr> GraphList;
	typedef std::vector<GraphList> TopoSort;
    //! sorted call graph
	TopoSort topo;
    //! roots of the call graph: this is where the results of the algorithm pop up
	GraphList roots;
    //! recycling pool of matrices
	MatrixPool pool;
public:
    //! @brief default constructor
	AlgorithmTopoSort();
    //! @brief destructor release all memory
	virtual ~AlgorithmTopoSort();

    /**
     * @brief re-implements base method; instead of calculating the Graph,
     * we just put a placeholder into the memoisation map.
     * The actual computation is done later.
     */
	virtual reach::Graph::ptr create_RG(int i) override;
    /**
     * @brief re-implements base method; instead of calculating the result,
     * we just put a placeholder into the memoisation map.
     * The actual computation is done later.
     */
    virtual reach::Graph::ptr MERGE_inner(const reach::Graph::ptr  A, const reach::Graph::ptr  B) override;
    /**
     * @brief re-implements base method; instead of calculating the result,
     * we just put a placeholder into the memoisation map.
     * The actual computation is done later.
     */
    virtual reach::Graph::ptr MERGE_outer(const reach::Graph::ptr  A, const reach::Graph::ptr  B) override;
    /**
     * @brief re-implements base method; instead of calculating the result,
     * we just put a placeholder into the memoisation map.
     * The actual computation is done later.
     */
    virtual void COMBINE(reach::Graph::ptr  A, int di, int dj) override;
    /**
     * @brief re-implements the main loop of the decision variant;
     * traverse the call-graph bottom-up and compute the CGRs
     * level by level.
     * Unneeded CRGs are released asap.
     * @return resulting CRG, or nullptr
     */
	virtual GraphPtr findFeasiblePath() override;
	virtual bool isSolution(GraphPtr G) override;

protected:
    //! @brief insert a CGR into the topologically sorted list of calls
    //! @param G an empty CGR that acts as placeholder
	void addTopoSort(Graph::ptr G);
    //! @brief finally compute the contents of a CGR
    //! @param G a CRG placeholder that will now be filled
    void calculate(Graph::ptr G);
    //! @brief release CGRs that are not needed anymore
    //! @param G a CRG whose contents has just been computed; release uneeded predecessor graphs
    void reclaimPredecessors(Graph::ptr G);
    //! @brief reclaim graph memory
    void reclaim(Graph* G);
    //! @brief release all memory
    virtual void cleanup() override;
    //! @brief put up a memory barrier between each level of the call graph
    //!  (necessary for OpenCL implementation)
    //! @brief finish
	void barrier(bool finish);
};


} }  // namespace frechet::poly

#endif // PARALLEL_H
