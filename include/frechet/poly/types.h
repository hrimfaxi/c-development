#ifndef POLY_TYPES_H
#define POLY_TYPES_H

#include <utility>
#include <list>
#include <set>
#include <data/types.h>
#include <data/linkedlist.h>
#include <QDebug>
#include <QLineF>

namespace frechet {

using namespace data;

namespace poly {

/**
 * @brief artifical vertex used by Triangulation
 */
static const int SOUTH_POLE_INDEX = -2;

/**
 *  @brief Represents a polygon line segment from node i to j.
 *
 *  Note that polygon edges are meant to be oriented, i.e. j==i+1
 *  The last edge back to the starting point is therefore encoded as (n-1,n)
 *  (instead of (n-1,0)). The equation j==i+1 indicates proper polygon edges
 *  and indices should be taken modulo n.
 *
 *  *However*. For diagonals, orientation is *not* encoded, therefore i < j
 *  so that we can sort and identify diagonals.
 *
 *  If this seems confusing, we should think about introducing two data types,
 *  (one for edges, another one for diagonals), or introduce a flag for edges.
 *
 * @author Peter Schäfer
 */
class Segment: public std::pair<int,int>
{
public:
    /**
     * @brief empty constructor; creates an invalid polygon segment
     */
    Segment() : std::pair<int,int> (-1,-1) { }
    /**
     * @brief default constructor with vertices.
     * For poylgon edge segments, j==i+1 or j+1==i is expected.
     * For diagonal segements, j!=i+1.
     * @param i first vertex
     * @param j second vertex
     */
    Segment(int i, int j)
    {
        first = std::min(i,j);
        second = std::max(i,j);
        Q_ASSERT((first>=0 || first==SOUTH_POLE_INDEX) && second>=0);
    }
    /**
     * @return true if the segment is empty, or invalid
     */
    bool empty() const {
        return first==second;
    }
    /**
     * @param n number of vertices in polygon
     * @return true if this segment describes a polygon diagonal
     */
    bool is_diagonal(int n) const {
        return ! is_edge(n);
    }
    /**
     * @param n number of vertices in polygon
     * @return true if this segment describes a polygon egde segment
     */
    bool is_edge(int n) const {
        return (second==(first+1)) || ((first==0) && (second==(n-1)));
    }
    /**
     * @brief ordering operator.
     * Imposes a natural ordering on segments, which is useful for
     * sorting sets of segments.
     * @param that segment to compare with
     * @return true if *this < that
     */
    bool operator< (const Segment& that) const
    {
        if (this->first != that.first)
            return this->first < that.first;
        else
            return this->second > that.second;
    }
    /**
     * @brief map (embed) segment to a line in the plane
     * @param curve a polygonal curve
     * @return segment embedded in the plane
     */
    QLineF map(const Curve& curve) const {
        return QLineF(map(curve,first),map(curve,second));
    }
    /**
     * @brief map a polygon vertex to a point in the plane
     * @param curve a polygonal curve
     * @param i index of vertex (modulo n)
     * @return point in the plane
     */
    static QPointF map(const Curve& curve, int i) {
        return curve[i % (curve.size()-1)];
    }
};

//! @brief Polygon a sub-set of vertex indexes
typedef std::vector<int> Polygon;
//! @brief a set of Segment objects
typedef std::set<Segment> Segments;
//! @brief a partitioning of a polygon, that is a list of sub-sets
typedef std::list<Polygon> Partition;

/**
 * @brief print operator; for debuggin
 * @param out output stream
 * @param seg a Segment object
 * @return out
 */
std::ostream& operator<< (std::ostream& out, const Segment& seg);

} } //  namespace frechet::poly

#endif // POLY_TYPES_H
