#ifndef JOBS_H
#define JOBS_H

#include <algorithm.h>
#include <app/workerthread.h>

namespace frechet { namespace poly {
/**
 * @brief  Abstract base class for executing
 * the algorithm for simple polygons in a background thread.
 * As any worker job, it may be interrupted by the user.
 */
class PolygonWorkerJob : public frechet::app::WorkerJob
{
protected:
    //! the algorithm
    poly::Algorithm::ptr alg;
    //! the underlying free-space
    frechet::FreeSpace::ptr fs;
    //! holds the feasible path on return
    frechet::reach::FSPath::ptr fspath;
    //! previous algorithm status; will be reset in the case of user interruption
    poly::Algorithm::Status status_before;
    //! parameter to the algorithm (epsilon)
    double param;
public:
    /**
     * @brief default constructor
     * @param analg the algorithm
     * @param anfs the underlying free-space
     * @param apath holds the feasible path on return
     * @param aparam parameter to the algorithm.
     * This is either epsilon, or an approximation bound.
     */
    PolygonWorkerJob(poly::Algorithm::ptr analg,
            frechet::FreeSpace::ptr anfs,
            frechet::reach::FSPath::ptr apath,
            double aparam)
    : alg(analg), fs(anfs), fspath(apath), param(aparam), status_before(alg->status())
    { }

    /**
     * @brief implemented by derived classes
     */
    virtual void runAlg() =0;
    /**
     * @brief implements abstract method.
     * Sets up the cancel flags and delegates to runAlg().
     */
    virtual void runJob() override {
        alg->cancelFlag = &this->cancelRequested;
        runAlg();
        alg->cancelFlag = nullptr;
    }
    /**
     * @brief implements abstract method.
     * Handles user interruption.
     * Resets the status of the algorithm
     * (see poly::Algorithm::Status).
     */
    virtual void afterInterrupted() override {
        alg->cancelFlag = nullptr;
        alg->resetStatus(true);
        alg->setStatus(status_before);
        if (fspath) fspath->clear();
    }
};

/**
 * @brief worker job for executing the decision variant
 * of the algorithm for simple polygons.
 */
class DecideWorkerJob : public PolygonWorkerJob
{
public:
    /**
     * @brief default constructor
     * @param analg the algorithm
     * @param anfs the underlying free-space
     * @param apath holds the feasible path on return
     * @param epsilon free-space epsilon
     */
    DecideWorkerJob(poly::Algorithm::ptr analg,
            frechet::FreeSpace::ptr anfs,
            frechet::reach::FSPath::ptr apath,
            double epsilon)
        : PolygonWorkerJob(analg,anfs,apath,epsilon) {}
    /**
     * @brief executes the decision variant.
     * Results are placed in fspath and poly::Algorithm::Status.
     */
    virtual void runAlg() override {
        alg->decidePolygon(fs, fspath, param, true);
    }
};

/**
 * @brief worker job for executing the optimisation variant,
 * or approximation variant,
 * of the algorithm for simple polygons.
 *
 * The result of the algorithm is broadcast as
 * signal optimisationResult(epsilon)
 */
class OptimisePolyWorkerJob : public PolygonWorkerJob
{
public:
    /**
     * @brief default constructor
     * @param analg the algorithm
     * @param apath holds the feasible path on return
     * @param approx if ==0, execute the optimisation variant
     * of the algorithm.
     * If > 0 execute the approximation variant with the given
     * bound.
     */
    OptimisePolyWorkerJob(poly::Algorithm::ptr analg,
            frechet::reach::FSPath::ptr apath,
            double approx)
        : PolygonWorkerJob(analg,FreeSpace::ptr(),apath,approx) {}

    /**
     * @brief implements actual execution
     */
    virtual void runAlg() override {
        alg->optimisePolygon(fspath, param);
    }
};

/**
 * @brief worker job for executing the optimisation variant,
 * or approximation variant, on curves (not on simple polygons).
 *
 * The algotihm for curves is considerably easier than the one for polygons.
 * We use the same Algorithm class, however.
 *
 * The result of the algorithm is broadcast as
 * signal optimisationResult(epsilon)
 */
class OptimiseCurveWorkerJob : public PolygonWorkerJob
{
public:
    /**
     * @brief default constructor
     * @param analg the algorithm
     * @param approx if ==0, execute the optimisation variant
     * of the algorithm.
     * If > 0 execute the approximation variant with the given
     * bound.
     */
    OptimiseCurveWorkerJob(poly::Algorithm::ptr analg, double approx)
        : PolygonWorkerJob(analg,FreeSpace::ptr(),reach::FSPath::ptr(),approx) {}

    /**
     * @brief implements actual execution
     */
    virtual void runAlg() {
        alg->optimiseCurve(param);
    }
};

} } //  namespace frechet::poly

#endif // JOBS_H
