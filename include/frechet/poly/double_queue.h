#ifndef DOUBLE_ENDED_QUEUE_H
#define DOUBLE_ENDED_QUEUE_H

#include <stack>
#include <vector>

namespace frechet { namespace poly {

/**
 *  @brief A double ended queue, or "Funnel".
 *  Used by the algorithm for Shortest Paths Trees (Guibas et al.).
 *
 *  Holds an "Apex" pointing inside the double-ended queue.
 *
 *  Keeps a stack of previous operations for backtracking.
 */
/**
@verbatim
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
  ...       | x | x | x | x | x | x | x | x | x |   ...
+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
              ^               ^               ^
              |               |               |
             LEFT            APEX            RIGHT
@endverbatim
 */
/**
 *  @tparam T payload data
 *  @author Peter Schäfer
 */
template<typename T>
class DoubleEndedQueue {
private:
    /**
     * @brief encodes an edit operation
     */
    enum OpCode {
        ADD_LEFT, //!< push a new entry on the left
        ADD_RIGHT, //!< push a new entry on the right
        REMOVE_LEFT, //!< remove entry from the left
        REMOVE_RIGHT //!< remove entry from the right
    };
    /**
     * @brief a stack frame that tracks the necessary information
     * for backtracking
     */
    struct Frame {
        /**
         * @brief constructor with ADD operation
         * @param anop ADD_LEFT/RIGHT; undefined otherwise
         * @param adata reference to payload data
         */
        Frame(OpCode anop, const T& adata);
        /**
         * @brief constructor with REMOVE operation
         * @param anop REMOVE_LEFT/RIGHT; undefined otherwise
         * @param lr current left, or right end
         * @param apex current apex
         */
        Frame(OpCode anop, T* lr, T* apex);
        /**
         * @brief copy constructor
         * @param that objec to copy from
         */
        Frame(const Frame& that);
        /**
         * @brief assignment operator
         * @param that object to copy from
         * @return reference to this, after assignment
         */
        Frame& operator=(const Frame& that) {
            assignFrom(that);
            return *this;
        }

    protected:
        friend class DoubleEndedQueue;
        //! edit operation
        OpCode op;
        //! backtracking data
        union {
            T data; //!< payload to restore
            struct {
                T* LR;  //!< left/right end to restore
                T* A;   //!< apex pointer to restore
            };
        };        
        //! @brief assignment operator
        //! @param that objec to copy from
        void assignFrom(const Frame& that);
    };

    T *buffer;  //!< entry buffer
    int capacity;  //!< buffer capacity
    T *L; //!< pointer to left end (inclusive)
    T *R; //!< pointer to right end (inclusive)
    T *A; //!< pointer to funnel apex

    //! stack of previous operations for backtracking
    std::stack<Frame,std::vector<Frame>> hist;

    /**
     * @brief undo an operation
     * @param fr stack frame that holds the data to restore
     */
    void undo(const Frame& fr);

public:
    /**
     * @brief constructor with fixed capacity
     * @param capacity max. numer of entries to accomodate
     */
    DoubleEndedQueue(int capacity);
    /**
      * @brief destructor; releases all memory
      */
    ~DoubleEndedQueue();

    //! @return current number of entries
    int size() const;
    //! @return number of entries to the left of the apex
    //! (not including apex)
    int leftSize() const;
    //! @return number of entries to the right of the apex
    //! (not including apex)
    int rightSize() const;

    //! @return true if the queue is empty
    bool empty() const;

    /**
     * @brief access operator relative to apex
     * @param offset from apex
     * @return element at
     * 0 = element at apex
     * > 0 elements to the right of the apex
     * < 0 elements to the left of the apex
     * undefined if queue is empty
     */
    const T& operator[] (int offset) const;
    /**
     * @return the element at the apex; undefined if queue is empty
     */
    const T& apex() const;

    /**
     * @return the element at the left end; undefined if queue is empty
     */
    const T& leftEnd() const;
    /**
     * @return the element at the right end; undefined if queue is empty
     */
    const T& rightEnd() const;

    /**
     * @brief push a new element to the left side of the queue
     * @param t a new element
     */
    void addLeft(const T& t);
    /**
     * @brief push a new element to the right side of the queue
     * @param t a new element
     */
    void addRight(const T& t);
    /**
     * @brief reset the qeueue with exactly one element
     * @param apex new apex = left end = right end
     */
    void reset(const T& apex);

    /**
     * @brief pop entries from the left end;
     * Adjust the left end pointer and the apex, as necessary.
     * @param t new location of the left end from the apex
     */
    void removeLeftUpto(int t);
    /**
     * @brief pop entries from the right end;
     * Adjust the right end pointer and the apex, as necessary.
     * @param t new location of the right end from the apex
     */
    void removeRightUpto(int t);

    /**
     * @brief undo the last call to addLeft/Right, or removeLeft/Right.
     * Undefined if there was no such call.
     */
    void undo();
};


} }

#include <double_queue_impl.h>

#endif // DOUBLE_ENDED_QUEUE_H
