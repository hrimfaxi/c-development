
#ifndef GRAPHCL_H
#define GRAPHCL_H

#include <graph_m4ri.h>
#include <list>
#include <iterator>
#include <data/matrix_pool.h>
#include <clm4rm.h>

namespace frechet { namespace reach {
/**
 * @brief Reachability Graph with additional storage in GPU memory.
 *
 * Methods are provided to copy data from main memory to GPU memory
 * and back.
 *
 * MERGE and COMBINE methods are scheduled for GPU execution
 * (as OpenCL kernels).
 *
 * @author Peter Schäfer
 */
class GraphCL : public Graph {
private:
    //!	data stored on GPU memory
	clmatrix_t* clmtx[2][2];
    //! temporary matrixes
	mutable std::list<clmatrix_t*> temps;
    //!	cl_events for out-of-order dependencies
	mutable clm4rm_conditions cond;
    //!	result of searchDiagonalElement
	mutable cl_mem diagonalElementBuffer;

protected:

    friend GraphPtr frechet::reach::newGraph(const GraphModel::ptr model);
	friend GraphPtr frechet::reach::newGraph(const GraphModel::ptr model, IndexRange hmask);
	friend GraphPtr frechet::reach::newGraph(const GraphModel::ptr model, Structure& str);

    /**
     * @brief empty constructor
     * @param model maps reachability intervals to graph nodes
     */
	GraphCL(const GraphModel::ptr model);
    /**
     * @brief constructor with node range
     * @param model maps reachability intervals to graph nodes
     * @param hmask column range covered by the graph
     */
	GraphCL(const GraphModel::ptr model, IndexRange hmask);
    /**
     * @brief constructor from reachability structure
     * @param model maps reachability intervals to graph nodes
     * @param str a reachability structure
     */
	GraphCL(const GraphModel::ptr model, Structure& str);
public:
    /**
     * @brief copy constructor
     * @param that graph to copy from
     */
    GraphCL(const GraphCL& that);
    /**
     * @brief move constructor
     * @param that graph to copy from; empty on return
     */
    GraphCL(GraphCL&& that);
    /**
     * @brief destructor; release all memory, including GPU memory
     */
	virtual ~GraphCL();

    /**
     * @brief assginment operator
     * @param that graph to copy from
     * @return reference to this, after assignment
     */
	GraphCL& operator= (const GraphCL& that);
    /**
     * @brief move assignment operator
     * @param that that graph to copy from; empty on return
     * @return reference to this, after assignment
     */
	GraphCL& operator= (GraphCL&& that);

	//TODO? bool operator== (const Graph& that) const;
    /**
     * @brief copy adjacancy matrix data to GPU memory
     */
    virtual void synchToGpu() override;
    /**
     * @brief copy adjacancy matrix data back from GPU memory to CPU memory
     */
    virtual void synchFromGpu() override;

    /**
     *	@name methods delegated to OpenCL kernels.
     *  All methods return immediately.
     *  Kernels are scheduled for asynchronous execution.
     *  Use OpenCL to wait for execution, then retrieve results
     *  with synchFromGpu, or foundDiagonalElement.
     *
     *  @{
	 */
    virtual void finalize() override;
    virtual void combine(const Graph* P) override;
    virtual void merge2(const Graph* A, const Graph* B, MatrixPool* pool) override;
    virtual void merge3(const Graph* A, const Graph* B, MatrixPool* pool) override;

	virtual void queryDiagonalElement() const override;
	virtual int foundDiagonalElement() const override;

	virtual void release(Orientation o1, Orientation o2) override;
	virtual void release(Orientation o1, Orientation o2, MatrixPool* pool) override;
	virtual void resetConditions() override;
    /** @} */
protected:
    /**
     * @brief allocate a temporary matrix
     * @param rows number of rows
     * @param cols number of columns
     * @param pool pool for re-cycled matrix data
     * @return a Boolean matrix
     */
	clmatrix_t* tempMatrix(int rows, int cols, MatrixPool* pool) const;
    /**
     * @brief copy data
     * @param that graph to copy from
     */
    void copy(const GraphCL& that);
    /**
     * @brief swap data
     * @param that graph to swap with
     */
    void swap(GraphCL& that);
};

} }	//	namespace frechet::reach

#endif // GRAPHCL_H
