#ifndef REACHABILITY_H
#define REACHABILITY_H

#include <freespace.h>
#include <boundary.h>
#include <poly/types.h>


namespace frechet { namespace reach {

using namespace data;
using namespace free;
using namespace poly;

/**
 *  @brief Calculates a feasible path in the Free-Space
 *  given a start point (0,0) and an end point (n-1,m-1).
 *
 *  Maintains an Array of L^R and B^R intervals, that are reachable
 *  from the starting point.
 *
 *  Allows arbitrary start points (x,0), or (0,y)
 *  wrap around right / top border if curves are closed.
 *
 *  The feasible path, as displayed on screen, may contain
 *  two parts (because we fold the double-free-space into one).
 *
 *  Floating point precision is an issue:
 *  Free-Space values are in [0..1]
 *
 *  Interval in Reachability structure are shifted in [i..i+1]
 *  introducing potential rounding errors.
 *  Be careful to compare value either
 *  -# without offset
 *  -# with identically calculated offset
 *
 *  Avoid calculating offsets back and fort.
 *
 *  Alternatively, use a fixed point representation
 *  like FixedPoint { int i; double mantissa; }
 *
 * @author Peter Schäfer
 */
class FSPath {

protected:
    //!  underlying free-space
    FreeSpace::ptr fs;
    //! free-space wraps at top, Q is closed
    bool wrapTop;
    //! free-space wraps right, P is closed
    bool wrapRight;
    //!  reachable intervals
    Array2D<Interval> LR,BR;
    //!  fix points
    Curve fix;
    //!  Path curves(s)
    Curve path[2];
public:
    //! floating point precision
    static const double PRECISION;

public:
    //! smart pointer to an FSPath object
    typedef boost::shared_ptr<FSPath> ptr;

    /**
     * @brief constructor with free-space
     * @param afs the underlying free-space
     */
    FSPath(FreeSpace::ptr afs);
    /**
     * @brief destructor
     */
    virtual ~FSPath() {}

    /**
     * @return true, if there is no feasible path
     */
    bool empty() const {
        return path[0].empty() && path[1].empty();
    }
    /**
     * @brief get the feasible path for display on screen.
     * May be split into two parts.
     * @param i 0 or 1
     * @return first or second part of the feasible path
     */
    Curve getPath(int i) const;

    /**
     * @brief for a line segment in P, find the
     * mapped part of the feasible path
     * @param pseg line segment in P
     * @param result on return, holds the feasible path segment
     * that maps to the P line segment
     */
    void mapFromP(Segment pseg, Curve result[2]) const;
    void mapFromQ(Segment pseg, Curve result[2]) const;

    void mapToP(Curve path[2]) const;
    void mapToQ(Curve path[2]) const;

    Point mapFromP(int p) const;
    Point mapFromQ(int q) const;

    double mapFromPToQ(int p) const;
    double mapFromQToP(int q) const;

    const frechet::data::Interval& left(int i, int j) const {
        return LR.at(i,j);
    }

    const frechet::data::Interval& bottom(int i, int j) const {
        return BR.at(i,j);
    }

    const Curve& fixPoints() const { return fix; }

    void clearReachability();

    virtual void clear();

    /**
     * @brief update L^R and B from Free-Space diagram
     * @param start start location in free-space diagram (column and row)
     * @param end destination location in free-space diagram (column and row)
     * @param allow_wrap allow to wrap at edges of free-space
     * @param prec numerical precision for comparing intervals
     */
    void calculateReachability(Point start, Point end, bool allow_wrap, double prec=PRECISION);

    /**
     * @brief compute a feasible path from
     * the free-space diagram
     * @param start start point
     * @param epsilon
     */
    void update(Point start, double epsilon);

    /**
     * @brief pick a point from a reachability interval
     * @param segment interval of a reachability structure
     * @return a point within the interval, a candidate for a feasible path
     */
    Point toPoint(Pointer segment);

    /**
     * @brief follow the reachable intervals
     * and test whether a point is reachable from the start
     * @param i column in free-space
     * @param j row in free-space
     * @param prec numerical precision for comparing intervals
     * @return true, if q is reachable from p
     */    
    bool isReachable(int i, int j, double prec=PRECISION) const;
    /**
     * @brief follow the reachable intervals
     * and test whether a point is reachable from the start
     * @param a point in free-space
     * @param prec numerical precision for comparing intervals
     * @return true, if q is reachable from p
     */
    bool isReachable(Point a, double prec=PRECISION) const;
    /**
     * @return start of feasible path
     */
    Point startPoint() const { return fix.first(); }

    /**
     * Calculate reachability interval opposite to a given
     * interval.
     *
     * @param LR Left (or bottom) Rechability
     * @param RF Right (or top) Free-Space
     * @returns result RR Right Reachability
     */
    static Interval opposite(const Interval& LR, const Interval& RF);

public:
    /**
     * @brief propagate the reachable intervals within one cell.
     * from bottom and left to right and top
     * @param RF free-space interval on the right edge of the cell
     * @param TF free-space interval on the top edge of the cell
     * @param LR reachable interval on the left edge
     * @param BR reachable interval on the bottom edge
     * @param RR holds on return the reachable interval on the right edge
     * @param TR holds on return the reachable interval on the top edge
     */
    static void propagate(const Interval& RF, const Interval& TF,
                           const Interval& LR, const Interval& BR,
                           Interval& RR, Interval& TR);
protected:
    //! @brief constructor for derived classes only
    FSPath();

    //! @brief calculate the feasible path
    void calculatePath();

    /**
     * @brief compute one segment of the feasible path.
     * THe path is completed from  top-right backwards to bottom-left.
     * @param a destination point (bottom-left)
     * @param b start point (top-right)
     * @param curve holds the new segments on return
     * @param cc holds the number of curve segments on return
     */
    void findPathSegment(Point a, Point b, Curve curve[2], int& cc);
    /**
     * @brief propagate the reachable intervals along the horizontal axis
     * @param a start point
     * @param bx destination coordinate
     * @param prec numerical precision for comparing intervals
     * @return false, if the start point is not reachable
     */
    bool propagateHorizontalEdge(Point a, double bx, double prec=PRECISION);
    /**
     * @brief propagate the reachable intervals along the vertical axis
     * @param a start point
     * @param by destination coordinate
     * @param prec numerical precision for comparing intervals
     * @return false, if the start point is not reachable
     */
    bool propagateVerticalEdge(Point a, double by, double prec=PRECISION);
    /**
     * @brief propagate the reachable intervals within one cell.
     * The cell is located in the inner part of the free-space diagram.
     * @param i column in free-space
     * @param j row in free-space
     * @param bx x-coordinate of destination point,
     * @param by y-coordinate of destination point
     */
    void propagateInner(int i, int j, double bx, double by);
    /**
     * @brief propagate the reachable intervals within one cell.
     * The cell is located at the bottom of the free-space diagram.
     * @param i column in free-space
     * @param j row in free-space
     * @param bx x-coordinate of destination point
     * @param by y-coordinate of destination point
     */
    void propagateBottom(int i, int j, double bx, double by);
    /**
     * @return the next column, possible wrapping around the right edge
     */
    int next_hor(int i);
    /**
     * @return the next row, possible wrapping around the top edge
     */
    int next_vert(int j);
    /**
     * @brief test the reachability interval on the left of a cell
     * @param i column in free-space
     * @param j row in free-space
     * @param y point to test
     * @param prec numerical precision for comparing intervals
     * @return true if y is contained in the left reachable interval in cell (i,j)
     */
    bool left_contains(int i, int j, double y, double prec=PRECISION) const;
    /**
     * @brief test the reachability interval on the bottom of a cell
     * @param i column in free-space
     * @param j row in free-space
     * @param x point to test
     * @param prec numerical precision for comparing intervals
     * @return true if x is contained in the bottom reachable interval in cell (i,j)
     */
    bool bottom_contains(int i, int j, double x, double prec=PRECISION) const;

    /**
     * @brief propagate the reachable intervals along a column of the free-space diagram.
     * @param i column in free-space
     * @param j0 bottom row in free-space
     * @param bx x-coordinate of destination point
     * @param by y-coordinate of destination point
     * @param allow_wrap if true, wrap at the top edge
     */
    void propagateColumn(int i, int j0, double bx, double by, bool allow_wrap);
    /**
     * @brief propagate the reachable intervals along a bottom row of the free-space diagram.
     * @param i column in free-space
     * @param j0 bottom row in free-space
     * @param bx x-coordinate of destination point
     * @param by y-coordinate of destination point
     * @param allow_wrap if true, wrap at the top edge
     */
    void propagateBottom(int i, int j0, double bx, double by, bool allow_wrap);
    /**
     * @brief map a relative offset to a line segment.
     * For values in [0..1] the resulting point is one the line segment.
     * For value <0 or >1 the resulting point would be off the line segment.
     * @param a relative offset on line segment
     * @param p1 start of line
     * @param p2 end of line
     * @return mapped point
     */
    static double mapToSegment(double a, Point p1, Point p2);
    /**
     * @brief append a value to a vector, avoiding duplicates
     * @param map a vector of values
     * @param x new value
     */
    static void append(std::vector<double>& map, double x);

    /**
     * @brief find a point by its x coordinate
     * @param curve a polygonal curve, assumed to be sorted
     * @param x x-coordinate of a point
     * @return index of x, or -i-1 for the index where x should be inserted
     */
    static int binSearchX(const Curve& curve, int x);
    /**
     * @brief find a point by its y coordinate
     * @param curve a polygonal curve, assumed to be sorted
     * @param y y-coordinate of a point
     * @return index of y, or -i-1 for the index where x should be inserted
     */
    static int binSearchY(const Curve& curve, int y);
    /**
     * @brief copy parts of a curve
     * @param dest destination curve
     * @param source input curve
     * @param i start index to copy from
     * @param j end index
     */
    static void copy(Curve& dest, const Curve& source, int i, int j);
    /**
     * @brief test if two curves are consistent with being a feasible path.
     * Both curves must be monotone (asecning x and y-coordinates),
     * wrap at the right or top edge.
     * Used for debuggin and unit tests.
     * @param path0 first part of feasible path
     * @param path1 second part of feasible path
     * @param n number of vertexes on P
     * @param m number of vertexes on Q
     * @return true, if the two parts are properly monotone
     */
    static bool are_consistent(Curve path0, Curve path1, int n, int m);
    //void copy2Sections(Curve dest[2], int i1, int j1,  int i2, int j2) const;
};


} }  //  namespace frechet::reach

#endif // REACHABILITY_H
