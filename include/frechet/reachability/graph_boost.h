#ifndef GRAPH_H
#define GRAPH_H

#include <structure.h>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
//#include <boost/graph/adjacency_matrix.hpp>


namespace frechet { namespace reach {

class GraphModel;

/**
 *  Represents a Reachability Graph.
 *  Vertices correspond to Boundary intervals that are connected
 *  to every reachable interval on the opposite side.
 *
 *  Graphs can be merged.
 *
 *  Transitive Closure:
 *  uses boost::Graph O(|V||E|) which is pretty good for sparse graphs (|E| << n^2)
 *  see https://www.boost.org/doc/libs/1_48_0/libs/graph/doc/transitive_closure.html
 *  - would profit somewhat from persisted condensation graphs
 *
 *  Theoretically, asymptotically better for dense Graphs:
 *  transitive closure based an Boolean Matrix Multiplication.
 *  However, implementation is more difficult.
 *  - strongly connected components form the condensation graph (persist!)
 *  - sort in topological order
 *  - divide & conquer recursively, see https://www.cs.bgu.ac.il/~dinitz/Course/SS-12/transitiveClosure.pdf
 *  - finally, uses BMM. M4RI(E) is a good candidate for BMM,
 *      but it can't operate on sub-matrices; requires some copying.
 *      maybe, it would pay-off for large matrices.
 *
 *  We should also consider "dynamic" algorithms for transitive closure.
 *  Asymptotically similar, but may be more efficient in keeping data structures.
 */
class Graph
{
private:
    typedef boost::property <boost::vertex_name_t, int> Name;
    typedef boost::property <boost::vertex_index_t, std::size_t, Name> Index;
    typedef boost::adjacency_list < boost::setS, boost::listS, boost::directedS, Index > graph_t;
    //typedef boost::adjacency_matrix <boost::directedS> graph_t;
    typedef boost::graph_traits < graph_t >::vertex_descriptor vertex_t;

    //  TODO Map Indexes to vertices. This a awkward and error-prone. There must be better ways with boost::graph !!
    typedef boost::unordered_map<int,vertex_t> Index2VertexMap;
    typedef boost::property_map<graph_t,boost::vertex_index_t>::type Vertex2IndexMap;

    graph_t* g;
    Index2VertexMap i2v;

    const GraphModel& model;

    Graph(const GraphModel& model, graph_t* ag);
public:
    typedef boost::shared_ptr<Graph> ptr;

    Graph(const GraphModel& model);
    Graph(const GraphModel& model, Structure& str, int i0);
    Graph(const Graph&);
    Graph(Graph&&);

    ~Graph();

    int vertices() const;
    int edges() const;

    Graph& operator= (const Graph&);
    Graph& operator= (Graph&&);

    Graph& operator+= (const Graph&);

    Graph::ptr transitiveClosure() const;
    void transitiveClosureInPlace();

    void add_edge(int i, int j);

    void clear();

private:    

    void copy(const Graph&);
    void swap(Graph&);
    vertex_t vertex(int i);

    //  copy reachability info from Structure to Graph
    void copy(const Structure& str, Pointer i1, Pointer i2);
};

/**
 *  Maps reachability intervals (BoundarySegment*)
 *  to Graph vertices (indexes for rows/columns of adjacency matrix).
 *
 *  To create comparable Graphs, the boundary intervals must be unified.
 *  We use a prototype Structure that conains the "most refined" intervals.
 *
 *  TODO TODO TODO TODO
 *
 *  Test Cases (Google Test?)
 *
 *
 *  Alternatively, we could calculate the prototype intervals by intersecting
 *  all Free-Space intervals (saves us the trouble of building a reach::Structure).
 *  But that would mainly duplicate code from Structure, so we leave it at that (yet)...
 */
class GraphModel {
private:
    //  horizontal an vertical model (interval lists)
    BoundaryList _model[2];
    //  entry points for columns (reachability graphs starting at a certain free-space column)
    std::vector<Pointer> _column_entry;
    //  TODO do we need row entry points, too ?
    //  number of vertices
    int _count;
public:
    GraphModel();
    GraphModel(Structure& prototype);

    void init(Structure& prototype);

    int count() const { return _count; }

    //  refince (= adjust intervals to prototype = the "most refined" structure)
    void refine(Structure& str, int i0) const;

    //  (1) index Boundary Segments
    //  @deprecated should be already part of 'refine'
    void indexify(Structure& str, int i0) const;

private:
    BoundaryList& list(Orientation ori) { return _model[ori]; }

    BoundaryList& horiz() { return _model[HORIZONTAL]; }
    BoundaryList& vert() { return _model[VERTICAL]; }

    const BoundaryList& horiz() const { return _model[HORIZONTAL]; }
    const BoundaryList& vert() const { return _model[VERTICAL]; }

    void indexify(Structure& str, Orientation ori, const Pointer model) const;
    void refine(Structure& str, Orientation ori, const Pointer model) const;
};

} }     //  namespace frecht::reach

#endif // GRAPH_H
