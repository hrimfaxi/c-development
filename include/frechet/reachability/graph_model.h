#ifndef GRAPH_MODEL_H
#define GRAPH_MODEL_H

#include <boundary.h>
#include <freespace.h>
#include <boost/container/map.hpp>

namespace frechet { namespace reach {

using namespace data;
using namespace free;

/**
 * @brief a range of node indices in a Reachability Graph
 * @author Peter Schäfer
 */
struct IndexRange {
    //!  horizontal or vertical part of the reachability structure?
    Orientation ori;
    //! lower index (inclusive)
    int lower;
    //! upper index (exclusive)
    int upper;

    //! @brief empty constructor
    IndexRange();
    /**
     * @brief default constructor
     * @param ori horizontal or vertical part of the reachability structure?
     * @param l lower index (inclusive)
     * @param u upper index (exclusive)
     */
    IndexRange(Orientation ori, int l, int u);

    /**
     * @return true if the range is empty
     */
    bool empty() const;
    /**
     * @brief clear range
     */
    void clear();
    /**
     * @return number of nodes in range
     */
    int len() const;

    /**
     * @brief containment test
     * @param i a node index
     * @return true, if the the node is within the range
     */
    bool contains(int i) const;
    /**
     * @brief containment test
     * @param that another range
     * @return true, if 'that' is contained in 'this'
     */
    bool contains(const IndexRange& that) const;
    /**
     * @brief comparison operator
     * @param that another range
     * @return true, if 'that' is equal to 'this'
     */
    bool operator==(const IndexRange& that) const;
    /**
     * @brief comparison operator
     * @param that another range
     * @return true, if 'that' is not equal to 'this'
     */
    bool operator!=(const IndexRange& that) const;

    /**
     * @brief union operator
     * @param b another range, required to be adjacent to this range
     * @return the union of both ranges, undefined if the ranges are not adjacent
     */
    IndexRange operator+ (const IndexRange& b) const;
    /**
     * @brief shift operator
     * @param offset offset to apply to the range
     * @return a copy of this, shifted by offset
     */
    IndexRange operator+ (int offset) const;
    /**
     * @brief shift operator
     * @param offset offset to apply to the range
     * @return a copy of this, shifted by offset
     */
    IndexRange operator- (int offset) const;

    /**
     * @brief intersection operator
     * @param b another range, required to have the same orientation
     * @return intersection of both ranges, undefined if orientation differs
     */
    IndexRange operator& (const IndexRange& b) const;
    /**
     * @brief union operator
     * @param b  another range, required to be adjacent to this range
     * @return reference to this, after being updated
     */
    IndexRange& operator+= (const IndexRange& b);
    /**
     * @brief shift operator
     * @param offset offset to apply to the range
     * @return reference to this, after being shifted
     */
    IndexRange& operator-= (int offset);

    /**
     * @brief intersection operator
     * @param b another range, required to have the same orientation
     * @return reference to this, after being update to the intersection of both ranges
     */
    IndexRange& operator&= (const IndexRange& b);

    /**
     * @brief intersection test
     * @param b another range
     * @return true if both ranges intersect
     */
    bool intersects(const IndexRange& b) const;
};



/**
 * @brief placement of a diagonal point in free-space diagram
 * When calculating valid placements in [buchin06]
 */
struct Placement
{
    //! Free-Space Interval
    Interval fs;
    //! Arbitrary point within the interval.
    //! preferred to be a vertex (=integral) point
    double w;
    //! mapping to nodes of the reachability graph (see GraphModel)
    IndexRange gn;
};

//! @brief a list of Placement objects
typedef std::vector<frechet::reach::Placement> Placements;

/**
 * @brief bias of a GraphModel boundary
 * (how shall we explain this?)
 */
enum Bias: uint8_t {
    UNBIASED=0, //!< boundary is not inclusive to any interval
    LOWER=1,    //!< boundary is inclusive to adjacent lower interval
    UPPER=2,    //!< boundary is inclusive to adjacent upper interval
    LOWER_UPPER=3   ///!< boundary is inclusive to both adjacent intervals
};

/**
 * @brief union operator
 * @param a a bias
 * @param b another bias
 * @return combined bias
 */
Bias operator| (Bias a, Bias b);
/**
 * @brief union operator
 * @param a a bias, updated on return
 * @param b another bias
 * @return reference to this, after being updated
 */
Bias& operator|= (Bias& a, Bias b);

/**
 * @brief mapping within a GraphModel
 */
struct BoundsIndex {
    //! index of node in reachability graph
    int index;
    //! bias: inclusive to lower / upper interval
    Bias bias;
    /**
     * @brief default constructor
     * @param i index of node in reachability graph
     * @param b bias: inclusive to lower / upper interval
     */
    BoundsIndex(int i, Bias b) : index(i),bias(b) {}
    /**
     * @brief shift operator
     * @param offset offset to add to node index
     * @return copy of this object, with shifted node index
     */
    BoundsIndex operator+ (int offset) const {
        return BoundsIndex(index+offset,bias);
    }
};

/**
 * @brief manages a mapping between
 *  free-space intervals (continous, floating point)
 *  to reachability graph nodes (discrete, integer)
 *
 *  Some effort is taken to model free-space intervals
 *  consisting of exactly one point. Though this is a
 *  border case, it must be properly treated.
 *
 * @author Peter Schäfer
 */
class GraphModelAxis {
private:
    //! @brief maps free-space coordinates to graph nodes
    //!  (identified by node index, with an optional bias to adjacent intervals)
    typedef boost::container::map<double,BoundsIndex> Map;
    Map map;
    //! maximum node index
    int _max_index;
    //! width or height of the free-space diagram
    int _dim;

    friend class GraphModel;

public:
    /**
     * @brief empty constructor
     */
    GraphModelAxis();

    /**
     * @brief insert one value, with an optional bias.
     * Initially will be mapped to an invalid Graph node.
     * Later, the entries will be assigned the correct graph nodes
     * (see indexify)
     * @param x boundary value
     * @param bias inclusive to adjacent intervals?
     */
    void insert1(double x, Bias bias);
    /**
     * @brief insert a value plus value shifted by free-space width
     * (so that we can model a double-free-space diagram)
     * @param x boundary value
     * @param bias
     */
    void insert2(double x, Bias bias);

    /**
     * @brief look up a value and map it to a graph node
     * @param x a point in free-space
     * @return associated graph node index
     */
    const BoundsIndex& lookup(double x) const;
    /**
     * @brief look up a node that is lower or equal to value
     * @param x a point in free-space
     * @return associated graph node index, or nullptr if not found
     */
    const BoundsIndex* lookupLowerEqual(double x) const;
    /**
     * @brief look up a node that is larger than value
     * @param x a point in free-space
     * @return associated graph node index, or nullptr if not found
     */
    const BoundsIndex* lookupLarger(double x) const;

    /**
     * @brief create a reverse mapping;
     * mapping graph nodes to free-space intervals
     * @param result a list of free-space intervals
     */
    void createReverseMap(std::vector<Interval>& result) const;

private:
    /**
     * @brief remove a value
     * @param x free-space point
     */
    void remove(double x);
    /**
     * @brief assign graph node indexes to all values (intervals)
     * @param start initiali nodex index
     */
    void indexify(int start);
};

/**
 * @brief model the mapping of free-space intervals
 * to nodes in a frechet::reach::Graph.
 *
 * For each horizontal or vertical interval in free-space,
 * there will be a node in the reachability graph.
 *
 * The model is copmuted by overlaying all free-space intervals.
 *
 * Accomodates double-free-space diagrams, too.
 * We do not model the duplicated part of the free-space diagram,
 * but the reachability graph does.
 *
 * @author Peter Schäfer
 */
class GraphModel {

private:
    //! horizontal and vertical mapping
    GraphModelAxis axis[2];
    //! reverse mapping from nodes to intervals (horizontal and vertical)
    std::vector<Interval> reversed[2];

public:
    //! smart pointer to a GraphModel object
    typedef boost::shared_ptr<GraphModel> ptr;

    /**
     * @brief empty constructor
     */
    GraphModel() : axis{GraphModelAxis(),GraphModelAxis()} {}

    /**
     * @brief initialize from free-space intervals
     * @param fs a free-space diagram
     */
    void init(const FreeSpace::ptr fs);
    /**
     * @return true, if there is no mapping
     */
    bool empty() const;

    /**
     * @param ori horizontal or vertical
     * @return width or height of single free-space diagram
     */
    int dim1(Orientation ori) const;
    /**
     * @param ori horizontal or vertical
     * @return width or height of double free-space diagram
     */
    int dim2(Orientation ori) const;

    /**
     * @brief map a node index to its counterpart in the duplicated
     * part of a double-free-space
     * @param hindex a reachability graph node index
     * @return the node that represents the same interval shifted
     *  to the right part of a double-free-space diagram
     */
    int lowerShiftedHorizontally(int hindex) const {
        double x = reversed[HORIZONTAL][hindex].lower();
        double offset = dim1(HORIZONTAL);
        return map_lower(HORIZONTAL,x+offset);
    }

    /**
     * @brief map a node range to its counterpart in the duplicated
     * part of a double-free-space
     * @param r a range of node indexes
     * @return the range that represents the same interval shifted
     *  to the right part of a double-free-space diagram
     */
    IndexRange shiftedHorizontally(const IndexRange& r) const {
        Q_ASSERT(r.ori==HORIZONTAL);
        return IndexRange(HORIZONTAL,
                          lowerShiftedHorizontally(r.lower),
                          lowerShiftedHorizontally(r.upper));
    }
    /**
     * @param ori horizontal or vertical
     * @return total number of graph nodes in a dimension
     */
    int count2(Orientation ori) const;
    /**
     * @return total number of graph nodes, horizontal and vertical
     */
    int count2() const;

    /**
     * @brief map a free-space interval to a range of
     * graph nodes
     * @param ori horizontal or vertical
     * @param upperBoundInclusive is the upper interval bound inclusive?
     * @return a range of graph nodes
     */
    IndexRange map(Orientation ori, const Interval&,
                    bool upperBoundInclusive) const;

    /**
     * @brief map a reachability structure interval
     * to a range of graph nodes
     * @param b pointer to a reachability structure interval
     * @return a range of graph nodes
     */
    IndexRange map(Pointer b) const;

    /**
     * @brief map the lower bound of a reachability structure interval
     * @param p pointer to a reachability structure interval
     * @return a graph node
     */
    int map_lower(Pointer p) const;
    /**
     * @brief map the upper bound of a reachability structure interval
     * @param p pointer to a reachability structure interval
     * @return a graph node
     */
    int map_upper(Pointer p) const;

    /**
     * @brief map an interval lower bound
     * @param ori horizontal or vertical
     * @param x a lower bound of a free-space interval
     * @return the associated reachability graph node
     */
    int map_lower(Orientation ori, double x) const;
    /**
     * @brief map an interval upper bound
     * @param ori horizontal or vertical
     * @param x a lower bound of a free-space interval
     * @param inclusive is the upper bound inclusive?
     * @return the associated reachability graph node
     */
    int map_upper(Orientation ori, double x, bool inclusive) const;

    /**
     * @brief map a free-space interval to the closest graph nodes
     * @param ori horizontal or vertical
     * @param ival a free-space interval
     * @return a range of graph nodes
     */
    IndexRange mapClosest(Orientation ori, const Interval& ival) const;

    /**
     * @brief create a reverse map, mapping graph nodes
     * to free-space interval
     * @param ori horizontal or vertical
     * @return a list of free-space intervals
     */
    const std::vector<Interval>& reverseMap(Orientation ori) const {
        return reversed[ori];
    }

    /**
     * @brief merged two node ranges representing the horizontal masks
     * of reachability graph (see frechet::reach::Graph memory layout).
     * @param m1 horizontal range of a graph
     * @param m2 adjacent horizontal range of another graph
     * @return merged range
     */
    IndexRange mergedHMask(const IndexRange& m1, const IndexRange& m2) const;

private:
    /**
     * @brief set up the mappings for vertical intervals
     * @param fs a free-space
     */
    void initVertical(const FreeSpace::ptr fs);
    /**
     * @brief set up the mappings for horizontal intervals
     * @param fs a free-space
     */
    void initHorizontal(const FreeSpace::ptr fs);
};

} }     //  namespace frechet::reach

#endif // GRAPH_MODEL_H
