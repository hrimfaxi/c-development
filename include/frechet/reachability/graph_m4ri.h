#ifndef GRAPH_H
#define GRAPH_H

#include <structure.h>
#include <graph_model.h>
#include <iostream>
#include <data/matrix_pool.h>

struct mzd_t;

namespace frechet { namespace reach {

//class GraphModel;
class Graph;
typedef boost::shared_ptr<Graph> GraphPtr;

/*	Factory methods
*/
GraphPtr newGraph(const GraphModel::ptr model);
GraphPtr newGraph(const GraphModel::ptr model, IndexRange hmask);
GraphPtr newGraph(const GraphModel::ptr model, Structure& str);

/**
 *  @brief Represents a Reachability Graph.
 *  Vertices correspond to intervals in the reachability structure,
 *  broken down to the most fine-grained intervals.
 *
 *  Mapping from reachability intervals to graph nodes
 *  is managed by GraphModel.
 *
 *  Graphs can be merged (transitive closure).
 *
 *  Memory layout of adjacancy matrixes is explained below.
 *  Boolean matrixes are modelled by M4RI.
 *
 *  @author Peter Schäfer
 */
class Graph
{
public:
	typedef boost::shared_ptr<Graph> ptr;
protected:
    //!  @brief adjacency matrix (M4RI structure)
    //!  split into four parts to allow for memory savings.
    mzd_t* mtx[2][2];

    /** RG(i,j) describes only a curve segment of P (and possibly all of Q).
     *  Adjacacency matrices are somewhat sparse, like this:
     *
     @verbatim
             +--+            +-----------------------
             |//|            |//////////////////////
             +--+            +-----------------------
                             |
                             |
     --------+--+------------+-----------------------
             |//|            | / / / / / / / / / / /
             |//|            | / / / / / / / / / / /
             |//|            | / / / / / / / / / / /
             |//|            | / / / / / / / / / / /
             |//|            | / / / / / / / / / / /
             |//|            | / / / / / / / / / / /
             |//|            | / / / / / / / / / / /
     @endverbatim
     *
     *  RG(i,i+1) is only about 25% dense. Later, RG(i,j) become denser.
     *
     *  Merge operations (transitive closure) are ony between adjacent ranges of (i,j) - (j,h)
     *
     *  We can save storage space, multiplication must be adjusted accordingly
     */
    IndexRange mask[2];

    //! mapping from intervals -> graph nodes
    //!  = rows and columns of the adjacancy matrix
    const GraphModel::ptr model;
    //!  result of call to searchDiagonalElement
    mutable int diagonalElement;

    //@deprecated friend class CGraph;
public:
    /**
     * @brief How was this graph constructed?
     * This info is useful for visualisation & debugging.
     * And for constructing a topologoically sorted list of tasks.
     * (see frechet::poly::AlgorithmTopoSort).
     */
    struct Origin {
        //!
        enum {
            PLACEMENT,  //!< this graph represents a set of valid placements
            RG,         //!< this is a rechability graph, constructed from a reachability structure
            MERGE2,     //!< this graph is the result of a MERGE operation (transitive closure) on two graphs
            MERGE3      //!< this graph is the result of a MERGE operation (transitive closure) on three graphs
        } operation;
        //!	bottom-level (for topologial sorting; leaves = blevel 0
		int blevel;
        //!	number of successors
		int succesors;
        //! @brief if this is a reachability graph: column range in free-space.
        //! this = RG(i,j), or CRG(i,j)
        int i,j;
        //! @{
        //! @brief if this graph was constructed as transitive closure: original graphs
        //! this = MERGE(A,B)
        Graph::ptr A,B;
        //! @}
        //! @{
        //!	@brief valid placement applied to the graph
		Graph::ptr P;
        //! @}
    } origin;

protected:	
    /**
     * @brief static, empty constructor
     * @param model maps intervals to nodes
     * @return a newly created, empty graph
     */
	friend GraphPtr frechet::reach::newGraph(const GraphModel::ptr model);
    /**
     * @brief static constructor with column range
     * @param model maps intervals to nodes
     * @param hmask column range covered by the graph
     * @return a newly created, empty graph
     */
	friend GraphPtr frechet::reach::newGraph(const GraphModel::ptr model, IndexRange hmask);
    /**
     * @brief static constructor from reachability structure
     * @param model maps intervals to nodes
     * @param str a reachability structure
     * @return  a newly created graph that represents the reachability structure
     */
	friend GraphPtr frechet::reach::newGraph(const GraphModel::ptr model, Structure& str);

    /**
     * @brief empty constructor
     * @param amodel maps intervals to nodes
     */
    Graph(const GraphModel::ptr amodel);
    /**
     * @brief constructor with column range
     * @param amodel maps intervals to nodes
     * @param range column range covered by the graph
     */
    Graph(const GraphModel::ptr amodel, IndexRange range);
    /**
     * @brief constructor from reachability structure
     * @param amodel maps intervals to nodes
     * @param str a reachability structure
     */
    Graph(const GraphModel::ptr amodel, Structure& str);
public:
    /**
     * @brief copy constructor
     * @param that graph to copy from
     */
    Graph(const Graph& that);
    /**
     * @brief move constructor
     * @param that graph to copy from, empty on return
     */
    Graph(Graph&& that);

public:
    /**
     * @brief destructor, releases memory
     */
	virtual ~Graph();
	
    /**
     * @brief allocate data for adjacenty matrixes (all four parts)
     */
	void allocateAll();
    /**
     * @brief allocate an adjacancy sub-matrix (one of four parts)
     * @param o1 horizontal part
     * @param o2 vertical part
     * @return pointer to a M4RI boolean matrix
     */
    mzd_t* allocate(Orientation o1, Orientation o2);

    /**
     * @brief horizontal range covered by this graph
     * @return range of nodes that is covered by this graph
     */
    const IndexRange& hmask() const { return mask[HORIZONTAL]; }
    /**
     * @brief range covered by this graph
     * @param o1 horizontal part
     * @param o2 vertical part
     * @return range of nodes that is covered by this one part of the adjacancy matrix
     */
    Rect rect(Orientation o1, Orientation o2) const;
    /**
     * @return model that maps reachability intervals for graph nodes
     */
    const GraphModel::ptr graphModel() const { return model; }

    /**
     * @brief this graph represents a set of valid placements
     * @param di first diagonal endpoint
     * @param dj second diagonal endpoint
     */
    void setOriginPlacement(int di, int dj);
    /**
     * @brief this graph represents a reachability graph,
     * it was created from a reachability structure
     * @param i free-space column
     */
    void setOriginRG(int i);
    /**
     * @brief this graph is the transitive closure of two other graphs.
     * It is the result of a MERGE operation.
     * @param A first successor
     * @param B seocnd succssor
     */
    void setOriginMerge2(Graph::ptr A, Graph::ptr B);
    /**
     * @brief this graph is the transitive closure of two other graphs.
     * It is the result of a final MERGE operation.
     * @param A first successor
     * @param B seocnd succssor
     */
    void setOriginMerge3(Graph::ptr A, Graph::ptr B);
    /**
     * @brief a sub-set of valid placements is applied to the graph
     * @param P a set of valid placements
     */
	void setOriginCombine(Graph::ptr P);
    /**
     * @brief debug output
     */
    void printOrigin(std::ostream& out) const;

    /**
     * @brief assigment operator
     * @param that graph to copy from
     * @return reference to this, after assignment
     */
    Graph& operator= (const Graph& that);
    /**
     * @brief move assigment operator
     * @param that graph to copy from; empty on return
     * @return reference to this, after assignment
     */
    Graph& operator= (Graph&& that);
    /**
     * @brief comparison operator
     * @param that graph to compare with
     * @return true if both graphs are equal
     */
    bool operator== (const Graph& that) const;

    /**
     * @name Edit and Query operations
     * @{ */
    /**
     * @brief add an edge to the graph
     * @param ori_from source orientention (horizontal or vertical)
     * @param i_from source node (=column or row)
     * @param ori_to destination orientation (horizontal or vertical)
     * @param i_to destination node (=column or row)
     */
    void add_edge(Orientation ori_from, int i_from,
                  Orientation ori_to, int i_to);
    /**
     * @brief add a set of edges to the graph,
     *  covering a range of nodes
     * @param r_from source nodes
     * @param r_to destination nodes
     */
    void add_range(IndexRange r_from, IndexRange r_to);
    //void add_range(IndexRange r_form, IndexRangePair r_to);
    /**
     * @brief check if an edge is present
     * @param ori_from source orientention (horizontal or vertical)
     * @param i_from source node (=column or row)
     * @param ori_to destination orientation (horizontal or vertical)
     * @param i_to destination node (=column or row)
     * @return true, if the edge is present
     */
    bool contains_edge(Orientation ori_from, int i_from,
                       Orientation ori_to, int i_to) const;
    /**
     * @brief check a range of edges
     * @param r_from source nodes
     * @param r_to destination nodes
     * @return true, if at least one is edge is present in the queries range
     */
    bool contains_edge(IndexRange r_from, IndexRange r_to) const;
    /**
     * @brief empty test for one of the four sub-graphs
     * @param o1 source orientention (horizontal or vertical)
     * @param o2 destination orientation (horizontal or vertical)
     * @return true if the sub-graph is missing
     */
    bool empty(Orientation o1, Orientation o2) const;
    /**
     * @brief empty test for one of the four sub-graphs
     * @param o1 source orientention (horizontal or vertical)
     * @param o2 destination orientation (horizontal or vertical)
     * @return true if the sub-graph is missing, or empty
     */
    bool zero(Orientation o1, Orientation o2) const;
    /**
     * @brief release memory that is not neeeded (empty sub-graphs)
     */
	virtual void finalize();
    /**
     * @brief release memory that is not neeeded (empty sub-graphs)
     */
    void releaseIfZero();

    /**
     * @brief release memory for all parts of the adjacancy matrix
     */
    virtual void release();
    /**
     * @brief release memory for one part of the adjacancy matrix
     * @param o1 horizontal part
     * @param o2 vertical part
     */
    virtual void release(Orientation o1, Orientation o2);
    /**
     * @brief release memory for one part of the adjacancy matrix
     * @param o1 horizontal part
     * @param o2 vertical part
     * @param pool a pool of re-cycled matrix data
     */
    virtual void release(Orientation o1, Orientation o2, MatrixPool* pool);

    /** @} */

    /**
     * @name Iterator methods
     * @{ */
    /**
     * @brief find the next edge in a sub-graph
     * @param o1 horizontal part
     * @param i start column
     * @param o2 vertical part
     * @param j start row
     * @return the next edge in the sub-graph, or INT_MAX if there is none
     */
    int find_next1(Orientation o1, int i, Orientation o2, int j) const;
    /**
     * @brief find the next missing edge in a sub-graph
     * @param o1 horizontal part
     * @param i start column
     * @param o2 vertical part
     * @param j start row
     * @return the next missing edge in the sub-graph, or INT_MAX if there is none
     */
    int find_next0(Orientation o1, int i, Orientation o2, int j) const;
    /**
     * @brief find a range of consecutive edges in a sub-graph
     * @param o1 horizontal part
     * @param i start column
     * @param o2 vertical part
     * @param j start row
     * @return next consecutive range of edges, or an empty range if there are none
     */
    IndexRange find_next_range(Orientation o1, int i, Orientation o2, int j) const;

    /** @} */

    /**
     * @name Bulk operations
     * @{ */
    /**
     * @brief apply the COMBINE operation, filtering edges with valid placements.
     * Effectively performs a Boolean AND oepration: (*this)_VV &= that_VV
     * @param P a set of valid placements
     */
    virtual void combine (const Graph* P);

    /**
     * @brief apply the MERGE operation, computing the transitive closure of two graphs.
     * Effectively performs a matrix multiplication: (*result)_VV = (*this)_VV * that_VV
     * @param B another graph
     * @param pool pool of re-cycled matrixes (optional)
     * @return a newly created graph representing the result of the MERGE operation
     */
	Graph::ptr merge2(const Graph* B, MatrixPool* pool=nullptr) const;
	//  this_VV := A_VV * B_VV (@see setOrigin)
    /**
     * @brief apply the MERGE operation, computing the transitive closure of two graphs.
     * Effectively performs a matrix multiplication: (*this)_VV = *A_VV * *B_VV
     * @param A a graph
     * @param B another graph
     * @param pool pool of re-cycled matrixes (optional)
     */
    virtual void merge2(const Graph* A, const Graph* B, MatrixPool* pool=nullptr);

    /**
     * @brief apply the final MERGE operation, computing the transitive closure of two graphs.
     * Effectively performs a matrix multiplication: (*result)_HH =  (*this)_HV * that_VV * (*this)_VH
     * @param B another graph
     * @param pool
     * @return a newly created graph representing the result of the MERGE operation
     */
    Graph::ptr merge3(const Graph* B, MatrixPool* pool=nullptr) const;
    /**
     * @brief apply the final MERGE operation, computing the transitive closure of two graphs.
     * Effectively performs a matrix multiplication: this_HH = A_HV * B_VV * A_VH
     * @param A a graph
     * @param B another graph
     * @param pool pool of re-cycled matrixes (optional)
     */
    virtual void merge3(const Graph* A, const Graph* B, MatrixPool* pool=nullptr);
    /**
     * @brief find an edge on the diagonal of the adjacancy matrix. Does not return a result.
     * To query the result of this method, call foundDiagonalElement.
     * (reason: we also want to perform this query asynchronously on a GPU).
     */
    virtual void queryDiagonalElement() const;
    /**
     * @return the result of the last call to queryDiagonalElement.
     * Or -1 if the diagonal is empty.
     */
    virtual int foundDiagonalElement() const;

    /** @} */

    /**
     * @name stubs for derived classes
     * @{ */
    //! @brief implemented by GraphCL; copy matrix data to GPU memory
    virtual void synchToGpu() { /*no-op. implemented by GraphCL */ }
    //! @brief implemented by GraphCL; copy matrix data backt to CPU memory
    virtual void synchFromGpu() { /*no-op. implemented by GraphCL */ }
    //! @brief implemented by GraphCL
    virtual void resetConditions() { /*no-op. implemented by GraphCL */ }
    /** @} */

    /**
     * @name Memory Diaganotics
     * @{ */
    /**
     * @brief compute the memory footprint of an adjacancy matrix
     * @param ori1 horizontal part
     * @param ori2 vertical part
     * @return memory, in Bytes, used by a (sub-)adjacancy matrix
     */
    double memory(Orientation ori1, Orientation ori2) const;
    /**
     * @brief compute the density (amount of set edges) of an adjacancy matrix
     * @param ori1 horizontal part
     * @param ori2 vertical part
     * @return relative density
     */
    double density(Orientation ori1, Orientation ori2) const;

    /**
     * @return the memory footprint for all adjacancy matrixes
     */
    double memory() const;
    /**
     * @return the density (amount of set edges) of all adjacancy matrixes
     */
    double density() const;

    /** @} */

    /**
     * @name Debugging
     * @{ */
    /**
     * @brief remove all edges
     */
    void clear();
    /**
     * @brief remove all edges of a sub-graph
     * @param ori1 horizontal part
     * @param ori2 vertical part
     */
    void clear(Orientation ori1, Orientation ori2);
    /**
     * @param ori1 horizontal part
     * @param ori2 vertical part
     * @return true if the sub-adjancy matrix is an upper triangular matrix
     */
    bool is_upper_triangular(Orientation ori1, Orientation ori2) const;
    /**
     * @param mzd a boolean matrix (in M4RI format)
     * @return true if the matrix is an upper triangular matrix
     */
    static bool is_upper_triangular(const mzd_t* mzd);
    /**
     * @brief print a sub-adjacancy matrix
     * @param out output stream
     * @param o1 horizontal part
     * @param o2 vertical part
     */
    void print(std::ostream& out, Orientation o1, Orientation o2) const;
    /** @} */

protected:
    /**
     * @brief copy graph
     * @param that graph to copy from
     */
    void copy(const Graph& that);
    /**
     * @brief swap graph
     * @param that grap to swap with; holds *this on return
     */
    void swap(Graph& that);

    /**
     * @brief copy reachability info from Structure to Graph
     * @param str a reachability structure
     * @param ori horizontal or vertical
     */
    void copy(const Structure& str, Orientation ori);
    /**
     * @brief print a Boolean matrix
     * @param out output stream
     * @param M a boolean matrix (in M4RI format)
     */
    static void print(std::ostream& out, const mzd_t* M);

public:
    /**
     * @param A a graph
     * @param B another graph
     * @return true if the column ranges of both graphs are adjacent
     */
    static bool is_adjacent_to(const Graph& A, const Graph& B);

    /**
     * @name for unit tests
     * @{
     */
    /**
     * @brief set some bits in a Boolean matrix
     * @param M a Boolean matrix in M4RI format
     * @param row matrix row
     * @param i first column
     * @param j last column (exclusive)
     */
    static void set_bits(mzd_t* M, int row, int i, int j);
    /**
     * @brief query bits in a Boolean matrix
     * @param M a Boolean matrix in M4RI format
     * @param row matrix row
     * @param i first column
     * @param j last column (exclusive)
     * @return true if at least one bit is set
     */
    static bool has_bits(mzd_t* M, int row, int i, int j);
    /**
     * @brief clear a row in a Boolean matrix
     * @param M a Boolean matrix in M4RI format
     * @param row matrix row
     */
    static void clear_row(mzd_t* M, int row);
    /**
     * @brief apply bitwise and of two matrix rows
     * @param M source Boolean matrix in M4RI format
     * @param r matrix row
     * @param M2 a Boolean matrix in M4RI format
     * @param r2 matrix row
     */
    static void and_row(mzd_t* M, int r, mzd_t* M2, int r2);

    /**
     * @brief find next set bit in a Boolean matrix
     * @param M a Boolean matrix in M4RI format
     * @param row matrix row
     * @param i column
     * @return column of next set bit in row, or INT_MAX if there is none
     */
    static int find_next1(mzd_t* M, int row, int i);
    /**
     * @brief find next cleared bit in a Boolean matrix
     * @param M a Boolean matrix in M4RI format
     * @param row matrix row
     * @param i column
     * @return column of next cleared bit in row, or INT_MAX if there is none
     */
    static int find_next0(mzd_t* M, int row, int i);
    /** @} */
};



} }     //  namespace frecht::reach

#endif // GRAPH_H
