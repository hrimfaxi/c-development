#ifndef STRUCTURE_H
#define STRUCTURE_H

#include <boundary.h>
#include <freespace.h>
#include <tbb/tbb.h>

namespace frechet {
using namespace free;

namespace reach {

class StructureIterator;

/**
 *  @brief The Reachability Structure;
 *  maintains a list of intervals on the border
 *  of Free Space, along with pointers to reachable
 *  intervals on the opposite side.
 *
 *  Allows to query for feasible paths in O(1).
 *
 *  Implements the Divide-and-Conquer algorithm for calculating
 *  the Reachability Structure recursively.
 *
 *  Interval structure is identical for Left and Right,
 *  respectively Bottom and Top.
 *
 *  @see [alt95]
 *  @author Peter Schäfer
 */
class Structure {

private:    
    //! the underlying free-space; may be nullptr
    const FreeSpace::ptr fs;
    //!  Boundaries [HORIZONTAL,VERTICAL] [FIRST,SECOND]
    BoundaryList _boundary[2][2];
    //! number of parallel threads to use
    int concurrency;
    //! used when running ia a background thread to indicate cancellation by the user
    volatile bool* cancelFlag;

    friend class StructureIterator;
    friend class GraphModel;
    friend class CalculateTask;
    friend class MergeTask;
public:
    //! smart pointer to a Structure object
    typedef boost::shared_ptr<Structure> ptr;

    /**
     * @brief empty constructor
     * @param concurrency number of threads for parallel execution
     * @param cancelFlag indicates user interruption
     */
    Structure(int concurrency=1, volatile bool* cancelFlag=nullptr);
    /**
     * @brief default constructor
     * @param fs underlying free-space
     * @param concurrency number of threads for parallel execution
     * @param cancelFlag indicates user interruption
     */
    Structure(const FreeSpace::ptr fs, int concurrency=1, volatile bool* cancelFlag=nullptr);

    /**
     * @brief copy constructor
     * @param that object to copy from
     */
    Structure(const Structure& that);
    /**
     * @brief move constructor
     * @param that object to move data from; empty on return
     */
    Structure(Structure&& that);

    /**
     * @brief assigment operator
     * @param that object to copy from
     * @return reference to this object, after assignment
     */
    Structure& operator= (const Structure& that);
    /**
     * @brief move assigment operator
     * @param that object to move data from; empty on return
     * @return reference to this object, after assignment
     */
    Structure& operator= (Structure&& that);

    /**
     * @brief assigment function
     * @param that  object to copy from
     */
    void copy(const Structure& that);
    /**
     * @brief swap content with another object
     * @param that to swap data with; on return, holds contents of this object
     */
    void swap(Structure& that);
    /**
     * @brief destructor; releases all memory
     */
    virtual ~Structure();

    //! @return pointer to the underlying free-space
    const FreeSpace::ptr freeSpace() const { return fs; }

    /**
     * @param ori horizontal or vertical
     * @param dir first(=bottom,left) or second (=top,right)
     * @return one of the four boundaries of the reachability structure
     */
    BoundaryList& boundary(Orientation ori, Direction dir) {
        return _boundary[ori][dir];
    }

    /**
     * @param ori horizontal or vertical
     * @param dir first(=bottom,left) or second (=top,right)
     * @return immutable reference to one of the four boundaries of the reachability structure
     */
    const BoundaryList& boundary(Orientation ori, Direction dir) const {
        return _boundary[ori][dir];
    }
    /**
     * @param ori horizontal or vertical
     * @return bottom or left boundary of the reachability structure
     */
    BoundaryList& first(Orientation ori)    { return _boundary[ori][FIRST]; }
    /**
     * @param ori horizontal or vertical
     * @return top or right boundary of the reachability structure
     */
    BoundaryList& second(Orientation ori)   { return _boundary[ori][SECOND]; }

    /**
     * @param ori horizontal or vertical
     * @return immutable reference to bottom or left boundary of the reachability structure
     */
    const BoundaryList& first(Orientation ori) const    { return _boundary[ori][FIRST]; }
    /**
     * @param ori horizontal or vertical
     * @return mmutable reference to top or right boundary of the reachability structure
     */
    const BoundaryList& second(Orientation ori) const   { return _boundary[ori][SECOND]; }

    /**
     * @brief first or second part of the reachability structure
     * @param dir first=bottom, second=top
     * @return bottom or top boundary of the reachability structure
     */
    BoundaryList& horizontal(Direction dir) { return _boundary[HORIZONTAL][dir]; }
    /**
     * @brief first or second part of the reachability structure
     * @param dir first=left, second=right
     * @return left or right boundary of the reachability structure
     */
    BoundaryList& vertical(Direction dir) { return _boundary[VERTICAL][dir]; }

    //! @return left boundary of the reachability structure
    BoundaryList& left()        { return _boundary[VERTICAL][FIRST]; }
    //! @return right boundary of the reachability structure
    BoundaryList& right()       { return _boundary[VERTICAL][SECOND]; }
    //! @return bottom boundary of the reachability structure
    BoundaryList& bottom()      { return _boundary[HORIZONTAL][FIRST]; }
    //! @return top boundary of the reachability structure
    BoundaryList& top()         { return _boundary[HORIZONTAL][SECOND]; }

    //! @return immutable reference to left boundary of the reachability structure
    const BoundaryList& left() const       { return _boundary[VERTICAL][FIRST]; }
    //! @return immutable reference to right boundary of the reachability structure
    const BoundaryList& right() const      { return _boundary[VERTICAL][SECOND]; }
    //! @return immutable reference to bottom boundary of the reachability structure
    const BoundaryList& bottom() const     { return _boundary[HORIZONTAL][FIRST]; }
    //! @return immutable reference to top boundary of the reachability structure
    const BoundaryList& top() const        { return _boundary[HORIZONTAL][SECOND]; }

    /**
     * @brief compute the reachability structure from the underlying free-space diagram.
     * Uses a divide-and-conquere algorithms that recursively merges reachability structures.
     * Uses parallel threads, optionally.
     * If there is a feasible path, find the starting interval of the path.
     * Works for both, closed and open curves.
     * @return starting interval of feasible path, of nullptr if there is none
     */
    Pointer calculate();

    /**
     * @brief compute the reachability structure for a closed curve,
     * based on a double-free-space-diagram.
     * @return starting interval of feasible path, of nullptr if there is none
     */
    Pointer calculateDouble();

    /**
     * @brief compute the reachability structure for an open curve,
     * based on a single free-space-diagram.
     * @return starting interval of feasible path, of nullptr if there is none
     */
    Pointer calculateSingle();

    /**
     * @brief compute a region of the reachability structure.
     *  may span the double free-space!.
     * @param i first column
     * @param j last column, exclusive
     */
    void calculateColumns(int i, int j);

    //
    /**
     * @brief create a BoundarySegment from one cell of the free-space-diagram
     * @param i column in free-space
     * @param j row in free-space
     */
    void singleCell(int i, int j);

    /**
     * @brief compute the reachability structure for a closed curve,
     * based on a double-free-space-diagram.
     * @param fringe duplicate reachability structure along the horizontal edge,
     * or the vertical edge
     */
    void calculateDouble(Orientation fringe);

    /**
     * @brief after the reachability structure is computed,
     * find the starting interval of a feasible path.
     * Works for closed and open curves.
     * @param edge look for starting point on horizontal edge,
     * of vertical edge
     * @return starting interval of feasible path, of nullptr if there is none
     */
    Pointer findStartingPoint(Orientation edge);

    /**
     * @brief shift the reachability structure by a given offset.
     * The shifted second copy is then merged to compute a double-reachability structure.
     * @param offset horizontal and vertical offset
     */
    void shift(Point offset);

    /**
     * @brief used when traversing reachability segments.
     * Traversal direction is discussed in frechet::reach::Direction.
     * @param p a pointer to a reachability segment
     * @return pointer to next neighbor, or null
     */
    Pointer next(Pointer p) const;
    /**
     * @brief used when traversing reachability segments.
     * Traversal direction is discussed in frechet::reach::Direction.
     * @param p a pointer to a reachability segment
     * @return pointer to previous neighbor, or null
     */
    Pointer prev(Pointer p) const;
    /**
     * @brief before mergin two reachabiliry structures,
     * its intervals must be synchronized
     * @param fringe synchronize along the horizontal edge,
     * or along the verical edge
     * @param that reachability structure to synchronize with
     */
    void synchIntervals(Orientation fringe, Structure& that);

private:
    /**
     * @brief find previous reachable segment
     * @param p a pointer to a reachability segment
     * @return previous segment in traversal order that is REACHABLE
     */
    Pointer prevReachable(Pointer p);
    /**
     * @brief find next reachable segment
     * @param p a pointer to a reachability segment
     * @return next segment in traversal order that is REACHABLE
     */
    Pointer nextReachable(Pointer p);

    /**
     * @brief Divide & Conquer algorithm. Merge regions recursively.
     * Makes use of paralel threads.
     * @param r region to merge (columns,rows in free-space)
     */
    void calcRecursive(const Rect& r);
    /**
     * @brief Merge regions recursively.
     * Uses only one thread.
     * @param r region to merge (columns,rows in free-space)
     */
    void doCalcRecursive(const Rect& r);

    //  Structure Merging
    /**
     * @brief merge this structure with another structure
     * @param fringe merge along the horizontal edge,
     * or along the vertical edge?
     * @param that structure to merge with
     * @param r1 region of this structue (columns,rows in free-space)
     * @param r2 region of that structure (columns,rows in free-space)
     */
    void merge(Orientation fringe, Structure& that,
               const Rect& r1, const Rect& r2);
    //  buildings blocks of the merge process:
    /**
     * @brief split a reachabiliy segment
     * Adjust l,h pointers into that segment.
     * @param seg segment to split
     * @param y point in interval where to split
     * @param twin pointer to opposite segment
     * (remains unmodified, but used to track incoming pointers)
     * @return pointer to the newly created segment, below the old one
     */
    Pointer split(Pointer seg, double y, Pointer twin);
    /**
     * @brief split a reachabiliy segment and its opposite segment
     * @param a segment to split
     * @param x point in interval where to split
     * @param b opposite segment to split
     */
    void split2(Pointer& a, double x, Pointer& b);
    /**
     * @brief set up links between four opposite segments.
     * This pointer chain will be used to traverse a reachability structure horizontally.
     * @param i1 segment on the outer left edge
     * @param i2 segment on the center left edge
     * @param j1 segment on the center right edge
     * @param j2 segment on the outer right edge
     */
    void crossLink(Pointer i1, Pointer i2, Pointer j1, Pointer j2);
    /**
     * @brief merge two adjacent segments.  b := a union b
     * @param list list containing both segment
     * @param a left neighbor; will be removed
     * @param b will be extended to hold a union b
     */
    void merge(BoundaryList& list, Pointer a, Pointer b);
    /**
     * @brief could be used to merge indentical intervals.
     * Reduces the number of intervals (and thus the complexity of the algorithm).
     * Causes some trouble later, with fine-grained segmentation in simple polygons algorithm.
     * @param ori horizontal or vertical
     * @deprecated not used
     */
    void mergeConsecutive(Orientation ori);
    /**
     * @brief scan and merge one segment
     * @param K pointer to a reachability segment, see [alt95]
     * @param fringe merge along horizonal edge, or vertical egde
     */
    void scanAndMerge (Pointer K, Orientation fringe);
    /**
     * @brief mark non-accesible segments
     * @param fringe along horizonal edge, or vertical egde
     * @param that structure to merge with
     */
    void markNonAccesible(Orientation fringe, Structure& that);
    /**
     * @brief mark one non-accesible segment
     * @param p pointer to a reachability segment
     * @param p2  pointer to a reachability segment
     */
    void markNonAccesible(Pointer p, Pointer p2);
    /**
     * @brief update all l,h, pointers that point into a segment
     * @param K  target segment
     * @param l  start of range of incoming segments
     * @param h  end of range of incoming segments
     * @param K1 opposite to K
     */
    void updatePointers(Pointer K, Pointer l, Pointer h, Pointer K1);


    //! @brief aux. data structure that is used to construct an initial reachability cell
    struct SingleCellAuxData {
        //!  Free-Space edges
        data::Interval F[2][2];           //  [Orientation][Direction]
        //!  F arrangements
        int arr[2];                 //  [Orientation]
        //!  reachability rectangle segments
        Pointer seg[2][2][5];       //  [Orientation][Direction][0..4]
        //!  min/max for each rectangle edge
        PointerInterval P[2][2];    //  [Orientation][Direction]
    };

public:
    /**
     * @brief compute the arrangement of free-space intervals for one cell
     * @param LF left free-space interval
     * @param RF right free-space interval
     * @return a value in [0..15], encoding an arrangement
     */
    static int freeSpaceArrangement(data::Interval LF, data::Interval RF);
private:
    /**
     * @brief compute the arrangement of free-space intervals for one cell
     * @param aux aux. data
     * @param ori horizontal or vertical intervals?
     * @return a value in [0..15], encoding an arrangement
     */
    static int freeSpaceArrangement(SingleCellAuxData& aux, Orientation ori);
    /**
     * @brief create segments for a single cell of the reachability structure
     * @param aux aux. data
     * @param ori  horizontal or vertical intervals?
     * @param y0 lower bound
     */
    void createSingleCellSegments(SingleCellAuxData& aux, Orientation ori, double y0);
    /**
     * @brief set up the l,h pointers within a single cell
     * @param aux  aux. data
     * @param ori horizontal or vertical intervals?
     */
    void linkSingleCellSegments(SingleCellAuxData& aux, Orientation ori);
    /**
     * @brief adjust the see-through pointers.
     * For see-through segments l:=nullptr, resp. h:=nullptr
     * @param p points to a reachability segment
     */
    void clipSeeThroughPointer(Pointer p);
    /**
     * @brief find a reachable interval
     * @param seg list of segments on the opposite edge
     * @return an interval of reachable segments
     */
    PointerInterval reachableInterval(Pointer seg[5]);
    /**
     * @brief create a pair of reachability segments, on opposite sides
     * of the structure
     * @param first holds the first segment on return
     * @param second holds the second segment on return
     * @param ival free-space interval, identical to both segments
     * @param ori horizontal or vertical
     * @param t1 reachability label for first segment
     * @param t2 reachability label for second segment
     */
    void create2Segments(Pointer& first, Pointer& second, data::Interval ival, Orientation ori, Type t1, Type t2);
    /**
     * @brief construct part of a single-sell segment
     * @param list list to append
     * @param seg boundary segments that will be assembled into the list
     */
    void copySegments(BoundaryList& list, Pointer seg[5]);
    /**
     * @brief check for user interrupion
     * The canceFlag should be polled in regular intervals.
     * If the user requested an interruption, throw an
     * exception to abort current computation.
     * @throw InterruptedException thrown when the user
     * requests an interruption. Caught by application logic.
     */
    void testCancelFlag();

    /**
     * @brief for debugging and unit tests, check if reachability intervals are consistent
     * @return true if intervals are consistent
     */
    static bool assertSynchedIntervals(Pointer i1, Pointer i2, Pointer i3, Pointer i4);

public:
    /**
     * @brief for debugging and unit tests, check if reachability intervals are consistent
     * @return true if intervals are consistent
     */
    static bool assertPointerInterval(Pointer p, const PointerInterval& ival);

    //! @brief  hooks for Unit Tests. Only used in debug mode.
    typedef void (*assert_hook) (
            Structure*, const Rect*,
            Structure*, const Rect*,
            Orientation);

    static assert_hook after_single_cell, before_merge, after_merge;
    //! @brief for debugging: fill in owner pointers
    void takeOwnership();
};

/**
 * @brief The StructureIterator class
 *
 * Iterates segments on the border of the reachability structure.
 * An interval can be passed. If the interval spans two edges (bottom->left, or right->top)
 * the iterator takes the bend at the corner.
 *
 * @see Direction
 * @author Peter Schäfer
 */
class StructureIterator
{
private:
    //! the associated reachability structure
    const Structure& str;
    //! current location of the iterator
    Pointer current;
    //! last segment
    Pointer last;

public:
    /**
     * @brief constructor with two segments
     * @param str the associated reachability structure
     * @param first first segment (inclusive)
     * @param last last segment (inclusive)
     */
    StructureIterator(const Structure& str, Pointer first, Pointer last);
    /**
     * @brief constructor with interval of segments
     * @param str the associated reachability structure
     * @param ival interval
     * @param ifnull fallback, if one of the interval pointers is null
     * (which is often the case with see-through segments)
     */
    StructureIterator(const Structure& str,
                      const PointerInterval& ival, Pointer ifnull=nullptr);

    /**
     * @brief true if the iterator is pointing to a valid interval;
     * false if the iterator has gone off the range
     */
    operator bool() const { return current; }
    /**
     * @return  true, if the iterator has gone off the range
     */
    bool operator!() const { return !current; }

    /**
     * @brief pointer to current interval
     */
    explicit operator Pointer() { return current; }
    /**
     * @return reference to current interval;
     * (undefined if the iterator is not valid)
     */
    BoundarySegment& operator *() { return *current; }
    /**
     * @return pointer to current interval
     */
    BoundarySegment* operator ->() { return current; }
    /**
     * @brief pre-increment; advance the iterator
     * @return reference to this iterator, after being advanced
     */
    StructureIterator& operator++();
};

/**
 * @brief task object for parallel execution.
 * When the reachability structur is computed in parallel threads,
 * we create a tree of task objects that are distributed over threads.
 *
 * Workflow and dependencies between tasks are managed by a tbb::flow::graph.
 */
class StructureTask
{
public:
    /**
     * @brief constructor with Structure
     * @param owner the underlying reachability structure
     */
    StructureTask(Structure* owner);
    /**
     * @brief destructor
     */
    virtual ~StructureTask();

    /**
     * @brief abstract method for starting a task
     */
    virtual void start()=0;
    /**
     * @brief static constructor
     * @param owner the underlying reachability structure
     * @param r bounding region (columns and rows in free-space)
     * @param graph tbb flow-graph modeling tasks and dependencies
     * @param concurrency number of threads to use
     * @return a newly created task, with all dependecies
     */
    static StructureTask* createTask(Structure* owner, const frechet::Rect& r, tbb::flow::graph& graph, int concurrency);

    //! @brief used to connect nodes in the dependency graph
    typedef tbb::flow::continue_msg msg_t;
    //! @brief a node in the dependency graph
    typedef tbb::flow::continue_node<msg_t> node_t;

    //! the underlying reachability structure
    Structure* owner;
    //! a node in the dependency graph
    node_t* node;
};

/**
 * @brief a sub-task, computing a region of the reachability structure
 */
class CalculateTask: public StructureTask {
public:
    /**
     * @brief default constructor
     * @param owner the underlying reachability structure
     * @param r bounding region (columns and rows in free-space)
     * @param graph tbb flow-graph modeling tasks and dependencies
     */
    CalculateTask(Structure* owner, frechet::Rect r, tbb::flow::graph& graph);
    /**
     * @brief computes the reachability structure for the given region
     */
    virtual void start() override;
};

/**
 * @brief a sub-taks, performing the merging of two reachability structures
 */
class MergeTask: public StructureTask {
private:
    //! structure to merge with
    Structure buddy;
    //! prerquisite task; only after child1 and child2 are finished, start the merging task
    StructureTask *child1;
    //! prerquisite task; only after child1 and child2 are finished, start the merging task
    StructureTask *child2;
public:
    /**
     * @brief default constructor
     * @param owner  the underlying reachability structure
     * @param r bounding region (columns and rows in free-space)
     * @param graph tbb flow-graph modeling tasks and dependencies
     * @param concurrency number of threads to use
     */
    MergeTask(Structure* owner, frechet::Rect r, tbb::flow::graph& graph, int concurrency);
    /**
     * @brief destructor; releases memory
     */
    virtual ~MergeTask();
    /**
     * @brief perform the merging
     */
    virtual void start() override;
};

/**
 * @brief print operator used for debugging; prints diagnostic
 * info about a reachability structure
 * @param out output stream
 * @param str reachability structur
 * @return same as out
 */
std::ostream& operator<< (std::ostream& out, const Structure& str);

} }     //  namespace frechet::reach
#endif // STRUCTURE_H
