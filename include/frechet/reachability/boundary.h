#ifndef BOUNDARY_H
#define BOUNDARY_H

#include <interval.h>
#include <linkedlist.h>

namespace frechet { namespace reach {

/**
 * @brief Segment Types
 *
 * each segment of a reachability structure is labelled
 * with its reachability status
 *
 * @see [alt95]
 */
enum Type : uint8_t {
    //!  non-accesible. no interval is reachable from this one
    NON_ACCESSIBLE,
    //!  reachable: other intervals are reachable
    REACHABLE,
    //!  see-through: other interals are visible with a straight line
    SEE_THROUGH
};

/**
 * @brief Segment Orientation
 *
 * some segments are horizontal, others are vertical.
 */
enum Orientation : uint8_t {
    HORIZONTAL=0, VERTICAL=1
};

/**
 * @param ori an orientation
 * @return the opposite to the given orientation
 */
inline Orientation opposite(Orientation ori) {
    return (Orientation)(1-ori);
}
/**
 * @brief pre-increment an orientation variable.
 * Note that an orientation variable can only
 * be incremented once from HORIZONTAL to VERTICAL.
 * Incrementing VERTICAL causes an undefined result.
 * @param ori an orientation
 * @return reference after incrementing
 */
inline Orientation& operator++ (Orientation& ori) {
    ori = (Orientation)(ori+1);
    return ori;
}

class BoundarySegment;

/**
 * @brief (dumb) pointer to a BoundarySegment object
 */
typedef BoundarySegment* Pointer;


/**
  @brief direction of a Pointer inside the reachability structure.

  Direction describes three (related) concepts:
  -#   the two parts of a reachability cell:
        bottom-left segement and right-top segment
  -#   the direction of an l,h pointer
        forward pointer = pointing from bottom-left INTO right-top
        backward pointer pointer from right-top segment BACK INTO bottom-left
  -#   the orientation of an l,h interval, or traversal sequence of segments
        - counter-clockwise: start at lower right corner, traverse right edge bottom-up, bend at top-right corner, traverse top edge from right to left, finish at top-left corner.
        - clockwise: start at lower right corner, traverse bottom edge from right to left, bend and lower left corner, then left edge bottom up,  finish at top-left corner.

        As you can see, vertical edges are traversed bottom-up. Horizontal edges are traversed left-to-right.

    Each BoundarySegment knows its Direction. The l,h pointers of a segement
    always point into the opposite segment.

@verbatim
   end            <-- TOP = part of SECOND segment
   traversal      <-- traverse COUNTER-CLOCKWISE
       O-----------+-----------+-----------+-----------+-----------+
       |               /                                           |
       |              /                                           ^|
       +             /                                           / |
       |            /                                          /   |
       |           / BACKWARD pointer                        /     |
   ^   +          /                                        /       +  ^
   |   |         /                                       /         |  | traverse COUNTER-CLOCKWISE
       |        /                                      /           |
  LEFT +       /                                     /             + RIGHT
 =FIRST|      /                                    /               | = part of SECOND segment
       |     /                                   /                 |
       +    /                                  /                   +
       |   /                                 /                     |
       |  /                                /                       |
       + v                               /                         +
       |                               /                           |
       |                             /                             |
       +                           /   FORWARD pointer             +
       |                         /                                 |
       |                       /                                   |
       +                     /                                     +
       |                   /                                       |
       |                 /                                         |
       +-----------+-----------+-----------+-----------+-----------O start traversal

                     <--  BOTTOM = part of FIRST segment
                     <--  traverse CLOCKWISE

@endverbatim
 */
enum Direction : uint8_t {
    // Two Segments
    FIRST=0,      //!< first segment = bottom and left edge
    SECOND=1,     //!< second segment = right and top edge

    //  Rectangle sections
    BOTTOM_LEFT = FIRST, //!< bottom and left part of the reachability structure
    RIGHT_TOP = SECOND,  //!< right and top part of the reachability structure

    //  l,h pointer direction
    FORWARD = RIGHT_TOP,      //!<  forward l,h pointer = from first INTO second segment
    BACKWARD = BOTTOM_LEFT,   //!<  backward l,h pointer = from second INTO first segment

    //  interval orientation
    //!  counter-clockwise interval (right-then-top)
    COUNTER_CLOCKWISE = RIGHT_TOP,
    //!  clockwise interval (bottom-then-left)
    CLOCKWISE = BOTTOM_LEFT
};

/**
 * @param dir a Direction
 * @return the opposite direction
 */
inline Direction opposite(Direction dir) {
    return (Direction)(1-dir);
}
/**
 * @brief pre-increment a direction variable.
 * Note that a direction variable can only
 * be incremented once from FIRST to SECOND.
 * Incrementing SECOND causes an undefined result.
 * @param dir a direction
 * @return reference after incrementing
 */
inline Direction& operator++ (Direction& dir) {
    dir = (Direction)(dir+1);
    return dir;
}

/**
 * @brief describes l,h pointers.
 * Both pointers are assumed to point into the same rectangle section (right-top OR bottom-left)
 * @see [alt95]
 */
struct PointerInterval {    
  Pointer l;    //!< points to the lowest segment of the interval
  Pointer h;    //!< points to the highest segment of the interval

  //! @brief empty constructor;
  //! constructs an invalid interval, with both pointers being nullptr
  PointerInterval();
  //! @brief default constructor
  //! @param a lowest segment
  //! @param b highest segment
  PointerInterval(Pointer a, Pointer b);

  //! @brief true if the interal is valid, i.e. both pointers are
  operator bool () const;
  //! @return  true if the interal is ibvalid, i.e. at least one pointers is
  bool operator! () const;

  //! @brief assigment
  //! @param p object to assign from
  //! @return this, after assigment
  PointerInterval& operator= (const Pointer& p);
  /**
   * @brief conditional swap
   * @param doit if true, replace l and h pointers
   * @return this, conditionally with swapped pointers
   */
  PointerInterval& swap(bool doit=true);
  /**
   * @return a PointerInterval(h,l) object with swapped pointers
   */
  PointerInterval swapped() const;

  /**
   * @brief containment test
   * @param p a pointer to a segment
   * @return true, if p points to a segment inbetween l and h
   */
  bool contains(Pointer p) const;

  //! @return a copy of this interval, with proper order of l,h pointers.
  //!   i.e. l <= h for vertical intervals and l >= h for horizontal intervals
  PointerInterval normalized() const;
  /**
   * @brief merge operator
   * @param that interval to merge with
   * @return a copy of this interval, merged with 'that'
   */
  PointerInterval operator+ (const PointerInterval& that) const;

  /**
   * @brief comparison operator
   * @param that interval to compare with
   * @return true if both pointers are equal
   */
  bool operator== (const PointerInterval& that) const {
      return (this->l==that.l) && (this->h==that.h);
  }
  /**
   * @brief clear both pointer (assgin nullptr)
   */
  void clear();
};

/**
 * @brief minimum of two reachability intervals; compare lower bound, then upper bound
 * @param a a pointer to a reachability interval
 * @param b a pointer to a reachability interval
 * @return pointer to lowest interval
 */
Pointer min(const Pointer a, const Pointer b);
/**
 * @brief minimum of two reachability intervals; compare lower bound, then upper bound
 * @param ival an interval of l,h reachability intervals
 * @return pointer to lowest interval
 */
Pointer min(const PointerInterval& ival);
/**
 * @brief minimum of two reachability intervals; compare lower bound, then upper bound
 * @param a an interval of l,h reachability intervals
 * @param b an interval of l,h reachability intervals
 * @return pointer to lowest interval
 */
Pointer min(const PointerInterval& a, const PointerInterval& b);

/**
 * @brief maximum of two reachability intervals; compare lower bound, then upper bound
 * @param a a pointer to a reachability interval
 * @param b a pointer to a reachability interval
 * @return pointer to lowest interval
 */
Pointer max(const Pointer a, const Pointer b);
/**
 * @brief maximum of two reachability intervals; compare lower bound, then upper bound
 * @param ival an interval of l,h reachability intervals
 * @return pointer to lowest interval
 */
Pointer max(const PointerInterval& ival);
/**
 * @brief maximum of two reachability intervals; compare lower bound, then upper bound
 * @param a an interval of l,h reachability intervals
 * @param b an interval of l,h reachability intervals
 * @return pointer to lowest interval
 */
Pointer max(const PointerInterval& a, const PointerInterval& b);


/**
 *  @brief boundary interval in the reachability structure.
 *  Represents an interval on the boundary of the FreeSpace (bottom.left,top, or right)
 *
 *  Stores references to the corresponding intervals (l,h) on the opposite border.
 *  @see [alt95]
 *
 *  Implements an intrusive double-linked-list. Intrusive lists are efficient,
 *  but it is important that each object is only contained in no more than one list !
 *
 *  Note: we keep an eye on memory footprint.
 *  sizeof(BoundarySegment)==64 is cache-friendly. Try to avoid sizeof > 64.
 *
 *  Also note that LinkedListElement has NO virtual destructor, since
 *  BoundarySegment does not need a destructor (and we can save 8 bytes for
 *  the vptr).
 *
 * @author Peter Schäfer
 */
class BoundarySegment :
        public data::LinkedListElement<BoundarySegment>,
        public data::Interval,        //  bounds
        public PointerInterval  //  l,h
{
private:
    friend class Structure;   
    //! @brief temporary data used during merge and split operations
    mutable struct {
        union { //!  points to a clone of this object
            Pointer clone;
            //!  @brief points to a segment on the opposite edge of the reachability structure.
            //!  twins have the same boundary interval (but different l,h pointers)
            Pointer _twin;
        };
#ifdef QT_DEBUG
        //!  used for debug assertions
        void* owner;
#endif
    } temp;    
    /**
     * @param hops distance (1=opposite, 2=...)
     * @return segment on the opposite edge of the reachability structure
     */
    Pointer twin(int hops) {
        Pointer result=this;
        while(hops--) result = result->temp._twin;
        return result;
    }

public:
    //! reachability label
    Type type;
    const Orientation ori;    //!<  horizontal or vertical
    const Direction dir;      //!<  left/right or bottom/top

public:
    /**
     * @brief create an identical copy of this segment
     * @return a newly allocated copy
     */
    Pointer createClone() const {
        Pointer clone = new BoundarySegment(*this);
        temp.clone = clone;
        clone->temp.clone = (Pointer)this;  //  non-const, but mostly harmless
        return clone;
    }
    //! @return the clone of this object, if it exists
    Pointer clone() const               { return temp.clone; }
    //! @brief disconnect from clone. Does not release any memory.
    void clearClone() const             { temp.clone=nullptr; }

#ifdef QT_DEBUG
    //! @return owner object, for debugging
    void* owner() const                 { return temp.owner; }
    //! @brief assign owner object, for debugging
    //! @param own reference to owning structure
    void  setOwner(void* own) const     { temp.owner=own; }
#endif

public:
    /**
     * @brief default constructor
     * @param ival free-space interval bounding this segment
     * @param ori is this a horizontal or vertical segment?
     * @param dir is is located in the bottom-left part, or in the top-right part of a reachability structure
     * @param t label that indicates of the interval is reachable, or not
     * @param owner for debugging only
     */
    BoundarySegment(const Interval& ival, Orientation ori, Direction dir, Type t, void* owner=nullptr);

    /**
     * @brief clears pointers, keep interval bounds
     */
    void clear() {
        type = NON_ACCESSIBLE;
        PointerInterval::clear();
        //  NEVER Interval::clear()
    }
    /**
     * @brief containment test
     * @param x a point in the free-space
     * @return true, if x is contained in this interval
     */
    bool contains(double x) const {
        return Interval::contains(x);
    }
    /**
     * @brief containment test
     * @param p pointer to an invterval
     * @return true, if the p-interval is contained in this interval
     */
    bool contains(Pointer p) const {
        return PointerInterval::contains(p);
    }
    /**
     * @brief assigment operator from interval
     * @param lh an interval
     * @return reference to this, after assigment
     */
    BoundarySegment& operator= (const PointerInterval& lh) {
        PointerInterval::operator= (lh);
        return *this;
    }
    /**
     * @brief comparison operator
     * @param that another segment
     * @return true, if both segment have indentical properties
     *  (except temporary data)
     */
    bool operator== (const BoundarySegment& that) const {
        return (this->type==that.type)
                && (this->ori==that.ori)
                && (this->dir==that.dir)
                && PointerInterval::operator== (that);
    }
#ifdef BOUNDARY_VDESTRUCTOR
    /**
     * @brief virtual destructor
     * @deprecated we need no destructor, actually. Would increase memory footprint without need.
     */
    virtual ~BoundarySegment() {
#ifdef QT_DEBUG
        temp.owner=nullptr;
#endif
    }
#endif
};

/**
 * @brief compare two pointers in the same part of a reachability structure
 *  (either right-top, or bottom-left)
 * @param a pointer to a segment, in the same part as b
 * @param b pointer to a segment, in the same part as a
 * @return <0 if a<b, ==0 if a==b, >0 if a>b
 */
int compare_interval(Pointer a, Pointer b);
/**
 * @brief compare two pointers in the same part of a reachability structure
 *  (either right-top, or bottom-left)
 * @param ival a segment interval
 * @return <0 if a<b, ==0 if a==b, >0 if a>b
 */
int compare_interval(const PointerInterval& ival);
/**
 * @brief test intersection of intervals
 * @param a pointer to a segment, in the same part as b
 * @param b pointer to a segment, in the same part as a
 * @return true if a>b
 */
bool empty_interval(Pointer a, Pointer b);
/**
 * @brief test intersection of intervals
 * @param ival a segment interval
 * @return true if a>b
 */
bool empty_interval(const PointerInterval& ival);
/**
 * @brief test intersection of intervals.
 * Assumes a SEE_THROUGH segment.
 * @param p a segment interval
 * @return true if a>b
 */
bool empty_see_through_interval(Pointer p);

/**
 * @brief compare two pointers on opposite parts of a reachability structure
 * (eitehr left->right-top, or bottom->right-top)
 * @param a pointer to a segment, in the opposite part as b
 * @param b pointer to a segment, in the opposite part as a
 * @return <0 if a<b, ==0 if a==b, >0 if a>b
 */
int compare_pointers(Pointer a, Pointer b);

//! @brief a double-linked list of reachability segments.
//! makes up one of the four edges of a reachability structure
typedef data::LinkedList<BoundarySegment> BoundaryList;

//! @brief print operator for debugging
std::ostream& operator<< (std::ostream& out, const BoundaryList& list);

} } //  namespace frechet::reach

#endif // BOUNDARY_H
