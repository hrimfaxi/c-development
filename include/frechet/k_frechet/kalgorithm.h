#ifndef ALGORITHMBASE_H
#define ALGORITHMBASE_H

#include <freespace.h>
#include <app/workerthread.h>

#include <exception>
#include <stack>
#include <functional>

//#include <boost/thread/mutex.hpp>
#include <QMutex>

//  throw (...) considered deprecated (but still useful)
#if defined __GNUC__
# pragma GCC diagnostic ignored "-Wdeprecated"
#elif defined _MSC_VER
# pragma warning( disable : 4290 )
#endif

namespace frechet { namespace k {

/**
 * @brief attaches a component ID to an interval
 *
 * In the k-Frechet algorithm, components are projected to domain axes.
 * The MappedInterval class represents such a projection with
 * the associated component identifier.
 *
 * @author Peter Schäfer
 */
class MappedInterval : public data::Interval {
public:
    //! unique identifier of the component
    //! @see component_id_t
    int componentID;

    //! @brief empty constructor; creates an invalid interval
    MappedInterval()
            : Interval(), componentID(-1) {}

    /**
     * @brief default constructor
     * @param ival a projection of a component to a domain axis
     * @param compID the associated component identifier
     */
    MappedInterval(const Interval &ival, int compID)
            : Interval(ival), componentID(compID) {}

    /**
     * @brief comparison operator, needed for sorting a list of MappedInterval objects.
     *  We first compare the lower bounds of both intervals.
     *  Next, we compare the upper bounds in reversed order (i.e. larger intervals are sorted in front of smaller ones)
     *  Thirdly, we compare compoent IDs.
     * @param that object to compare with
     * @return true if (*this) < that
     */
    bool operator<(const MappedInterval &that) const;
    /**
     * @brief equality operator, needed for sorting a list of MappedInterval objects.
     * @param that object to compare with
     * @return true if this equals that
     */
    bool operator==(const MappedInterval &that) const;
};
/**
 * @brief a vector of MappedInterval objects.
 * Usually these are sorted according to the natural ordering
 * imposed by MappedInterval::operator<
 */
typedef std::vector<MappedInterval> IntervalMap;


/**
 *  @brief The k-Frechet algorithm.
 *
 *  Given a set of components, and their projections to the domain axes,
 *  select a subset such that both axes are covered.
 *  Don't use more than k components;
 *
 *  This is the decision version of the problem. As result, it returns
 *  the minimum number of k components.
 *
 *  There is a 'greedy' variant and a 'brute-force' variant.
 *  The greedy variant returns two appromixation values for k.
 *  The brute-force variant returns the optimal value for k.
 *
 *  Note that the greedy algorithm is optimal *for one axis*, and has
 *  an approximation factor 2 for both axes.
 *  We can use the intermediate results of the greedy algorithms as bounds for the
 *  optimal results.
 *
 *  Since the brute-force variant has exponential time complexity,
 *  it is run in a worker thread that can be interrupted by the user.
 *
 *  Basically, the brute-force algorithm is an exhaustive search,
 *  picking at most k elements from a base set. Breadth-first search is not appropriate,
 *  we are using a depth-first approach.
 *
 *  The depth of the search tree is limited, but can still become huge.
 *  For practicality, we use iterative deepening. The tree depth is first limited to k_min.
 *  With each iteration, the tree-depth limit is increased by one, until we reach k_max.
 *
 *  The iterative deepening approach performs redundant work, but it
 *  stops at the *first* solution. This seems to be a huge practical advantage
 *  over an exhaustive full-depth search.
 *
 *  The amount of redundant work is tolerable, considering exponential growth
 *  of the seach tree.
 *
 * @author Peter Schäfer
 */
class kAlgorithm : public QObject {
    Q_OBJECT
signals:
    //! @brief raised when the 'greedy' algorithm finishes
    //! Connected to UI components to update their states.
    void greedyFinished();
    //! @brief raised after an iteration of the 'brute-force' algorithm
    //! Connected to UI components to update their states.
    void bruteForceIteration(int);
    //! @brief raised when the 'brute-force' algorithm finishes
    //! Connected to UI components to update their states.
    void bruteForceFinished();
    //void bruteForceCancelled();

public:
    /**
     * @brief negative values for k indicate the special conditions of the computation
     */
    enum Result {
        UNKNOWN = -3,       //!< initial status; the algorithm has not ye been run
        RUNNING = -2,       //!< indicates that the algorithm is currently running
        NO_SOLUTION = -1    //!< there is no solution at all
    };

protected:
    /**
     * @brief a stack of componentIDs and axis locations.
     * Used by the brute-force algorithm for backtracking.
     */
    typedef std::vector<std::pair<int, double>> Stack;

    /**
     * @brief connected components of the free-space diagram
     */
    const free::Components &components;
    /**
     * @brief the x-axis and the y-axis of the free-space diagram (from Grid)
     */
    data::IntervalPair domain;

    /**
     *  @brief Current working set of mapped intervals.
     *  Horizontal intervals are mapped to the range [0..n]
     *  Vertical intervals are mapped to the range [n..n+m]
     * */
    struct WorkingSet {
        //! map to horizontal (x-)axis
        IntervalMap hmap;
        //! map to vertical (y-)axis
        IntervalMap vmap;
        //! subset of currently selected interval
        data::BitSet set;
        //!  protects concurrent access to working set
        //!  (because the algorithm is usually running within a separate thread)
        QMutex lock;

        //! @brief empty constructor
        WorkingSet() : hmap(),vmap(),set(),lock() {}
    } working;

    /**
     * @brief add a mapped interval to the current working-set
     * @param ival an interval with a component
     */
    void working_add(const MappedInterval &ival);
    /**
     * @brief remove a mapped interval from the current working-set
     * @param ival an interval with a component
     */
    void working_remove(const MappedInterval &ival);
    /**
     * @brief add a mapped interval to the current working-set
     * @param comp a free-space component
     */
    void working_add(component_id_t comp);
    /**
     * @brief remove a mapped interval from the current working-set
     * @param comp a free-space component
     */
    void working_remove(component_id_t comp);

public:
    //! @return the working-set of currently selected horizontal intervals
    const IntervalMap &horizontalIntervals() const { return working.hmap; }
    //! @return the working-set of currently selected vertical intervals
    const IntervalMap &verticalIntervals() const { return working.vmap; }

    /** @brief Results of the Greedy algorithm.
     *
     *  The greedy algorithms scans first the x-axis, then the y-axis.
     *  It can be run in reversed order, producing a valid result, too.
     *  We usually run *both* variants to get better appromiation bounds
     *  for the brute-force variant.
     */
    struct Greedy {
        //! number of components that is needed to cover the x-axis. 0 if the x-axis can not be covered.
        int k_x;
        //! number of components that is needed to cover the y-axis.  0 if the y-axis can not be covered.
        int k_y;
        //! number of components, when scanning the x-axis first. 0 if the axes can no be covered.
        int k_xy;
        //! number of components, when scanning the y-axis first. 0 if the axes can no be covered.
        int k_yx;
        //! subset of selected components
        data::BitSet result;

        //! @return true if there is a valid result
        bool valid() const { return k_yx > 0; }

        /**
         * @brief get a lower bound for the optimal result.
         * Observe that k_x,k_y <= k_optimal
         * @return lower bound for the optimal result, negative if there is no result at all.
         */
        Result lowerBound() const { return (Result) std::max(k_x, k_y); }
        /**
         * @brief get an upper bound for the optimal result.
         * Observe that k_optimal <= k_xy,k_yx
         * @return lower bound for the optimal result, negative if there is no result at all.
         */
        Result upperBound() const { return (Result) std::min(k_xy, k_yx); }

        //! @brief empty constructor
        Greedy() :  k_x(UNKNOWN),k_y(UNKNOWN), k_xy(UNKNOWN), k_yx(UNKNOWN),
                    result()
        { }
    };

    //! @return results of the greedy algorithm (if available)
    const struct Greedy &greedyResult() const { return greedy; }

private:
    //! results of the greedy algorithm
    struct Greedy greedy;

public:
    /**
     * @brief results of the brute-force algorithm
     *
     * The brute-force algorithm performs an exhausitve search on any
     * combination of intervals. It has exponential time complexity.
     *
     * Results of the greedy algorithm are used to derive lower and upper
     * bounds (and thus allow to reduce computation time, somewhat).
     */
    struct BruteForce {
        /*  Bounds for the BF algorthm.
         *  Initially from the Greedy results; k_min may increase when the BF returns unsucessfully
         */
        //! lower bound derived from greedy results. k_min = max{greedy.k_x,greedy.k_y}
        int k_min = 0;
        //! upper bound derived from greedy results. k_max = min{greedy.k_xy,greedy.k_yx}
        int k_max = 0;
        //! current number of selected components
        int k_current;
        //! optimal value of last iteration
        int k_iteration;
        //! optimal value so far
        int k_optimal;

        //! subsset of selected components
        data::BitSet result;
        //! stacks used for backtracking
        Stack stack1, stack2;

        //! @return true if there is a valid result
        bool valid() const { return k_optimal > 0; }

        //! @brief empty constructor
        BruteForce() :  k_min(0),k_max(0),
                        k_current(UNKNOWN), k_iteration(UNKNOWN), k_optimal(UNKNOWN),
                        result(), stack1(), stack2()
        { }
    };

    //! @return result of the brute-force algorithm (if available). This result is optimal.
    const struct BruteForce &optimalResult() const { return bf; }

private:
    //! result of the brute-force algorithm
    struct BruteForce bf;

    //! @return top of first backtracking stack
    std::pair<int, double> &bf_top1();
    //! @return top of second backtracking stack
    std::pair<int, double> &bf_top2();

    //! @brief push an value to first stack
    //! \param i component ID
    //! \param x location on x-axis
    void bf_push1(int i, double x);
    //! @brief pop from first stack
    //! @return true if the stack contains more elements
    bool bf_pop1();
    //! @brief push an value to second stack
    //! \param i component ID
    //! \param y location on y-axis
    void bf_push2(int i, double y);
    //! @brief clear second stack
    //! @return always false
    bool bf_backtrack2();

    //! @brief initialize working-set data structures
    void setupMapsAndResults();

public:
    //! @brief smart pointer to an kAlgorithm object
    typedef boost::shared_ptr<kAlgorithm> ptr;

    //! @brief default constructor
    //! @param fs free-space diagram data
    kAlgorithm(free::FreeSpace::ptr fs);

    //! @brief run the greedy algorithm
    //! @return min. number of components. Negative values indicate that no result is available.
    int runGreedy();

    /**
     * @brief run the brute force algorithm.
     * The greedy algorithm must have been run before!
     * @param cancelFlag a flag that indicates interruption by the user.
     * The algorithm polls this flag regularly and throws an InterruptedException
     * when the flag is found to be true.
     * The application logic will catch the exception and clean up.
     * @throw InterruptedException when the user requests and interruption
     * @return result of the brute-force algorithm Negative values indicate that no result is available.
     */
    int runBruteForce(volatile bool *cancelFlag = 0) throw(frechet::app::InterruptedException);

private:
    //! background worker job for the k-Frechet algorithm
    friend class KWorkerJob;

    //! @brief run the greedy algorithm (x-axis first)
    //! @return k_xy, min. number of components. Negative values indicate no result.
    int greedy_xy();
    //! @brief run greedy algorithm (y-axis first)
    //! @return k_yx, min. number of components. Negative values indicate no result.
    int greedy_yx();

    //! @brief run the greedy algorithm on tow axes
    //! @param map1 intervals on first axis
    //! @param range1 first axis domain
    //! @param map2 intervals on second axis
    //! @param range2 second axis domain
    //! @param k_1 on return, hold result of first axis
    //! @return min. number of components. Negative values indicate no result.
    int greedy_12(
            const IntervalMap &map1, const data::Interval &range1,
            const IntervalMap &map2, const data::Interval &range2,
            int &k_1);

    //! @brief run the greedy algorithm on the first axis
    //! @param map intervals on first axis
    //! @param range axis domain
    //! @return min. number of components. Negative values indicate no result.
    int greedy_1(const IntervalMap &map, const data::Interval &range);
    //! @brief run the greedy algorithm on the second axis
    //! @param map intervals on first axis
    //! @param range axis domain
    //! @return min. number of components. Negative values indicate no result.
    int greedy_2(const IntervalMap &map, const data::Interval &range);

    //! @brief run the brute-force algorithm on the x-axis. Results are placed in this->bf.
    //! @param cancelFlag a flag that indicates interruption by the user.
    //! @throw InterruptedException when the user requests and interruption
    //! @return true if there is a result
    bool bf_search_1(volatile bool *cancelFlag = 0) throw(frechet::app::InterruptedException);
    //! @brief run the brute-force algorithm on the y-axis. Results are placed in this->bf.
    //! @throw InterruptedException when the user requests and interruption
    //! @return true if there is a result
    bool bf_search_2() throw(frechet::app::InterruptedException);
};

/**
 * @brief background worker job for running the k-Frechet brute-force algorithm.
 */
class KWorkerJob : public frechet::app::WorkerJob {
private:
    //! pointer to kAlgorithm object
    kAlgorithm *alg;
public:
    //! @brief default constructor
    //! @param owner the algorithm object
    KWorkerJob(kAlgorithm *owner) : alg(owner) {}

    //! @brief run the brute-force algorithm
    virtual void runJob() {
        alg->runBruteForce(&cancelRequested);
    }

    //! @brief called after the algorithm was interrupted
    //! Raise a Qt signal that triggers UI updates.
    virtual void afterInterrupted() {
        emit alg->bruteForceFinished();
    }
};

} } //  namespaces

#endif // ALGORITHMBASE_H
