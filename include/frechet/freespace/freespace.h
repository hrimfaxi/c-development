#ifndef FREESPACE_H
#define FREESPACE_H

#include <types.h>
#include <interval.h>
#include <components.h>
#include <array2d.h>
#include <numeric.h>

#include <QLineF>

#include <cmath>

#include <boost/smart_ptr.hpp>

namespace frechet { namespace free {
/**
 * @brief holds data for one cell of the free-space diagram
 *
 * That is, the intervals L_ij and B_ij
 *
 * For use by the k-Frechet distance, we store the bounding box of the free-space area
 * as bounds.H and bounds.V
 *
 * @author Peter Schäfer
 */
class Cell {
public:
    //!  Intersection of Free Space with Grid
    //!  L_ij = (a,b),  B_ij = (c,d)
    data::Interval L,B;

public:
    //!  Free Space boundary; determined by L,B plus ellipse bounds
    //!  (only needed for k-Frechet algorithm)
    /**  Note: bounds are needed to calculate component intervals (projections)
     *  *but* need not be persisted for the k-Frechet algorithm.
     *  We persist it *only* for the purpose of visualisation.
     * */
    data::IntervalPair bounds;

public:
    /*
     * Free-Space segments of a cell
     * */
    //! @return lower bound of L_ij
    double&     a()         { return L.lower(); }
    //! @return upper bound of L_ij
    double&     b()         { return L.upper(); }
    //! @return lower bound of B_ij
    double&     c()         { return B.lower(); }
    //! @return upper bound of B_ij
    double&     d()         { return B.upper(); }

    //! @return lower bound of L_ij
    double      a() const   { return L.lower(); }
    //! @return upper bound of L_ij
    double      b() const   { return L.upper(); }
    //! @return lower bound of B_ij
    double      c() const   { return B.lower(); }
    //! @return upper bound of B_ij
    double      d() const   { return B.upper(); }

    /**
     * @param l left of bottom?
     * @return L or B
     */
    const data::Interval& LB(bool l) { return l ? L:B; }
};

//  TODO type_traits: Interval, IntervalPair, and Cell are trivially-copyable

/**
 * @brief The FreeSpace class models the Free-Space Diagram
 *  calculated from two curves.
 *
 * Holds a two-dimensional array of free-space Cell objects.
 *
 * For k-Frechet we keep a map of connected components.
 *
 * @author Peter Schäfer
 */
class FreeSpace {

public:
    const Curve &P; //!< first input curve
    const Curve &Q; //!< second input curve
    const int n; //!< number of vertexes in P
    const int m; //!< number of vertexes in Q

    //! smart pointer to FreeSpace object
    typedef boost::shared_ptr<FreeSpace> ptr;

    //! the unit rectangle [0,1]x[0,1]
    static const QRectF UNIT_RECT;
    //! minimum determinant to avoid roundoff errors
    static const double DET_MINIMUM;
private:
    //! @brief free line segments as Intervals subset of [0..1]
    /** Question: Is is really necessary to store the whole array of cells?
     * A workings sets of two columns would be sufficient.
     *
     * Answer: the algorithm for simple polygons needs the complete information.
     * Memory requirements are modest, anywway.
     */
    data::Array2D<Cell> cells;
    //! tracks connected components for k-Frechet algorithm
    Components _components;

public:
    /**
     * @brief default constructor with two input curves
     * @param ap first input curve
     * @param aq second input curve
     */
    FreeSpace(const Curve& ap, const Curve& aq);
    /**
     * @brief copy constructor
     * @param that object to copy from
     */
    FreeSpace(const FreeSpace& that);

    //! @return map of connected components (used by k-Frechet algorithm)
    Components& components() { return _components; }

    /**
     * @brief find the component ID for a cell.
     * Empty cells return NO_COMPONENT.
     * @param i grid column
     * @param j grid row
     * @return the component representative ("ID")
     */
    size_t component(int i, int j)
    {
        if (cellEmptyBounds(i,j))
            return Components::NO_COMPONENT;
        else
            return _components.representative(i,j);
    }
    /**
     * @brief calculate all free space intervals
     * @param epsilon current value of epsilon
     */
    void calculateFreeSpace(double epsilon);
    /**
     * @brief calculate the bounding boxes of the free-space areas
     * (k-Frechet only)
     * @param epsilon current value of epsilon
     */
    void calculateBounds(double epsilon);

    /*
     *  Free-Space Cells
     * */
    /**
     * @param x grid column
     * @param y grid row
     * @return the free-space data for a cell
     */
    Cell& cell(int x, int y) {
        Q_ASSERT(x>=0 && x < n);
        Q_ASSERT(y>=0 && y < m);
        return cells.at(x,y);
    }
    /**
     * @param x grid column
     * @param y grid row
     * @return the free-space data for a cell
     */
    const Cell& cell(int x, int y) const {
        Q_ASSERT(x>=0 && x < n);
        Q_ASSERT(y>=0 && y < m);
        return cells.at(x,y);
    }
    /**
     * @brief test if a cell is empty
     * @param i grid column
     * @param j grid row
     * @return true if all four intervals are empty
     */
    bool cellEmptyIntervals(int i, int j) const;
    /**
     * @brief test if the bounding box of a cell is empty
     * @param i grid column
     * @param j grid row
     * @return true if all four bounding intervals are empty
     */
    bool cellEmptyBounds(int i, int j) const;

    /**
     * @brief test if a cell is completely covered by the free-space area
     * @param i grid column
     * @param j grid row
     * @return true if the cell is full
     */
    bool cellFull(int i, int j) const;

    /**
     * @brief test if the free-space area of a cell is a degenerate ellipse,
     *  i.e. two parallel lines. This is the case if the determinant is zero.
     * @param i grid column
     * @param j grid row
     * @return true if the free-space area is "degenerate"
     */
    bool isParallel(int i, int j) const {
        return determinant(i,j) == 0.0;
    }

    /**
     * @brief test if the free-space area of a cell is a *almost* degenerate ellipse,
     *  This is the case if the determinant is very close to zero.
     *  For visualisation, the ellipse is replaced by two parallel lines.
     * @param i grid column
     * @param j grid row
     * @return true if the free-space area is "degenerate"
     */
    bool isAlmostParallel(int i, int j) const {
        return abs(determinant(i,j)) < DET_MINIMUM;
    }

    /**
     * @brief if P is a closed curve, the free space diagram wraps at right edge
     * @return true if the free-space wraps at the right edge
     */
    bool wrapRight() const { return P.isClosed(); }
    /**
     * @brief if Q is a closed curve, the free space diagram wraps at top edge
     * @return true if the free-space wraps at the right edge
     */
    bool wrapTop() const { return Q.isClosed(); }

    /**
     * @param i grid column
     * @param j grid row
     * @return bounding rectangle of a free-space cell
     */
    QRectF segmentBounds(int i, int j);

    /**
     * @brief traverse one column of the free-space and find all connected intervals.
     * This method is needed by the algorithm for simple polygons during the computation
     * of valid placements.
     * @param i grid column
     * @return a list of connected intervals in a given column
     */
    std::list<data::Interval> collectFreeVerticalIntervals(int i) const;

public:
    //void calculateGridOffsets();
    /**
     * @brief compute intervals for one point and segment.
     * As parameters we expect a point on one curve (P,or Q)
     * and a line segment on the other curve (Q,or P).
     *
     * @param p point on P (resp. Q)
     * @param q start of segment on Q (resp. P)
     * @param r end of segment on Q (resp. P)
     * @param epsilon value of epsilon
     * @tparam Float floating point type for intermediate results.
     * 'long double' values may be used for improved accuracy.
     * End results are always 'double' precision.
     * @param result on return, holds the free-space interval (L_ij, resp. B_ij)
     */
    template<typename Float>
    static void calculateFreeSpaceSegment(const Point& p, const Point& q, const Point& r,
                                   double epsilon,
                                   data::Interval& result);
    /**
     * @brief fix border case at the joint of four intervals
     * @param N north interval
     * @param E east interval
     * @param S south interval
     * @param W west interval
     */
    static void fixBorderCase(data::Interval* N, data::Interval* E, data::Interval* S, data::Interval* W);

private:
    /**
     * @param i grid column
     * @param j grid row
     * @return determinant of the ellipse transform
     */
    double determinant(int i, int j) const;

    /**
     * @brief compute the bounding box of one cell in the free-space are.
     * As parameters we expect two segments of P and Q
     * @param p1 start point of segment on P
     * @param p2 end point of segment on P
     * @param q1 start point of segment on Q
     * @param q2 end point of segment on Q
     * @param epsilon value of epsilon
     * @param lambda on return, holds two bounding intervals
     * 'long double' values may be used for improved accuracy.
     * End results are always 'double' precision.
     * @tparam Float floating point type for intermediate results.
     */
    template<typename Float>
    void calculateEllipseBoundaries(
            const Point& p1, const Point& p2,
            const Point& q1, const Point& q2,
            double epsilon,
            double lambda[2]);
};

} }  //  namespace frechet

#include <freespace_impl.h>

#endif // FREESPACE_H
