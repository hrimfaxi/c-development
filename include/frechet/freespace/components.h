#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <boost/pending/disjoint_sets.hpp>
#include <interval.h>
#include <array2d.h>
#include <bitset.h>

namespace frechet { namespace free {

class FreeSpace;
class Cell;

/**
 * @brief Free-Space connected components and their projections to the domain axes.
 *
 *  Used by the k-Frechet algorithm. Atfer computing the free-space diagram,
 *  we find connected components and project their bounding box to the x- and y-axis.
 *
 *  For finding connected component, we use a union-find structure.
 *  Free-space cells are connected if their adjacent free intervals are not empty.
 *
 *  Each component is identified by a unique ID of type component_id_t
 *
 * @author Peter Schäfer
 */
class Components {

public:
    //! union-find structure, or disjoint set
    typedef boost::disjoint_sets_with_storage<> DisjointSet;
    //! maps components to their component bounding boxes.
    //! Bounding boxes are represented by a pair of intervals.
    typedef data::Array2D<data::IntervalPair> IntervalArray;

private:
    int n;  //!< number of vertexes in P. Number of grid columns is n-1.
    int m;  //!< number of vertexes in Q. Number of grid rows is m-1.
    DisjointSet disjointSet;    //!< disjoint set of component IDs.
    //size_t countID;
    //! has an entry for every cell (nxm)
    /**
     * Alternatively, we could use a hash map <component_id_t,IntervalPair> and save some memory.
     * Memory requirements are modest, however.
     */
    IntervalArray intervalArray;
    //! contains the valid representatives (a subset of all cells)
    data::BitSet intervalSet;

    /**
     * @brief get the component for a cell
     * @param i grid column
     * @param j grid row
     * @return the component ID of a cell
     */
    component_id_t componentID(int i, int j);

public:
    //! empty cells have an invalid component ID
    static const component_id_t NO_COMPONENT;
    /**
     * @brief default constrcutor
     * @param n number of vertexes in P
     * @param m number of vertexes in Q
     */
    Components(int n, int m);
    /**
     * @brief clear the component set
     */
    void clear();
    /**
     * @brief find the component ID of a cell
     * @param i grid column
     * @param j grid row
     * @return the cell's component ID, or NO_COMPONENT
     */
    component_id_t representative(int i, int j);

    /**
     * @brief calculate connected components
     * @param fs the underlying Free-space diagram
     */
    void calculateComponents(FreeSpace& fs);

    //! @return total number of connected components
    component_id_t count() const     { return intervalSet.count(); }

    //! @return iterator to the beginning of the component set
    data::BitSet::iterator begin() const   { return intervalSet.begin(); }
    //! @return iterator past the end of the component set
    data::BitSet::iterator end() const     { return intervalSet.end(); }

    //! @return iterator to the first entry of the component set
    data::BitSet::iterator first() const   { return intervalSet.first(); }
    //! @return iterator to the last entry of the component set
    data::BitSet::iterator last() const    { return intervalSet.last(); }

    //! @return bounding boxes for components
    const IntervalArray& intervals() const { return intervalArray; }

private:
    /**
     * @brief compute component ID for a cell
     * @param fs free-space diagram
     * @param i grid column
     * @param j grid row
     * @return component ID for the cell
     */
    component_id_t assignComponent(const FreeSpace& fs, int i, int j);
    /**
     * @brief normalize union-find structure.
     * for each component, find the smallest representative.
     */
    void normalize();
};

} }  //  namespace

#endif // COMPONENTS_H
