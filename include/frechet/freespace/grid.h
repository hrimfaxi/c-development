#ifndef GRID_H
#define GRID_H

#include <vector>
#include <types.h>
#include <interval.h>

#include <boost/smart_ptr.hpp>

/**
 * @brief display style of grid lines in free-space diagram
 * @see Path::lineStyle
 * @see Path::setDefaultLineStyle
 */
enum LineStyle {
    //! default is a solid line, unless overriden by
    //! @see Path::setDefaultLineStyle
    DEFAULT=0,    
    SOLID,  //!< draw a solid line
    THIN,   //!< draw a thin, solid line
    DOTTED, //!< draw a dotted line
    NONE    //!< draw no grid line
};
//! a list of grid line styles
//! @see Path::lineStyles
typedef std::vector<LineStyle> LineStyleVector;

namespace frechet { namespace free {

/**
 * @brief a grid axis maps the segments of the input curve to an axis of the free-space diagram.
 *
 * Input curve P is mapped to the x-axis.
 * Input curve Q is mapped to the y-axis.
 *
 * The GridAxis class keeps a list of segment lengths.
 *
 * In addition we assign line styles for visualisation.
 *
 * @author Peter Schäfer
 */
class GridAxis : public std::vector<data::Interval>
{
private:
    //! default line style for drawing a grid line
    LineStyle defaultLineStyle;
    //! custom line styles for grid lines
    LineStyleVector lineStyles;

public:    
    //! empty constructor
    GridAxis();

    /**
     * @brief compute the length of each polygon segment
     * @param P an input curve
     */
    void setCurve(const Curve& P);
    /**
     * @brief assign line styles for visualisation
     * @param linesStyles list of line styles
     * @param defaultLineStyle default line style
     */
    void setLineStyles(const LineStyleVector& linesStyles, LineStyle defaultLineStyle);

    /**
     * @param i column, or row in grid
     * @return the line style for drawing a grid line
     */
    LineStyle lineStyle(int i) const;

    /**
     * @brief maps a value from [0..n] onto the axis [0..|P|]
     * @param x offset from [0..n]
     * @return offset into [0..|P|]
     */
    double map(double x) const;
    /**
     * @brief maps an interval from [0..n] onto the axis [0..|P|]
     * @param other interval from [0..n]
     * @return interval into [0..|P|]
     */
    data::Interval map(const Interval& other) const;
    /**
     * @return total length of the axis == total length of the associated input curve
     */
    double length() const;
};

/**
 * @brief performs mappings to and from the free-space grid
 *
 *  Calculations are done in three coordinate systems:
 *
 *  (1) the unit square [0..1]x[0..1]
 *      free-space segments are usually in this coordinate system
 *
 *  (2) [0..n]x[0..m]
 *      each free-space cell is mapped to its location in the grid
 *      with each segment having length 1
 *
 *      2-intervals for the k-Frechet algorithm are expressed in this coordinate system
 *
 *  (3) [0..|P|]x[0..|Q|]
 *      each free-space cell is scaled to arc-length.
 *      this is the coordinate system used for visualisation.
 *
 *
 *  The Grid class provides functions for converting from one coordinate system to another.
 *
 *  In particular from:
 *  [1] to [2] and [3]
 *  [2] to [3] and back to [1]
 *
 *
 * @author Peter Schäfer
 */
class Grid {

private:
    //! arc lengths of P == horizontal grid coordinates
    GridAxis _hor;
    //! arc lengths of Q == vertical grid coordinates
    GridAxis _vert;

public:
    //! smart pointer to a Grid object
    typedef boost::shared_ptr<Grid> ptr;
public:
    //! @brief empty constructor
    Grid() : _hor(), _vert() {}

    /**
     * @brief calculate grid from input curves
     * @param P first input curve
     * @param Q second input curve
     */
    void setCurves(const Curve& P, const Curve& Q);

    //! @return mapping for horizontal grid axis
    const GridAxis& hor() const     { return _hor; }
    //! @return mapping for vertical grid axis
    const GridAxis& vert() const    { return _vert; }

    //! @return mapping for horizontal grid axis
    GridAxis& hor()                 { return _hor; }
    //! @return mapping for vertical grid axis
    GridAxis& vert()                { return _vert; }


    /**
     * @return number of grid cells = number of curve segments = number of curve vertexes-1
     */
    QSizeF size() const {
        return QSizeF(_hor.size(),_vert.size());
    }
    /**
     * @return total size of the grid == lengths of input curves
     */
    QSizeF extent() const;

    /**
     * @param i grid column
     * @param j grid row
     * @return bounding box for a grid cell
     */
    QRectF cellBounds(int i, int j) const;

    /**
     * @brief map a point from [0..n]x[0..m] to the grid coordinates [0..|P|]x[0..|Q|]
     * @param p a location in free-space [0..n]x[0..m]
     * @return the mapped location in the grid
     */
    Point mapPoint(Point p);

    /**
     * @brief map a line segment from [0..n]x[0..m] to the grid coordinates [0..|P|]x[0..|Q|]
     * @param l a line segment in free-space [0..n]x[0..m]
     * @return the mapped line segment in the grid
     */
    QLineF mapLine(QLineF l);

    /**
     * @brief map a rectangle from [0..n]x[0..m] to the grid coordinates [0..|P|]x[0..|Q|]
     * @param r a rectangle in free-space [0..n]x[0..m]
     * @return the mapped rectangle in the grid
     */
    QRectF mapRect(QRectF r);

    /**
     * @brief map a sequence of line segments from [0..n]x[0..m] to the grid coordinates [0..|P|]x[0..|Q|]
     * @param c a sequence of line segments in free-space [0..n]x[0..m]
     * @return the mapped curve in the grid
     */
    Curve mapCurve(Curve c);

    /**
     * @brief map a vertical line
     * @param i grid column
     * @return a vertical grid line, in grid coordinates
     */
    QLineF verticalGridLine(int i);

    /**
     * @brief map a horizontal line
     * @param j grid row
     * @return a horizontal grid line, in grid coordinates
     */
    QLineF horizontalGridLine(int j);

    /**
     * @return the bounding rectangle for the complete grid
     */
    QRectF sceneRect();

    /**
     * @brief given a polygonal curve and an offset, compute the point on the curve
     * @param C a polygonal curve
     * @param x offset on the curve
     * @return point that corresponds to the offset
     */
    static Point mapToPoint(const Curve &C, double x);
    /**
     * @brief given a polygonal curve and two offsets, compute the corresponding line segment
     * @param C a polygonal curve
     * @param x1 first offset on the curve
     * @param x2 second offset on the curve
     * @return line segment that connects both offsets
     */
    static QLineF mapToSegment(const Curve &C, double x1, double x2);
    /**
     * @brief given a polygonal curve and two offsets, compute the sequence of line segment connecting both offsets
     * @param C a polygonal curve
     * @param x1 first offset on the curve
     * @param x2 second offset on the curve
     * @return sequence of line segments that connects both offsets
     */
    static Curve mapToSequence(const Curve& C,  double x1, double x2);
    /**
     * @brief given a polygonal curve and list of offsets, compute the sequence of line segment connects the offsets
     * @param C a polygonal curve
     * @param seq a list of offsets on the curve
     * @return sequence of line segments that connects the offsets
     */
    static Curve mapToSequence(const Curve& C, const std::vector<double>& seq);
};

} } // namespace

#endif // GRID_H
