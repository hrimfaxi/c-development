#pragma once

#ifndef MATRIX_POOL_H
#define MATRIX_POOL_H

#include <list>
#include <boost/unordered_map.hpp>

struct mzd_t;
struct clmatrix_t;
struct clm4rm_conditions;

namespace frechet { namespace data {

/**
 *	@brief memory pool for matrix objects
 *  (M4RI matrices mzd_t* and OpenCL matrices clm4rm_t*)
 *
 *  Keeps a list of recycled objects to reduce the need
 *  for new memory allocations. During the course of the poly-algorithm,
 *  all matrixes have similar sizes, so the chances for reusability
 *  are very good.
 *
 *  @author Peter Schäfer
 */
class MatrixPool {
private:
	typedef std::pair<int,int> Key;
	typedef std::list<mzd_t*> mzdList;
	typedef std::list<clmatrix_t*> clmatrixList;

	typedef boost::unordered_map<Key,mzdList> mzdMap;
	typedef boost::unordered_map<Key,clmatrixList> clmatrixMap;

    //! set of recyclable mzd_t objects
	mzdMap free_mzd;
    //! set of recyclable clmatrix_t objects
    clmatrixMap free_clmatrix;

public:
    //! @brief empty constructor
	MatrixPool();
    //! @brief destructor; releases all resources
	~MatrixPool();

    /**
     * @brief allocate a new mzd_t structure (a matrix for the M4RI algorithms)
     * @param rows number of matrix rows
     * @param cols number of matrix columns
     * @return a mzd_t object, preferrably a recycled one
     */
	mzd_t*		new_mzd_t		(int rows, int cols);
    /**
     * @brief allocate a new clmatrix_t structure (a matrix for the CLM4RM algorithms)
     * @param rows number of matrix rows
     * @param cols number of matrix columns
     * @param cond clm4rm_conditions keeps track of OpenCL working queue
     * @return a clmatrix_t object, preferrably a recycled one
     */
    clmatrix_t*	new_clmatrix_t	(int rows, int cols, clm4rm_conditions* cond);

    /**
     * @brief reclaim an object (i.e. put it into the recycling list)
     */
	void reclaim(mzd_t*);
    /**
     * @brief reclaim an object (i.e. put it into the recycling list)
     */
    void reclaim(clmatrix_t*);

    /**
     * @brief release all resources
     */
    void clear();

private:
    //! @return the mzd_t recycling-list for a given matrix size
	mzdList& getMzdFreeList(int rows, int cols);
    //! @return the clmatrix_t recycling-list for a given matrix size
    clmatrixList& getClmatrixFreeList(int rows, int cols);
};

/**
 * @brief allocate a new mzd_t structure (a matrix for the M4RI algorithms)
 * @param pool a memory pool, optional, may be nullptr
 * @param rows number of matrix rows
 * @param cols number of matrix columns
 * @return a mzd_t object, preferrably a recycled one
 */
mzd_t* 		new_mzd		(int rows, int cols, MatrixPool* pool);
/**
 * @brief allocate a new clmatrix_t structure (a matrix for the CLM4RM algorithms)
 * @param pool a memory pool, optional, may be nullptr
 * @param rows number of matrix rows
 * @param cols number of matrix columns
 * @param cond keeps track of OpenCL working queue
 * @return a clmatrix_t object, preferrably a recycled one
 */
clmatrix_t* new_clmatrix(int rows, int cols, MatrixPool* pool, clm4rm_conditions* cond);

/**
 * @brief reclaim an object (i.e. put it into the recycling list)
 * @param m a matrix in M4RI format
 * @param pool a memory pool, optional, may be nullptr
 */
void reclaim(mzd_t* m, MatrixPool* pool);
/**
 * @brief reclaim an object (i.e. put it into the recycling list)
 * @param clm  a matrix structure
 * @param pool a memory pool, optional, may be nullptr
 */
void reclaim(clmatrix_t* clm, MatrixPool* pool);

} }	//	namespace frechet::data

#endif // MATRIX_POOL_H
