
#ifndef FRECHET_VIEW_SPIROLATOR_H
#define FRECHET_VIEW_SPIROLATOR_H

namespace frechet { namespace data {

/**
 * @brief an integer iterator that goes in "spirals", like this:
 *
 *      i, i+1, i-1, i+2, i-2, ...
 *
 * It is used for searching a range of numbers, preferring values "in the middle".
 *
 * @author Peter Schäfer
 */
class Spirolator {
private:
    int min;    //!< minimum value
    int max;    //!< maximaum value
    int current;    //!< current value
    int increment;  //!< increment for next step
public:
    /**
     * @brief constructor with number range
     * @param amin minimum value, inclusive
     * @param amax maximum value, exclusive
     */
    Spirolator(int amin, int amax)
    : min(amin),max(amax),current((amin+amax-1)/2),increment(0)
    { }
    /**
     * @return true, if the iterator is in the valid range; false, if it is out of range
     */
    operator bool() { return current >= min && current < max; }
    /**
     * @return the current value
     */
    operator int()  { return current; }
    /**
     * @brief advance the iterator
     * @return this object, after being advanced
     */
    Spirolator& operator++() {
        ++increment;
        if (increment&1)
            current += increment;   //  add odd numbers
        else
            current -= increment;   //  subtract even numbers
        return *this;
    }
};

} } // namespace

#endif //FRECHET_VIEW_SPIROLATOR_H
