
#include <type_traits>

namespace frechet { namespace data {

template<class T>
Array2D<T>& Array2D<T>::operator=(const Array2D& that) {
    delete[] d;
    const_cast<int&>(n)=that.n;
    const_cast<int&>(m)=that.m;
    const_cast<T*&>(d) = new T[n*m];
    copy(const_cast<T*&>(d),that.d,n*m);
    return *this;
}

template<class T>
Array2D<T>& Array2D<T>::operator=(Array2D&& that) {
    delete[] d;
    const_cast<int&>(n)=that.n;
    const_cast<int&>(m)=that.m;
    const_cast<T*&>(d)=that.d;
    that.d=NULL;
    return *this;
}

template<class T>
Array2D<T>::Array2D(int an, int am) : n(an), m(am), d(NULL)
{
    Q_ASSERT(n>=0 && m>=0);
    if (n > 0 && m > 0)
        d = new T[n*m];
}

template<class T>
Array2D<T>::Array2D(Array2D&& that) : n(that.n), m(that.m), d(that.d)
{
    that.d=NULL;
}

template<class T>
Array2D<T>::Array2D(const Array2D& that) : n(that.n), m(that.m), d(new T[that.n*that.m])
{
    copy(d,that.d,n*m);
}

template<class T>
Array2D<T>::~Array2D()
{
    delete[] d;
}

template<class T>
void Array2D<T>::copy(T* d, T* thatd, int count) {
    //  if T is copyable
    if (std::is_trivially_copyable<T>()) {
        memcpy(d,thatd, sizeof(T)*count);
    }
    else {
        while(count-- > 0)
            *d++ = *thatd++;
    }
}

} }  //  namespace
