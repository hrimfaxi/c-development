#ifndef ARRAY2D_H
#define ARRAY2D_H

#include <limits.h>
#include <memory.h>
#include <QtGlobal>

namespace frechet { namespace data {

/**
 *  @brief A simple two-dimensional array of *fixed* size.
 *
 *  Data is arranged in column-major order.
 *
 *  @tparam T entry type (must be default constructible)
 *  @author Peter Schäfer
 */
template<class T>
class Array2D
{
protected:
    T* d; //!< array data
public:
    //! number of columns
    const int n;
    //! number of rows
    const int m;

    //! @brief default constructor
    //! @param an number of columns
    //! @param am number of rows
    Array2D(int an=0, int am=0);
    //! @brief copy constructor
    //! @param that object to copy from
    Array2D(const Array2D& that);
    //! @brief move constructor
    //! @param that object to copy from
    Array2D(Array2D&& that);
    //! destructor; releases data
    ~Array2D();
    //! @brief assignment operator
    //! @param that object to copy from
    //! @return this object, after assigning a copy
    Array2D& operator= (const Array2D& that);
    //! @brief move assignment operator
    //! @param that object to copy from
    //! @return this object, after assigning a copy
    Array2D& operator= (Array2D&& that);

    //! @brief array accessor
    //! @param i column index
    //! @param j row index
    //! @return current value at index (i,j)
    T&          at (int i, int j)       { return d[offset(i,j)]; }
    //! @brief array accessor
    //! @param i column index
    //! @param j row index
    //! @return current value at index (i,j)
    const T&    at (int i, int j) const { return d[offset(i,j)]; }

    //! @brief array accessor
    //! @param offset linear offset
    //! @return current value at index (offset)
    T&          at (int offset)         {
        Q_ASSERT(offset>=0 && offset < n*m);
        return d[offset];
    }

    //! @brief array accessor
    //! @param offset linear offset
    //! @return current value at index (offset)
    const T&    at (int offset) const   {
        Q_ASSERT(offset>=0 && offset < n*m);
        return d[offset];
    }

    //! @brief array accessor
    //! @param offset linear offset
    //! @return current value at index (offset)
    T&          operator[] (int offset)         { return at(offset); }
    //! @brief array accessor
    //! @param offset linear offset
    //! @return current value at index (offset)
    const T&    operator[] (int offset) const   { return at(offset); }

    /**
     * @brief an Array2D iterator
     *
     * Iterates the array in column-major order.
     * Allows for (column,row) indexing, as well as linear indexing.
     *
     * Note that iterators allow for modifications to the underlying array
     * (contrary to some stand iterators).
     */
    class iterator {
    public:
        //! @brief default constructor
        iterator() : parent(NULL), _offset(0) {}
        //! assignment operator
        //! @param that iterator to copy from
        //! @return this iterator
        iterator& operator= (const iterator& that) {
            parent = that.parent;
            _offset = that._offset;
            return *this;
        }
        //! @brief equality comparator
        //! @param that iterator to compare with
        //! @return true, if both iterators point to the same index; and refer to the same Array2D object.
        bool operator== (const iterator& that) const {
            return (parent==that.parent) && (_offset==that._offset);
        }
        //! @brief non-equality comparator
        //! @param that iterator to compare with
        //! @return true, if both iterators point to different indexexs; or refer to different Array2D objects.
        bool operator!= (const iterator& that) const {
            return ! operator==(that);
        }
        //! @return true if the iterator points to a valid index. false, if the index is out of range.
        bool valid() const { return valid(_offset); }
        //! @param off linear offset into Array2D
        //! @return true if the offset points to a valid index. false, if the index is out of range.
        bool valid(int off) const { return (off >= 0) && (off < parent->n * parent->m); }

        //! @return current offset
        int offset() const  { return _offset; }
        //! @return current column index
        int i() const { return _offset / parent->m; }
        //! @return current row index
        int j() const { return _offset % parent->m; }

        //! @return referenc to entry at current index
        T&          operator* ()        { return parent->at(_offset); }
        //! @return pointer to entry at current index
        T*          operator-> ()       { return & parent->at(_offset); }

        //! @brief pre-increment
        //! @return this iterator, after advancing it
        iterator&    operator++ (/*pre-increment*/)       { _offset++; return *this; }
        //! @brief post-increment
        //! @return a copy of this iterator, before advancing it
        iterator     operator++ (int/*post-increment*/)   { iterator it=*this; _offset++; return it; }

        //! @brief pre-decrement
        //! @return this iterator, after advancing it
        iterator&    operator-- (/*pre-increment*/)       { _offset--; return *this; }
        //! @brief post-decrement
        //! @return a copy of this iterator, before advancing it
        iterator     operator-- (int/*post-increment*/)   { iterator it=*this; _offset--; return it; }

        //! @brief move index one row up (increment)
        //! @return this iterator, after advancing it
        iterator&    up()       { _offset++; return *this; }
        //! @brief move index one row down (decrement)
        //! @return this iterator, after advancing it
        iterator&    down()     { _offset--; return *this; }
        //! @brief move index one column to the right (increment)
        //! @return this iterator, after advancing it
        iterator&    right()    { _offset += parent->m; return *this; }
        //! @brief move index one column to the left (decrement)
        //! @return this iterator, after advancing it
        iterator&    left()     { _offset -= parent->m; return *this; }

        //! @brief move iterator to a given index
        //! @param i column index
        //! @param j row index
        //! @return this iterator, after modification
        iterator&    to(int i, int j)   { to(i*parent->m + j); }
        //! @brief move iterator to a given linear offset
        //! @param off linear offset
        //! @return this iterator, after modification
        iterator&    to(int off)        { _offset = off; }

        //! @return pointer to left neighbor entry, or nullptr
        T* leftNeighbor() {
            if (i() > 0)
                return &parent->at(_offset-parent->m);
            else
                return nullptr;
        }
        //! @return pointer to right neighbor entry, or nullptr
        T* rightNeighbor() {
            if (i()+1 < parent->n)
                return &parent->at(_offset+parent->m);
            else
                return nullptr;
        }
        //! @return pointer to top neighbor entry, or nullptr
        T* topNeighbor() {
            if (j()+1 < parent->m)
                return &parent->at(_offset+1);
            else
                return nullptr;
        }
        //! @return pointer to bottom neighbor entry, or nullptr
        T* bottomNeighbor() {
            if (j() > 0)
                return &parent->at(_offset-parent->m);
            else
                return nullptr;
        }

    protected:
        //! @brief constructor with parent and start index
        //! @param aparent parent array
        //! @param start initial index
        //!
        iterator(Array2D* aparent, int start) : parent(aparent), _offset(start) { }
        //! reference to paran
        Array2D* parent;
        //! current offset
        int _offset;
    };
    //! @return an iterator that points to the first entry in this Array2D
    iterator    begin() { return iterator(this,0); }
    //! @return an iterator that points one beyond the last entry. This iterator is invalid, but can be decremented.
    iterator    end()   { return iterator(this,n*m); }

public:
    /**
     * @brief compute linear offset
     * @param i column
     * @param j row
     * @return linear offset into the array
     */
    size_t offset(int i, int j) const {
        Q_ASSERT(i>=0 && i < n && j>=0 && j < m);
        return i*m+j;
    }
    /**
     * @brief compute columnd and row from linear index
     * @param off linear offset
     * @param i holds column index on return
     * @param j holds row index on return
     */
    void indices(int off, int* i, int* j) const {
        Q_ASSERT(off>=0 && off<n*m);
        *i = off/m;
        *j = off%m;
    }
    /**
     * @brief copy a number of entries
     * @param dst destination
     * @param src source
     * @param count number of entries to copy
     */
    static void copy(T* dst, T* src, int count);
};

} }  //  namespace

#include <array2d_impl.h>

#endif // ARRAY2D_H
