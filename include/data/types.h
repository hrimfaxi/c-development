#ifndef DATA_TYPES_H
#define DATA_TYPES_H

#include <QVector>
#include <QPolygon>
#include <QLineF>
#include <cmath>

namespace frechet { namespace data {
/**
 * @brief a point in the plane; with double floating point precision.
 * This type is heavily used throughout all Fréchet distance algorithms.
 */
typedef QPointF Point;
//typedef QLineF Segment;
/**
 * @brief a polygonal curve in the plane; with double floating point precision.
 * This type is heavily used throughout all Fréchet distance algorithms.
 */
typedef QPolygonF Curve;
/**
 * @brief a list of polygonal curves
 */
typedef QVector<Curve> CurveList;

/**
 * @brief a very simple Rectangle structure, with integer boundaries.
 *
 * Left and top boundaries are inclusive.
 * Right and bottom boundaries are exclusive.
 *
 * (Note: we do not use QRect because it is often unclear
 *  whether boundaries are inclusive or exclusive).
 */
struct Rect {
    int i0; //!< left bound, inclusive
    int j0; //!< top bound, inclusive
    int i1; //!< right bound, exclusive
    int j1; //!< top bound, exclusive

    //! @brief empty constructor; creates an undefined rectangle
    Rect() {}
    /**
     * @brief constructor with bounds
     * @param a left bound, inclusive
     * @param b top bound, inclusive
     * @param c right bound, exclusive
     * @param d top bound, exclusive
     */
    Rect(int a, int b, int c, int d) : i0(a),j0(b), i1(c),j1(d) { }

    /**
     * @brief equality comparator
     * @param that another rectangle
     * @return true if both rectangles are equal
     */
    bool operator==(const Rect& that) const {
        return (i0==that.i0) && (j0==that.j0) && (i1==that.i1) && (j1==that.j1);
    }
    /**
     * @return width of the rectangle
     */
    int width() const   { return i1-i0; }
    /**
     * @return height of the rectangle
     */
    int height() const  { return j1-j0; }
};


/**
 * @brief used as identifier for free-space components
 */
typedef unsigned int component_id_t;


} }  // namespace frechet

#endif // DATA_TYPES_H

