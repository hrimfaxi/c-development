
#ifndef FRECHET_VIEW_NUM_ERROR_H
#define FRECHET_VIEW_NUM_ERROR_H

#include <limits>
#include <math.h>
#include <qdebug.h>
#include <types.h>

#include <boost/config.hpp>
#include <boost/math/special_functions/next.hpp>

#ifdef QUAD_ARITHMETIC
/*	Quadruple precision floats, supported by the quadmath library.
	Depends on platform/compiler:
		Windows+MSC = NO
		Windows+INTEL = YES
		Linux+gcc = probably?
		Linux+INTEL = probably?
		MacOS+clang = NO
		MacOS+gcc = NO (but should?)
		MacOS+INTEL = probably?
*/
# if defined(BOOST_INTEL) || defined(__GNUC__)
#   include <boost/multiprecision/float128.hpp>
	typedef boost::multiprecision::float128 float128; 
#else
	//  fall-back to long double, which in turn may fall back to double
#   pragma message ("no float128 support. falling back to double precision.")
	typedef long double float128;
# endif
#endif

/**
	Use increased precision for intermediate values	when calculating Free-Space
	The actual size of 'long double' depends on compiler and/or OS settings
    -	Microsoft Visual C = 64 bit = double (= useless)
    -   Intel C++ compiler = 80 bit
    -	gcc on Linux = 80 bit
    -	clang on macOS = 80 bit
*/
typedef long double accurate_float;


namespace frechet {

using namespace data;

namespace numeric {

//! smallest double value
static const constexpr double double_eps = std::numeric_limits<double>::epsilon();

/**
 *  @brief square function template for arbitrary number types
 *  @param x a number
 *  @tparam Number a number type (like int,double,long double,...)
 *  @returns x*x
 */
template<typename Number>
inline Number sq(const Number& x)   { return x*x; }

/**
 *  @brief square-root function template for floating point types
 *  @param x a number
 *  @tparam Float a floating point type (like double,long double,...)
 *  @returns sqrt(x)
 */
template<typename Float>
inline Float sqrt(const Float& x);

/**
 * @brief default sqrt() specialisation using the run-time libraries
 */
template<>
inline double sqrt(const double& x) { return ::sqrt(x); }

/**
 * @brief sqrt() specialisation for "long double"
 */
template<>
inline long double sqrt(const long double& x) { return ::sqrtl(x); }

/**
 *  @brief abs() function template for arbitrary numerical types
 *  @param x a number
 *  @tparam Number a number type (like int,double,long double,...)
 *  @returns abs(x)
 */
template<typename Number>
inline Number abs(const Number& x) { return (x<0) ? -x : x; }

#if defined(BOOST_MP_USE_FLOAT128) || defined(BOOST_MP_USE_QUAD)
/**
 * @brief sqrt() instantiation for Quad types
 */
template<>
inline float128 sqrt(const float128& x) { return sqrt(x); }

inline const float128& min(const float128& a, const float128& b)
{
    return (a<b) ? a:b;
}
#endif
/**
 * @brief rounding function with 'units in the last place'
 * Roughly speaking, a ulp is the smallest representable floating point value.
 * @param x a value to be rounded
 * @param ulps number of units in the last place to round up
 * @return x rounded upwards by ulps
 */
inline double round_up(double x, int ulps=1) {
    return boost::math::float_advance(x,ulps);
}

/**
 * @brief euclidean distance function template for arbitrary floating point types
 * @param px x-coordinate of first point
 * @param py y-coordinate of first point
 * @param qx x-coordinate of second point
 * @param qy y-coordinate of second point
 * @tparam Float a floating point type (float,double,long double,...)
 * @return the square of the euclidean distance
 */
template<typename Float>
inline Float euclidean_distance_sq(
        const Float& px, const Float& py,
        const Float& qx, const Float& qy)
{
    return sq<Float>(px-qx) + sq<Float>(py-qy);
}

/**
 * @brief euclidean distance function template for arbitrary floating point types
 * @param px x-coordinate of first point
 * @param py y-coordinate of first point
 * @param qx x-coordinate of second point
 * @param qy y-coordinate of second point
 * @tparam Float a floating point type (float,double,long double,...)
 * @return the euclidean distance between p and q
 */
template<typename Float>
inline Float euclidean_distance(
        const Float& px, const Float& py,
        const Float& qx, const Float& qy)
{
    return sqrt<Float>(euclidean_distance_sq<Float>(px,py,qx,qy));
}

/**
 * @brief euclidean distance function template for arbitrary floating point types
 * @param p the first point
 * @param q the second point
 * @tparam Float a floating point type (float,double,long double,...)
 * @return the euclidean distance between p and q
 */
template<typename Float>
inline Float euclidean_distance(const Point& p, const Point& q)
{
    return euclidean_distance<Float>(p.x(),p.y(),q.x(),q.y());
}

/**
 * @brief euclidean distance function template for arbitrary floating point types
 * @param p the first point
 * @param q the second point
 * @tparam Float a floating point type (float,double,long double,...)
 * @return the squared euclidean distance between p and q
 */
template<typename Float>
inline Float euclidean_distance_sq(const Point& p, const Point& q)
{
    return euclidean_distance_sq<Float>(p.x(),p.y(),q.x(),q.y());
}


/**
 * @brief Euclidean Distance between a point and a segment; function template for arbitrary floating point types.
 * @param ax an endpoint of a line segment
 * @param ay an endpoint of a line segment
 * @param bx the second endpoint of a line segment
 * @param by the second endpoint of a line segment
 * @param px a point
 * @param py a point
 * @tparam Float a floating point type (float,double,long double,...)
 * @return the euclidean distance between p and the segment [ab]
 */
template<typename Float>
inline Float euclidean_segment_distance(
        const Float& ax, const Float& ay,
        const Float& bx, const Float& by,
        const Float& px, const Float& py)
{
    Float n = (by-ay)*px - by*ax - (bx-ax)*py + bx*ay;
    Float d = euclidean_distance(ax,ay, bx,by);
    Q_ASSERT(d!=0.0);
    return abs(n) / d;
}

/**
 * @brief Euclidean Distance between a point and a segment; function template for arbitrary floating point types.
 * @param s a line segment
 * @param p a point
 * @tparam Float a floating point type (float,double,long double,...)
 * @return the euclidean distance between p and the segment
 */
template<typename Float>
inline Float euclidean_segment_distance(const QLineF& s, const Point& p)
{
    return euclidean_segment_distance<Float>(
            s.p1().x(), s.p1().y(),
            s.p2().x(), s.p2().y(),
            p.x(),p.y());
}

/**
 * @brief minimum function with checks for NAN
 * @param a a floating point value, may be NAN
 * @param b a floating point value, may be NAN
 * @return the minimum of a and b
 */
inline double min(double a, double b)
{
    return (std::isnan(a) || (b < a)) ? b:a;
}

/**
 * @brief maximum function with checks for NAN
 * @param a a floating point value, may be NAN
 * @param b a floating point value, may be NAN
 * @return the maximum of a and b
 */
inline double max(double a, double b)
{
    return (std::isnan(a) || (b > a)) ? b:a;
}
/**
 * @brief floor function with lower limit
 * @param x a number
 * @param min the minimum return value
 * @return the largest integer properly smaller than x and >= min
 */
inline int lower_floor(double x, int min=0) {
    int i = (int)x;
    if (i==x && i > min) --i;
    return i;
}
/**
 * @brief comparator function
 * @param a a number
 * @param b a number
 * @return +1 if (a>b); -1 if (a<b); 0 if (a==b)
 */
inline int compare(double a, double b)
{
    if (a > b) return +1;
    if (a < b) return -1;
    return 0;
}
/**
 * @brief normalize a vector to unit length
 * @param p a point
 * @return vector scaled to unit length
 */
inline Point normalized(Point p)
{
    double len = sqrt( sq(p.x()) + sq(p.y()) );
    return Point(p.x()/len, p.y()/len);
}
/**
 * @brief compute the intersection of two lines
 * @param p1 a point on the first line
 * @param p2 another point of the first line
 * @param p3 a point on the second line
 * @param p4 another point of the second line
 * @return the intersection of both lines, or NAN if they do not intersect
 */
inline Point intersection(Point p1, Point p2, Point p3, Point p4)
{
    double det = (p1.x()-p2.x())*(p3.y()-p4.y()) - (p1.y()-p2.y())*(p3.x()-p4.x());
    double Px = (p1.x()*p2.y()-p1.y()*p2.x())*(p3.x()-p4.x()) - (p1.x()-p2.x())*(p3.x()*p4.y()-p3.y()*p4.x());
    double Py = (p1.x()*p2.y()-p1.y()*p2.x())*(p3.y()-p4.y()) - (p1.y()-p2.y())*(p3.x()*p4.y()-p3.y()*p4.x());

    return Point(Px/det,Py/det);
}
/**
 * @brief compute a point on a line segment
 * @param p an endppoint of a line segment
 * @param q the second endppoint of the line segment
 * @param w distance from p (w in [0,1])
 * @return the point on the line segment given by the distance w.
 *  Returns p, if w==0. Return q, if w==1.
 *  Returns a point between p and q, if 0 < w < 1.
 */
inline Point between(Point p, Point q, double w)
{
    Q_ASSERT(w>=0 && w<=1.0);
    return p*(1-w) + q*w;
}
/**
 * @brief compute the length of a polygon segment
 * @param c a polygon
 * @param i index of segment
 * @return euclidean length of the i-th polygon segment
 */
inline double segment_length(const Curve& c, int i)
{
    return euclidean_distance<double> (c[i],c[i+1]);
}
/**
 * @brief compute the maximum distance between two polygons
 * This value serves as an upper bound for the Frechet Distance between polyongs.
 *
 * @param P a polygon
 * @param Q another polygon
 * @return maximum distance between two vertexes of the polygons
 */
inline double max_euclidean_distance(const Curve& P, const Curve& Q)
{
    double max=0.0;
    for(int i=0; i < P.size(); ++i)
    {
        const Point& p = P[i];
        for(int j=0; j < Q.size(); ++j)
        {
            double dsq = euclidean_distance_sq<double> (p,Q[j]);
            if (dsq > max) max = dsq;
        }
    }
    return sqrt(max);
}



}}  //  namespace frechet::numeric

#endif //FRECHET_VIEW_NUM_ERROR_H
