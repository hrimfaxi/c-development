#ifndef BITSET_H
#define BITSET_H

#include <stdint.h>

namespace frechet { namespace data {

/**
 *  @brief A simple bit vector of fixed size.
 *
 *  Contrary to standard implementations (like std::bitset, or QBitArray),
 *  this implementation provides an *iterator*.
 *
 * @author Peter Schäfer
 */
class BitSet {
private:
    //! number of valid bits
    int _size;
    //! number of allocated words (1 word = 64 bits)
    int numWords;
    //! bit data
    uint64_t* bits;

public:
    //! @brief default constructor; creates an empty set
    BitSet();
    //! @brief constructor with size
    //! @param asize number of allocated bits; all bits are initialized to false.
    BitSet(int asize);
    //! @brief copy constructor
    //! @param that objec to copy from
    BitSet(const BitSet& that);
    //BitSet(BitSet&& that);
    //! @brief destructor, releases all data
    ~BitSet();
    //! @brief assigment operator
    //! @param that object to copy from
    //! @return this object, after assigning a copy
    BitSet& operator= (const BitSet& that);
    //BitSet& operator= (BitSet&& that);
    //! @brief swap contents with other object
    //! @param that object to copy from
    //! @return this object, after exchanging content with 'that'
    BitSet& swap (BitSet& that);

    //! @return number of valid bits
    int size() const;
    /**
     * @brief This methods is expensive, as it scans the whole array.
     * @return number of set (true) bits
     */
    int count() const;
    //! @brief assign false to all bits
    void clear();

    //! @param offset index into bit set
    //! @return true if the bit at 'offset' is true
    bool contains(int offset) const;
    //! @brief assign true
    //! @param offset index into bit set
    void add(int offset);
    //! @brief assign false
    //! @param offset index into bit set
    void remove(int offset);

    //! @param offset index into bit set
    //! @return true if the bit at 'offset' is true
    bool operator[] (int offset) const  { return contains(offset); }
    //! @brief assign true
    //! @param offset index into bit set
    //! @return this object, after adding a bit
    BitSet& operator+= (int offset)     { add(offset); return *this; }
    //! @brief assign false
    //! @param offset index into bit set
    //! @return this object, after clearing a bit
    BitSet& operator-= (int offset)     { remove(offset); return *this; }

    //! @brief lock-free add operator; not used and not tested!
    void add_lf(int offset);
    //! @brief lock-free add operator; not used and not tested!
    void remove_lf(int offset);


    /**
     * @brief an iterator over a BitSet
     *
     *  Iterates over the set (true) bits of a BitSet.
     *
     * Note that iterators allow for modifications to the underlying BitSet
     * (contrary to some standard iterators).
     */
    class iterator {
    public:
        //! @brief empty constructor; creates an invalid iterator
        iterator();
        //! @brief copy constructor
        //! @param that iterator to copy from
        iterator(const iterator& that);

        //! @brief assignment operator
        //! @param that iterator to copy from
        //! @return this iterator, after assigning a copy
        iterator& operator= (const iterator& that);

        //! @brief equality comparator
        //! @param that iterator to compare with
        //! @return true if both iterators point to the same offset; and refer to the same BitSet
        bool operator== (const iterator& that) const;

        //! @return true if the iterator points to a valid offet
        bool    valid() const;
        //! @return current offset of iterator
        int     operator* () const;

        //! @brief pre-increment; move to the next set (true) bit
        //! @return this iterator, after incrementing
        iterator&    operator++ (/*pre-increment*/);
        //! @brief post-increment; move to the next set (true) bit
        //! @return copy of this iterator, before incrementing
        iterator     operator++ (int/*post-increment*/);

        //! @brief pre-decrement; move to the previous set (true) bit
        //! @return this iterator, after decrementing
        iterator&    operator-- (/*pre-decrement*/);
        //! @brief post-decrement; move to the previous set (true) bit
        //! @return copy of this iterator, before decrementing
        iterator     operator-- (int/*post-decrement*/);

        //! @brief skip operator
        //! @param n number of set bits to skip
        //! @return this iterator after incrementing
        iterator&    operator+= (int n);
        //! @brief skip operator
        //! @param n number of set bits to skip
        //! @return copy of this iterator after incrementing
        iterator     operator+ (int n) const;

        //! @brief skip operator
        //! @param n number of set bits to skip
        //! @return this iterator after decrementing
        iterator&    operator-= (int n);
        //! @brief skip operator
        //! @param n number of set bits to skip
        //! @return copy of this iterator after decrementing
        iterator     operator- (int n);

        //! @brief difference operator
        //! @param that iterator to compare with
        //! @return difference between this and 'that' iterator
        int          operator- (const iterator& that);

    private:
        friend class BitSet;
        //! parent BitSet
        const BitSet* parent;
        //! current offset
        int _offset;
        //! @brief constructor with parent and offset
        //! @param aparent parent BitSet
        //! @param start initial offset
        iterator(const BitSet* aparent,int start);
    };
    //! @return an iterator that points to the first set (true) bit
    iterator    begin() const;
    //! @return an iterator that points one beyond the end of this BitSet. This iterator is invalid, but can be decremented.
    iterator    end() const     { return iterator(this,_size); }

    //! @return an iterator that points to the first set (true) bit
    iterator    first() const   { return begin(); }
    //! @return an iterator that points to the last set (true) bit
    iterator    last() const    {
        iterator it = end();
        if (it._offset > 0) --it;
        return it;
    }

private:
    //!  @brief copy data
    //!  @param that object to copy from
    void copyBits(const BitSet& that);
    //!  @brief steal data
    //!  @param that object to copy from; is empty on return
    void stealBits(BitSet&& that);

    //! @brief find next index with a set (true) bit
    //! @param index start index
    int next(int index) const;
    //! @brief find previos index with a set (true) bit
    //! @param off start index
    int prev(int off) const;

    //! @brief compute number of trailing zeros in a 64 bit word
    //! @param word a 64 bit word
    //! @return number of trailing zeros
    static int numberOfTrailingZeros(uint64_t word);
    //! @brief compute number of leading zeros in a 64 bit word
    //! @param word a 64 bit word
    //! @return number of leading zeros
    static int numberOfLeadingZeros(uint64_t word);
    //! @brief compute number of set bits in a 64 bit word
    //! @param word a 64 bit word
    //! @return number of set bits
    static int bitCount(uint64_t word);
    //! @brief atomic compare-and-swap
    //! @param value pointer to current value
    //! @param old_value expected old value
    //! @param update_value update value
    //! @return actual old value
    static int64_t compare_and_swap(volatile int64_t* value, int64_t old_value, int64_t update_value);
};

} } // namespace

#endif // BITSET_H
