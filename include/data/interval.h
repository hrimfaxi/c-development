#ifndef INTERVAL_H
#define INTERVAL_H

#include <numeric.h>
#include <QDebug>

namespace frechet { namespace data {

/**
 * @brief an interval of two double values.
 *
 * Contains methods for intersection, union, and more.
 *
 * This is one of the most-used basic classes in free-space and reachability algorithms.
 *
 * It is up to the user of an interval object, whether the interval is interpreted as a closed, open or semi-open interval.
 * (e.g. when constructing a free-space diagram, intervals are usually meant to be closed).
 * The Interval class just manages two interval borders, without imposing an interpretation.
 *
 * Invalid intervals are indicated by NAN values.
 *
 * Empty intervals have lower bound == upper bound. Those intervals can be interpreted as empty, or as
 * containing exactly one point.
 *
 * Empty intervals with lower bound > upper are valid to, but are usually to be avoided.
 *
 * Since Interval objects are small (two words) it is perfectly alright to pass them by value.
 *
 * @author Peter Schäfer
 */
class Interval {
private:
    //! lower and upper border of the interval
    double _lower,_upper;
public:
    //! @brief the unit interval [0,1]
    static const Interval UNIT;
    //! @brief an invalid interval (contains NAN values)
    static const Interval INVALID;
public:
    /**
     * @brief Default constructor. Creates an *invalid* Interval.
     */
    Interval() : _lower(NAN),_upper(NAN) {}
    /**
     * @brief Constructor from boundary values.
     * @param lower lower bound
     * @param upper upper bound
     */
    Interval(double lower, double upper) : _lower(lower), _upper(upper) {}
    /**
     * @brief Copy constructor.
     * @param that object to copy from
     */
    Interval(const Interval& that) : _lower(that._lower), _upper(that._upper) {}
    /**
     * @brief Copy constructor with shift offset
     * @param that object to copy from
     * @param shift offset resulting interval by this value
     */
    Interval(const Interval& that,double shift) : _lower(that._lower+shift), _upper(that._upper+shift) {}

    /**
     * @brief size of the interval (upper - lower).
     * @return upper - lower
     */
    double size() const     { return _upper-_lower; }

    /**
     * @brief validity test
     * @return true if both values are valid (!= NAN)
     * @deprecated use operator bool instead
     */
    bool valid() const      { return !std::isnan(_lower) && !std::isnan(_upper); }
    /**
     * @brief empty test
     * @return true, if lower > upper
     */
    bool empty() const      { return _lower > _upper; }
    /**
     * @return true, if valid and not empty
     */
    operator bool () const  { return valid() && !empty(); }
    /**
     * @return true, if not valid or empty
     */
    bool operator! () const { return !valid() || empty(); }

    /**
     * @return the lower bound of the interval
     */
    double lower() const    { return _lower; }
    /**
     * @return the upper bound of the interval
     */
    double upper() const    { return _upper; }

    /**
     * @return an updatable reference to the lower bound
     */
    double& lower()         { return _lower; }
    /**
     * @return an updatable reference to the upper bound
     */
    double& upper()         { return _upper; }
    /**
     * @brief update lower bound
     * @param value new value for lower bound
     * @return this object, after being updated
     */
    Interval& setLower(double value) { _lower=value; return *this; }
    /**
     * @brief update upper bound
     * @param value new value for upper bound
     * @return this object, after being updated
     */
    Interval& setUpper(double value) { _upper=value; return *this; }
    /**
     * @return the middle of the interval, (lower+upper)/2
     */
    double mid() const {
        return (_lower+_upper)/2;
    }

    /**
     * @brief assignment operator
     * @param that interval to copy from
     * @return this interval, after being updated
     */
    Interval& operator= (const Interval& that) {
        return assign(that._lower,that._upper);
    }
    /**
     * @brief equality comparator
     * @return true, if a == b, or if both values are NAN
     */
    inline static bool equals(double a, double b) {
        if (std::isnan(a))
            return std::isnan(b);
        else
            return a==b;
    }

    /**
     * @brief equality comparator
     * @param that interval to compare to
     * @return true, if both intervals are equal
     */
    bool operator== (const Interval& that) const {
        return equals(_lower,that._lower) && equals(_upper,that._upper);
        //return (_lower==that._lower) && (_upper==that._upper);
    }
    /**
     * @brief equalality comparator with tolerance
     * @param that interval to compare to
     * @param precision tolerance
     * @return true, if both intervals are equal with regard to the given tolerance.
     */
    bool equals(const Interval& that, double precision) const {
        return (abs(_lower-that._lower) <= precision) && ((abs(_upper-that._upper) <= precision));
    }
    /**
     * @brief non-equality comparator
     * @param that interval to compare to
     * @return true, if both intervals are not equal
     */
    bool operator!= (const Interval& that) const {
        return ! operator==(that);
    }

    /*
     * */
    /**
     * @brief normalized
     * @return
     */
    Interval    normalized() const {
        if (empty())
            return Interval(_upper,_lower);
        else
            return *this;
    }
    /**
     * @brief make sure that lower <= upper, swapping bounds, if necessary
     * @return this interval, possibly after swapping bounds
     */
    Interval&   normalize() {
        if (empty())
            return assign(_upper,_lower);
        else
            return* this;
    }
    /**
     * @brief make this an invalid interval
     * @return this interval, now invalid
     */
    Interval& clear() {
        _lower = _upper = NAN;
        return *this;
    }
    /**
     * @brief containment test (assumes closed interval, bounds inclusive)
     * @param x a value
     * @return true, if x >= lower and x <= upper.
     */
    bool contains(double x) const {
        return (x >= _lower) && (x <= _upper);
    }

    /**
     * @brief containment test with tolerance (assumes closed interval, bounds inclusive)
     * @param x a value
     * @param precision tolerance
     * @return true, if x >= lower-precision and x <= upper+precision.
     */
    bool contains(double x, double precision) const {
        return (x >= (_lower-precision))
                && (x <= (_upper+precision));
    }
    /**
     * @brief containment test (closed interval, bounds inclusive)
     * @param that interval to test for
     * @return true if that subseteq this
     */
    bool contains(const Interval& that) const {
        return (that._lower >= _lower) && (that._upper <= _upper);
    }

    /**
     * @brief intersection test (closed interval, bounds inclusive)
     * @param that interval to test for
     * @return true if that intersected with this is not empty
     */
    bool intersects(const Interval& that) const {
        return (that._upper >= _lower) && (that._lower <= _upper);
    }

    /**
     * @brief intersection test (open interval, bounds exclusive)
     * @param that interval to test for
     * @return true if that intersected with this is not empty
     */
    bool intersects_proper(const Interval& that) const {
        return (that._upper > _lower) && (that._lower < _upper);
    }

    /**
     * @brief adjacency test
     * @param that interval to compare to
     * @return true, if both intervals are adjacent
     */
    bool bordersTo(const Interval& that) const {
        return (that._upper == _lower) || (that._lower == _upper);
    }
    /**
     * @brief re-map a value from this interval to the unit interval
     * @param x a value (possibly contained in this interval)
     * @return x remapped to the unit interval
     */
    double mapToUnitInterval(double x) const {
        //  map to Unit
        if (size()==0.0)
            return (x-lower());
        else
            return (x-lower()) / size();
    }

    /**
     * @brief re-map a value from the unit interval to this interval
     * @param x a value (possibly contained in this interval)
     * @return x remapped to the this interval
     */
    double mapFromUnitInterval(double x) const {
        return lower() + x*size();
    }

    /**
     * @brief re-map a value to another interval
     * @param x a value (possibly contained in this interval)
     * @param other another interval
     * @return x remapped to the otehr interval
     */
    double mapTo(double x, const Interval& other) const {
        return other.mapFromUnitInterval(mapToUnitInterval(x));
    }

    /**
     * @brief re-map a value from another interval to this interval
     * @param x a value (possibly contained in this interval)
     * @param other another interval
     * @return x remapped to this interval
     */
    double mapFrom(double x, const Interval& other) const {
        return mapFromUnitInterval(other.mapToUnitInterval(x));
    }

    /**
     * @brief shift the interval by a fixed offset
     * @return copy of this interval, shifted by offset
     */
    Interval    operator+ (double offset) const   {
        return Interval(_lower+offset,_upper+offset);
    }
    /**
     * @brief shift the interval by a fixed offset
     * @return this interval, after being shifted by offset
     */
    Interval&   operator+= (double offset) {
        return assign(_lower+offset,_upper+offset);
    }

    /**
     * @brief shift the interval by a fixed offset
     * @return copy of this interval, shifted by offset
     */
    Interval    operator- (double offset) const   {
        return Interval(_lower-offset,_upper-offset);
    }
    /**
     * @brief shift the interval by a fixed offset
     * @return this interval, after being shifted by offset
     */
    Interval&   operator-= (double offset) {
        return assign(_lower-offset,_upper-offset);
    }
    /**
     * @brief multiply by a factor
     * @return copy of this interval, with bounds multiplied
     */
    Interval    operator* (double factor) const   {
        return Interval(_lower*factor,_upper*factor);
    }
    /**
     * @brief multiply by a factor
     * @return this interval, after bounds multiplied
     */
    Interval&   operator*= (double factor) {
        return assign(_lower*factor,_upper*factor);
    }

    /**
     * @brief divide by a factor
     * @return copy of this interval, with bounds updated
     */
    Interval    operator/ (double factor) const   {
        return Interval(_lower/factor,_upper/factor);
    }
    /**
     * @brief multiply by a factor
     * @return this interval, after bounds updated
     */
    Interval&   operator/= (double factor) {
        return assign(_lower/factor,_upper/factor);
    }

    /**
     * @brief union operator
     * @param that another interval
     * @return the union of both intervals
     */
    Interval    operator+ (const Interval& that) const {
        return Interval(
                    numeric::min(_lower,that._lower),
                    numeric::max(_upper,that._upper));
    }

    /**
     * @brief union operator
     * @param that another interval
     * @return this interval, containing the union of both intervals
     */
    Interval&    operator+= (const Interval& that) {
        return assign(
                    numeric::min(_lower,that._lower),
                    numeric::max(_upper,that._upper));
    }

    /**
     * @brief difference operator
     * @param that another interval
     * @return the difference (this-that)
     */
    Interval    operator- (const Interval& that) const {
        return Interval(
                    numeric::max(_lower,that._upper),
                    numeric::min(_upper,that._lower));
    }

    /**
     * @brief difference operator
     * @param that another interval
     * @return this interval, containing the difference (this-that)
     */
    Interval    operator-= (const Interval& that) {
        return assign(
                    numeric::max(_lower,that._upper),
                    numeric::min(_upper,that._lower));
    }

    /**
     * @brief intersection operator
     * @param that another interval
     * @return the intersection of both intervals
     */
    Interval    operator& (const Interval& that) const {
        return Interval(
                    numeric::max(_lower,that._lower),
                    numeric::min(_upper,that._upper));
    }

    /**
     * @brief intersection operator
     * @param that another interval
     * @return this interval, containng the intersection of both intervals
     */
    Interval&    operator&= (const Interval& that) {
        return assign(
                    numeric::max(_lower,that._lower),
                    numeric::min(_upper,that._upper));
    }

    /**
     * @brief lock-free union operator; not used and not tested
     */
    Interval& union_lf(const Interval& that);

private:
    /**
     * @brief assign new bounds
     * @param lower new lower bound
     * @param upper new upper bound
     * @return this interval, with new bounds
     */
    Interval& assign(double lower, double upper) {
        _lower = lower;
        _upper = upper;
        return *this;
    }
    //  sketch for a lock-free union operator
    //! @brief lock-free assigment; not used and not tested
    static void assign_min_lf(volatile double* value, double update_value);
    //! @brief lock-free assigment; not used and not tested
    static void assign_max_lf(volatile double* value, double update_value);
    //! @brief lock-free compare-and-swap; not used and not tested
    static int64_t compare_and_swap(volatile int64_t* value, int64_t old_value, int64_t update_value);
};


/**
 * @brief a pair of horizonal / vertical intervals.
 *
 * Two intervals in separate domains. IntervalPair is heavily used within the k-Fréchet algorithms.
 *
 * @author Peter Schäfer
 */
class IntervalPair
{
public:
    Interval H; //!< horizontal interval
    Interval V; //!< vertical interval

    /**
     * @brief empty constructor; creates a pair of invalid intervals
     */
    IntervalPair() : H(), V() {}
    /**
     * @brief constructor with intervals
     * @param ah a horizontal interval
     * @param av a vertical interval
     */
    IntervalPair(const Interval& ah, const Interval& av)
        : H(ah),V(av)
    { }

    /**
     * @brief calculate the bounding rectangle,
     *  i.e. a rectangle that is defined by the horizontal and vertical intervals
     * @return the bounding rectangle
     */
    QRectF boundingRect() const
    {
        QRectF r;
        r.setCoords(H.lower(),V.lower(), H.upper(),V.upper());
        return r;
    }
    /**
     * @brief equality comparator
     * @param that another pair of intervals
     * @return true, if bouth pairs are equal
     */
    bool operator== (const IntervalPair& that) {
        return H==that.H && V==that.V;
    }
    /**
     * @brief non-equality comparator
     * @param that another pair of intervals
     * @return true, if bouth pairs are not equal
     */
    bool operator!= (const IntervalPair& that) {
        return H!=that.H || V!=that.V;
    }
    /**
     * @brief union operator
     * @param that another pair of intervals
     * @return this interval pair, containing the union of both interval pairs
     */
    IntervalPair& operator+= (const IntervalPair& that)
    {
        H += that.H;
        V += that.V;
        return *this;
    }

    /**
     * @brief clear both intervals (assigning NAN as boundary values)
     * @return this interval pair, now invalid
     */
    IntervalPair& clear()
    {
        H.clear();
        V.clear();
        return *this;
    }
    /**
     * @brief translate both intervals
     * @param i horizontal offset
     * @param j vertical offset
     * @return a copy of this object, shifted by offsets
     */
    IntervalPair translated(double i, double j) const
    {
        return IntervalPair(H+i, V+j);
    }
};

//! @brief operator for printing debug info to a std::ostream
std::ostream& operator <<(std::ostream& stream, const frechet::data::Interval& ival);
//! @brief operator for printing debug info to a std::ostream
std::ostream& operator <<(std::ostream& stream, const frechet::data::IntervalPair& ival);

} }  //  namespace frechet::data

//! @brief operator for printing debug info to a QDebug stream
QDebug operator<< (QDebug debug, const frechet::data::Interval& ival);
//! @brief operator for printing debug info to a QDebug stream
QDebug operator<< (QDebug debug, const frechet::data::IntervalPair& ival);

#endif // INTERVAL_H
