#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <boost/static_assert.hpp>
#include <boost/type_traits.hpp>
#include <list>

namespace frechet { namespace data {

/**
 * @brief base class for elements of a BLinkedList
 *
 * Link references are stored inside the elements.
 *
 * Derived class must not have a destructor!
 * If they do, make sure to define LINKED_LIST_VDESTRUCTOR=1.
 * (slightly increasing the memory footprint of objects)
 *
 * @see BLinkedList, LinkedList
 *
 * @author Peter Schäfer
 */
class BLinkedListElement {
protected:
    //! double links (previous, next)
    BLinkedListElement *link[2];
    friend class BLinkedList;
public:
    //! empty constructor
    BLinkedListElement();

#ifdef LINKED_LIST_VDESTRUCTOR
    /**
     * destructor must be virtual, so that
     * LinkedList can delete derived objects.
     *
     * However, the only derived class (BoundarySegment) needs no
     * destructor at all. So we can save the vptr (which is -indeed- relevant for sizeof).
     * @brief ~LinkeListElement
     */
    virtual ~BLinkedListElement();
#endif
    //! @return reference to previous element in the double-linked-list, or nullptr
    BLinkedListElement*& prev() { return link[0]; }
    //! @return reference to next element in the double-linked-list, or nullptr
    BLinkedListElement*& next() { return link[1]; }
};

/**
 * @brief base template for elements of a LinkedList
 * @tparam T element type; must be derived from BLinkedListElement
 */
template<class T>
class LinkedListElement : public BLinkedListElement {
    //BOOST_STATIC_ASSERT((boost::is_base_of<BLinkedListElement, T>::value));
    //moved to LinkedList<T>
public:
    //! empty constructor
    LinkedListElement() : BLinkedListElement() {}

    //! @return reference to previous element in the double-linked-list, or nullptr
    T* prev() const         { return (T*)link[0]; }
    //! @return reference to next element in the double-linked-list, or nullptr
    T* next() const         { return (T*)link[1]; }

    //! @param forward direction of travel
    //! @return reference to adjacent element in the double-linked-list, or nullptr
    T* next(bool forward)   { return (T*)link[(int)forward]; }
};

class BLinkedList;

/**
 * @brief abstract base class for LinkedList
 */
class BLinkedList {
protected:
    //! the first element in the list (or nullptr)
    BLinkedListElement *_first;
    //! the last element in the list (or nullptr)
    BLinkedListElement *_last;
    //! current number of elements
    int _size;
    //! if true, this is a circular list, i.e. the last element is connected to the first element
    bool _circular;
public:
    /**
     * @brief default constructor; creates an empty list
     * @param circular make this list circular (optional)
     */
    BLinkedList(bool circular=false);
    /**
     * @brief move constructor
     *
     * Note that there is **no copy constructor**
     * because we must prevent shallow copies;
     * double-linked elements must not be part of two separate lists.
     *
     * @param that list to move elements from; will be empty on return
     */
    BLinkedList(BLinkedList&& that);
    /** @brief destructor; releases all element objects
      */
    ~BLinkedList();

    //! @return true if the list is empty
    bool empty() const;
    //! @return number of elements
    int size() const;

    //! @return true if the list is circular
    bool circular() const;
    //! @brief update the circularity property of this list
    void setCircular(bool);

    /**
     * @brief move assigment operator
     *
     * Note that there is **no copy assigment** operator
     * because we must prevent shallow copies;
     * double-linked elements must not be part of two separate lists.
     *
     * @param that list to move elements from; will be empty on return
     * @return this list, containing the elements from 'that'
     */
    BLinkedList& operator= (BLinkedList&& that);

    /**
     * @brief insert a new element at the beginning of the list;
     * the list takes ownership of the element.
     * Do not delete it yourself.
     * @param el a new element
     */
    void insert_first(BLinkedListElement* el);
    /**
     * @brief insert a new element at the end of the list;
     * the list takes ownership of the element.
     * Do not delete it yourself.
     * @param el a new element
     */
    void insert_last(BLinkedListElement* el);

    /**
     * @brief insert a new element before a certain element of the list;
     * the list takes ownership of the element.
     * Do not delete it yourself.
     * @param el a new element
     * @param before place to insert the new element before
     */
    void insert_before(BLinkedListElement* el, BLinkedListElement* before);
    /**
     * @brief insert a new element after a certain element of the list;
     * the list takes ownership of the element.
     * Do not delete it yourself.
     * @param el a new element
     * @param after place to insert the new element after
     */
    void insert_after(BLinkedListElement* el, BLinkedListElement* after);

    /**
     * @brief remove an element from the list and release its memory
     * @param el an element, which must be part of this list
     * @return the successor of the deleted element (may be nullptr)
     */
    BLinkedListElement* remove(BLinkedListElement* el);
    /**
     * @brief release all elements
     */
    void clear();

    /**
     * @brief append all elements of another list, taking ownership of the new elements
     * @param that list to move elements from; will be empty on return
     * @return size of the list after appending
     */
    int concat(BLinkedList& that);
    /**
     * @brief exchange all elements with another list, taking ownership of the new elements
     * @param that list to move elements from
     * @return size of the list after swap
     */
    int swap(BLinkedList& that);

protected:
    /**
     * @brief positional lookup (note: O(n), use with care)
     * @param i index in list
     * @return the i-th element, or nullptr
     */
    BLinkedListElement* operator[] (int i) const;
private:
    //! @brief release all elements and update anchors
    void dispose();
    //! @brief release all elements
    void releaseAll();
    //  shallow copies are not allowed
    //! @brief copy constructor. do not use.
    BLinkedList(const BLinkedList&) { throw "illegal"; }
    //! @brief copy assigment operator. do not use.
    BLinkedList& operator= (const BLinkedList&) { throw "illegal"; }
};

/**
 * @brief An intrusive double-linked list, similar to boost::intrusive.
 *
 * Link references are stored inside the element objects.
 *
 * Simplifies a few things:
 * - small memory footprint (we don't need element container objects)
 * - no iterators are needed: each element contains the information for iterating
 * - concatenating two lists is easy
 *
 *
 *  Major restrictions apply: each element can only be part of **one** list.
 *  Shallow copies are not allowed.
 *  The list takes ownership of elements and deletes them on removal.
 *
 *  The implementation is *not thread-safe*.
 *
 *  Iterator methods (first,next,prev,last) are type-safe.
 *  but insertions are not type-safe (contrary to boost::intrusive classes).
 *
 *  Optionally, LinkedList can be *circular*, i.e. the last element is directly
 *  linked to the first element. This property can be modified an run-time.
 *
 *  Simply use T* for an iterator:
 *
 *  @code
 *  for(T* i = list.first(); i; i = i->next())
 *      ...
 *  @endcode
 *
 *  Modifications to the underlying list do not break the iterator
 *  (except when deleting the current object).
 *
 *  Concurrently iterating and deleting should be done like this:
 *
 *  @code
 *  for(T* i = list.first(); i; ) {
 *      if (...)
 *          i = list.remove(i);
 *      else
 *          i = i->next();
 *  }
 *  @endcode
 *
 * @tparam T element type; must derive from BLinkedListElement
 *
 * @author Peter Schäfer
 */
template<class T>
class LinkedList : public BLinkedList {
    BOOST_STATIC_ASSERT((boost::is_base_of<BLinkedListElement, T>::value));
public:
    //! @brief empty constructor; creates an empty list
    LinkedList() : BLinkedList() {}
    //! @brief move constructor; takes ownership of another list
    //! @param that list to move elements from; will be empty on return
    LinkedList(LinkedList&& that) : BLinkedList((BLinkedList&&)that) {}

    /**
     * @brief deep copy constructor
     * @param that list to copy from
     * @param cloner a functor that creates deep copies of elements
     * @tparam Cloner a functor class that creates deep copies of elements
     */
    template<typename Cloner>
    LinkedList(const LinkedList& that, Cloner cloner) : BLinkedList(that.allocator)
    {
        for(T* p = that.first(); p; p = p->next())
            insert_last(cloner(p));
    }

    /**
     * @brief move assigment operator
     *
     * Note that there is **no copy assigment** operator
     * because we must prevent shallow copies;
     * double-linked elements must not be part of two separate lists.
     *
     * @param that list to move elements from; will be empty on return
     * @return this list, containing the elements from 'that'
     */
    LinkedList& operator= (LinkedList&& that) {
        BLinkedList::operator= ((BLinkedList&&)that);
        return *this;
    }

    /**
     * @return first element of the list (or nullptr if empty)
     */
    T* first() const { return (T*)_first; }
    /**
     * @return last element of the list (or nullptr if empty)
     */
    T* last() const  { return (T*)_last; }

    T* remove(T* el) { return (T*)BLinkedList::remove(el); }

    /**
     * @brief positional lookup (note: O(n), use with care)
     * @param i index in list
     * @return the i-th element, or nullptr
     */
    T* operator[] (int i) const { return (T*) BLinkedList::operator[] (i); }

    /**
     * @brief exchange all elements with another list, taking ownership of the new elements
     * @param that list to move elements from
     * @return size of the list after swap
     */
    void swap(LinkedList<T>& that)  {
        BLinkedList::swap(that);
    }
};


/*
//namespace std {
    template<typename T>
    void swap(LinkedList<T>& a, LinkedList<T>& b) {
        a.swap(b);
    }
//}

namespace std {
    template<typename T>
    void swap(LinkedList<T>& a, LinkedList<T>& b) {
        a.swap(b);
    }
}
*/

} } // namespace

#endif // LINKEDLIST_H
