
#include <interval.h>
#include <iostream>

#ifdef _MSC_VER
#include <intrin.h>
#pragma intrinsic(_InterlockedCompareExchange64)
#endif

using namespace frechet;
using namespace data;

const Interval Interval::UNIT (0.0,1.0);
const Interval Interval::INVALID = Interval ();

/**
 * Sketch for a lock-free union operator
 */
Interval &Interval::union_lf(const Interval &that)
{
    //  Q: do the member variables have to be 'volatile'
    //  do we have to use atomic<double> instead ?
    assign_min_lf(&_lower, that._lower);
    assign_max_lf(&_upper, that._upper);
    return *this;
}

void Interval::assign_min_lf(volatile double *value, double update_value)
{
    double old_value = *value;  //  when reading this value, do we need a memory barrier?
    while (update_value < old_value)
    {
        int64_t prev_ivalue = compare_and_swap(
                    (volatile int64_t*)value,
                    *(int64_t*)&old_value,
                    *(int64_t*)&update_value);

        double prev_value = *(double*)&prev_ivalue;
        if (prev_value == old_value)
            return;
        else
            old_value = prev_value;
    }
}

void Interval::assign_max_lf(volatile double *value, double update_value)
{
    double old_value = *value;  //  when reading this value, do we need a memory barrier?
    while (update_value > old_value)
    {
        int64_t prev_ivalue = compare_and_swap(
                    (volatile int64_t*)value,
                    *(int64_t*)&old_value,
                    *(int64_t*)&update_value);

        double prev_value = *(double*)&prev_ivalue;
        if (prev_value == old_value)
            return;
        else
            old_value = prev_value;
    }
}

int64_t Interval::compare_and_swap(volatile int64_t* value, int64_t old_value, int64_t update_value)
{
#if defined __GNUC__
    return __sync_val_compare_and_swap (value, old_value, update_value);
#elif defined _MSC_VER
    //  Destination = value
    //  Comparand = old_value
    //  Exchange = new value
    //  returns actual value of *Destination
    return _InterlockedCompareExchange64(value,update_value,old_value);
#else
# error
#endif
}

std::ostream& frechet::data::operator <<(std::ostream &stream, const frechet::data::Interval &ival)
{
    return stream << "["<<ival.lower()<<".."<<ival.upper()<<"]";
}

std::ostream& frechet::data::operator <<(std::ostream &stream, const frechet::data::IntervalPair &ival)
{
    return stream << ival.H << " x " << ival.V;
}

QDebug operator<<(QDebug debug, const frechet::data::Interval &ival)
{
    return debug << "["<<ival.lower()<<".."<<ival.upper()<<"]";
}

QDebug operator<<(QDebug debug, const frechet::data::IntervalPair &ival)
{
    return debug << ival.H << " x " << ival.V;
}
