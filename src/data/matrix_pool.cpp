
#include <matrix_pool.h>
#include <clm4rm/clm4rm.h>

#include <concurrency.h>
#include <qdebug.h>

#if defined(__cplusplus) && !defined(MSC_VER)
extern "C" {
#endif
#include <m4ri/mzd.h>
#if defined(__cplusplus) && !defined(MSC_VER)
}
#endif

using namespace frechet;
using namespace data;

MatrixPool::MatrixPool()
	: free_mzd(),free_clmatrix()
{ }

MatrixPool::~MatrixPool() {
	clear();
}

MatrixPool::mzdList& MatrixPool::getMzdFreeList(int rows, int cols) {
	Key k (rows,cols);
	auto i = free_mzd.find(k);
	if (i==free_mzd.end()) {
		i = free_mzd.insert(std::make_pair(k,mzdList())).first;
	}
	return i->second;
}

MatrixPool::clmatrixList& MatrixPool::getClmatrixFreeList(int rows, int cols) {
	Key k (rows,cols);
	auto i = free_clmatrix.find(k);
	if (i==free_clmatrix.end()) {
		i = free_clmatrix.insert(std::make_pair(k,clmatrixList())).first;
	}
	return i->second;
}


mzd_t*		MatrixPool::new_mzd_t(int rows, int cols)
{
	mzd_t* result;
	mzdList& free_list = getMzdFreeList(rows,cols);

	if (! free_list.empty()) {
		result = free_list.back();
		free_list.pop_back();
	}
	else {
		result = new_mzd(rows, cols, nullptr);
		//	Note: already filled with zero
	}

	//mzd_set_ui(result,0);
	Q_ASSERT(result->nrows == rows);
	Q_ASSERT(result->ncols == cols);
	return result;
}

clmatrix_t*	MatrixPool::new_clmatrix_t(int rows, int cols, clm4rm_conditions* cond)
{
	clmatrix_t* result;
	clmatrixList& free_list = getClmatrixFreeList(rows,cols);

	if (! free_list.empty()) {
		result = free_list.back();
		free_list.pop_back();
	}
	else {
		result = new_clmatrix(rows,cols, nullptr, cond);
		//	TODO fill with zero !! ?? !!
	}
	Q_ASSERT(result->nrows == rows);
	Q_ASSERT(result->ncols == cols);
	return result;
}

void MatrixPool::reclaim(mzd_t* M) {
	mzdList& free_list = getMzdFreeList(M->nrows,M->ncols);
	free_list.push_back(M);
}

void MatrixPool::reclaim(clmatrix_t* M) {
	clmatrixList& free_list = getClmatrixFreeList(M->nrows,M->ncols);
	free_list.push_back(M);
}

void MatrixPool::clear() {

	for(auto i : free_mzd)
		for(mzd_t* M : i.second)
			mzd_free(M);

	for(auto i : free_clmatrix)
		for(clmatrix_t* M : i.second)
			clm4rm_free(M);

	free_mzd.clear();
	free_clmatrix.clear();
}


mzd_t* frechet::data::new_mzd(int rows, int cols, MatrixPool* pool) {
	if (pool)
		return pool->new_mzd_t(rows,cols);
	else
		return mzd_init(rows,cols);
}

clmatrix_t* frechet::data::new_clmatrix(int rows, int cols, MatrixPool* pool, clm4rm_conditions* cond) {
	if (pool) {
        return pool->new_clmatrix_t(rows, cols, cond);
    }
	else {
        clmatrix_t* M = clm4rm_create(rows, cols, 32, false, app::ConcurrencyContext::clContext());
        //  fill with zero !?
        clm4rm_zero_fill(M,app::ConcurrencyContext::clQueue(),cond);
        return M;
    }
}

void frechet::data::reclaim(mzd_t* M, MatrixPool* pool) {
	if (pool)
		pool->reclaim(M);
	else
		mzd_free(M);
}

void frechet::data::reclaim(clmatrix_t* M, MatrixPool* pool) {
	if (pool)
		pool->reclaim(M);
	else
		clm4rm_free(M);
}
