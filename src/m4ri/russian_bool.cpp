/*******************************************************************
*
*                 M4RI: Linear Algebra over GF(2)
*
*    Copyright (C) 2007, 2008 Gregory Bard <bard@fordham.edu>
*    Copyright (C) 2008-2010 Martin Albrecht <M.R.Albrecht@rhul.ac.uk>
*
*  Distributed under the terms of the GNU General Public License (GPL)
*  version 2 or higher.
*
*    This code is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*  The full text of the GPL is available at:
*
*                  http://www.gnu.org/licenses/
*
********************************************************************/

#ifdef HAVE_CONFIG_H
#include <m4ri/config.h>
#endif

#if defined(__cplusplus) && ! defined(_MSC_VER)
extern "C" {
#endif
#include <m4ri/russian_bool.h>
#include <m4ri/or.h>
#include <m4ri/graycode.h>
#if defined(__cplusplus) && ! defined(_MSC_VER)
};
#endif

#include <tbb/tbb.h>

#define __M4RI_M4RM_NTABLES 8

typedef mzd_t * lookup_tables_t[__M4RI_M4RM_NTABLES];

//  allocate Lookup Tables
void _mzd_allocate_lookup_tables(
        mzd_t *C, mzd_t const *B, int k,
        lookup_tables_t T, lookup_tables_t Talign)
{
    rci_t const b_nc = B->ncols;
#ifdef __M4RI_HAVE_SSE2
    int c_align = (__M4RI_ALIGNMENT(C->rows[0], 16) == 8);
#endif

    for (int z = 0; z<__M4RI_M4RM_NTABLES; z++) {
#ifdef __M4RI_HAVE_SSE2
        /* we make sure that T are aligned as C */
        Talign[z] = mzd_init(__M4RI_TWOPOW(k), b_nc + m4ri_radix);
        T[z] = mzd_init_window(Talign[z], 0, c_align*m4ri_radix, Talign[z]->nrows, b_nc + c_align * m4ri_radix);
#else
        Talign[z] = NULL;
		T[z] = mzd_init(__M4RI_TWOPOW(k), nr);
#endif
    }
}

void _mzd_free_lookup_tables(lookup_tables_t T)
{
    for (int j = 0; j<__M4RI_M4RM_NTABLES; j++)
        if (T[j])
            mzd_free(T[j]);
}

//  Create table row from two previous rows:
//  T[i] = T[i1] + T[i2]
void mzd_bool_sum_table_row(word* ti, const word* ti1, const word* ti2,
                            wi_t const wide, word const mask_begin, word const mask_end)
{
    *ti++ = (*ti2++ | *ti1++) & mask_begin;

    wi_t j;
    for(j = 1; j + 8 <= wide - 1; j += 8) {
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
    }
    switch(wide - j) {
    case 8:  *ti++ = *ti2++ | *ti1++;
    case 7:  *ti++ = *ti2++ | *ti1++;
    case 6:  *ti++ = *ti2++ | *ti1++;
    case 5:  *ti++ = *ti2++ | *ti1++;
    case 4:  *ti++ = *ti2++ | *ti1++;
    case 3:  *ti++ = *ti2++ | *ti1++;
    case 2:  *ti++ = *ti2++ | *ti1++;
    case 1:  *ti = (*ti2 | *ti1) & mask_end;
    }
}

//  Copy Table row from Matrix
//  T[i] = m
void mzd_bool_copy_table_row(word* ti, const word* m,
                            wi_t const wide, word const mask_begin, word const mask_end)
{
    memcpy(ti, m, sizeof(word)*wide);
    ti[0] &= mask_begin;
    ti[wide-1] &= mask_end;
}

//  return the least significant bit
#define lsb(i) ((i) & -(i))

/**
 * @return the next number with the same number of bits
 */
int _snoob(int i)
{
    int least = lsb(i);
    int ripple = i + least;
    return (((ripple ^ i) >> 2) / least) | ripple;
}


/**
 * The original construction using Gray codes doesn't work in our case
 *  (because we have no subtraction, only addition).
 *
 *  Instead, we construct the entries in order of increasing Hamming weights (number of set bits).
 *  1 bit entries are copied from M, 2 bit entries are sums of two previous rows, etc...
 *
 *  All we need is a simple way to enumerate indexes with the same number of bits.
 *  We are using a bit twiddling trick due to Bill Gosper, originally called HAKMEM ITEM 175,
 *  or known as "snoob".
 *
 @code
 unsigned snoob(unsigned a) {
    unsigned c = (a & -a);
    unsigned r = a+c;
    return (((r ^ a) >> 2) / c) | r);
 }
 @endcode
 *
 * Cute, isn't it?
 */
void mzd_bool_make_table(mzd_t const *M, rci_t r, int k,
                         wi_t offset, wi_t wide,
                         mzd_t *T)
{    
    assert(offset+wide <= M->width);

    word mask_end, mask_begin;
    if (offset+wide == M->width)
        mask_end = __M4RI_LEFT_BITMASK(M->ncols % m4ri_radix);
    else
        mask_end = m4ri_ffff;

    if (wide==1)
        mask_begin = mask_end;
    else
        mask_begin = m4ri_ffff;

    int const twokay = __M4RI_TWOPOW(k);
    rci_t i;

    //  1 bit
    for(int j=0; j < k; ++j)
    {
        i = __M4RI_TWOPOW(j);
        rci_t const rowneeded = r + j;
        assert(rowneeded < M->nrows);

        word *ti = T->rows[i] + offset;
        word *m = M->rows[rowneeded] + offset;

        mzd_bool_copy_table_row(ti,m, wide,mask_begin,mask_end);
    }
    //  2 bits, ...
    for(int h=2; h <= k; ++h) {
        //  iterate all integers with h bits, and < 2^k
        i = __M4RI_TWOPOW(h) - 1;
        for( ; i < twokay; i = _snoob(i)) {
            rci_t least = lsb(i);
            rci_t rest = i-least;

            //  T[least] and T[rest] have already been calculated
            word *ti = T->rows[i] + offset;
            word *ti1 = T->rows[least] + offset;
            word *ti2 = T->rows[rest] + offset;

            mzd_bool_sum_table_row(ti,ti1,ti2, wide,mask_begin,mask_end);
        }
    }

    __M4RI_DD_MZD(T);
}

mzd_t *mzd_bool_mul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int blocksize, int threads) {
    assert(A && B);
  rci_t a = A->nrows;
  rci_t c = B->ncols;

  if(A->ncols != B->nrows)
    m4ri_die("mzd_mul_m4rm: A ncols (%d) need to match B nrows (%d).\n", A->ncols, B->nrows);
  if (C == NULL) {
    C = mzd_init(a, c);
  } else {
    if (C->nrows != a || C->ncols != c)
      m4ri_die("mzd_mul_m4rm: C (%d x %d) has wrong dimensions.\n", C->nrows, C->ncols);
  }
  return _mzd_bool_mul_m4rm(C, A, B, k, TRUE, blocksize, threads);
}

void mzd_optimal_parameters(
        const mzd_t *A, const mzd_t *B,
	    int l2_cache_size, int l3_cache_size,
	    int *k, int *blocksize)
{
	if (k) {
		/* __M4RI_CPU_L2_CACHE == 2^k * B->width * 8 * 8 */
		*k = (int)log2(((double)l2_cache_size / 64) / (double)B->width);
		if ((l2_cache_size - 64 * __M4RI_TWOPOW(*k)*B->width) > (64 * __M4RI_TWOPOW(*k + 1)*B->width - l2_cache_size))
            (*k)++;

		int klog = (int)round(0.75 * log2_floor(A->nrows));
		if (klog < *k)
			*k = klog;

		if (*k < 2)
			*k = 2;
		else if (*k > 8)
			*k = 8;
	}
	if (blocksize) {
//		int cache_size = l3_cache_size;
//		if (concurrency > 1)
//			cache_size /= concurrency;
		/* if multiple instances are running, the L3 cache must be shared (not recommended) */

		*blocksize = MIN(((int)sqrt((double)(4 * l3_cache_size))) / 2, 2048);
		//	TODO where does the row width come in here?
		//	TODO why is it limited to 2048 ?		
	}
}


mzd_t *mzd_bool_mul_uptri(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int blocksize, int threads) {
    assert(A && B);
    rci_t a = A->nrows;

    if (A->nrows != A->ncols || B->nrows != B->ncols)
        m4ri_die("mzd_mul_uptri: A and B must be symmtrical.");   //  or must they not ?

    if(A->ncols != B->nrows)
        m4ri_die("mzd_mul_uptri: A ncols (%d) need to match B nrows (%d).\n", A->ncols, B->nrows);

    if (C == NULL) {
        C = mzd_init(a, a);
    } else {
        if (C->nrows != a || C->ncols != a)
            m4ri_die("mzd_mul_m4rm: C (%d x %d) has wrong dimensions.\n", C->nrows, C->ncols);
    }
    return _mzd_bool_mul_uptri(C, A, B, k, TRUE, blocksize, threads);
}


mzd_t *mzd_bool_addmul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int blocksize, int threads) {
  rci_t a = A->nrows;
  rci_t c = B->ncols;

  if(C->ncols == 0 || C->nrows == 0)
    return C;

  if(A->ncols != B->nrows)
    m4ri_die("mzd_mul_m4rm A ncols (%d) need to match B nrows (%d) .\n", A->ncols, B->nrows);
  if (C == NULL) {
    C = mzd_init(a, c);
  } else {
    if (C->nrows != a || C->ncols != c)
      m4ri_die("mzd_mul_m4rm: C has wrong dimensions.\n");
  }
  return _mzd_bool_mul_m4rm(C, A, B, k, FALSE, blocksize, threads);
}

void _mzd_bool_mul_m4rm_rowstep(
        const mzd_t* A, const mzd_t* C, lookup_tables_t T,
        int k, rci_t i, rci_t j);

mzd_t *_mzd_bool_mul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int clear, int blocksize, int threads)
{
    /**
     * The algorithm proceeds as follows:
     *
     * Step 1. Make a Gray code table of all the \f$2^k\f$ linear combinations
     * of the \f$k\f$ rows of \f$B_i\f$.  Call the \f$x\f$-th row
     * \f$T_x\f$.
     *
     * Step 2. Read the entries
     *    \f$a_{j,(i-1)k+1}, a_{j,(i-1)k+2} , ... , a_{j,(i-1)k+k}.\f$
     *
     * Let \f$x\f$ be the \f$k\f$ bit binary number formed by the
     * concatenation of \f$a_{j,(i-1)k+1}, ... , a_{j,ik}\f$.
     *
     * Step 3. for \f$h = 1,2, ... , c\f$ do
     *   calculate \f$C_{jh} = C_{jh} + T_{xh}\f$.
     */

    lookup_tables_t T;
    lookup_tables_t Talign;

    rci_t const a_nr = A->nrows;
    rci_t const a_nc = A->ncols;
    rci_t const b_nc = B->ncols;

    //    Short-Cut for small matrices
    if (b_nc < m4ri_radix-10 || a_nr < 16) {
        if(clear)
            return mzd_bool_mul_naive(C, A, B, 0);
        else
            return mzd_bool_addmul_naive(C, A, B, 0);
    }
    /* clear first */
    if (clear) {
        mzd_set_ui(C, 0);
    }

    if(k==0)
        mzd_optimal_parameters(A,B, __M4RI_CPU_L2_CACHE, __M4RI_CPU_L3_CACHE, &k, NULL);
    if (k<2)
        k=2;
    else if(k>8)
        k=8;
    if (blocksize==0)
        mzd_optimal_parameters(A,B, __M4RI_CPU_L2_CACHE, __M4RI_CPU_L3_CACHE, NULL, &blocksize);

    _mzd_allocate_lookup_tables(C,B,k, T,Talign);
    /* we make sure that T are aligned as C */

    /* process stuff that fits into multiple of k first, but blockwise (babystep-giantstep)*/
    int const kk = __M4RI_M4RM_NTABLES * k;
    assert(kk <= m4ri_radix);
    rci_t const end = a_nc / kk;
    const word bm = __M4RI_TWOPOW(k)-1;

    for (rci_t giantstep = 0; giantstep < a_nr; giantstep += blocksize)
    {
        const rci_t blockend = MIN(giantstep+blocksize, a_nr);
        for(rci_t i = 0; i < end; ++i)
        {
            if (threads > 1)
            {   //  parallel

                tbb::static_partitioner sp;
//#pragma omp parallel for schedule(static,1) num_threads(threads)
                tbb::parallel_for(0,__M4RI_M4RM_NTABLES, [&](int z) {
                    mzd_bool_make_table( B, kk*i + k*z, k, 0,B->width, T[z]);
                },sp);

//#pragma omp parallel for schedule(static,512) num_threads(threads)
                tbb::parallel_for(giantstep, blockend, [&](rci_t j) {
                    _mzd_bool_mul_m4rm_rowstep(A, C, T, k,i,j);
                },sp);
            }
            else
            {   //  sequential
                for(int z=0; z<__M4RI_M4RM_NTABLES; z++) {
                    mzd_bool_make_table( B, kk*i + k*z, k, 0,B->width, T[z]);
                }

                for(rci_t j = giantstep; j < blockend; j++) {
                    _mzd_bool_mul_m4rm_rowstep(A, C, T, k,i,j);
                }
            }
        }
    }

    /* handle stuff that doesn't fit into multiple of kk */
    const wi_t wide = C->width;
    if (a_nc%kk) {
        rci_t i;
        for (i = kk / k * end; i < a_nc / k; ++i) {
            mzd_bool_make_table( B, k*i, k, 0,B->width, T[0]);
            for(rci_t j = 0; j < a_nr; ++j) {
                word x = mzd_read_bits_int(A, j, k*i, k);
                word *c = C->rows[j];
                word *t = T[0]->rows[x];
                for(wi_t ii = 0; ii < wide; ++ii) {
                    c[ii] |= t[ii];
                }
            }
        }
        /* handle stuff that doesn't fit into multiple of k */
        if (a_nc%k) {
            mzd_bool_make_table( B, k*(a_nc/k), a_nc%k, 0,B->width, T[0]);
            for(rci_t j = 0; j < a_nr; ++j) {
                word x = mzd_read_bits_int(A, j, k*i, a_nc%k);
                word *c = C->rows[j];
                word *t = T[0]->rows[x];
                for(wi_t ii = 0; ii < wide; ++ii) {
                    c[ii] |= t[ii];
                }
            }
        }
    }

    _mzd_free_lookup_tables(T);
    _mzd_free_lookup_tables(Talign);

    __M4RI_DD_MZD(C);
    return C;
}


void _mzd_bool_mul_m4rm_rowstep(
        const mzd_t* A, const mzd_t* C, lookup_tables_t T,
        int k, rci_t i, rci_t j)
{
    int const kk = __M4RI_M4RM_NTABLES * k;
    const word a = mzd_read_bits(A, j, kk*i, kk);
    const word bm = __M4RI_TWOPOW(k)-1;
    word  const *t[__M4RI_M4RM_NTABLES];

    switch(__M4RI_M4RM_NTABLES) {
        case 8: t[7] = T[ 7]->rows[ (a >> 7*k) & bm ];
        case 7: t[6] = T[ 6]->rows[ (a >> 6*k) & bm ];
        case 6: t[5] = T[ 5]->rows[ (a >> 5*k) & bm ];
        case 5: t[4] = T[ 4]->rows[ (a >> 4*k) & bm ];
        case 4: t[3] = T[ 3]->rows[ (a >> 3*k) & bm ];
        case 3: t[2] = T[ 2]->rows[ (a >> 2*k) & bm ];
        case 2: t[1] = T[ 1]->rows[ (a >> 1*k) & bm ];
        case 1: t[0] = T[ 0]->rows[ (a >> 0*k) & bm ];
            break;
        default:
            m4ri_die("__M4RI_M4RM_NTABLES must be <= 8 but got %d", __M4RI_M4RM_NTABLES);
    }

    word *c = C->rows[j];
    wi_t wide = C->width;

    switch(__M4RI_M4RM_NTABLES) {
        case 8: _mzd_bool_combine_8(c, t, wide); break;
        case 7: _mzd_bool_combine_7(c, t, wide); break;
        case 6: _mzd_bool_combine_6(c, t, wide); break;
        case 5: _mzd_bool_combine_5(c, t, wide); break;
        case 4: _mzd_bool_combine_4(c, t, wide); break;
        case 3: _mzd_bool_combine_3(c, t, wide); break;
        case 2: _mzd_bool_combine_2(c, t, wide); break;
        case 1: _mzd_bool_combine(c, t[0], wide);
            break;
        default:
            m4ri_die("__M4RI_M4RM_NTABLES must be <= 8 but got %d", __M4RI_M4RM_NTABLES);
    }
}


#define sq(x) ((x)*(x))

rci_t tri_step(double p, double t, double b1, double b2)
{
    return (rci_t) round(b1 + b2
    - sqrt( sq(t)*(sq(b1+b2)) - p*t*b1*(b1 + 2*b2) ) / t );
}

void _mzd_bool_mul_uptri_rowstep(
        const mzd_t* A, const mzd_t* C, lookup_tables_t T,
        int k, rci_t i, rci_t j);

mzd_t *_mzd_bool_mul_uptri(
	mzd_t *C, mzd_t const *A, mzd_t const *B, 
	int k, int clear, 
	int blocksize, int threads) 
{
    /**
     * The algorithm proceeds as follows:
     *
     * Step 1. Make a Gray code table of all the \f$2^k\f$ linear combinations
     * of the \f$k\f$ rows of \f$B_i\f$.  Call the \f$x\f$-th row
     * \f$T_x\f$.
     *
     * Step 2. Read the entries
     *    \f$a_{j,(i-1)k+1}, a_{j,(i-1)k+2} , ... , a_{j,(i-1)k+k}.\f$
     *
     * Let \f$x\f$ be the \f$k\f$ bit binary number formed by the
     * concatenation of \f$a_{j,(i-1)k+1}, ... , a_{j,ik}\f$.
     *
     * Step 3. for \f$h = 1,2, ... , c\f$ do
     *   calculate \f$C_{jh} = C_{jh} + T_{xh}\f$.
     */

    lookup_tables_t T;
    lookup_tables_t Talign;

    word *c;
    rci_t const nr = A->nrows;
    wi_t offset, wide;
    assert(B->width==C->width);

    //    Short-Cut for small matrices
    if (nr < m4ri_radix-10 || nr < 16) {
        if(clear)
            return mzd_bool_mul_naive(C, A, B, 0);
        else
            return mzd_bool_addmul_naive(C, A, B, 0);
    }
    /* clear first */
    if (clear) {
        mzd_set_ui(C, 0);
    }

    if (k==0)
        mzd_optimal_parameters(A,B, __M4RI_CPU_L2_CACHE, __M4RI_CPU_L3_CACHE, &k,NULL);
    if (k<2)
        k=2;
    else if(k>8)
        k=8;
    if (blocksize==0)
        mzd_optimal_parameters(A,B, __M4RI_CPU_L2_CACHE, __M4RI_CPU_L3_CACHE, NULL,&blocksize);

    _mzd_allocate_lookup_tables(C,B,k, T,Talign);

    /* process stuff that fits into multiple of k first, but blockwise (babystep-giantstep)*/
    int const kk = __M4RI_M4RM_NTABLES * k;
    assert(kk <= m4ri_radix);
    rci_t const end = nr / kk;

    tbb::spin_mutex mutex;

    for (rci_t giantstep = 0; giantstep < nr; giantstep += blocksize)
    {
        offset = giantstep/m4ri_radix;
        wide = B->width - offset;

        for(rci_t i = giantstep/kk; i < end; ++i)
        {
            const rci_t blockend = MIN(giantstep+blocksize, MIN(kk*(i+1),nr));
            if (threads > 1) {
                // parallel loops
//#pragma omp parallel for schedule(static,1) num_threads(threads)
                tbb::static_partitioner sp;
                tbb::parallel_for(0, __M4RI_M4RM_NTABLES, [&] (int z) {
                    mzd_bool_make_table(B, kk*i + k*z, k, offset,wide, T[z]);
                },sp);

                double b1=blockend-giantstep;
                double b2=B->ncols-blockend;

//#pragma omp parallel for schedule(static,1) num_threads(threads)
                tbb::parallel_for(0, threads, [&] (int p) {
                    int j1 = (p==0) ? giantstep : (giantstep+tri_step(p,threads,b1,b2));
                    int j2 = (p==threads-1) ? blockend : (giantstep+tri_step(p+1,threads,b1,b2));

                    for(rci_t j=j1; j < j2; ++j) {
                        _mzd_bool_mul_uptri_rowstep(A, C, T, k, i, j);
                    }
                },sp);
            }
            else {
                //sequential loops
                for(int z=0; z<__M4RI_M4RM_NTABLES; z++) {
                    mzd_bool_make_table(B, kk*i + k*z, k, offset,wide, T[z]);
                }

                for(rci_t j = giantstep; j < blockend; j++) {
                    _mzd_bool_mul_uptri_rowstep(A,C,T,k, i,j);
                }
            }
        }
    }

    /* handle stuff that doesn't fit into multiple of kk */
    if (nr%kk) {
        rci_t i;
        for (i = kk / k * end; i < nr / k; ++i) {
            mzd_bool_make_table( B, k*i, k, 0,B->width, T[0]);
            const rci_t blockend = MIN(k*(i+1),nr);
            for(rci_t j = 0; j < blockend; ++j) {
                word x = mzd_read_bits_int(A, j, k*i, k);

                offset = j/m4ri_radix;
                wide = C->width-offset;

                c = C->rows[j]+offset;
                word* t = T[0]->rows[x]+offset;
                for(wi_t ii = 0; ii < wide; ++ii) {
                    c[ii] |= t[ii];
                }
            }
        }
        /* handle stuff that doesn't fit into multiple of k */
        if (nr%k) {
            //  TODO variable width
            // offset = jmin/m4ri_radix;
            // wide = B->width-offset;
            assert(i==nr/k);
            mzd_bool_make_table( B, k*i, nr%k, 0,B->width, T[0]);
            const rci_t blockend = MIN(k*i+nr%k, nr);
            for(rci_t j = 0; j < blockend; ++j) {
                word x = mzd_read_bits_int(A, j, k*i, nr%k);

                offset = j/m4ri_radix;
                wide = C->width-offset;

                c = C->rows[j]+offset; //  TODO adjust c offset and width
                word* t = T[0]->rows[x]+offset;
                for(wi_t ii = 0; ii < wide; ++ii) {
                    c[ii] |= t[ii];
                }
            }
        }
    }

    _mzd_free_lookup_tables(T);
    _mzd_free_lookup_tables(Talign);

    __M4RI_DD_MZD(C);
    return C;
}

void _mzd_bool_mul_uptri_rowstep(
        const mzd_t* A, const mzd_t* C, lookup_tables_t T,
        int k, rci_t i, rci_t j)
{
    int const kk = __M4RI_M4RM_NTABLES * k;
    const word a = mzd_read_bits(A, j, kk*i, kk);
    const word bm = __M4RI_TWOPOW(k)-1;

    wi_t offset = j/m4ri_radix;
    wi_t wide = C->width - offset;
    word  const *t[__M4RI_M4RM_NTABLES];

    switch(__M4RI_M4RM_NTABLES) {
        case 8: t[7] = T[ 7]->rows[ (a >> 7*k) & bm ] + offset;
        case 7: t[6] = T[ 6]->rows[ (a >> 6*k) & bm ] + offset;
        case 6: t[5] = T[ 5]->rows[ (a >> 5*k) & bm ] + offset;
        case 5: t[4] = T[ 4]->rows[ (a >> 4*k) & bm ] + offset;
        case 4: t[3] = T[ 3]->rows[ (a >> 3*k) & bm ] + offset;
        case 3: t[2] = T[ 2]->rows[ (a >> 2*k) & bm ] + offset;
        case 2: t[1] = T[ 1]->rows[ (a >> 1*k) & bm ] + offset;
        case 1: t[0] = T[ 0]->rows[ (a >> 0*k) & bm ] + offset;
            break;
        default:
            m4ri_die("__M4RI_M4RM_NTABLES must be <= 8 but got %d", __M4RI_M4RM_NTABLES);
    }

    word *c = C->rows[j]+offset;

    switch(__M4RI_M4RM_NTABLES) {
        case 8: _mzd_bool_combine_8(c, t, wide); break;
        case 7: _mzd_bool_combine_7(c, t, wide); break;
        case 6: _mzd_bool_combine_6(c, t, wide); break;
        case 5: _mzd_bool_combine_5(c, t, wide); break;
        case 4: _mzd_bool_combine_4(c, t, wide); break;
        case 3: _mzd_bool_combine_3(c, t, wide); break;
        case 2: _mzd_bool_combine_2(c, t, wide); break;
        case 1: _mzd_bool_combine(c, t[0], wide);
            break;
        default:
            m4ri_die("__M4RI_M4RM_NTABLES must be <= 8 but got %d", __M4RI_M4RM_NTABLES);
    }
}
