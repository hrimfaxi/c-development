
#include <datapath.h>
#include <limits>
using namespace frechet;
using namespace input;
/**
 * Within an SVG file, we look for
 * /svg/g//path/data
 * or
 * /svg//path/data
 */
const QString DataPath::SVG_QUERY (
        "declare default element namespace \"http://www.w3.org/2000/svg\"; "
        "if(/svg/g) "
        "then /svg/g//path/data(@d) "
        "else /svg//path/data(@d)");
// QXmlQuery is peculiar about namespaces.
/**
 * Within an IPE file, we look for
 * //page//path
 */
const QString DataPath::IPE_QUERY (
    "//page//path");

/**
 * Accepted input files:
 * - *.svg, *.ipe
 * - *.js, *.jscript
 */
const QStringList DataPath::INPUT_FILTERS = QStringList()
        << "Input Files (*.svg *.ipe *.js *.jscript)"
        << "Vector Graphics (*.svg *.ipe)"
//              << "Google Earth files (*.kml *.kmz)"
//              << "CSV files (*.csv)"
        << "Scripts (*.js *.jscript)"
        << "Any files (*)";

DataPath::DataPath(QString arg)
{
    int k1 = arg.indexOf(ZIP_SEPARATOR);
    int k2 = arg.indexOf(XML_SEPARATOR);

    if (k1<0) k1=arg.length();
    if (k2<0) k2=arg.length();

    file = arg.mid(0,std::min(k1,k2));
    archFile = arg.mid(std::min(k1,k2)+1,k2);
    selector = arg.mid(k2+1);

    intval = IndexInterval(0, std::numeric_limits<int>::max());
    //  TODO
}
