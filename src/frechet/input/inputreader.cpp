
#include <inputreader.h>

#ifdef HAVE_QUAZIP
#include <quazip.h>
#endif
#include <QXmlQuery>
#include <QXmlResultItems>
#include <QDebug>
#include <QBuffer>
#include <QXmlSerializer>
#include <QXmlFormatter>
#include <QAbstractXmlNodeModel>

//#include <QScriptEngine>

using namespace frechet;
using namespace input;

void InputReader::clear() {
    results.clear();
    error_message.clear();
    is_svg = is_ipe = is_kml = is_script = false;
}

void InputReader::readAll(const DataPath &path)
{
    QString fileSuffix = path.getFile().suffix().toLower();
    error_message = "";
    if (InputReader::isZipFile(fileSuffix)) {
        error_message = "zip not yet supported";
#ifdef HAVE_QUAZIP
        //  navigate into zip file
        QuaZip zip(path.getFile().absoluteFilePath());
        zip.open(QuaZip::Mode::mdUnzip);
        zip.setCurrentFile(path.getArchFile().absolutePath());
        //  TODO if path is empty, and there is exactly one zip file, use it !
        readAll(zip.getIoDevice(), path.getArchFile().suffix().toLower(), path.getSelector());
#endif
    }
    else
    if (InputReader::isGzipFile(fileSuffix)) {
        //  uncompress gzip file
        error_message = "gzip not yet supported";
    }
    else {
        QString filePath = path.getFile().absoluteFilePath();
        QFile input(filePath);
        if(! input.open(QIODevice::ReadOnly))
            qDebug() << input.errorString() << "\n";

        readAll(&input, fileSuffix, path.getSelector());
    }
}

void InputReader::readAll(QIODevice* device, QString fileSuffix, QString queryString)
{
    if (isXmlFile(fileSuffix))
    {
        readXml(device,fileSuffix,queryString);
    }
    else if (isScriptFile(fileSuffix))
    {
        QTextStream stream(device);
        QString program = stream.readAll();
        device->close();

        is_script = true;
        readScript(program);
    }
    else if (isCsvFile(fileSuffix))
    {
        //  read CSV file
        error_message = "csv not yet supported";
    }
}

void InputReader::readXml(QIODevice* device, QString fileSuffix, QString queryString)
{
    //  navigate into xml file
    //  make a XPath query
    QXmlQuery query;
    QXmlResultItems xmlitems;
#ifdef DEBUG_XML
    XmlMessageHandler handler;
    query.setMessageHandler(&handler);
#endif
    if(! query.setFocus(device))
        throw "invalid IO device\n";

    is_svg = isSvgFile(fileSuffix);
    is_ipe = isIpeFile(fileSuffix);

    if (queryString.isEmpty()) {
        if (is_svg)
            queryString = DataPath::SVG_QUERY;
        else if (is_ipe)
            queryString = DataPath::IPE_QUERY;
        else
            throw "query string for type "+fileSuffix+" expected";
    }

    query.setQuery(queryString);
    if(! query.isValid())
        throw "invalid query\n";
    //  if xmlPath is empty, use default paths for known file types (ipe,svg,kml)
    query.evaluateTo(&xmlitems);

    //  parse text data to polygons
    for(QXmlItem item=xmlitems.next(); ! item.isNull(); item=xmlitems.next()) {
        if (xmlitems.hasError())
            throw "Error\n";

        if (item.isAtomicValue()) {
            QVariant value = item.toAtomicValue();
            if (is_ipe)
                results.append(readIpePath(value.toString()));
            else
                results.append(readSvgPath(value.toString()));
            //  else ?
            //qDebug() << item.toAtomicValue() << "\n";
        }
        else if (item.isNode()) {
            QXmlNodeModelIndex nmi = item.toNodeModelIndex();
            //qDebug() << nmi << "\n";
            QTransform tf = readTransformationMatrix(item, query.namePool());
            QString text = nmi.model()->stringValue(nmi);
            if (is_ipe)
                results.append(readIpePath(text,tf));
            else
                results.append(readSvgPath(text,tf));
            //qDebug() << text << "\n";
            //  TODO ipe: <path matrix="2x3"> transformation matrix
        }
    }
}

QTransform InputReader::readTransformationMatrix(QXmlItem item, QXmlNamePool namepool)
{
    //  IMPORTANT
    //  name pool must be inherited from parent query !!!!!
    QXmlQuery query(namepool);

    query.setFocus(item);
    query.setQuery("@matrix/string()");
    if(! query.isValid())
        throw "invalid query\n";

    QString value;
    query.evaluateTo(&value);

    QStringList values = value.split(' ');
    if (values.length()==6)
        return QTransform(
                values[0].toDouble(), values[1].toDouble(),
                values[2].toDouble(), values[3].toDouble(),
                values[4].toDouble(), values[5].toDouble());
    else
        return QTransform();
}

Path InputReader::readSvgPath(QString pathData, QTransform tf)
{
    Path path = Path::svgPath(pathData);
    path.map(tf);
    return path;
}

Path InputReader::readIpePath(QString pathData, QTransform tf)
{
    Path path = Path::ipePath(pathData);
    path.map(tf);
    return path;
}

bool InputReader::isZipFile(QString suffix) {
    return (suffix=="zip") || (suffix=="kmz");
}

bool InputReader::isGzipFile(QString suffix) {
    return suffix=="gz" || suffix=="svgz";
}

bool InputReader::isXmlFile(QString suffix)   {
    return (suffix=="xml") || (suffix=="svg") || (suffix=="ipe") || (suffix=="kml");
}

bool InputReader::isCsvFile(QString suffix) {
    return (suffix=="csv");
}

bool InputReader::isSvgFile(QString suffix) {
    return (suffix=="svg") || (suffix=="svgz");
}

bool InputReader::isIpeFile(QString suffix) {
    return (suffix=="ipe");
}

bool InputReader::isScriptFile(QString suffix) {
    return (suffix=="js") || (suffix=="jscript");
}

void InputReader::XmlMessageHandler::handleMessage(QtMsgType,
                                                 const QString &description,
                                                 const QUrl&,
                                                 const QSourceLocation&)
{
    //qDebug() << description << "\n";
}
 //  namespace frechet
