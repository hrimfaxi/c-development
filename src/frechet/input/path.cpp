#define _USE_MATH_DEFINES
#include <math.h>
#include <path.h>

using namespace frechet;

Path::Path(QObject* parent)
    : QObject(parent),
      points(),opcode(),
      turtle_angle(0.0),
      defaultLineStyle(SOLID),
      lineStyles()
{ }

Path::Path(const Path &that)
    : QObject(that.parent()),
      points(that.points), opcode(that.opcode),
      turtle_angle(that.turtle_angle),
      defaultLineStyle(that.defaultLineStyle),
      lineStyles(that.lineStyles)
{ }

Path::Path(const Path *that)
    : Path(*that)
{ }

Path::Path(QString svg, bool forceAbsolute, QObject* parent)
    : Path(parent)
{
    parseSvg(svg,forceAbsolute);
}

Path Path::svgPath(QString str, bool forceAbsolute)
{
    Path path;
    path.parseSvg(str,forceAbsolute);
    return path;
}

Path Path::ipePath(QString str)
{
    Path path;
    path.parseIpe(str);
    return path;
}

const QPolygonF& Path::toCurve(int precision)
{
    absolutize();
    if (precision > 0)
        setPrecision(precision);
    //  remove identical points (because they create various problems, later)
    removeDuplicates();
    return points;
}

Path& Path::operator=(const Path& that)
{
    points = that.points;
    opcode = that.opcode;
    defaultLineStyle = that.defaultLineStyle;
    lineStyles = that.lineStyles;
    return *this;
}

Path& Path::operator+=(const Path& that)
{
    Q_ASSERT(points.size()==(int)opcode.size());

    points += that.points;
    opcode += that.opcode;

    if (!that.lineStyles.empty()) {
        lineStyles.resize(size());
        lineStyles.insert(lineStyles.end(),
                          that.lineStyles.begin(),that.lineStyles.end());
    }
    // do not copy default line style

    Q_ASSERT(points.size()==(int)opcode.size());
    return *this;
}


Path& Path::operator+=(QString svg)
{
    return (*this) += Path(svg);
}

Path* Path::close() {
    if(!points.empty())
        append(points[0],'M');
    return this;
}

Path* Path::Z() {
    return close();
}

Path* Path::undo() {
    if (!points.empty()) {
        points.pop_back();
        opcode.pop_back();
    }
    return this;
}

Path* Path::ensureClosed() {
    if (points.size() > 2) {
        undo();
        close();
    }
    return this;
}

Path *Path::polar(double angle, double distance) {
    angle = -angle*M_PI/180.0;
    return m( cos(angle)*distance, sin(angle)*distance );
}

Path* Path::map(QTransform tf) {
    absolutize();   //  rotating relative movements would be awful
    points = tf.map(points);
    return this;
}

Path* Path::map(QMatrix mx) {
    return map(QTransform(mx));
}

Path* Path::scale(double ascale) {
    return scale(ascale,ascale);
}

Path* Path::scale(double scalex, double scaley) {
    QTransform tf;
    tf.scale(scalex,scaley);
    return map(tf);
}

Path* Path::rotate(double angle) {
    QTransform tf;
    tf.rotate(angle);
    return map(tf);
}

Path* Path::translate(QPointF offset) {
    return translate(offset.x(),offset.y());
}

Path* Path::translate(double dx, double dy) {
    QTransform tf;
    tf.translate(dx,dy);
    return map(tf);
}

Path* Path::mirrorx() {
    QMatrix matrix(-1,0,0, +1,0,0);
    return map(matrix);
}

Path* Path::mirrory() {
    QMatrix matrix(+1,0,0, -1,0,0);
    return map(matrix);
}


void swap(qreal& a, qreal& b) {
    qreal x = a;
    a=b;
    b=x;
}

void swap(QPointF& a, QPointF& b) {
    swap (a.rx(),b.rx());
    swap (a.ry(),b.ry());
}

Path* Path::revert()
{
    if (!points.empty())
    {
        absolutize();
        QPointF* a = &points.first();
        QPointF* b = &points.last();
        while(a < b)
            swap(*a++,*b--);
    }
    return this;
}

Path *Path::left(double angle) {
    turtle_angle += angle;
    return this;
}

Path *Path::right(double angle) {
    turtle_angle -= angle;
    return this;
}

Path *Path::reset(double angle) {
    turtle_angle = angle;
    return this;
}

Path *Path::forward(double distance) {
    return polar(turtle_angle,distance);
}

void Path::setDefaultLineStyle(int style)
{
    Q_ASSERT(style > DEFAULT && style <= NONE);
    //  DEFAULT := DEFAULT makes no sense
    defaultLineStyle = (LineStyle)style;
}

void Path::lineStyle(int style)
{
    if (size()==0) return;

    Q_ASSERT(style >= 0 && style <= NONE);

    if (size() > lineStyles.size())
        lineStyles.resize(size());
    lineStyles[size()-1] = (LineStyle)style;
}

void Path::outerLineStyle(int style)
{
    lineStyle(style);
    if (lineStyles.size() > 0)
        lineStyles[0] = (LineStyle)style;
}

void Path::parseSvg(QString str, bool forceAbsolute)
{
    //  SVG path string
    //  split string into numbers
    bool x=true;
    bool ok;
    QPointF point;
    QRegExp numexp("(\\-?[0-9]*\\.?[0-9]+(e[+\\-]?[0-9]+)?)|[MmLlVvHhZz]");
    char op='M';
    int pos = numexp.indexIn(str);
    while(pos >= 0)
    {
        int len = numexp.matchedLength();

        QChar qc = str[pos];
        if (qc.isLetter()) {
            if (forceAbsolute) qc = qc.toUpper();
            op = qc.toLatin1();
            x = true;

            switch (op) {
            case 'Z':
            case 'z':   Z(); break;
            }
        }
        else
        {
            double d = str.midRef(pos,len).toDouble(&ok);
            if (ok)
            {
                switch(op)
                {
                case 'M':
                case 'L':
                case 'm':
                case 'l':
                    if (x)
                        point.rx() = d;
                    else
                        point.ry() = d;

                    if (!x) append(point,op);
                    x = !x;
                    break;
                case 'H':
                case 'h':
                    point.rx() = d;
                    //point.ry() = 0.0;
                    append(point,op);
                    break;
                case 'V':
                case 'v':
                    //point.rx() = 0.0;
                    point.ry() = d;
                    append(point,op);
                    break;
                }
            }
        }

        pos = numexp.indexIn(str, pos+len);
    }
}

void Path::parseIpe(QString str)
{
    //  IPE path string
    //
    //  simply a sequence of
    //  x y (m|l)
    //  where all coordinates are absolute
    //
    //  h closes the polygon
    bool x=true;
    bool ok;
    QPointF point;
    QRegExp numexp("(\\-?[0-9]*\\.?[0-9]+(e[+\\-]?[0-9]+)?)|[mlh]");
    char op='M';
    int pos = numexp.indexIn(str);
    while(pos >= 0)
    {
        int len = numexp.matchedLength();

        QChar qc = str[pos];
        if (qc.isLetter()) {
            qc = qc.toUpper();
            op = qc.toLatin1();
            x = true;

            switch (op) {
            case 'H':
                Z(); break;
            case 'M':
            case 'L':
                append(point,op);
                break;
            default:
                Q_ASSERT(false);
                break;
            }
        }
        else
        {   //  number
            double d = str.midRef(pos,len).toDouble(&ok);
            if (ok)
            {
                if (x)
                    point.rx() = d;
                else
                    point.ry() = d;
                x = !x;
            }
        }

        pos = numexp.indexIn(str, pos+len);
    }
}

void Path::append(QPointF p, char op)
{
    Q_ASSERT(points.size()==(int)opcode.size());

    points.push_back(p);
    opcode.push_back(op);

    Q_ASSERT(points.size()==(int)opcode.size());
}

void Path::absolutize()
{
    QPointF ZERO(0,0);
    QPointF* prev=&ZERO;
    QPointF* next;
    for(int i=0; i < opcode.size(); i++)
    {
        next = &points[i];
        switch(opcode[i])
        {
        case 'm': case 'l':
            next->rx() += prev->rx();
            next->ry() += prev->ry();
            break;
        case 'h':
            next->rx() += prev->rx();
            next->ry()  = prev->ry();
            break;
        case 'v':
            next->rx()  = prev->rx();
            next->ry() += prev->ry();
            break;
        case 'H':
            next->ry()  = prev->ry();
            break;
        case 'V':
            next->rx()  = prev->rx();
            break;
        }
        opcode[i] = 'M';
        prev = next;
    }
}

void Path::setPrecision(int precision)
{
    double p = pow(10.0f,precision);
    for(int i=0; i < points.size(); ++i)
    {
        points[i].rx() = floor(points[i].x()*p + 0.5) / p;
        points[i].ry() = floor(points[i].y()*p + 0.5) / p;
    }
}

/** Duplicates are not forbidden per se, but they are
 *  meaningless w.r.t. the Fréchet distance. Besides, duplicates
 *  create confusion in many polygon algorithms, so better remove
 *  them on input.
 */
void Path::removeDuplicates()
{
    for(int i=1; i < points.size(); )
        if (isnan(points[i].x()) || isnan(points[i].y()) || (points[i-1] == points[i])) {
            points.remove(i);
            opcode.erase(i,1);
            if (i < lineStyles.size())
                lineStyles.erase(lineStyles.cbegin()+i);
        }
        else {
            ++i;
        }

}

