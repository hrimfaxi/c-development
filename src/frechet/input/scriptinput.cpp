
#include <inputreader.h>

#include <QJSEngine>
#include <QQmlEngine>
#include <QTextStream>
#include <QDebug>

Q_DECLARE_METATYPE(Path)
Q_DECLARE_METATYPE(Path*)

bool frechet::input::InputReader::readScript(QString program) {

    //QQmlEngine qmlEngine;
    //qmlRegisterType<Path>();
    qmlRegisterType<Path>("de.fuhagen.ti", 1,0, "Path");
    qmlRegisterType<Path>("de.fuhagen.ti", 1,0, "frechet::input::Path");
    //qmlRegisterType<LineStyle>("de.fuhagen.ti", 1,0, "LineStyle");
    //qmlRegisterType<Path*>("de.fugahen.ti", 1.0,"Path");
    //qmlRegisterInterface<Path>("Path");

    QJSEngine engine;
    engine.installExtensions(QJSEngine::ConsoleExtension);
    //  provide a logging facility

    Path P, Q;
    QJSValue global = engine.globalObject();

    global.setProperty("Path", engine.newQMetaObject<Path>());
    //  important for Constructors!!
    global.setProperty("P", engine.newQObject(&P));
    global.setProperty("Q", engine.newQObject(&Q));

    global.setProperty("SOLID", SOLID);
    global.setProperty("THIN", THIN);
    global.setProperty("DOTTED", DOTTED);
    global.setProperty("NONE", NONE);

    QJSValue value = engine.evaluate(program);
    if (value.isError()) {
        //  an error happened
        QTextStream str(&error_message);
        str << "Script Error " << "\n"
                 << " line "<< value.property("lineNumber").toInt() << "\n"
                 << value.toString() << "\n";
        qDebug() << error_message;
        return false;
    }

    Path* pp = dynamic_cast<Path*> (global.property("P").toQObject());
    Path* pq = dynamic_cast<Path*> (global.property("Q").toQObject());
    if (!pp || ! pq) {
        error_message = "P, Q undefined";
        return false;
    }

    results.append(*pp);
    results.append(*pq);

    //  TODO copy lineStyles to grid

    return true;
}
