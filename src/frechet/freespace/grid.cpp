
#include <grid.h>

using namespace frechet;
using namespace data;
using namespace free;

GridAxis::GridAxis()
    : std::vector<Interval>(),
        defaultLineStyle(SOLID),
        lineStyles()
{ }

void GridAxis::setCurve(const frechet::Curve &P)
{
    clear();

    int n = P.size();

    double x = 0.0;
    for(int i=0; i < n-1; ++i) {
        double x2 = x + numeric::segment_length(P,i);
        push_back(Interval(x,x2));
        x = x2;
    }
}

void GridAxis::setLineStyles(const LineStyleVector& styles, LineStyle defaultStyle)
{
    lineStyles = styles;
    Q_ASSERT (defaultStyle != DEFAULT);  // DEFAULT := DEFAULT makes no sense
    defaultLineStyle = defaultStyle;
}

LineStyle GridAxis::lineStyle(int i) const
{
    Q_ASSERT(i >= 0 && i <= size());
    if ((i >= lineStyles.size()) || (lineStyles[i]==DEFAULT))
        return defaultLineStyle;
    else
        return lineStyles[i];
}

double GridAxis::map(double x) const {
    if (empty())
        return 0.0;
    if (x==size())
        return back().upper();
    int i = trunc(x);
    Q_ASSERT(i >= 0 && i < size());
    return (*this)[i].mapFromUnitInterval(x-i);
}

Interval GridAxis::map(const Interval &other) const {
    double lower = map(other.lower());
    double upper = map(other.upper());
    return Interval(lower,upper);
}

double GridAxis::length() const {
    return back().upper();
}

void Grid::setCurves(const Curve &P, const Curve &Q)
{
    _hor.setCurve(P);
    _vert.setCurve(Q);
}

QSizeF Grid::extent() const {
    return QSizeF(
                _hor.empty() ? 0.0 : _hor.back().upper(),
                _vert.empty() ? 0.0 : _vert.back().upper());
}

QRectF Grid::cellBounds(int i, int j) const {
    Q_ASSERT(i>=0 && i < _hor.size());
    Q_ASSERT(j>=0 && j < _vert.size());

    QRectF r;
    r.setCoords(
                _hor[i].lower(), _vert[j].lower(),
                _hor[i].upper(), _vert[j].upper());
    return r;
}

Point Grid::mapPoint(frechet::Point p) {
    double x = _hor.map(p.x());
    double y = _vert.map(p.y());
    return Point(x,y);
}

QLineF Grid::mapLine(QLineF l) {
    Point p1 = mapPoint(l.p1());
    Point p2 = mapPoint(l.p2());
    return QLineF(p1,p2);
}

QRectF Grid::mapRect(QRectF r) {
    Point topleft = mapPoint(r.topLeft());
    Point bottomright = mapPoint(r.bottomRight());
    return QRectF(topleft,bottomright);
}

Curve Grid::mapCurve(Curve c) {
    Curve result;
    foreach(Point p, c)
        result.push_back(mapPoint(p));
    return result;
}

QLineF Grid::verticalGridLine(int i)
{
    QLineF line (i,0.0, i,vert().size());
    return mapLine(line);
}

QLineF Grid::horizontalGridLine(int j)
{
    QLineF line (0.0,j, hor().size(),j);
    return mapLine(line);
}

QRectF Grid::sceneRect() {
    QRectF r;
    r.setCoords(0.0,0.0, hor().size(), vert().size());
    return mapRect(r);
}

Point Grid::mapToPoint(const Curve &C, double x)
{
    Q_ASSERT(C.isClosed() || (x >= 0.0) && (x <= (C.size()-1)));
    if (C.isClosed())
        x = std::fmod(x,C.size()-1);
    int i = (int)floor(x);
    x -= i;
    Point p = C[i % C.size()];
    if (x==0.0) return p;

    Point q = C[(i+1) % C.size()];
    return (1-x)*p + x*q;
}

QLineF Grid::mapToSegment(const Curve &C, double x1, double x2)
{
    return QLineF(mapToPoint(C, x1), mapToPoint(C, x2));
}

Curve Grid::mapToSequence(const Curve& C,  double x1, double x2)
{
    Curve result;
    int i1 = ceil(x1);
    if (x1 < i1)
        result.append(mapToPoint(C,x1));

    for( ; i1 < x2; i1 += 1)
        result.append(mapToPoint(C,i1));

    result.append(mapToPoint(C,x2));
    return result;
}

Curve Grid::mapToSequence(const Curve& C, const std::vector<double>& seq)
{
    Curve result;
    for(int i=0; i < seq.size(); ++i)
        result.push_back(mapToPoint(C,seq[i]));
    return result;
}
