#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <frechetviewapplication.h>
#include <QHBoxLayout>

using namespace frechet;
using namespace view;
using namespace app;
/**
 * Set up UI and wire menu actions to slots.
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->actionAbout->setMenuRole(QAction::AboutRole);
    ui->actionAbout_Qt->setMenuRole(QAction::AboutQtRole);

    splitter = new QSplitter(Qt::Horizontal);
    QIcon wicon(":/frechet-view.svg");
    setWindowIcon(wicon);

    overlayWindow=nullptr;

    curveView = new CurveView(this);
    freeSpaceView = new FreeSpaceView(this);
    freeSpaceView->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    controlPanel = new ControlPanel(this);

    QWidget* leftPane = new QWidget(this);
    QBoxLayout*  vlayout = new QVBoxLayout(leftPane);
    vlayout->setContentsMargins(0,0,0,0);
    vlayout->addWidget(curveView);
    vlayout->addWidget(controlPanel);
    leftPane->setLayout(vlayout);

    splitter->addWidget(leftPane);
    splitter->addWidget(freeSpaceView);
    splitter->setStretchFactor(0,1);
    splitter->setStretchFactor(1,4);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(splitter);

    //setCentralWidget(new QWidget());
    centralWidget()->setLayout(layout);
    addToolBar(Qt::TopToolBarArea, ui->mainToolBar);

    setWindowTitle(tr("Fréchet View"));
    //restoreSettings();
    setUnifiedTitleAndToolBarOnMac(true);

    //ui->actionPrint_Curve->setIcon(QIcon(":/awesome/print.svg"));
    //ui->actionPrint_Free_Space->setIcon(QIcon(":/awesome/print.svg"));

    connect(    ui->actionEdit_Curve, SIGNAL(triggered(bool)),
                FrechetViewApplication::instance(), SLOT(editInputFile()));

    connect(    ui->actionSave_Curve_As, SIGNAL(triggered()),
                curveView, SLOT(saveAs()));

    connect(    ui->actionSave_Free_Space_As, SIGNAL(triggered()),
                freeSpaceView, SLOT(saveAs()));

    connect(    ui->actionPrint_Curve, SIGNAL(triggered()),
                curveView, SLOT(print()));

    connect(    ui->actionPrint_Free_Space, SIGNAL(triggered()),
                freeSpaceView, SLOT(print()));

    connect(    ui->actionShow_Bounds, SIGNAL(triggered(bool)),
                freeSpaceView, SLOT(showBounds(bool)));

    connect(    ui->actionShow_Bounds, SIGNAL(triggered(bool)),
                controlPanel, SLOT(onShowBounds(bool)));

    connect(    ui->actionFull_Screen, SIGNAL(triggered(bool)),
                this, SLOT(enterFullScreen(bool)));

    connect(    ui->actionSeparate_Curves, SIGNAL(triggered(bool)),
                curveView, SLOT(setSeparateCurves(bool)));

    connect(    ui->actionAbout, SIGNAL(triggered(bool)),
                FrechetViewApplication::instance(), SLOT(showAboutDialog()));

    connect(    ui->actionAbout_Qt, SIGNAL(triggered(bool)),
                qApp, SLOT(aboutQt()));

    connect(    ui->actionCreate_Desktop_Icon, SIGNAL(triggered(bool)),
                FrechetViewApplication::instance(), SLOT(createDesktopEntry()));

    connect(    controlPanel,   SIGNAL(epsilonChanged(double)),
                freeSpaceView,  SLOT(calculateFreeSpace(double)));

    connect(    controlPanel, SIGNAL(showGreedyResult()),
                freeSpaceView, SLOT(showGreedyResult()));
    connect(    controlPanel, SIGNAL(showOptimalResult()),
                freeSpaceView, SLOT(showOptimalResult()));
    connect(    controlPanel, SIGNAL(hideResult()),
                freeSpaceView, SLOT(hideResult()));

    connect(    freeSpaceView, SIGNAL(curveFinished(bool)),
                controlPanel, SLOT(onCurveFinished(bool)));

    connect(    curveView, SIGNAL(hiliteSegment(int,int,int)),
                freeSpaceView, SLOT(onHiliteSegment(int,int,int)));
    connect(    freeSpaceView, SIGNAL(hiliteSegment(int,int,int)),
                curveView, SLOT(onHiliteSegment(int,int,int)));

    connect(    FrechetViewApplication::instance(), SIGNAL(fileOpened(QString)),
                controlPanel, SLOT(updatePolyResult()));

    connect(    FrechetViewApplication::instance(), SIGNAL(algorithmChanged(int)),
                curveView, SLOT(onAlgorithmChanged(int)));
    connect(    FrechetViewApplication::instance(), SIGNAL(algorithmChanged(int)),
                freeSpaceView, SLOT(onAlgorithmChanged(int)));

    connect(    ui->actionShow_Animation, SIGNAL(triggered(bool)),
                this, SLOT(showHideAnimation()), Qt::QueuedConnection);
    connect(    ui->actionShow_XAnimation, SIGNAL(triggered(bool)),
                this, SLOT(showHideXAnimation()), Qt::QueuedConnection);
    connect(    ui->actionStart_Animation, SIGNAL(triggered(bool)),
                this, SLOT(startStopAnimation()), Qt::QueuedConnection);
    connect(    ui->actionStart_FSAnimation, SIGNAL(triggered(bool)),
                this, SLOT(startStopFSAnimation()), Qt::QueuedConnection);
    connect(    ui->actionStart_CVAnimation, SIGNAL(triggered(bool)),
                this, SLOT(startStopCVAnimation()), Qt::QueuedConnection);
    connect(    ui->actionStart_ProjectionAnimation, SIGNAL(triggered(bool)),
                this, SLOT(startStopProjectionAnimation()), Qt::QueuedConnection);

}

MainWindow::~MainWindow()
{
    //no need to delete because all objects a children of this
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    FrechetViewApplication::instance()->windowClosed(this);
    QMainWindow::closeEvent(event);
}

QAction* MainWindow::actionOpen()           { return ui->actionOpen; }
QAction* MainWindow::actionOpenRecent()     { return ui->actionOpen_Recent; }
QAction* MainWindow::actionQuit()           { return ui->actionQuit; }

void MainWindow::saveSettings(QSettings& settings)
{
    settings.setValue("main.Window.Geometry", saveGeometry());
    settings.setValue("main.Window.State", saveState());

    settings.setValue("main.Window.Splitter.Geometry", splitter->saveGeometry());
    settings.setValue("main.Window.Splitter.State", splitter->saveState());

    curveView->saveSettings(settings,"view.curve");
    freeSpaceView->saveSettings(settings,"view.freeSpace");
    controlPanel->saveSettings(settings,"controls");
}
/**
 * MacOS Fun Fact: QSettings are stored in /Users/hrimfaxi/Library/Preferences/de.fu-hagen.ti.Frechet View.plist
 *  however, they are CACHED. If you want to reset QSettings, it is not sufficient to delete
 *  the file, you ALSO have to clear the cache:
 *     killall -u yourusername cfprefsd
 */
void MainWindow::restoreSettings(QSettings& settings)
{

    restoreGeometry(settings.value("main.Window.Geometry").toByteArray());
    restoreState(settings.value("main.Window.State").toByteArray());

    splitter->restoreGeometry(settings.value("main.Window.Splitter.Geometry").toByteArray());
    splitter->restoreState(settings.value("main.Window.Splitter.State").toByteArray());

    curveView->restoreSettings(settings,"view.curve");
    freeSpaceView->restoreSettings(settings,"view.freeSpace");
    controlPanel->restoreSettings(settings,"controls");

    setShowBounds(freeSpaceView->showBounds());
    setSeparateCurves(curveView->separateCurves());
    ui->actionFull_Screen->setChecked(this->isFullScreen());
}

void MainWindow::setShowBounds(bool on)
{
    ui->actionShow_Bounds->setChecked(on);
}

void MainWindow::setSeparateCurves(bool on)
{
    ui->actionSeparate_Curves->setChecked(on);
}

void MainWindow::enterFullScreen(bool full)
{
    if (full)
        this->showFullScreen();
    else
        this->showNormal();
}
