
#include <baseview.h>

#include <QtPrintSupport/qtprintsupportglobal.h>
#include <QPrinter>
#include <QPrintDialog>
#include <QPrinterInfo>
#include <QtOpenGL>
#include <QSvgGenerator>
#include <iostream>

#include <qmath.h>
#include <frechet/view/baseview.h>
#include <poly/types.h>
#include <numeric.h>

#undef USE_GLWIDGET

using namespace frechet;
using namespace view;

/**
 * Graphics scene can be stored as:
 * - PDF file (@see saveAsPdf)
 * - SVG file (@see saveAsSvg).
 */
const QStringList BaseView::OUTPUT_FILTERS = QStringList()
        << "Portable Document Files (*.pdf)"
        << "Vector Graphics (*.svg)"
        << "Any files (*)";

const QPen BaseView::PEN_SELECT(QColor(0,144,0,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);

//#if QT_CONFIG(wheelevent)
GraphicsView::GraphicsView(BaseView *v)
    : QGraphicsView(), view(v)
{   //  grab gestures
    grabGesture(Qt::PanGesture);
    grabGesture(Qt::PinchGesture);
}

bool GraphicsView::event(QEvent* event)
{
    switch(event->type())
    {
    case QEvent::Gesture:
        return gestureEvent(static_cast<QGestureEvent*>(event));

    case QEvent::HoverEnter:
    case QEvent::HoverLeave:
    case QEvent::MouseButtonPress:
    case QEvent::GraphicsSceneMousePress:
            std::cout << "break" << std::endl;
            break;


    }
//    if (event->type() == QEvent::NativeGesture)
//        return nativeGestureEvent(static_cast<QNativeGestureEvent*>(event));
    return QGraphicsView::event(event);
}

void GraphicsView::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    view->mouseMoveEvent(event);
    QGraphicsView::mouseMoveEvent(event);
}

void GraphicsView::wheelEvent(QWheelEvent* event)
{
    if (event->modifiers() & Qt::ControlModifier) {
        //  Ctrl-Wheel = Scroll
        QGraphicsView::wheelEvent(event);
    }
    else {
        //  Zoom
        if (event->delta() > 0)
            view->zoomIn(view->zoomStepMouse);
        else
            view->zoomOut(view->zoomStepMouse);
        event->accept();
    }
}

void GraphicsView::keyPressEvent(QKeyEvent* event)
{
    switch(event->key())
    {
    case Qt::Key_Plus:      view->zoomIn(); break;
    case Qt::Key_Minus:     view->zoomOut(); break;
    case Qt::Key_Insert:
    case Qt::Key_0:         view->resetView(); break;

    case Qt::Key_Left:
    case Qt::Key_4:         singleStep(horizontalScrollBar(),-1); break;
    case Qt::Key_Right:
    case Qt::Key_6:         singleStep(horizontalScrollBar(),+1); break;
    case Qt::Key_Up:
    case Qt::Key_8:         singleStep(verticalScrollBar(),-1); break;
    case Qt::Key_Down:
    case Qt::Key_2:         singleStep(verticalScrollBar(),+1); break;

    case Qt::Key_PageUp:
    case Qt::Key_9:         pageStep(verticalScrollBar(),-1); break;
    case Qt::Key_PageDown:
    case Qt::Key_3:         pageStep(verticalScrollBar(),+1); break;
    }
}

bool GraphicsView::gestureEvent(QGestureEvent* event)
{
    QGesture* gest;
    if (gest = event->gesture(Qt::PanGesture)) {
        QPanGesture* pan = static_cast<QPanGesture*>(gest);
        QPointF delta = pan->delta();

        singleStep(horizontalScrollBar(), delta.x());
        singleStep(verticalScrollBar(), delta.y());
        return true;
    }

    if (gest = event->gesture(Qt::PinchGesture))
    {
        QPinchGesture* pinch = static_cast<QPinchGesture *>(gest);
        QPointF center = pinch->centerPoint();
        double scale = pinch->scaleFactor();    //  relative to 1.0        

        if (scale > 1.0)
            view->zoomIn(scale);
        if (scale < 1.0)
            view->zoomOut(1.0/scale);
        // TODO regard Center Point !

        switch(pinch->state())
        {
        case Qt::GestureStarted:
            rot0 = view->rotation();
            //fall-through intended
        case Qt::GestureUpdated:
        case Qt::GestureFinished:
            double angle = pinch->rotationAngle();
            view->setRotation(rot0-angle);
            break;
        }

        return true;
    }

    return false;
}


void frechet::view::singleStep(QAbstractSlider* bar, double factor)
{
    bar->setValue(bar->value() + round(factor*bar->singleStep()));
}

void frechet::view::pageStep(QAbstractSlider* bar, double factor)
{
    bar->setValue(bar->value() + round(factor*bar->pageStep()));
}
//#endif


BaseView::BaseView(QWidget *parent, QString title, bool showRotate)
    : QFrame(parent)
{    
    isFlipped = false;
    zoomStepMouse = 6;
    zoomStepDefault = 1;

    //setFrameStyle(Sunken | StyledPanel);
    graphicsView = new GraphicsView(this);
    graphicsView->setRenderHint(QPainter::Antialiasing, true);
    graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    graphicsView->setOptimizationFlags(QGraphicsView::DontSavePainterState);
#ifdef USE_GLWIDGET
    /*  QGraphicsView can be connected to q OpenGL widget.
     *  Turns out, however, that performance becomes desastrous, compared to normal QWidget.
     *  (Q: isn't OpenGL not already part of the default graphics stack?)
     */
    graphicsView->setViewport(
//                new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering))
                new QOpenGLWidget);
    graphicsView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
#else
    graphicsView->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
#endif
    graphicsView->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
    scene = new QGraphicsScene(graphicsView);
    graphicsView->setScene(scene);

    int size = 20;//style()->pixelMetric(QStyle::PM_ToolBarIconSize);
    QSize iconSize(size, size);
/*
    double dpiRatio = devicePixelRatioF();
    int physdpi = physicalDpiX();
    int logdpi = logicalDpiX();
*/
    QToolButton *zoomInIcon = new QToolButton;
    zoomInIcon->setAutoRepeat(true);
    zoomInIcon->setAutoRepeatInterval(33);
    zoomInIcon->setAutoRepeatDelay(0);
    zoomInIcon->setIcon(QPixmap(":/awesome/search-plus.svg"));
    zoomInIcon->setIconSize(iconSize);    
    QToolButton *zoomOutIcon = new QToolButton;
    zoomOutIcon->setAutoRepeat(true);
    zoomOutIcon->setAutoRepeatInterval(33);
    zoomOutIcon->setAutoRepeatDelay(0);
    zoomOutIcon->setIcon(QPixmap(":/awesome/search-minus.svg"));
    zoomOutIcon->setIconSize(iconSize);
    zoomSlider = new QSlider;
    zoomSlider->setTickPosition(QSlider::NoTicks);
    zoomSlider->setMinimum(0);
    zoomSlider->setMaximum(300);
    zoomSlider->setValue(0);

    zoomStepMouse = 6;
    zoomStepDefault = 1;


    // Zoom slider layout
    QVBoxLayout *zoomSliderLayout = new QVBoxLayout;
    zoomSliderLayout->addWidget(zoomInIcon);
    zoomSliderLayout->addWidget(zoomSlider);
    zoomSliderLayout->addWidget(zoomOutIcon);

    QToolButton *rotateLeftIcon = new QToolButton;
    rotateLeftIcon->setIcon(QPixmap(":/awesome/reply.svg"));
    rotateLeftIcon->setIconSize(iconSize);
    QToolButton *rotateRightIcon = new QToolButton;
    rotateRightIcon->setIcon(QPixmap(":/awesome/reply-right.svg"));
    rotateRightIcon->setIconSize(iconSize);
    rotateSlider = new QSlider;
    rotateSlider->setOrientation(Qt::Horizontal);
    rotateSlider->setMinimum(-360);
    rotateSlider->setMaximum(360);
    rotateSlider->setValue(ROTATE_DEFAULT_VALUE);
    rotateSlider->setTickPosition(QSlider::NoTicks);

    // Rotate slider layout
    QHBoxLayout *rotateSliderLayout = new QHBoxLayout;
    rotateSliderLayout->addWidget(rotateLeftIcon);
    rotateSliderLayout->addWidget(rotateSlider);
    rotateSliderLayout->addWidget(rotateRightIcon);

    resetButton = new QToolButton;
    //resetButton->setText(tr("0"));
    resetButton->setIcon(QPixmap(":/awesome/circle.svg"));
    resetButton->setIconSize(iconSize);
    resetButton->setEnabled(false);

    if (!showRotate) {
        rotateLeftIcon->setVisible(false);
        rotateRightIcon->setVisible(false);
        rotateSlider->setVisible(false);        
    }

#ifndef QT_NO_OPENGL
//    openGlButton->setEnabled(QGLFormat::hasOpenGL());
#else
//    openGlButton->setEnabled(false);
#endif
    QGridLayout *topLayout = new QGridLayout;
//    topLayout->addLayout(labelLayout, 0, 0);
    topLayout->addWidget(graphicsView, 1, 0);
    topLayout->addLayout(zoomSliderLayout, 1, 1);
    topLayout->addLayout(rotateSliderLayout, 2, 0);
    if (showRotate)
        topLayout->addWidget(resetButton, 2, 1);
    else
        zoomSliderLayout->addWidget(resetButton);
    setLayout(topLayout);

    //  Drag Mode
    //graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    graphicsView->setInteractive(false);
    //  Antialiasing
    graphicsView->setRenderHint(QPainter::Antialiasing, true);

    connect(resetButton, SIGNAL(clicked()), this, SLOT(resetView()));
    connect(zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(setupMatrix()));
    connect(rotateSlider, SIGNAL(valueChanged(int)), this, SLOT(setupMatrix()));
    connect(graphicsView->verticalScrollBar(), SIGNAL(valueChanged(int)),
            this, SLOT(setResetButtonEnabled()));
    connect(graphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)),
            this, SLOT(setResetButtonEnabled()));
    connect(rotateLeftIcon, SIGNAL(clicked()), this, SLOT(rotateLeft()));
    connect(rotateRightIcon, SIGNAL(clicked()), this, SLOT(rotateRight()));
    connect(zoomInIcon, SIGNAL(clicked()), this, SLOT(zoomIn()));
    connect(zoomOutIcon, SIGNAL(clicked()), this, SLOT(zoomOut()));

    setupMatrix();
    selected_item = nullptr;
}

QGraphicsView *BaseView::view() const
{
    return static_cast<QGraphicsView *>(graphicsView);
}

void BaseView::resetView()
{
    zoomSlider->setValue(0);
    rotateSlider->setValue(0);
    setupMatrix();
    graphicsView->ensureVisible(QRectF(0, 0, 0, 0));

    resetButton->setEnabled(false);
}

void BaseView::setResetButtonEnabled()
{
    resetButton->setEnabled(true);
}

void BaseView::flipVertical(bool flip)
{
    isFlipped = flip;
    setupMatrix();
}
/**
 *  Dispatch Mouse Hover Events
 *
 *  Note that QGraphicsScene can dispatch hover events, too.
 *  But it is quite unreliable with overlapping items, so we have to do it ourselves.
 *  Besides, we can influence the sensitivity.
 */
void BaseView::mouseMoveEvent(QMouseEvent *event)
{
    //  Map coordinates to QGraphicsScene
    QPoint pv = event->pos();    //  relative to QGraphicsView
    QPointF ps = graphicsView->mapToScene(pv);

    //  Find all close items
    static const int HOVER_DIST = 8;
    QList<QGraphicsItem *> items = graphicsView->items(pv.x()-HOVER_DIST, pv.y()-HOVER_DIST,
                                                       2*HOVER_DIST, 2*HOVER_DIST,
                                                       Qt::IntersectsItemShape);
                                                       // Qt::IntersectsItemBoundingRect
    for(QGraphicsItem* item : items)
    {
        GraphicsHoverLineItem* hover_item = dynamic_cast<GraphicsHoverLineItem*> (item);
        if (hover_item && (hover_item->loc != GraphicsHoverLineItem::None)) // && hover_item->is_close_to(ps,HOVER_DIST))
        {
            if (hover_item != selected_item)
                segmentSelected(selected_item=hover_item);
            return;
        }
    }
    if (selected_item)
        segmentSelected(selected_item=nullptr);
}

QPoint BaseView::mapSceneToGlobal(QPointF ps) const
{
    //  GraphicsView to Viewport coordinates
    QPoint pv = graphicsView->mapFromScene(ps);
    //  Viewport to Widget coordinates
    return graphicsView->mapToGlobal(pv);
}

void BaseView::resizeEvent(QResizeEvent *event)
{
    QFrame::resizeEvent(event);
    setupMatrix();
}

void BaseView::setupMatrix()
{
    double zoomScale = qPow(2.0, zoomSlider->value() / 50.0);

    QMatrix matrix = baseMatrix();
    matrix.scale(zoomScale, zoomScale);
    matrix.rotate(rotateSlider->value());

    graphicsView->setMatrix(matrix);
    setResetButtonEnabled();
}

QMatrix BaseView::baseMatrix()
{
    //  fits the scene rect into the widget
    QRectF sceneRect = scene->sceneRect();
    QRect widgetRect = graphicsView->viewport()->rect();

    double scale = std::min( widgetRect.width() / sceneRect.width(),
                             widgetRect.height() / sceneRect.height());
    QPointF offset = sceneRect.topLeft() - widgetRect.topLeft();

    //  Note: transformation anchor is at the center of the view
    return QMatrix(scale,0, 0,isFlipped ? -scale:+scale, offset.rx()/2, offset.ry()/2);
}

void BaseView::print()
{
//#if QT_CONFIG(printdialog)
    QPrinter printer(QPrinterInfo::defaultPrinter());
    //  Note: default ctor crashed on Windows.
    QPrintDialog dialog(&printer, this);
    if (dialog.exec() == QDialog::Accepted) {
        QPainter painter(&printer);
        render(&painter);
    }
//#endif
}

void BaseView::saveAs()
{
    QSettings settings;
    QFileDialog fdlg;

    fdlg.restoreState(settings.value("filedialog.saveas").toByteArray());
    fdlg.setAcceptMode(QFileDialog::AcceptSave);
    fdlg.setFileMode(QFileDialog::AnyFile);
    fdlg.setNameFilters(OUTPUT_FILTERS);
    fdlg.setDirectory(settings.value("filedialog.saveas.dir").toString());
//    fdlg.setHistory(history);
    QStringList fileNames;
    if (fdlg.exec() == QDialog::Accepted) {
        fileNames = fdlg.selectedFiles();
        if (!fileNames.isEmpty()) {
            QString file = fileNames.first();
            QString suffix = QFileInfo(file).suffix().toLower();
            QString filter = fdlg.selectedNameFilter();

            if (suffix=="pdf")
                saveAsPdf(file);
            else if ((suffix=="svg") || (filter=="svg"))
                saveAsSvg(file);
            else
                saveAsPdf(file);
        }
    }

    settings.setValue("filedialog.saveas",fdlg.saveState());
    settings.setValue("filedialog.saveas.dir",fdlg.directory().absolutePath());
}

void BaseView::saveAsPdf(QString file)
{
    QPrinter printer;
    //  Note: default ctor crashes on Windows in debug mode.
    //  Release mode seems to work.
    printer.setOutputFormat(QPrinter::PdfFormat);
    //printer.setPaperSize(QPrinter::A4); //   ?
    printer.setOutputFileName(file);

    QPainter painter(&printer);
    render(&painter);
}

/**  Note: sadly, clipping seems not to work with QSvgGenerator.
 *   Also, the scene size is not accurate.
 * */
void BaseView::saveAsSvg(QString file)
{
    QSvgGenerator generator;
    QSizeF size = graphicsView->sceneRect().size();
    generator.setFileName(file);
    generator.setSize(QSize(size.width(),size.height()));
    //generator.setViewBox(graphicsView->sceneRect());
    generator.setTitle(windowTitle());
    generator.setDescription("Created by "
                             +QApplication::applicationDisplayName()+" "
                             +QApplication::applicationVersion());

    QPainter painter;
    painter.begin(&generator);
    render(&painter,
           QRectF(QPointF(0,0),size));
}

void BaseView::render(QPainter* painter, QRectF target)
{
    //  map sceneRect to view coordinates
    QRectF r = scene->sceneRect();
    QPoint topLeft = graphicsView->mapFromScene(r.topLeft());
    QPoint botRight = graphicsView->mapFromScene(r.bottomRight());

    QRect source;
    source.setCoords(std::min(topLeft.x(),botRight.x()),
                     std::min(topLeft.y(),botRight.y()),
                     std::max(topLeft.x(),botRight.x()),
                     std::max(topLeft.y(),botRight.y()));
    //  avoid rendering invisible (unused) items.
    //  they will be re-created on demand
    dropUnusedItems();
    graphicsView->render(painter,target,source);
}




void BaseView::zoomIn(int level)
{
    if (level <= 0) level = zoomStepDefault;
    zoomSlider->setValue(zoomSlider->value() + level);
}

void BaseView::zoomOut(int level)
{
    if (level <= 0) level = zoomStepDefault;
    zoomSlider->setValue(zoomSlider->value() - level);
}

void BaseView::saveSettings(QSettings& settings, QString group)
{
    settings.beginGroup(group);

    int zoom = zoomSlider->value();
    int rot = rotateSlider->value();
    int scroll_h = graphicsView->horizontalScrollBar()->value();
    int scroll_v = graphicsView->verticalScrollBar()->value();

    int max_h = graphicsView->horizontalScrollBar()->maximum();
    int max_v = graphicsView->verticalScrollBar()->maximum();

    settings.setValue("zoom", zoom);
    settings.setValue("rotate", rot);

    settings.setValue("scroll.horiz", scroll_h);
    settings.setValue("scroll.vert", scroll_v);

    settings.endGroup();
}

void BaseView::restoreSettings(QSettings& settings, QString group)
{
    settings.beginGroup(group);

    int zoom = settings.value("zoom",0).toInt();
    int rot = settings.value("rotate",ROTATE_DEFAULT_VALUE).toInt();
    int scroll_h = settings.value("scroll.horiz",0).toInt();
    int scroll_v = settings.value("scroll.vert",0).toInt();

    zoomSlider->setValue(zoom);
    rotateSlider->setValue(rot);

    int max_h = graphicsView->horizontalScrollBar()->maximum();
    int max_v = graphicsView->verticalScrollBar()->maximum();

    graphicsView->horizontalScrollBar()->setValue(scroll_h);
    graphicsView->verticalScrollBar()->setValue(scroll_v);

    settings.endGroup();

    setupMatrix();
}

void BaseView::rotateLeft(double angle)
{
    setRotation(rotation()-angle);
}

void BaseView::rotateRight(double angle)
{
    setRotation(rotation()+angle);
}

int BaseView::rotation() const {
    return rotateSlider->value();
}

void BaseView::setRotation(double angle) {
    while (angle > 306) angle -= 360;
    while (angle < -360) angle += 360;

    if (rotateSlider->isVisible())
        rotateSlider->setValue(round(angle));
}


void BaseView::setPenWidth(QGraphicsItem* item, double width)
{
    QAbstractGraphicsShapeItem* shape = dynamic_cast<QAbstractGraphicsShapeItem*>(item);
    if (shape) {
        QPen pen = shape->pen();
        if (pen.widthF() != width) {
            pen.setWidthF(width);
            shape->setPen(pen);
        }
    }
    QGraphicsLineItem* line = dynamic_cast<QGraphicsLineItem*>(item);
    if (line) {
        QPen pen = line->pen();
        if (pen.widthF() != width) {
            pen.setWidthF(width);
            line->setPen(pen);
        }
    }

    for(const auto& i : item->childItems())
        setPenWidth(i,width);
}

bool equals(QPointF a, QPointF b) {
    //  Note that QPointF::operator== compares with tolerance.
    //  We don't.
    return (a.x()==b.x()) && (a.y()==b.y());
}

void BaseView::addLine(QPainterPath& ppath, const QLineF& line)
{
    if (equals(line.p1(),line.p2())) {
        Q_ASSERT(false);
        //  this indicates most likely a broken homeomorphism
        // (though this is not the best place to decide)
        addPoint(ppath, line.p1());
    }
    else {
        ppath.moveTo(line.p1());
        ppath.lineTo(line.p2());
    }
}

void BaseView::addPolygon(QPainterPath &ppath, const QPolygonF &poly)
{
    switch(poly.size())
    {
    case 0:     break;
    case 1:     {
                //Q_ASSERT(false);
                //  this indicates most likely a broken homeomorphism
                // (though this is not the best place to decide)
                addLine(ppath,QLineF(poly[0], poly[0]+QPointF(1e-9,1e-9))); }
                break;
    case 2:     addLine(ppath,QLineF(poly[0],poly[1])); break;
    default:    ppath.addPolygon(poly); break;
    }
}

void BaseView::addPoint(QPainterPath &ppath, const QPointF &point, double diameter)
{
    ppath.addEllipse(point, diameter,diameter);
}

double normalizedValue(QAbstractSlider* slider)
{
    int min = slider->minimum();
    int max = slider->maximum();
    int val = slider->value();

    return (double)(val-min) / (max-min);
}

void setNormalizedValue(QAbstractSlider* slider, double nval)
{
    int min = slider->minimum();
    int max = slider->maximum();
    int val = min + nval*(max-min);

    slider->setValue(val);
}

GraphicsHoverLineItem::GraphicsHoverLineItem(QLineF line,
                                             Location aloc, int aa, int bb,
                                             QGraphicsItemGroup* group)
: QGraphicsLineItem(line,group), loc(aloc), a(aa), b(bb)
{
    group->addToGroup(this);
}

void GraphicsHoverLineItem::update(int aa, int bb) {
    const_cast<int&>(a) = aa;
    const_cast<int&>(b)=bb;
}

bool GraphicsHoverLineItem::is_close_to(QPointF scene_pos, int distance) const
{
    QPointF q = mapFromScene(scene_pos);
    double dist;
    if (line().length()==0.0)
        dist = numeric::euclidean_distance<double>(line().p1(),q);
    else
        dist = numeric::euclidean_segment_distance<double>(line(),q);
    return dist <= distance;
}


