
#include <intervalview.h>
#include <freespaceview.h>

#include <QPen>
#include <QGraphicsScene>

using namespace frechet;
using namespace data;
using namespace view;

//const double IntervalView::ROW_HEIGHT = 3.0;
//const QPen IntervalView::LINE_PEN (Qt::black,2.0,Qt::SolidLine,Qt::FlatCap);

IntervalView::IntervalView(frechet::Grid::ptr agrid,
                           IntervalView::Orientation orient,
                           Palette* apalette,
                           double pen_width,
                           QGraphicsItem* parent)
    :   QGraphicsItemGroup(parent),
        orientation(orient),
        grid(agrid),
        stack(), stack_watermark(0),
        palette(apalette), itemPool()
{
    LINE_PEN = QPen(Qt::black,pen_width,Qt::SolidLine,Qt::FlatCap);
    ROW_HEIGHT = pen_width*3/2;
}

void IntervalView::clear() {
    stack.clear();
    //  remove all children
    foreach(QGraphicsItem* item, childItems())
        releaseItem(item);
}

QGraphicsLineItem* IntervalView::createItem()
{
    if (itemPool.isEmpty())
        return new QGraphicsLineItem(this);
    else
        return itemPool.takeFirst();
}

void IntervalView::releaseItem(QGraphicsItem* item)
{
    removeFromGroup(item);
    scene()->removeItem(item);
    itemPool.append((QGraphicsLineItem*)item);
}


void IntervalView::add(const Interval& ival, size_t component) {
    if (!ival)
        return;

    Interval gval;
    //  map to view coordinates
    if (orientation==HORIZONTAL)
        gval = grid->hor().map(ival);
    else
        gval = grid->vert().map(ival);
    //  convert to boost interval
    boost_ival bval(gval.lower(),gval.upper());

    //  add a QGraphicsLine
    QGraphicsLineItem* line = createItem();
    line->setLine(insert(bval));
    line->setData(0,(int)component);
    setItemColor(line, (*palette)[component]);

    addToGroup(line);
}

void IntervalView::add(const frechet::k::MappedInterval& mval)
{
    add(mval, mval.componentID);
}

void IntervalView::addAll(const frechet::k::IntervalMap& mvals)
{
    for(frechet::k::IntervalMap::const_iterator i = mvals.begin(); i != mvals.end(); ++i)
        add(*i);
}



void IntervalView::setItemColor(QGraphicsLineItem* item, QColor col)
{
    QPen pen = LINE_PEN;
    pen.setColor(col);
    item->setPen(pen);
}

void IntervalView::showResult(const BitSet* resultSet)
{
    foreach(QGraphicsItem* line, childItems())
    {
        size_t component = line->data(0).toUInt();
        setItemColor((QGraphicsLineItem*)line,
                     !resultSet || resultSet->contains(component) ?
                        (*palette)[component] : FreeSpaceView::LIGHT_GRAY);
    }
}


QLineF IntervalView::insert(const boost_ival& ival)
{
    int row = findRow(ival);
    stack[row].insert(ival);
    if (orientation==HORIZONTAL)
        return QLineF(
                    ival.lower(), -(row+1)*ROW_HEIGHT,
                    ival.upper(), -(row+1)*ROW_HEIGHT);
    else
        return QLineF(
                    -(row+1)*ROW_HEIGHT, ival.lower(),
                    -(row+1)*ROW_HEIGHT, ival.upper());
}

int IntervalView::findRow(const boost_ival& ival)
{
    //  find a free location in the stack
    for(int row=0; row < stack.size(); ++row)
    {
        if (ival.lower()==ival.upper())
        {
            if (stack[row].find(ival.lower()) == stack[row].end())
                return row;
        }
        else
        {
            if (stack[row].find(ival) == stack[row].end())
                return row;
        }
    }
    //  create a new row
    stack.push_back(boost::icl::interval_set<double>());
    if (stack.size() > stack_watermark)
        stack_watermark = stack.size();
    return stack.size()-1;
}
