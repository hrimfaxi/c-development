
#include <palette.h>
//#include <QRandomGenerator>
#include <QDebug>
#include <random>

using namespace frechet;
using namespace view;

std::mt19937 gen; //Standard mersenne_twister_engine seeded with rd()

std::uniform_int_distribution<> disHue(0, 360);
std::uniform_int_distribution<> disSat(200, 255);
std::uniform_int_distribution<> disLight(80, 140);

QColor Palette::randomColor() {
    return toColor(disHue(gen), disSat(gen), disLight(gen));
}

QColor Palette::nextColor(size_t key) {
    /*  colors should look random,
     *  but still be deterministic (w.r.t. key)
     * */
    gen.seed(key);
    //gen.discard(key);
    //gen.seed(gen());
    return randomColor();
}

QColor Palette::toColor(int x, int sat, int light) {
    return QColor::fromHsl(abs(x) % 360, sat,light);
}


Palette::Palette() : map()
{ }

QColor Palette::operator[](size_t key)
{
    typename ColorMap::const_iterator i = map.find(key);
    if (i==map.end()) {
        QColor col = nextColor(key);
        map.insert(key,col);
        //qDebug() << "Palette: " << key << "=" << col.hslHue() << "\n";
        return col;
    }
    else {
        return *i;
    }
}

void Palette::clear() {
    map.clear();
}

