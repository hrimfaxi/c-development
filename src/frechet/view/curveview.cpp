
#include <curveview.h>
#include <frechetviewapplication.h>
#include <poly_path.h>

using namespace frechet;
using namespace view;
using namespace app;
using namespace poly;
using namespace reach;

const QPen CurveView::PEN_A(QColor(255,0,0,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
const QPen CurveView::PEN_B(QColor(0,0,255,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);

const QPen CurveView::PEN_C_DIAGS(QColor(255,255,0,180), 2.0,      Qt::DashLine, Qt::RoundCap, Qt::RoundJoin);
const QPen CurveView::PEN_T_DIAGS(QColor(180,180,255,180), 2.0,      Qt::DotLine, Qt::RoundCap, Qt::RoundJoin);
const QPen CurveView::PEN_PASSIVE_DIAGS(QColor(180,180,180,180), 2.0,   Qt::DashLine, Qt::RoundCap, Qt::RoundJoin);

const double CurveView::SEPARATOR = 0.06;

CurveView::CurveView(QWidget *parent)
    : BaseView(parent,"Curve"),
      polygon_a(nullptr), polygon_b(nullptr),
      diagonals_a(nullptr), diagonals_b(nullptr),
      select_a(nullptr), select_b(nullptr),
      _separate(false)
{
    view()->setMouseTracking(true);
    view()->viewport()->setMouseTracking(true);
}

void CurveView::populateScene(
        const Curve& a, const Curve& b,
        poly::Algorithm::ptr poly)
{
    //graphicsView->setInteractive(true); //  we ant to process mouse click events
    scene->clear();    
    polygon_a = addPolygonToScene(a,PEN_A,GraphicsHoverLineItem::P);
    polygon_b = addPolygonToScene(b,PEN_B,GraphicsHoverLineItem::Q);
    //  common bounding box
    polygon_bounds = a.boundingRect().united(b.boundingRect());

    /* P: t-Diagonalen (Triangulation außer c-Diagonalen) = */
    diagonals_a = addSegmentsToScene(nullptr,
                       poly->PTriangulation().edges(),
                       poly->c_diagonals(), true,
                       PEN_T_DIAGS, GraphicsHoverLineItem::Pdiag);
    /* P: nur c-Diagonalen */
    addSegmentsToScene(diagonals_a,
                       poly->c_diagonals(),
                       false,
                       PEN_C_DIAGS, a, GraphicsHoverLineItem::Pdiag);
    /* Q: alle Diagonalen außer äußeren Kanten */
    diagonals_b = addSegmentsToScene(nullptr,
                       poly->QTriangulation().edges(),
                       Segments(), false,
                       PEN_PASSIVE_DIAGS, GraphicsHoverLineItem::Qdiag);
/*
 * QGraphicsItemGroup* group,
        frechet::poly::Triangulation::Edge_range d,
        const frechet::poly::Segments& exclude,
        bool exclude_edges,
        QPen pen
        */
    polygon_a->setZValue(+2);   // to front
    polygon_b->setZValue(+2);   // to front

    diagonals_a->setZValue(+1);
    diagonals_b->setZValue(+1);
    //  Note: Z-Value has bad influence on hoverability !

    select_a = new QGraphicsPathItem();
    select_b = new QGraphicsPathItem();

    select_a->setPen(PEN_SELECT);
    select_b->setPen(PEN_SELECT);

    select_a->setVisible(false);
    select_b->setVisible(false);

    select_a->setZValue(+3);
    select_b->setZValue(+3);

    polygon_a->addToGroup(select_a);
    polygon_b->addToGroup(select_b);

    //  set pen width relative to scene size
    double pen_width =  std::max(polygon_bounds.width(),polygon_bounds.height()) * 0.005;
    setPenWidth(polygon_a,pen_width);
    setPenWidth(polygon_b,pen_width);

    setPenWidth(diagonals_a,pen_width);
    setPenWidth(diagonals_b,pen_width);

    setPenWidth(select_a,3*pen_width);
    setPenWidth(select_b,3*pen_width);

    setSeparateCurves(_separate);

    //  TODO show (c- and t-) diagonals

//    scene->setSceneRect(polygon_bounds);
//    graphicsView->setSceneRect(polygon_bounds);
}

QGraphicsItemGroup* CurveView::addPolygonToScene(const Curve& poly, QPen pen, GraphicsHoverLineItem::Location hover)
{
    QGraphicsItemGroup* group = new QGraphicsItemGroup();
    group->setAcceptHoverEvents(hover);
    group->setHandlesChildEvents(false);    //  Children handle their mouse events
    for(int i=0; i+1 < poly.size(); ++i) {
        QLineF line(poly[i],poly[i+1]);
        createLineItem(line,pen, Segment(i,i+1), group, hover);
    }
    scene->addItem(group);
    return group;
}

QGraphicsItemGroup* CurveView::addSegmentsToScene(
        QGraphicsItemGroup* group,
        Triangulation::Edge_range dr,
        const Segments& exclude,
        bool with_edges,
        QPen pen, GraphicsHoverLineItem::Location hover)
{
    if (group==nullptr) {
        group = new QGraphicsItemGroup();
        group->setAcceptHoverEvents(hover);
        group->setHandlesChildEvents(false);    //  Children handle their mouse events
        scene->addItem(group);
    }
    for(auto d = dr.first; d != dr.second; ++d)
    {
        if (d.is_outer_edge()) continue;    //  always ignore
        if (exclude.find(d.segment()) != exclude.end()) continue;
        if (d.is_polygon_edge() && !with_edges) continue;

        QLineF line = d.line();
        createLineItem(line,pen, d.segment(), group,hover);
    }
    return group;
}

QGraphicsItemGroup* CurveView::addSegmentsToScene(
        QGraphicsItemGroup* group,
        const Segments& d,
        bool with_edges,
        QPen pen,
        const Curve& P, GraphicsHoverLineItem::Location hover)
{
    if (group==nullptr) {
        group = new QGraphicsItemGroup();
        group->setAcceptHoverEvents(hover);
        group->setHandlesChildEvents(false);    //  Children handle their mouse events
        scene->addItem(group);
    }
    for(const Segment& s : d)
    {
        if (s.is_edge(P.size()-1) && !with_edges) continue;

        QLineF line = Grid::mapToSegment(P,s.first,s.second);
        createLineItem(line,pen, s, group,hover);
    }
    return group;
}

QGraphicsLineItem* CurveView::createLineItem(QLineF line, QPen pen,
                                            Segment seg,
                                            QGraphicsItemGroup* group,
                                            GraphicsHoverLineItem::Location hover)
{
    QGraphicsLineItem* item = new GraphicsHoverLineItem(line,hover,seg.first,seg.second,group);
    item->setPen(pen);
    //item->setAcceptHoverEvents(hover);
    //group->addToGroup(item);
    return item;
}


void CurveView::saveSettings(QSettings& settings, QString group)
{
    settings.beginGroup(group);
    settings.setValue("separate.curves",_separate);
    settings.endGroup();

    BaseView::saveSettings(settings,group);
}

void CurveView::restoreSettings(QSettings& settings, QString group)
{
    settings.beginGroup(group);
    _separate = settings.value("separate.curves",false).toBool();
    settings.endGroup();

    BaseView::restoreSettings(settings,group);

    setSeparateCurves(_separate);
}

void CurveView::setSeparateCurves(bool on)
{
    _separate=on;

    if (!polygon_a || !polygon_b) return;

    QRectF ra = polygon_a->boundingRect();
    QRectF rb = polygon_b->boundingRect();

    QPointF offseta, offsetb;

    if (_separate) {
        //  Stack vertical, or horizontal
        //  choose minimal aspect ratio
        double sep_hor = SEPARATOR * std::max(ra.width(),rb.width());
        double sep_vert = SEPARATOR * std::max(ra.height(),rb.height());

        double ar_hor = (ra.width()+rb.width()+sep_hor) / std::max(ra.height(),rb.height());
        double ar_vert = (ra.height()+rb.height()+sep_vert) / std::max(ra.width(),rb.width());

        if (ar_hor < ar_vert) {
            //  stack horizontal. Move rects to...
            offseta = QPointF(-ra.width()-sep_hor/2, -ra.height()/2) - ra.topLeft();
            offsetb = QPointF(sep_hor/2, -rb.height()/2) - rb.topLeft();
        }
        else {
            //  stack vertical
            offseta = QPointF(-ra.width()/2, -ra.height()-sep_vert/2) - ra.topLeft();
            offsetb = QPointF(-rb.width()/2, sep_vert/2) - rb.topLeft();
        }

    }

    QTransform ta = QTransform::fromTranslate(offseta.x(),offseta.y());
    QTransform tb = QTransform::fromTranslate(offsetb.x(),offsetb.y());

    polygon_a->setTransform(ta);
    polygon_b->setTransform(tb);

    diagonals_a->setTransform(ta);
    diagonals_b->setTransform(tb);

    onAlgorithmChanged(0);  //  update visibility of diagonals

    //  adjust scene rect
    polygon_bounds = ra.translated(offseta.x(),offseta.y())
            .united(rb.translated(offsetb.x(),offsetb.y()));
    scene->setSceneRect(polygon_bounds);
    graphicsView->setSceneRect(polygon_bounds);
    setupMatrix();
}

void CurveView::segmentSelected(GraphicsHoverLineItem *item)
{
    const reach::FSPath::ptr fspath = FrechetViewApplication::instance()->getFSPath();
    if (!fspath || fspath->empty())
        item = selected_item = nullptr;

    if (item) {
        doHiliteSegment(item->loc,item->a,item->b);
        emit hiliteSegment(item->loc, item->a,item->b);
    }
    else {
        doHiliteSegment(GraphicsHoverLineItem::None, -1,-1);
        emit hiliteSegment(GraphicsHoverLineItem::None, -1,-1);
    }
}

void CurveView::onHiliteSegment(int loc, int a, int b)
{
    doHiliteSegment((GraphicsHoverLineItem::Location)loc, a,b);
}

void CurveView::doHiliteSegment(GraphicsHoverLineItem::Location loc, int a, int b)
{
    const FSPath::ptr fspath = FrechetViewApplication::instance()->getFSPath();
    if (!fspath || fspath->empty())
        loc = GraphicsHoverLineItem::None;

    const poly::Algorithm::ptr polyAlgo = FrechetViewApplication::instance()->getPolyAlgorithm();
    const Curve& P = FrechetViewApplication::instance()->getP();
    const Curve& Q = FrechetViewApplication::instance()->getQ();
    QLineF line;

    QPainterPath path_a, path_b;
    Curve mapped[2];

    switch(loc) {
    case GraphicsHoverLineItem::P:
        //  Map from P  to Q
        line = Grid::mapToSegment(P,a,b);
        Q_ASSERT(line.p1()!=line.p2());
        addLine(path_a,line);

        fspath->mapFromP(poly::Segment(a,b),mapped);
        fspath->mapToQ(mapped);
        addPolygon(path_b,mapped[0]);
        break;

    case GraphicsHoverLineItem::Q:
        //  Map from Q to P
        line = Grid::mapToSegment(Q,a,b);
        Q_ASSERT(line.p1() != line.p2());
        addLine(path_b,line);

        fspath->mapFromQ(poly::Segment(a,b),mapped);
        fspath->mapToP(mapped);
        addPolygon(path_a,mapped[0]);
        break;

    case GraphicsHoverLineItem::Pdiag: {
        line = Grid::mapToSegment(P, a, b);
        addLine(path_a, line);
        //  finc shortest path in Q
        poly::PolygonFSPath *polyPath = dynamic_cast<poly::PolygonFSPath *>(fspath.get());
        Q_ASSERT(polyPath);
        Curve sp = polyPath->findShortestPathQ(a, b, polyAlgo->QTriangulation());
        addPolygon(path_b, sp);
    }   break;

    case GraphicsHoverLineItem::Qdiag: {
        line = Grid::mapToSegment(Q, a, b);
        addLine(path_b, line);
        //  finc shortest path in P
        poly::PolygonFSPath *polyPath = dynamic_cast<poly::PolygonFSPath *>(fspath.get());
        Q_ASSERT(polyPath);
        Curve sp = polyPath->findShortestPathP(a, b, polyAlgo->displayPTriangulation());
        addPolygon(path_a, sp);
    }   break;

    case GraphicsHoverLineItem::FS: {
        Curve p = fspath->getPath(a);
        Point p1 = p[b];
        Point p2 = p[b+1];
        QLineF l1 = Grid::mapToSegment(P,p1.x(),p2.x());
        QLineF l2 = Grid::mapToSegment(Q,p1.y(),p2.y());
        addLine(path_a,l1);
        addLine(path_b,l2);
    }   break;

    case GraphicsHoverLineItem::None:
        //  clear selection
        break;

    default:
        Q_ASSERT(false);
    }

    select_a->setPath(path_a);
    select_b->setPath(path_b);

    select_a->setVisible(loc != GraphicsHoverLineItem::None);
    select_b->setVisible(loc != GraphicsHoverLineItem::None);
}

void CurveView::onAlgorithmChanged(int algorithm)
{
    bool poly_alg = (FrechetViewApplication::instance()->currentAlgorithm()
                     == FrechetViewApplication::ALGORITHM_POLYGON);

    if (diagonals_a)
        diagonals_a->setVisible(poly_alg && _separate);
    if (diagonals_b)
        diagonals_b->setVisible(poly_alg && _separate);

    if (select_a)
        select_a->setVisible(false);
    if (select_b)
        select_b->setVisible(false);
}


