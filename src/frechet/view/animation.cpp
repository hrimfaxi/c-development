#include <mainwindow.h>
#include <ui_mainwindow.h>


#include <QDialog>
#include <QDebug>
#include <QPropertyAnimation>
#include <QGraphicsSvgItem>
#include <QSequentialAnimationGroup>
#include <QParallelAnimationGroup>
#include <QPauseAnimation>

#include <animation.h>
#include <frechetviewapplication.h>

using namespace frechet;
using namespace data;
using namespace k;
using namespace view;
using namespace app;

void MainWindow::showHideAnimation(bool fspath)
{
    if (overlayWindow!=nullptr && overlayWindow->isVisible()) {
        //  Hide
        overlayWindow->stopAnimation();
        overlayWindow->setVisible(false);
    }
    else {
        //  Show
        if (! overlayWindow)
            overlayWindow = new AnimationDialog(this);

        overlayWindow->activate();
        overlayWindow->resetAnimation(fspath);
    }
}

void MainWindow::startStopAnimation()
{
    if (overlayWindow && overlayWindow->isVisible()) {
        overlayWindow->startStopAnimation();
    }
}

void MainWindow::startStopFSAnimation()
{
    if (overlayWindow) {
        double startValue = getControlPanel()->getEpsilon();
        overlayWindow->resetFSAnimation(startValue,startValue+10,5*1000);
        overlayWindow->startStopAnimation();
    }
}

void MainWindow::startStopCVAnimation()
{
    if (overlayWindow) {
        poly::Algorithm::ptr algo = FrechetViewApplication::instance()->getPolyAlgorithm();
        algo->collectCriticalValues(true,true,true,false);
        const std::vector<double>& cvlist = algo->currentCriticalValueList();

        double epsilon = getControlPanel()->getEpsilon();

        double startValue = *(std::lower_bound(cvlist.begin(),cvlist.end(), epsilon)-1);
        double endValue = *std::upper_bound(cvlist.begin(),cvlist.end(), epsilon);

        overlayWindow->resetFSAnimation(startValue,endValue,1000);
        overlayWindow->startStopAnimation();
    }
}

void MainWindow::startStopProjectionAnimation()
{
    if (overlayWindow) {
        QPointF x (3.8,3.3);
        overlayWindow->resetProjectionAnimation(x);
        overlayWindow->startStopAnimation();
    }
}

const QPen AnimationDialog::LEASH_PEN(QColor(0,144,0), 6.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
//const QPen AnimatedGroup::INDICATOR_PEN(QColor(0,144,0,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
const QPen AnimationDialog::PEN_P(QColor(255,0,0), 10.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
const QPen AnimationDialog::PEN_Q(QColor(0,0,255), 10.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
const QPen AnimationDialog::PROJ_PEN(QColor(0,0,0), 2.0, Qt::DashLine, Qt::RoundCap, Qt::RoundJoin);

const QFont AnimationDialog::TEXT_FONT("Palatino Linotype",12.0f);

AnimationDialog::AnimationDialog(QMainWindow *parent) {
    setParent(parent);
    //setModal(true);
    //setWindowModality(Qt::ApplicationModal);
    //setWindowFlags(Qt::FramelessWindowHint);
    QRect geom = parent->rect();
    setGeometry(geom);
    setTransparent(this);

    setupUI(geom);
}

void AnimationDialog::setTransparent(QWidget* widget)
{
/*
    widget->setAutoFillBackground(false);
    widget->setAttribute(Qt::WA_NoSystemBackground, true);
    widget->setAttribute(Qt::WA_TranslucentBackground, true);
    widget->setAttribute(Qt::WA_PaintOnScreen);
*/
    widget->setAttribute(Qt::WA_TransparentForMouseEvents);
/*  widget->setWindowOpacity(0.1);// better: 0.001
*/
    widget->setStyleSheet("background: transparent");
}

void AnimationDialog::activate() {
    show();
    raise();
    activateWindow();
}

double AnimationDialog::gridLength(QPolygonF segment) const
{
    if (segment.isEmpty())
        return 0.0;

    frechet::Grid::ptr grid = FrechetViewApplication::instance()->getGrid();
    double len = 0.0;
    QPointF pg,qg;
    pg = grid->mapPoint(segment[0]);

    for(int j=1; j < segment.size(); ++j) {
        qg = grid->mapPoint(segment[j]);
        len += frechet::euclidean_distance<double>(pg,qg);
        pg = qg;
    }
    return len;
}

double AnimationDialog::gridLength() const
{
    frechet::reach::FSPath::ptr fspath = FrechetViewApplication::instance()->getFSPath();
    return  gridLength(fspath->getPath(0)) +
            gridLength(fspath->getPath(1));
}

void AnimationDialog::resetAnimation(bool from_fspath)
{
    gview->scene()->clear();

    if (from_fspath)
    {
        switch(FrechetViewApplication::instance()->currentAlgorithm())
        {
        case FrechetViewApplication::ALGORITHM_CURVE:
        case FrechetViewApplication::ALGORITHM_POLYGON:
            this->animation = createManDogAnimation();
            break;
        case FrechetViewApplication::ALGORITHM_K_FRECHET:
            this->animation = createKangarooAnimation();
            break;
        }
    }
    else {
        this->animation = createAxesAnimation();
    }
}


void AnimationDialog::resetFSAnimation(double startValue, double endValue, int duration)
{
    MainWindow* mainWindow = (MainWindow*)parent();
    QPropertyAnimation* anim = new QPropertyAnimation(this);
    anim->setStartValue(startValue);
    anim->setEndValue(endValue);
    anim->setTargetObject(mainWindow->getControlPanel());
    anim->setPropertyName("epsilon");
    anim->setDuration(duration);

    this->animation = anim;
}

void AnimationDialog::resetProjectionAnimation(QPointF x)
{
    QSequentialAnimationGroup* seqgroup = new QSequentialAnimationGroup();
    MainWindow* mainWindow = (MainWindow*)parent();

    //  1. paint x
    ProjectionGroup* proj = new ProjectionGroup(mainWindow,gview,x);
    QPauseAnimation* pause = seqgroup->addPause(1000);
    connect(pause,SIGNAL(finished()), proj,SLOT(showHV()));

    //  2. project to axes
    QParallelAnimationGroup* pargroup = new QParallelAnimationGroup();
    pargroup->addAnimation( proj->createAxisProjection(false,2000) );
    pargroup->addAnimation( proj->createAxisProjection(true,2000) );
    seqgroup->addAnimation(pargroup);

    //  3. paint p,q
    connect(pargroup,SIGNAL(finished()), proj,SLOT(showPQ()));
    seqgroup->addPause(1000);

    //  4. move p,q
    pargroup = new QParallelAnimationGroup();
    pargroup->addAnimation( proj->createMovePoint(false,2000) );
    pargroup->addAnimation( proj->createMovePoint(true,2000) );
    seqgroup->addAnimation(pargroup);

    //  5.  paint leash
    pause = seqgroup->addPause(1000);
    connect(pause,SIGNAL(finished()), proj,SLOT(showD()));
    //pause = seqgroup->addPause(1000);
    //connect(pause,SIGNAL(finished()), proj,SLOT(showText()));

    //  (6.  paint \delta <= \epsilon)

    this->animation = seqgroup;
}

QAbstractAnimation* AnimationDialog::createManDogAnimation()
{
    frechet::FreeSpace::ptr fs = FrechetViewApplication::instance()->getFreeSpace();
    frechet::reach::FSPath::ptr fspath = FrechetViewApplication::instance()->getFSPath();
    frechet::Grid::ptr grid = FrechetViewApplication::instance()->getGrid();

    double len = 0.0;
    double total_len = gridLength();

    FeasiblePathAnimation* anim = new FeasiblePathAnimation(this);
    anim->setDuration(16*1000);
    anim->setKeyValues(QPropertyAnimation::KeyValues());

    QPointF p,q, pg,qg;
    QPointF start;

    QPolygonF path[2];
    path[0] = fspath->getPath(0);
    path[1] = fspath->getPath(1);
    /*  If one curve is closed, but not the other,
     *  we must carefully choose the starting point.
     *
     *  If both curves are not closed, there is only one path.
     *  If both curves are closed, it doesn't matter.
     * */
    if (!fs->wrapRight() && fs->wrapTop()) {
        //  have to start at x==0
        if (path[0].first().x() != 0)
            std::swap(path[0],path[1]);
        Q_ASSERT(path[0].first().x()==0);
    }
    if (!fs->wrapTop() && fs->wrapRight()) {
        //  have to start at y==0
        if (path[0].first().y() != 0)
            std::swap(path[0],path[1]);
        Q_ASSERT(path[0].first().y()==0);
    }

    for(int i=0; i<2; ++i) {
        if (path[i].isEmpty()) continue;

        p = path[i][0];
        pg = grid->mapPoint(p);
        if (len==0.0)
            anim->setStartValue(start=p);
        else {
            //anim->setKeyValueAt(len/total_len, p);
        }

        for(int j=1; j < path[i].size(); ++j) {
            q = path[i][j];
            qg = grid->mapPoint(q);
            len += frechet::euclidean_distance<double>(pg,qg);
            Q_ASSERT(len <= total_len+1e3);
            //  TODO euclidean distance in grid coordinates
            anim->setKeyValueAt(len/total_len, q);
            //  note: keys \in [0,1]
            p=q;
            pg=qg;
        }
    }

    anim->setEndValue(p);

    if (fs->wrapRight() && fs->wrapTop())
        anim->setLoopCount(-1);
    else
        anim->setLoopCount(1);

    //  Create animated objects
    ManDogGroup* group = new ManDogGroup((MainWindow*)parent(), gview, true);
    anim->setTargetObject(group);
    anim->setPropertyName("location");
    group->setLocation(start);

    return anim;
}

void printIntervals(const IntervalMap& ivals, const BitSet& selected)
{
    auto i = ivals.begin();
    auto end = ivals.end();
    for( ; i != end; ++i)
    if (selected.contains(i->componentID)) {
        std::cout << i->componentID << ": "
                  << "["<<i->lower()<<".."<<i->upper()<<"]\n";
    }
}

QAbstractAnimation* AnimationDialog::createKangarooAnimation()
{
    //  TODO collect components and their boundary points
    frechet::FreeSpace::ptr fs = FrechetViewApplication::instance()->getFreeSpace();
    frechet::k::kAlgorithm::ptr kalgo = FrechetViewApplication::instance()->getKAlgorithm();
    const BitSet& selected_components = kalgo->greedyResult().result;

    //  TODO print selected Intervals
    //  from IntervalView and selected_components
    const frechet::k::IntervalMap& hor_ivals = kalgo->horizontalIntervals();
    const frechet::k::IntervalMap& vert_ivals = kalgo->verticalIntervals();

    std::cout << "horizontal intervals:\n";
    printIntervals(hor_ivals,selected_components);

    std::cout << "\n\n"<<"vertical intervals:\n";
    printIntervals(vert_ivals,selected_components);

/*
    for(int i=0; i < fs->n-1; ++i)
        for(int j=0; j < fs->m-1; ++j)
        {
            size_t compid = fs->component(i,j);
            if (!selected_components.contains(compid)) continue;
            const frechet::Cell& cell = fs->cell(i,j);
//            b[0] = cell.westPoint();
//            b[1] = cell.northPoint();
//            b[2] = cell.eastPoint();
//            b[3] = cell.southPoint();
//            merge_into(boundaries,b);
        }
*/
    //  TODO hard-wire paths
    QPointF points[3][7] = {
        {{0,0.312719}, {0.2,1}, {0.5,2}, {1,3}, {2,2.6}, {3,2.3}, {3.79577,2} },
        {{3.63275,0}, {4.4,0.6875}, {4.59679,0.4}},
        {{4.2032,2}, {4.6,1.3125}, {5,2.79611}}
    };

    ManDogGroup* group = new ManDogGroup((MainWindow*)parent(), gview, false);

    //  TODO create sequential animation from paths; separated by JUMPs
    QSequentialAnimationGroup* seqgroup = new QSequentialAnimationGroup(this);
    seqgroup->addAnimation(createKangarooAnimation(group, points[0],7, 6000));
    seqgroup->addPause(1000);
    seqgroup->addAnimation(createKangarooJump(group, points[0][6], points[1][0], 400));
    seqgroup->addPause(1000);
    seqgroup->addAnimation(createKangarooAnimation(group, points[1],3, 2000));
    seqgroup->addPause(1000);
    seqgroup->addAnimation(createKangarooJump(group, points[1][2], points[2][0], 400));
    seqgroup->addPause(1000);
    seqgroup->addAnimation(createKangarooAnimation(group, points[2],3, 2000));

    //  Create animated objects
    group->setLocation(points[0][0]);
    return seqgroup;
}

QAbstractAnimation* AnimationDialog::createKangarooAnimation(AnimatedGroup* group, QPointF* points, int count, int duration)
{
    QPropertyAnimation* anim = new QPropertyAnimation();
    anim->setTargetObject(group);
    anim->setPropertyName("location");

    anim->setStartValue(points[0]);
    anim->setEndValue(points[count-1]);

    anim->setDuration(duration);
    for(int i=1; i <= count-2; ++i)
        anim->setKeyValueAt((double)i/(count-1), points[i]);
    return anim;
}

QAbstractAnimation* AnimationDialog::createKangarooJump(AnimatedGroup* group, QPointF from, QPointF to, int duration)
{
    //  QEasingCurve::InOutQuint
    //  QEasingCurve::InOutElastic
    QPropertyAnimation* anim = new QPropertyAnimation();
    anim->setTargetObject(group);
    anim->setPropertyName("location");
    anim->setStartValue(from);
    anim->setEndValue(to);
    anim->setDuration(duration);
    anim->setEasingCurve(QEasingCurve::InOutQuad);
    return anim;
}

QAbstractAnimation* AnimationDialog::createAxesAnimation()
{
    QSequentialAnimationGroup* seqgroup = new QSequentialAnimationGroup(this);

    seqgroup->addAnimation(create1AxisAnimation(true));
    seqgroup->addPause(1000);
    seqgroup->addAnimation(create1AxisAnimation(false));

    return seqgroup;
}

QPointF AnimationDialog::remapCurvePoint(QPointF p) const
{
    MainWindow* mainWindow = (MainWindow*)parent();
    QPoint pp = mainWindow->getCurveView()->mapSceneToGlobal(p);
    //  TODO what about separated curves?
    pp = gview->mapFromGlobal(pp);
    return gview->mapToScene(pp);
}

QPointF AnimationDialog::remapGridPoint(QPointF p) const
{
    MainWindow* mainWindow = (MainWindow*)parent();
    frechet::Grid::ptr grid = FrechetViewApplication::instance()->getGrid();
    p = grid->mapPoint(p);
    QPoint pp = mainWindow->getFreeSpaceView()->mapSceneToGlobal(p);
    pp = gview->mapFromGlobal(pp);
    p = gview->mapToScene(pp);
    return p;
}

QAbstractAnimation* AnimationDialog::create1AxisAnimation(bool horizontal)
{
     QPen pen = horizontal ? PEN_P : PEN_Q;
    frechet::Curve& curve = horizontal ?
                            FrechetViewApplication::instance()->getP() :
                            FrechetViewApplication::instance()->getQ();
    int m = horizontal ?    FrechetViewApplication::instance()->getQ().size() :
                            FrechetViewApplication::instance()->getP().size();

     Grid::ptr grid = FrechetViewApplication::instance()->getGrid();
     const GridAxis& axis = horizontal ? grid->hor() : grid->vert();

     QParallelAnimationGroup* pargroup = new  QParallelAnimationGroup(this);
     for(int i=0; i < curve.size()-1; ++i)
     {
         pargroup->addAnimation(create1LineAnimation(horizontal, i,0, pen));

         if (curve.isClosed())
             pargroup->addAnimation(create1LineAnimation(horizontal, i, m-1, pen));
     }
     return pargroup;
}

QPropertyAnimation *
AnimationDialog::create1LineAnimation(bool horizontal, int i, int j, const QPen &pen)
{
    frechet::Curve& curve = horizontal ?
                            FrechetViewApplication::instance()->getP() :
                            FrechetViewApplication::instance()->getQ();

    QPointF p1 = remapCurvePoint(curve[i]);
    QPointF p2 = remapCurvePoint(curve[i+1]);
    QLineF start(p1,p2);

    p1 = horizontal ? QPointF(i,j)  : QPointF(j,i);
    p2 = horizontal ? QPointF(i+1,j): QPointF(j,i+1);

    QLineF end(remapGridPoint(p1), remapGridPoint(p2));

    AnimatedLineItem* lineItem = new AnimatedLineItem();
    lineItem->setLine(start);
    lineItem->setPen(pen);
    gview->scene()->addItem(lineItem);

    QPropertyAnimation* anim = new QPropertyAnimation(this);
    anim->setTargetObject(lineItem);
    anim->setPropertyName("line");
    anim->setStartValue(start);
    anim->setEndValue(end);
    anim->setDuration(2000);
    anim->setEasingCurve(QEasingCurve::OutQuad);
    return anim;
}

void AnimationDialog::startStopAnimation()
{
    switch(animation->state()) {
    case QAbstractAnimation::Running:
        animation->pause();
        break;
    case QAbstractAnimation::Paused:
        animation->resume();
        break;
    default:
        animation->start();
        break;
    }
}

void AnimationDialog::stopAnimation()
{
    animation->stop();
}

void AnimationDialog::setupUI(QRect bounds) {

    gview = new QGraphicsView(this);
    scene = new QGraphicsScene(this);
    scene->setSceneRect(bounds);
    gview->setFixedWidth(bounds.width());
    gview->setFixedHeight(bounds.height());
    gview->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    gview->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    gview->setGeometry(bounds);
    gview->setScene(scene);
    gview->setRenderHint(QPainter::Antialiasing, true);
    gview->setOptimizationFlags(QGraphicsView::DontSavePainterState);
    gview->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    setTransparent(gview);
}

const double IMAGE_SCALE = 0.25;
const double KANGAROO_SCALE = 0.4;
const QPointF MAN_OFFSET = QPointF(-288,-256)*IMAGE_SCALE;
const QPointF DOG_OFFSET = QPointF(-95,-60)*IMAGE_SCALE;
const QPointF KANGAROO_OFFSET = QPointF(-164,-55)*KANGAROO_SCALE;

ManDogGroup::ManDogGroup(MainWindow* amainWindow, QGraphicsView* agview, bool dog_image)
    : AnimatedGroup(amainWindow,agview), loc()
{
    leash = gview->scene()->addLine(QLineF(),AnimationDialog::LEASH_PEN);
    man = new QGraphicsSvgItem(":/man.svg");
    dog = new QGraphicsSvgItem(dog_image ? ":/dog.svg":":/kangaroo.svg");
    dog_offset=dog_image ? DOG_OFFSET:KANGAROO_OFFSET;
    man->setScale(IMAGE_SCALE);
    dog->setScale(dog_image ? IMAGE_SCALE:KANGAROO_SCALE);
    gview->scene()->addItem(man);
    gview->scene()->addItem(dog);
}

QPointF ManDogGroup::location() const  {
    return loc;
}

void ManDogGroup::setLocation(QPointF aloc)
{
    frechet::Curve& P = FrechetViewApplication::instance()->getP();
    frechet::Curve& Q = FrechetViewApplication::instance()->getQ();

    //loc.rx() = fmod(aloc.x(),P.size()-1);
    //loc.ry() = fmod(aloc.y(),Q.size()-1);
    loc = aloc;

    //  The Leash
    //  map x-coordinate to P
    QPointF p = remapCurvePoint(P,loc.x());
    //  map y-coordinate to Q
    QPointF q = remapCurvePoint(Q,loc.y());
    leash->setLine(QLineF(p,q));
    man->setPos(p+MAN_OFFSET);
    dog->setPos(q+dog_offset);
    // Feasible Path indicator
    mainWindow->getFreeSpaceView()->hilitePoint(loc);
}

QPointF AnimatedGroup::remapCurvePoint(QPolygonF curve, double offset) const
{
    //offset = fmod(offset,curve.size()-1);
    Q_ASSERT(offset >= 0 && offset <= curve.size()-1);
    frechet::Grid::ptr grid = FrechetViewApplication::instance()->getGrid();
    QPointF p = grid->mapToPoint(curve,offset);
    QPoint pp = mainWindow->getCurveView()->mapSceneToGlobal(p);
    //  TODO what about separated curves?
    pp = gview->mapFromGlobal(pp);
    p = gview->mapToScene(pp);
    return p;
}

QPointF AnimatedGroup::remapGridPoint(QPointF p) const
{
    frechet::Grid::ptr grid = FrechetViewApplication::instance()->getGrid();
    p = grid->mapPoint(p);
    QPoint pp = mainWindow->getFreeSpaceView()->mapSceneToGlobal(p);
    pp = gview->mapFromGlobal(pp);
    p = gview->mapToScene(pp);
    return p;
}

FeasiblePathAnimation::FeasiblePathAnimation(QObject *parent)
    : QPropertyAnimation(parent)
{
    const QPolygonF& P = FrechetViewApplication::instance()->getP();
    const QPolygonF& Q = FrechetViewApplication::instance()->getQ();

    wrap_x = P.isClosed();
    wrap_y = Q.isClosed();

    max_x = P.size()-1;
    max_y = Q.size()-1;
}

QVariant FeasiblePathAnimation::interpolated(const QVariant& from, const QVariant& to, qreal progress) const
{
    QPointF p = from.toPointF();
    QPointF q = to.toPointF();

    //  we assume that a feasible path is monotonous in x and y
    //  if not, we are wrapping

    if (q.x() < p.x() /*&& wrap_x*/)
        p.rx() = fmod(p.x(),max_x);
    if (q.y() < p.y() /*&& wrap_y*/)
        p.ry() = fmod(p.y(),max_y);

    return (1-progress)*p + progress*q;
}

ProjectionGroup::ProjectionGroup(MainWindow *amainWindow, QGraphicsView *agview, QPointF ax)
    : AnimatedGroup(amainWindow,agview), xx(ax)
{
    QGraphicsScene* scene;
    QPointF px (xx.x(),0);
    QPointF qx (0,xx.y());

    QPolygonF& P = FrechetViewApplication::instance()->getP();
    QPolygonF& Q = FrechetViewApplication::instance()->getQ();

    QPointF pp = remapCurvePoint(P,xx.x());
    QPointF qq = remapCurvePoint(Q,xx.y());

    x = createPoint(remapGridPoint(xx),6,AnimationDialog::LEASH_PEN);
    h = createLine(QLineF(),AnimationDialog::PROJ_PEN);
    v = createLine(QLineF(),AnimationDialog::PROJ_PEN);
    p = createPoint(remapGridPoint(px),6,AnimationDialog::PEN_P);
    q = createPoint(remapGridPoint(qx),6,AnimationDialog::PEN_Q);
    d = createLine(QLineF(pp,qq),AnimationDialog::LEASH_PEN);

    text = new QGraphicsTextItem();
    text->setFont(AnimationDialog::TEXT_FONT);
    gview->scene()->addItem(text);

    x->setVisible(true);

    //  TODO locate x,p,q,h,v,d
}

void ProjectionGroup::showHV()
{
    h->setVisible(true);
    v->setVisible(true);
}

void ProjectionGroup::showPQ()
{
    p->setVisible(true);
    q->setVisible(true);
}

void ProjectionGroup::showD()
{
    d->setVisible(true);
}

void ProjectionGroup::showText()
{
    QPointF p1 = d->line().p1();
    QPointF p2 = d->line().p2();

    frechet::Grid::ptr grid = FrechetViewApplication::instance()->getGrid();
    QPolygonF& P = FrechetViewApplication::instance()->getP();
    QPolygonF& Q = FrechetViewApplication::instance()->getQ();

    QPointF p = grid->mapToPoint(P,xx.x());
    QPointF q = grid->mapToPoint(Q,xx.y());

    double delta = frechet::euclidean_distance<double>(p,q);
    double epsilon = mainWindow->getControlPanel()->getEpsilon();

    text->setPos((p1+p2)/2);
    text->setHtml(QString("δ = %1 &le; ε = %2").arg(delta).arg(epsilon));
    text->setVisible(true);
}

QAbstractAnimation* ProjectionGroup::createAxisProjection(bool horizontal, int duration)
{
    QPropertyAnimation* anim = new QPropertyAnimation();
    anim->setTargetObject(horizontal ? h : v);
    anim->setPropertyName("line");
    QPointF xg = x->center();
    QLineF start(xg,xg);

    anim->setStartValue(start);
    anim->setEndValue(QLineF((horizontal?q:p)->center(),xg));
    anim->setDuration(duration);
    return anim;
}

QAbstractAnimation* ProjectionGroup::createMovePoint(bool horizontal, int duration)
{
    QPropertyAnimation* anim = new QPropertyAnimation();
    anim->setTargetObject(horizontal ? p:q);
    anim->setPropertyName("center");
    anim->setStartValue((horizontal ? p:q)->center());
    anim->setEndValue(horizontal ? d->line().p1() : d->line().p2());
    anim->setDuration(duration);
    return anim;
}

AnimatedEllipseItem* ProjectionGroup::createPoint(QPointF x, int radius, QPen pen)
{
    AnimatedEllipseItem* item = new AnimatedEllipseItem();
    gview->scene()->addItem(item);
    item->setRect(QRectF(0,0,2*radius,2*radius));
    item->setCenter(x);
    item->setPen(pen);
    item->setVisible(false);
    return item;
}

AnimatedLineItem* ProjectionGroup::createLine(QLineF l, QPen pen)
{
    AnimatedLineItem* item = new AnimatedLineItem();
    gview->scene()->addItem(item);
    item->setLine(l);
    item->setPen(pen);
    item->setVisible(false);
    return item;
}
