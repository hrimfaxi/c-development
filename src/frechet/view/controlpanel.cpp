
#include <controlpanel.h>
#include <QSettings>
#include <QMovie>
#include <QWheelEvent>

#include <frechetviewapplication.h>
#include <k_frechet/kalgorithm.h>

using namespace frechet;
using namespace poly;
using namespace view;
using namespace app;

const int ControlPanel::DECIMALS = 8;
QMovie* ControlPanel::loaderMovie = nullptr;

ControlPanel::ControlPanel(QWidget *parent)
    : QWidget(parent),
      locale(), movieLabel(nullptr), curve_was_ok(false)
{
    ui.setupUi(this);
    eps = 0.0;
    eps_step = 1;

    ui.toolBox->setCurrentIndex(FrechetViewApplication::ALGORITHM_K_FRECHET);
    //  TODO select algorithm

    ui.epsSlider->setMinimum(0);
    ui.epsSlider->setMaximum(100);
    ui.epsSlider->setTickInterval(5);

    epsValidator = new QDoubleValidator;
    epsValidator->setDecimals(DECIMALS);
    epsValidator->setLocale(locale);
    ui.epsEdit->setValidator(epsValidator);

    //connect(ui.kBF, SIGNAL(valueChanged(int)), this, SLOT(clearResults()));
    connect(ui.showGreedy, SIGNAL(clicked()), this, SLOT(onGreedyButton()), Qt::QueuedConnection);
    //connect(ui.startBF, SIGNAL(clicked()), this, SIGNAL(startBruteForce()));
    connect(ui.showBF, SIGNAL(clicked()), this, SLOT(onBFButton()), Qt::QueuedConnection);

    FrechetViewApplication* app = FrechetViewApplication::instance();
    connect(ui.optCurve, SIGNAL(clicked()),     app, SLOT(startStopOptimiseCurve()), Qt::QueuedConnection);
    connect(ui.approxCurve, SIGNAL(clicked()),  app, SLOT(startStopApproximateCurve()), Qt::QueuedConnection);

    connect(ui.decidePoly, SIGNAL(clicked()),     app, SLOT(startStopDecidePolygon()), Qt::QueuedConnection);
    connect(ui.optPoly, SIGNAL(clicked()),      app, SLOT(startStopOptimisePolygon()), Qt::QueuedConnection);
    connect(ui.approxPoly, SIGNAL(clicked()),   app, SLOT(startStopApproximatePolygon()), Qt::QueuedConnection);

    //  forward signal AlgorithmChanged
    connect(ui.toolBox, SIGNAL(currentChanged(int)), this, SLOT(onAlgorithmChanged(int)));

    clearResults();
}

void ControlPanel::saveSettings(QSettings& settings, QString group)
{
    settings.beginGroup(group);

    settings.setValue("epsilon", getEpsilon());
    //settings.setValue("k", ui.kBF->value());
    settings.setValue("algorithm", FrechetViewApplication::instance()->currentAlgorithm());

    settings.endGroup();
}

void ControlPanel::restoreSettings(QSettings& settings, QString group)
{
    settings.beginGroup(group);

    setEpsilonWithNotify(settings.value("epsilon",30.0).toDouble());
    //ui.kBF->setValue(settings.value("k",5).toInt());
    int algo = settings.value("algorithm", FrechetViewApplication::ALGORITHM_K_FRECHET).toInt();
    FrechetViewApplication::instance()->setCurrentAlgorithm((FrechetViewApplication::Algorithm)algo);
    ui.toolBox->setCurrentIndex(algo);

    settings.endGroup();
}

void ControlPanel::setEpsilon(double eps, bool notify)
{
    //  don't trigger signals; respect notify
    ui.epsSlider->blockSignals(true);
    ui.epsSlider->setValue(toSliderValue(eps));
    ui.epsEdit->setText(toString(eps));
    ui.epsSlider->blockSignals(false);

    if (eps != this->eps) {
        clearResults();
        this->eps = eps;
        if (notify)
            emit epsilonChanged(this->eps);
    }
}

QString ControlPanel::toString(double number)
{
    //  format according to Locale.
    //  sadly, Qt has no such function
    QString s = QString::number(number);
    return s.replace('.',locale.decimalPoint());
}


double ControlPanel::toDouble(QString text)
{
    bool ok;
    double eps = locale.toDouble(text,&ok);
    return ok ? eps : NAN;
}

int ControlPanel::toSliderValue(double x)
{
    return round(x / eps_step);
}

double ControlPanel::fromSliderValue(int i)
{
    return i * eps_step;
}

void ControlPanel::setEpsilonMax(double eps_max)
{
    epsValidator->setRange(0.0,eps_max,DECIMALS);

    //  find "reasonable" values for eps_step ...

    if (eps_max <= 10.0) {
        eps_step = 0.1;
    }
    else if (eps_max <= 50.0) {
        eps_step = 0.5;
    }
    else  if (eps_max <= 100.0) {
        eps_step = 1.0;
    }
    else {
        eps_step = round(eps_max/100.0);
    }

    ui.epsSlider->setMaximum(eps_max/eps_step);
    ui.epsSlider->setSingleStep(1);
    ui.epsSlider->setPageStep(3);
    ui.epsSlider->setTickInterval(1);

    double current = toDouble(ui.epsEdit->text());
    if (std::isnan(current) || (current < 0.0))
        setEpsilon(0.0,false);
    else if (current > eps_max)
        setEpsilon(eps_max,false);
    else
        setEpsilon(current,false);
}

void ControlPanel::updateResults()
{
    k::kAlgorithm::ptr alg = FrechetViewApplication::instance()->getKAlgorithm();
    updateGreedyResult(alg->greedyResult());
    updateOptimalResult(alg->optimalResult());

    updatePolyResult();

    //  button activation and check state
    if (ui.showGreedy->isChecked())
        emit showGreedyResult();
    else if (ui.showBF->isChecked())
        emit showOptimalResult();
    else
        emit hideResult();
}

void ControlPanel::updateOptimalResult(const k::kAlgorithm::BruteForce& bf)
{
    updateOptimalResult((frechet::k::kAlgorithm::Result)bf.k_optimal);

    QString text;
    if (bf.k_optimal > 0) {
        text = "k="+QVariant(bf.k_optimal).toString();
    }
    else if (bf.k_optimal==k::kAlgorithm::NO_SOLUTION) {
        text = "--";
    }
    else {
        if (bf.k_min > 0)
            text += QString::number(bf.k_min)+" <= ";

        switch(bf.k_optimal)
        {
        case k::kAlgorithm::UNKNOWN:
            text += "?";
            break;
        case k::kAlgorithm::RUNNING:
            text += "...";
            break;
        }

        if (bf.k_max > 0)
            text += " <= "+QString::number(bf.k_max);
    }
    ui.resultBF->setText(text);
}

void ControlPanel::updateOptimalResult(frechet::k::kAlgorithm::Result k)
{
    switch(k)
    {
    case k::kAlgorithm::NO_SOLUTION:
        ui.resultBF->setText("--");
        break;
    case k::kAlgorithm::UNKNOWN:
        ui.resultBF->setText("?");
        break;
    case k::kAlgorithm::RUNNING:
        ui.resultBF->setText("...");
        break;
    default:
        //  see updateOptimalResult(const k::kAlgorithm::BruteForce& bf)
        break;
    }

    showIcon(ui.statusBF,k);

    switch(k)
    {
    case k::kAlgorithm::NO_SOLUTION:
        ui.showBF->setEnabled(false);
        ui.showBF->setCheckable(false);
        ui.showBF->setChecked(false);
        ui.showBF->setText("Show");
        break;
    case k::kAlgorithm::UNKNOWN:
        ui.showBF->setEnabled(true);
        ui.showBF->setCheckable(false);
        ui.showBF->setChecked(false);
        ui.showBF->setText("Go");
        break;
    case k::kAlgorithm::RUNNING:
        ui.showBF->setEnabled(true);
        ui.showBF->setCheckable(false);
        ui.showBF->setChecked(false);
        ui.showBF->setText("Stop");
        break;
    default:
        ui.showBF->setEnabled(true);
        ui.showBF->setCheckable(true);
        ui.showBF->setText("Show");
        break;
    }
}

void ControlPanel::updateGreedyResult(const k::kAlgorithm::Greedy& gr)
{
    updateGreedyResult(
            (frechet::k::kAlgorithm::Result)gr.k_xy,
            (frechet::k::kAlgorithm::Result)gr.k_yx);
}

void ControlPanel::updateGreedyResult(frechet::k::kAlgorithm::Result k, frechet::k::kAlgorithm::Result k2)
{
    showIcon(ui.statusGreedy,k);

    QString text;
    switch(k)
    {
    case k::kAlgorithm::UNKNOWN:
        text="?";
        break;
    case k::kAlgorithm::NO_SOLUTION:
        text="--";
        break;
    case k::kAlgorithm::RUNNING:
        text="...";
        break;
    default:
        Q_ASSERT(k > 0);
        Q_ASSERT(k2 > 0);
        if (k > k2) std::swap(k,k2);

        text = "k="+QString::number(k);
        if (k != k2)
            text += ", "+QString::number(k2);
        break;
    }

    ui.resultGreedy->setText(text);

    switch(k)
    {
    case k::kAlgorithm::UNKNOWN:
    case k::kAlgorithm::NO_SOLUTION:
        ui.showGreedy->setEnabled(false);
        ui.showGreedy->setChecked(false);
        break;
    case k::kAlgorithm::RUNNING:
        ui.showGreedy->setEnabled(false);
        break;
    default:
        Q_ASSERT(k > 0);
        ui.showGreedy->setEnabled(true);
        break;
    }
}

void ControlPanel::clearResults()
{
    updateGreedyResult(k::kAlgorithm::UNKNOWN,k::kAlgorithm::UNKNOWN);
    updateOptimalResult(k::kAlgorithm::UNKNOWN);
    updatePolyResult();
}

QString ControlPanel::POLY_STATUS [] = {
    "OK", "no solution",
    "NOT_SET_UP",   //  not supposed to happen
    "",
    "P is empty", "Q is empty",
    "P is not closed", "Q is not closed",
    //  beide Kurven müssen einfache Polygone seine
    "P is not simple", "Q is not simple",
    //  beide Polygone müssen gegen den Uhrzeigersinn orientiert sein
    "P_NOT_COUNTER_CLOCKWISE", "Q_NOT_COUNTER_CLOCKWISE",   //  not supposed to happen
    //  für konvexe Polygone könenn wir einen einfacheren Algorithmus verwenden
    "P is convex", "Q is convex"
};


void ControlPanel::updatePolyResult()
{
    Algorithm::ptr poly = FrechetViewApplication::instance()->getPolyAlgorithm();
    if (!poly) {
        ui.resultPoly->setText("?");
        showIcon(ui.statusPoly, k::kAlgorithm::UNKNOWN);
        return;
    }

    //  show poly status and results
    int status = FrechetViewApplication::instance()->getPolyAlgorithm()->status();
    if (status<=Algorithm::RUNNING) {
        ui.resultPoly->setText("...");
        ui.resultCurve->setText("...");
    }
    else {
        ui.resultPoly->setText(POLY_STATUS[status]);
    }

    if (status <= Algorithm::RUNNING) {
        if (status <= Algorithm::RUNNING_APPROX_POLY)
            showIcon(ui.statusPoly, k::kAlgorithm::RUNNING);
        else if (status <= Algorithm::RUNNING_APPROX_CURVE)
            showIcon(ui.statusCurve, k::kAlgorithm::RUNNING);
    }
    else if (FrechetViewApplication::instance()->currentAlgorithm()==FrechetViewApplication::ALGORITHM_CURVE) {
        showIcon(ui.statusCurve, curve_was_ok ? 1 : k::kAlgorithm::NO_SOLUTION);
        ui.resultCurve->setText(curve_was_ok ? "OK":"no solution");
    }

    ui.decidePoly->setEnabled     (poly->canDecidePoly());
    ui.optPoly->setEnabled      (poly->canOptimisePoly());
    ui.approxPoly->setEnabled   (poly->canOptimisePoly());
    ui.optCurve->setEnabled     (poly->canOptimiseCurve());
    ui.approxCurve->setEnabled  (poly->canOptimiseCurve());

    ui.decidePoly->setText("Decide");
    ui.optPoly->setText("Optimise");
    ui.approxPoly->setText("Approximate");
    ui.optCurve->setText("Optimise");
    ui.approxCurve->setText("Approximate");

    switch(status)
    {
    case Algorithm::YES:
        showIcon(ui.statusPoly, 1);
        ui.decidePoly->setEnabled(false);
        break;
    default:
    case Algorithm::NO:
        showIcon(ui.statusPoly, k::kAlgorithm::NO_SOLUTION);
        ui.decidePoly->setEnabled(false);
        break;
    case Algorithm::SET_UP:
        showIcon(ui.statusPoly, k::kAlgorithm::UNKNOWN);        
        ui.decidePoly->setEnabled(true);
        break;
    case Algorithm::RUNNING_OPT_CURVE:
        ui.optCurve->setText("Stop");
        ui.optCurve->setEnabled(true);
        break;
    case Algorithm::RUNNING_OPT_POLY:
        ui.optPoly->setText("Stop");
        ui.optPoly->setEnabled(true);
        break;
    case Algorithm::RUNNING_APPROX_CURVE:
        ui.approxCurve->setText("Stop");
        ui.approxCurve->setEnabled(true);
        break;
    case Algorithm::RUNNING_APPROX_POLY:
        ui.approxPoly->setText("Stop");
        ui.approxPoly->setEnabled(true);
        break;
    case Algorithm::RUNNING_DECIDE_POLY:
        ui.decidePoly->setText("Stop");
        ui.decidePoly->setEnabled(true);
        break;
    }
    ui.decidePoly->setChecked(false);
}

void ControlPanel::onShowBounds(bool)
{
    //  when the show-bounds parameter changes.
    //  force update of result sets (if necessary)
    if (ui.showBF->isChecked())
        emit showOptimalResult();
    if (ui.showGreedy->isChecked())
        emit showGreedyResult();
}

void ControlPanel::onCurveFinished(bool ok)
{
    if (FrechetViewApplication::instance()->currentAlgorithm()==
            FrechetViewApplication::ALGORITHM_CURVE)
    {
        curve_was_ok = ok;
        showIcon(ui.statusCurve, ok ? 1 : k::kAlgorithm::NO_SOLUTION);
        ui.resultCurve->setText(ok ? "OK":"no solution");
    }

    if (FrechetViewApplication::instance()->currentAlgorithm()==
        FrechetViewApplication::ALGORITHM_POLYGON)
    {
        //  TODO integrate result code and error code (like k-Frechet)
        updatePolyResult();
    }
}

void ControlPanel::onEdit() {
    double eps = toDouble(ui.epsEdit->text());
    if (! std::isnan(eps))
        setEpsilonWithNotify(eps);
}

void ControlPanel::onAlgorithmChanged(int algorithm) {
    clearResults();
    FrechetViewApplication::instance()->setCurrentAlgorithm((FrechetViewApplication::Algorithm)algorithm);
    // force redraw
    emit epsilonChanged(this->eps);
}

void ControlPanel::onSlider(int) {
    setEpsilonWithNotify(fromSliderValue(ui.epsSlider->value()));
}

void ControlPanel::onGreedyButton()
{
    //  if we have valid Greedy result, show it in FreeSpaceView
    k::kAlgorithm::ptr alg = FrechetViewApplication::instance()->getKAlgorithm();
    if (alg->greedyResult().valid() && ui.showGreedy->isChecked()) {
        ui.showBF->setChecked(false);
        emit showGreedyResult();
    }
    else {
        emit hideResult();
    }
}

void ControlPanel::onBFButton()
{
    //  Four States: Disabled, Go, Stop, Show
    k::kAlgorithm::ptr alg = FrechetViewApplication::instance()->getKAlgorithm();
    int k = alg->optimalResult().k_optimal;
    switch(k)
    {
    case k::kAlgorithm::NO_SOLUTION:
        Q_ASSERT(false);    //button must not be enabled
        break;
    case k::kAlgorithm::UNKNOWN:
        FrechetViewApplication::instance()->startKBruteForce();
        break;
    case k::kAlgorithm::RUNNING:
        FrechetViewApplication::instance()->cancelBackgroundJob();
        break;
    default:
        Q_ASSERT(k > 0);
        if (ui.showBF->isChecked()) {
            ui.showGreedy->setChecked(false);
            emit showOptimalResult();
        }
        else {
            emit hideResult();
        }
        break;
    }

 }


void ControlPanel::showIcon(QLabel *label, int k)
{
    if (loaderMovie && (label==movieLabel) && (k!=k::kAlgorithm::RUNNING))
        loaderMovie->stop();

    switch(k)
    {
    case k::kAlgorithm::UNKNOWN:
        setIcon(label,":/awesome/question-circle.svg", "solution not yet calculated");
        break;
    default:
        Q_ASSERT(k > 0);
        setIcon(label,":/awesome/check-circle.svg", "found a solution");
        break;
    case k::kAlgorithm::NO_SOLUTION:
        setIcon(label,":/awesome/times-circle.svg", "there is no solution");
        break;
    case k::kAlgorithm::RUNNING:
        if (!loaderMovie)
            loaderMovie = new QMovie(":/ajax-loader.gif");
        movieLabel=label;
        label->setMovie(loaderMovie);
        label->setToolTip("calculation in progress");
        loaderMovie->start();
        break;
    }
}

void ControlPanel::setIcon(QLabel *label, QString file, QString tooltip)
{
    QIcon icon(file);
    label->setPixmap(icon.pixmap(24,24));
    label->setToolTip(tooltip);
}

void ControlPanel::wheelEvent(QWheelEvent* wh)
{
    //  forward to slider
    double step=1.0;
    if (wh->modifiers() & Qt::ControlModifier) {
        double step = ui.epsSlider->singleStep()*0.05;
        setEpsilon(getEpsilon() + ((wh->delta() > 0) ? step:-step),true);
    }
    else {
        singleStep(ui.epsSlider, (wh->delta() > 0) ? +1:-1);
    }
}

void ControlPanel::keyPressEvent(QKeyEvent* event)
{
    switch(event->key())
    {
    case Qt::Key_Right:
    case Qt::Key_Up:
    case Qt::Key_Plus:      singleStep(ui.epsSlider,+1); break;
    case Qt::Key_Left:
    case Qt::Key_Down:
    case Qt::Key_Minus:     singleStep(ui.epsSlider,-1); break;

    case Qt::Key_PageUp:    pageStep(ui.epsSlider,+1); break;
    case Qt::Key_PageDown:  pageStep(ui.epsSlider,-1); break;
    }
}

