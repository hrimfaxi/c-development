
#include <frechetviewapplication.h>
#include <concurrency.h>
#include <input/datapath.h>
#include <poly_path.h>
#include <poly/jobs.h>

#include <QFileDialog>
#include <QErrorMessage>
#include <QMessageBox>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QDebug>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "ui_mainwindow.h"

using namespace frechet;
using namespace poly;
using namespace app;
using namespace view;
using namespace input;

FrechetViewApplication* FrechetViewApplication::frechetViewApp = nullptr;

FrechetViewApplication::FrechetViewApplication(QString exec_path, bool withGui)
    : history(), currentId(0),
      reader(),
      path(""),
      fileWatch(nullptr),
      P(), Q(),
      grid(NULL),
      freeSpace(NULL),
      fspath(NULL),
      kAlgorithm(NULL),
      polyAlgorithm(NULL),
      epsMax(0.0),
      window(NULL),
      bgJob(),
      _currentAlgorithm(ALGORITHM_CURVE)
{
    execPath = exec_path;
	if (withGui) {
		fileWatch = new QFileSystemWatcher;
	}
    Q_ASSERT(frechetViewApp==nullptr);
    frechetViewApp = this;
}

FrechetViewApplication::~FrechetViewApplication()
{
    bgJob.shutDown();
	delete fileWatch;
    frechetViewApp = nullptr;
}

void FrechetViewApplication::init(QString file)
{
    history.init("file.history");

    Q_ASSERT(P.empty() && Q.empty());
    grid = frechet::Grid::ptr(new frechet::Grid());
    freeSpace.reset();
    fspath.reset();
    kAlgorithm.reset();
    polyAlgorithm.reset();

    window = new MainWindow;

    QSettings settings;
    history.restore(settings);
    history.attachMenu(window->actionOpenRecent());

    window->getControlPanel()->blockSignals(true);  //  do not trigger free-space calculation, yet
    //  signals will be enabled at the end of open()
    window->restoreSettings(settings);

    //  read command line args
    if (file.isEmpty())
        file = history.lastFile();

    open(file);

    connect(window->actionOpen(), SIGNAL(triggered()),
            this, SLOT(open()));
    connect(window->actionQuit(), SIGNAL(triggered()),
            this, SLOT(quit()));

    connect(&history, SIGNAL(open(QString)), this, SLOT(open(QString)));

	if (fileWatch) {
		connect(fileWatch, SIGNAL(fileChanged(QString)),
			this, SLOT(open(QString)),
			Qt::QueuedConnection);
	}

    //  populate Bookmarks
    bool presentation_mode = populateBookmarks();
    presentation_mode = true;
	if (!presentation_mode) {
        window->ui->menuExperimental->setEnabled(false);
        window->ui->menuView->removeAction(window->ui->menuExperimental->menuAction());
        window->ui->actionShow_Animation->setEnabled(false);
        window->ui->actionShow_XAnimation->setEnabled(false);
        window->ui->actionStart_Animation->setEnabled(false);
        window->ui->actionStart_FSAnimation->setEnabled(false);
        window->ui->actionStart_CVAnimation->setEnabled(false);
        window->ui->actionStart_ProjectionAnimation->setEnabled(false);
    }

    //  Desktop Integration
#ifdef Q_OS_LINUX
    settings.beginGroup("desktop");
    //QString execPath = arguments().first();

    QString installPath = settings.value("exec.path").toString();
    bool iconsInstalled = settings.value("icons.installed").toBool();

    if (execPath != installPath) {
        installLinuxDesktopEntries(settings);
        settings.setValue("exec.path",execPath);
    }
    if (!iconsInstalled) {
        installLinuxDesktopIcons(settings);
        settings.setValue("icons.installed",true);
    }
    settings.endGroup();
#endif

    if (file.isEmpty() && showFirstTimeHint())
        open(true);
}
/**
 * Clears the feasible path and triggers the signal algorithmChanged().
 * This signal will cause subsequent changes to several parts of the GUI, 
 * like the control panel, the curve view, etc.
 */
void FrechetViewApplication::setCurrentAlgorithm(FrechetViewApplication::Algorithm alg)
{
    bool modified = (_currentAlgorithm != alg);
    _currentAlgorithm = alg;
    //  update FSPath
    if (freeSpace) {
        switch (_currentAlgorithm) {
            case ALGORITHM_CURVE:
                if (modified || !fspath)
                    fspath.reset(new frechet::reach::FSPath(freeSpace));
                break;
            case ALGORITHM_POLYGON:
                if (modified || !fspath)
                    fspath.reset(new frechet::poly::PolygonFSPath(freeSpace));
                break;
            case ALGORITHM_K_FRECHET:
                fspath.reset();
                break;
        }
    }   else {
        fspath.reset();
    }

    if (modified)
        emit algorithmChanged(_currentAlgorithm);
}

void FrechetViewApplication::resetAlgorithms()
{
    cancelBackgroundJob();
    polyAlgorithm->resetStatus();
    window->getControlPanel()->updateResults();
    //  TODO shouldn't this be done through a signal?
}

void FrechetViewApplication::open(bool firstTime) {

    QSettings settings;
    QFileDialog fdlg;

    fdlg.restoreState(settings.value("filedialog.open").toByteArray());
    fdlg.setAcceptMode(QFileDialog::AcceptOpen);
    fdlg.setFileMode(QFileDialog::ExistingFile);
    fdlg.setNameFilters(DataPath::INPUT_FILTERS);

    QString dir;
    if (firstTime)
        dir = "../../..";
    else
        dir = settings.value("filedialog.open.dir").toString();
    fdlg.setDirectory(dir);

//    fdlg.setHistory(history);
    QStringList fileNames;
    if (fdlg.exec()) {
        fileNames = fdlg.selectedFiles();
        if (!fileNames.isEmpty())
            open(fileNames.first());
    }

    settings.setValue("filedialog.open",fdlg.saveState());
    settings.setValue("filedialog.open.dir",fdlg.directory().absolutePath());
}

void FrechetViewApplication::open(QString filePath)
{
    QSettings settings;
    if (currentId)
        saveFileSettings(settings,currentId);

    QString oldPath = path.getFile().absoluteFilePath();
    path = filePath;
    QString newPath = path.getFile().absoluteFilePath();

	if (fileWatch) {
		if (!oldPath.isEmpty())
			fileWatch->removePath(oldPath);
		fileWatch->addPath(newPath);
	}

    reader.clear();
    freeSpace.reset();
    fspath.reset();
    kAlgorithm.reset();
    polyAlgorithm.reset();

    if (!filePath.isEmpty()) {        
        reader.readAll(path);

        if (reader.getResults().size() < 2) {
            //  something is missing
            QErrorMessage* msg = new QErrorMessage();
            msg->showMessage(reader.errorMessage());
        }
    }

    if (reader.getResults().size() >= 2) {
        QVector<Path> paths = reader.getResults();

        /*  Note: for scripted files,
         *  coordinates are rounded to 8 decimal places
         *  to avoid nasty geometrical problems (especially
         *  with convex decomposition, etc.).
         * */
        int precision = 0; //der.isScript() ? 8:0;

        P = paths[0].toCurve(precision);
        Q = paths[1].toCurve(precision);

        bool p_is_primary = setupCurves();

        grid->setCurves(P,Q);
        grid->hor().setLineStyles(
                paths[p_is_primary?0:1].getLineStyles(),
                paths[p_is_primary?0:1].getDefaultLineStyle());
        grid->vert().setLineStyles(
                paths[p_is_primary?1:0].getLineStyles(),
                paths[p_is_primary?1:0].getDefaultLineStyle());
    }
    else {
        P = frechet::Curve();
        Q = frechet::Curve();

        setupCurves();

        grid->setCurves(P,Q);
        grid->hor().setLineStyles(LineStyleVector(),SOLID);
        grid->vert().setLineStyles(LineStyleVector(),SOLID);
    }

    //  calculate Free-Space Diagram   
    freeSpace = FreeSpace::ptr(new frechet::FreeSpace(P,Q));
    //fspath = frechet::reach::FSPath::ptr(new frechet::reach::FSPath(freeSpace));
    kAlgorithm = k::kAlgorithm::ptr(new frechet::k::kAlgorithm(freeSpace));

    Q_ASSERT(freeSpace->P==P);
    Q_ASSERT(freeSpace->Q==Q);
    Q_ASSERT(freeSpace->n==P.size());
    Q_ASSERT(freeSpace->m==Q.size());

    //
    connect(polyAlgorithm.get(), SIGNAL(statusChanged()),
             window->getControlPanel(), SLOT(updateResults()), Qt::QueuedConnection);

//    connect(polyAlgorithm.get(), SIGNAL(optimisationResult(double)),
//            window->getFreeSpaceView(), SLOT(calculateFreeSpace(double)));
    connect(polyAlgorithm.get(), SIGNAL(optimisationResult(double)),
            window->getControlPanel(), SLOT(setEpsilonWithoutNotify(double)));
    connect(polyAlgorithm.get(), SIGNAL(optimisationResult(double)),
            window->getFreeSpaceView(), SLOT(calculateFreeSpace(double)));

    connect(polyAlgorithm.get(), SIGNAL(pathUpdated(bool)),
            window->getFreeSpaceView(), SLOT(showPath(bool)));

    connect(this, SIGNAL(algorithmChanged(int)),
            this, SLOT(resetAlgorithms()));
    connect(window->getControlPanel(),   SIGNAL(epsilonChanged(double)),
             this,  SLOT(resetAlgorithms()));

    connect(kAlgorithm.get(), SIGNAL(greedyFinished()),
             window->getControlPanel(), SLOT(updateResults()), Qt::QueuedConnection);

    connect(kAlgorithm.get(), SIGNAL(bruteForceFinished()),
             window->getControlPanel(), SLOT(updateResults()), Qt::QueuedConnection);
    connect(kAlgorithm.get(), SIGNAL(bruteForceIteration(int)),
             window->getControlPanel(), SLOT(updateResults()), Qt::QueuedConnection);
//    connect(kAlgorithm.get(), SIGNAL(bruteForceCancelled()),
//             window->getControlPanel(), SLOT(updateResults()));//, Qt::QueuedConnection);
//    connect(kAlgorithm.get(), SIGNAL(bruteForceFinished()),
//            window->getFreeSpaceView(), SLOT(showOptimalResult(true)), Qt::QueuedConnection);
//    connect(kAlgorithm.get(), SIGNAL(bruteForceCancelled()),
//            window->getFreeSpaceView(), SLOT(showOptimalResult(false)), Qt::QueuedConnection);

    //QRectF boundingRect = P.boundingRect().united(Q.boundingRect());
    epsMax = frechet::max_euclidean_distance(P,Q)+1.0;

    //  view
    window->setWindowTitle(path.getFile().fileName());
    window->getCurveView()->populateScene(P,Q, polyAlgorithm);
    window->getFreeSpaceView()->populateScene();
    window->getCurveView()->flipVertical(reader.isIpe() || reader.isScript());
    window->show();

    //  restore file settings
    if (!filePath.isEmpty()) {
        currentId = history.insert(filePath);        
        window->ui->actionEdit_Curve->setEnabled(true);
    }
    else {
        currentId = 0;
        window->ui->actionEdit_Curve->setEnabled(false);
    }

    //  set eps, but don't trigger redraw until parameters are complete
    //  (note: calculateFreeSpace will be called directly, below)
    window->getControlPanel()->blockSignals(true);
    window->getControlPanel()->setEpsilonMax(epsMax);
    window->getControlPanel()->clearResults();

//    window->show();
    if (currentId)
        restoreFileSettings(settings,currentId);

    window->getFreeSpaceView()->calculateFreeSpace(
                window->getControlPanel()->getEpsilon());

    if (currentId)
        restoreFileSettings(settings,currentId);    //  zoom,scroll
    //  Note: scroll settings need to be restored after the scene is constructed.
    //  That's why restoreFileSettings is called *twice*.
    window->getControlPanel()->blockSignals(false);

    emit fileOpened(filePath);
}

void FrechetViewApplication::showAboutDialog()
{
    QMessageBox msg(window);
    msg.setText(qApp->applicationDisplayName()+" "
                + qApp->applicationVersion()+ "\n\n"
                + "Build: "+__DATE__+" "+__TIME__);
    msg.setIconPixmap(window->windowIcon().pixmap(64,64));
    msg.exec();
}

void FrechetViewApplication::createDesktopEntry()
{
    QDir desktopDir = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    QDir appDir = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
#ifdef Q_OS_LINUX
    //  cp :/frechet-view.desktop ~/Desktop/frechet-view.desktop
    QString target = desktopDir.filePath("frechet-view.desktop");

    copyContents(linuxDesktopFile().toLocal8Bit(), target);
    //  chmod a+x
    QFile::setPermissions(target,
                          QFile::ReadUser|QFile::ReadGroup|QFile::ReadOther|
                          QFile::WriteUser|
                          QFile::ExeGroup|QFile::ExeUser|QFile::ExeOther);
#endif
#ifdef Q_OS_MAC
    //  create symbolic link on desktop
    //  point to "Fréchet View.app" instead of app/Contents/MacOS/executable
    QDir dir = QFileInfo(this->execPath).dir();
    dir.cdUp();
    dir.cdUp();
    bool created = QFile::link( dir.absolutePath(),
                                desktopDir.absoluteFilePath("Fréchet View"));
    Q_ASSERT(created);
#endif
#ifdef Q_OS_WIN
    //  create symbolic link on desktop
    bool created = QFile::link( this->execPath, desktopDir.absoluteFilePath("Fréchet View.lnk"));
    Q_ASSERT(created);
    //  and Start menu entry!
    created = QFile::link( this->execPath, appDir.absoluteFilePath("Fréchet View.lnk"));
#endif
}

void FrechetViewApplication::editInputFile()
{
    if (path.getFile().exists()) {
        QDesktopServices::openUrl(
                    QUrl::fromLocalFile(
                        path.getFile().absoluteFilePath()));
    }
}

void FrechetViewApplication::startStopDecidePolygon()
{
    double epsilon = window->getControlPanel()->getEpsilon();
    startStopDecidePolygon(epsilon);
}

void FrechetViewApplication::startStopDecidePolygon(double epsilon)
{
    cancelBackgroundJob();
    //Q_ASSERT(polyAlgorithm->canDecidePoly());
    if(polyAlgorithm->canDecidePoly())
    {
        PolygonWorkerJob* job = new DecideWorkerJob(
                polyAlgorithm,
                freeSpace, fspath,
                epsilon);
        bgJob.startJob(job);
    }
}

void FrechetViewApplication::startStopOptimisePolygon()
{
    startStopOptimisePolygon(0.0);
}

void FrechetViewApplication::startStopApproximatePolygon()
{
    double approx = epsMax * 1e-5;
    startStopOptimisePolygon(approx);
}

void FrechetViewApplication::startStopOptimisePolygon(double approx)
{
    cancelBackgroundJob();
   // Q_ASSERT(polyAlgorithm->canOptimisePoly());
    if(polyAlgorithm->canOptimisePoly())  //  SET_UP, YES,NO
    {
        PolygonWorkerJob* job = new OptimisePolyWorkerJob(polyAlgorithm, fspath, approx);
        bgJob.startJob(job);
    }
}

void FrechetViewApplication::startStopOptimiseCurve()
{
    startStopOptimiseCurve(0.0);
}

void FrechetViewApplication::startStopApproximateCurve()
{
    double approx = epsMax * 1e-5;
    startStopOptimiseCurve(approx);
}

void FrechetViewApplication::startStopOptimiseCurve(double approx)
{
    cancelBackgroundJob();

    //Q_ASSERT(polyAlgorithm->canOptimiseCurve());
    if(polyAlgorithm->canOptimiseCurve())   //  SET_UP, YES,NO,P_CONVEX,Q_CONVEX
    {
        PolygonWorkerJob* job = new OptimiseCurveWorkerJob(polyAlgorithm, approx);
        bgJob.startJob(job);
    }
}

void FrechetViewApplication::startKBruteForce()
{
    cancelBackgroundJob();
    frechet::k::KWorkerJob* job = new frechet::k::KWorkerJob(kAlgorithm.get());
    bgJob.startJob(job);
}

void FrechetViewApplication::cancelBackgroundJob()
{
    bgJob.cancelJob();
}


void FrechetViewApplication::installLinuxDesktopIcons(QSettings& )
{
    //  cp :/frechet-view.svg ~/.local/share/icons/default/scalable/frechet-view.svg
    QString iconDir = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    if (iconDir==nullptr || iconDir.isEmpty()) return;
    
    iconDir += "/.local/share/icons";

    QStringList themes = { "default","hicolor" };
    QStringList sizes = { "16","32","48","64","96","128","256"};

    foreach(QString theme, themes)
    {
        copyFile(":/frechet-view.svg",
                    iconDir+"/"+theme+"/scalable/apps/frechet-view.svg");

        foreach(QString size, sizes)
            copyFile(":/frechet-view-"+size+".png",
                        iconDir+"/"+theme+"/"+size+"x"+size+"/apps/frechet-view.png");
    }
}

void FrechetViewApplication::installLinuxDesktopEntries(QSettings& )
{
    //  cp :/frechet-view.desktop ~/.local/share/applications/frechet-view.desktop
    QString applDir = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
    if (applDir.isEmpty()) return;

    QString desktop = linuxDesktopFile();
    if (desktop.isEmpty()) return;

    copyContents(desktop.toLocal8Bit(),
                 applDir+"/frechet-view.desktop");
}

QString FrechetViewApplication::linuxDesktopFile()
{
    Q_ASSERT(!execPath.isEmpty());
    //QString execEntry = "Exec="+execPath;
    QByteArray execName = "frechet-view.sh";
    QString iconPath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    if (iconPath.isEmpty()) return "";

    iconPath += "/.local/share/icons/hicolor/scalable/apps/frechet-view.svg";
    QString iconEntry = "Icon="+iconPath;

    QFile desktopFile (":/frechet-view.desktop");
    if (!desktopFile.open(QFile::ReadOnly)) return "";

    QByteArray desktopContentsArray = desktopFile.readAll();
    if (desktopContentsArray.isEmpty()) return "";

    QString desktopContents = desktopContentsArray;

    int i1 = desktopContents.indexOf("Exec=");
    i1 = desktopContents.indexOf(execName,i1);

    desktopContents.replace(
                i1, execName.length(),
                execPath);

    desktopContents.replace(
                "Icon=frechet-view",
                iconEntry);
    return desktopContents;
}

bool FrechetViewApplication::populateBookmarks()
{
    /*  Bookmarks are stored in a json file, bookmarks.json
     *  in the preferences directory
     * */
    QString bookmarkPath = QStandardPaths::locate(
                QStandardPaths::AppConfigLocation,
                "bookmarks.json",QStandardPaths::LocateFile);
    //QStringList configDirectories = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation);
    if (bookmarkPath==nullptr || bookmarkPath.isEmpty())
        return false;

    //  read json data
    QFile bookmarkFile(bookmarkPath);
    if (!bookmarkFile.open(QFile::ReadOnly))
        return false;

    QByteArray jsonData = bookmarkFile.readAll();
    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData,&parseError);
    if (parseError.error != QJsonParseError::NoError) {
        std::cerr << parseError.errorString().toStdString() << std::endl;
        return false;
    }

    QToolBar* toolBar = window->ui->mainToolBar;
    bool any=false;
    QIcon fileIcon(":awesome/file-regular.svg");

    QJsonObject root = jsonDoc.object();
    QString baseDirectory = root["base-directory"].toString();

    QJsonArray bookmarks = root["bookmarks"].toArray();
    foreach(QJsonValue bookmark, bookmarks) {
       QString file = bookmark["file"].toString();
       QString label = bookmark["label"].toString();
       QString shortCut = bookmark["short-cut"].toString();

       if (!any) toolBar->addSeparator();
       any = true;

       QAction* action = toolBar->addAction(fileIcon, label, [=]{ this->open(baseDirectory+QDir::separator()+file); });
       action->setShortcut(QKeySequence::fromString(shortCut));
       action->setToolTip(file+" "+shortCut);
    }

    toolBar->setIconSize(QSize(14,14));
    return true;
}

void FrechetViewApplication::copyContents(QByteArray contents, QString dst)
{
    QFileInfo dinfo(dst);
    QDir("/").mkpath(dinfo.absolutePath());
    QFile dfile(dst);
    dfile.open(QFile::WriteOnly);
    dfile.write(contents.constData());
}

void FrechetViewApplication::copyFile(QString src, QString dst)
{
    QFileInfo dinfo(dst);
    QDir("/").mkpath(dinfo.absolutePath());
    QFile::copy(src,dst);
}


void FrechetViewApplication::saveFileSettings(QSettings& settings, size_t histId)
{
    history.beginFile(settings,histId);

    window->getCurveView()->saveSettings(settings,"view.curve");
    window->getFreeSpaceView()->saveSettings(settings,"view.freespace");
    window->getControlPanel()->saveSettings(settings,"controls");

    history.endFile(settings);
}

void FrechetViewApplication::restoreFileSettings(QSettings& settings, size_t histId)
{
    history.beginFile(settings,histId,true);

    window->getCurveView()->restoreSettings(settings,"view.curve");
    window->getFreeSpaceView()->restoreSettings(settings,"view.freespace");
    window->getControlPanel()->restoreSettings(settings,"controls");

    window->setShowBounds(window->getFreeSpaceView()->showBounds());
    window->setSeparateCurves((window->getCurveView()->separateCurves()));

    history.endFile(settings);
}

bool FrechetViewApplication::setupCurves(bool topo)
{
    polyAlgorithm = frechet::poly::newPolyAlgorithm(topo);	//	topological sorting is only applicable in command-line-version
    frechet::poly::Algorithm::Status stat = polyAlgorithm->setup(P,Q);

    if (stat <= frechet::poly::Algorithm::Status::RUNNING) {
        Q_ASSERT(false);    //  not supposed to happen
    }

    switch(stat)
    {
    case frechet::poly::Algorithm::Status::SET_UP:
        //  OK
        break;
    case frechet::poly::Algorithm::Status::RUNNING:
    case frechet::poly::Algorithm::Status::YES:
    case frechet::poly::Algorithm::Status::NO:
    case frechet::poly::Algorithm::Status::NOT_SET_UP:
        Q_ASSERT(false);
        break;

    case frechet::poly::Algorithm::Status::P_EMPTY:
    case frechet::poly::Algorithm::Status::Q_EMPTY:

    case frechet::poly::Algorithm::Status::P_NOT_SIMPLE:
    case frechet::poly::Algorithm::Status::Q_NOT_SIMPLE:
        //  nothing to do
        return true;

    case frechet::poly::Algorithm::Status::P_NOT_CLOSED:
    case frechet::poly::Algorithm::Status::Q_NOT_CLOSED:
        if (P.isClosed()) {
            return true;
        }
        else {
            //  prefer closed curve first
            polyAlgorithm->swap();
            std::swap(P,Q);
            return false;
        }

    case frechet::poly::Algorithm::Status::P_NOT_COUNTER_CLOCKWISE:
    case frechet::poly::Algorithm::Status::Q_NOT_COUNTER_CLOCKWISE:
        //  fix orientation
        stat = polyAlgorithm->setup(P,Q, true);
        break;
    }

    switch(stat)
    {
    case frechet::poly::Algorithm::Status::P_NOT_COUNTER_CLOCKWISE:
    case frechet::poly::Algorithm::Status::Q_NOT_COUNTER_CLOCKWISE:
        //  already fixed ! if not fixable, the curve is not a proper polygon
        //Q_ASSERT(false);
        return true;

    case frechet::poly::Algorithm::Status::P_CONVEX:
    case frechet::poly::Algorithm::Status::Q_CONVEX:
        //  convex allows for easier calculation.
        //  We do not care (but may show a hint to the user!?)
        return true;
    }

    /*
     * Calculate decomposition, choose the smaller one.
     * We do this VERY early, so that we can swap P and Q.
     *
     * (would create lots of dependencies later)
     * */
    //bool exact = std::max(P.size(),Q.size()) <= 30;
    //  Note: the exact algorithm is not only expensive O(n^4),
    //  but -so it seems- bug ridden. Better not use it.
    bool p_is_primary = polyAlgorithm->decompose(false);
    if (!p_is_primary) {
        //  swap P and Q !!
        polyAlgorithm->swap();
        std::swap(P,Q);
    }
    Q_ASSERT(polyAlgorithm->Pcurve()==P);
    Q_ASSERT(polyAlgorithm->Qcurve()==Q);

    //  calculate Triangulations for P an Q
    polyAlgorithm->triangulate();

    return p_is_primary;
}

bool FrechetViewApplication::showFirstTimeHint()
{
    QString message = "Please open an input file. \n"
                      "Accepted files are:\n\n"
                "- Vector graphics (SVG, IPE)\n\t containing two paths\n"
                "- Script files (*.js); see examples";

    return QMessageBox::information(window, "Welcome", message,
                                    QMessageBox::Open | QMessageBox::Cancel,
                                    QMessageBox::Open) == QMessageBox::Open;
}


