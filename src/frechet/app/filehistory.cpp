
#include <filehistory.h>

#include <QFileInfo>

#ifdef __unix__
# include<sys/stat.h>
#endif

using namespace frechet;
using namespace app;

FileEntry::FileEntry(QString apath, size_t anid, QDateTime adate)
    : path(apath), id(anid), used(adate)
{ }

bool FileEntry::operator<(const FileEntry &that) const {
    //  sort by Timestamp DESC
    return used > that.used;
}


size_t FileHistory::insert(QString path, QDateTime date)
{
    int idx = indexOf(path);
    if (idx < 0) {
        files.push_front(FileEntry(path,++next_id,date));

        createMenuItem(files[0],0);
    }
    else {
        files.move(idx,0);
        files[0].used = date;

        moveMenuItem(idx,0);
    }
    return files[0].id;

    //  TODO updte menu
}


FileHistory::FileHistory()
    : files(), groupName(), next_id(0), menu(NULL)
{ }

void FileHistory::init(QString agroup)
{
    groupName = agroup;
    menu = new QMenu();
}

void FileHistory::restore(QSettings& settings)
{
    settings.beginGroup(groupName);
    next_id = settings.value("next_id").toUInt();

    QStringList children = settings.childGroups();
    foreach(QString id, children)
    {
        settings.beginGroup(id);
        QString path = settings.value("path").toString();

        if (QFileInfo::exists(path))
        {
            QDateTime used = settings.value("used").toDateTime();
            files.push_back(FileEntry(path, id.toUInt(), used));
            settings.endGroup();
        }
        else
        {
            settings.endGroup();
            settings.remove(id);
        }
    }

    settings.endGroup();

    //  sort by Timestamp DESC
    qSort(files);
    //  create sub-menu
    for(int idx=0; idx < files.size(); ++idx)
        createMenuItem(files[idx], idx);
}

size_t FileHistory::lastId() const
{
    if (files.isEmpty())
        return 0;
    else
        return files[0].id;
}

QString FileHistory::lastFile() const {
    if (files.isEmpty())
        return QString();
    else
        return files[0].path;
}

void FileHistory::attachMenu(QAction* recent)
{
    recent->setEnabled(!files.isEmpty());
    recent->setMenu(menu);
}

void FileHistory::beginFile(QSettings &settings, size_t id, bool forWrite)
{
    int idx = indexOf(id);
    Q_ASSERT(idx >= 0);

    settings.beginGroup(groupName);
    if (forWrite) {
        settings.setValue("next_id",(uint)next_id);
    }

    settings.beginGroup(QString::number(files[idx].id));

    if (forWrite) {
        settings.setValue("path",files[idx].path);
        settings.setValue("used",files[idx].used);
    }
    //  note: path is not stored, as it is subject to change

    // next, let the appplication store their settings
}

void FileHistory::endFile(QSettings& settings)
{
    settings.endGroup();
    settings.endGroup();
}

void FileHistory::onMenuSelected()
{
    QAction* action = (QAction*) QObject::sender();
    size_t id = action->data().toUInt();
    int idx = indexOf(id);
    if (idx >= 0) {
        emit open(files[idx].path);
    }
}


int FileHistory::indexOf(size_t id)
{
    for(int i=0; i < files.size(); ++i)
        if (files[i].id == id)
            return i;
    return -1;
}

QAction* FileHistory::createMenuItem(const FileEntry& e, int position)
{
    QFileInfo finfo(e.path);
    QAction* action = menu->addAction(finfo.fileName());
    action->setData((uint)e.id);

    connect(action,SIGNAL(triggered()), this, SLOT(onMenuSelected()));

    moveMenuItem(menu->actions().size()-1, 0);
    return action;
}

void FileHistory::moveMenuItem(int from, int to)
{
    menu->actions().move(from,to);
}

int FileHistory::indexOf(QString path)
{
    for(int i=0; i < files.size(); ++i)
        if (files[i].path == path)
            return i;
    return -1;
}


