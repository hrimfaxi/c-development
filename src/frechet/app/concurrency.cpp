
#include <frechet/app/concurrency.h>
//#include <poly/parallel.h>
#include <stack>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <assert.h>

#ifndef UNIT_TEST
#include <QCoreApplication>
#include <QDir>
#include <qdebug.h>
#endif

#include <atomic>
#ifdef __GNUC__
#include <cpuid.h>
//#include <boost/thread.hpp>
#endif

using namespace frechet;
using namespace app;

ConcurrencyContext::ConcurrencyContext()
: tbb_context(nullptr), num_threads(0),num_cores(0)
{
    num_cores = tbb::task_scheduler_init::default_num_threads();
}

ConcurrencyContext::~ConcurrencyContext()
{
    close();
}


ConcurrencyContext ConcurrencyContext::instance;

void ConcurrencyContext::setup(int max_threads)
{
	assert(tbb_context == nullptr);
	num_threads = (max_threads >= 1) ? max_threads : (num_cores/*/2?*/);
	//  TODO better num_cores / 2 ?
	tbb_context = new tbb::task_scheduler_init(num_threads);
}

int ConcurrencyContext::countThreads() {
	return instance.num_threads; 
}

int ConcurrencyContext::countCores() { 
	return instance.num_cores; 
}

tbb::tbb_thread::id ConcurrencyContext::currentThread() { 
	return tbb::this_tbb_thread::get_id(); 
}

bool ConcurrencyContext::hasGpuSupport() {
	return instance.queue != nullptr;
}

std::string ConcurrencyContext::gpuName() {
	return instance.device_name;
}

cl_uint ConcurrencyContext::countGpuUnits() {
	return instance.gpu_units;
}

cl_context ConcurrencyContext::clContext() {
	assert(instance.ctx != nullptr);
	return instance.ctx;
}

cl_command_queue ConcurrencyContext::clQueue() {
	assert(instance.queue != nullptr);
	return instance.queue;
}

void ConcurrencyContext::maxMaxtrixTile(size2_t& result) {
	result[0] = instance.max_tile[0];
    result[1] = instance.max_tile[1];
}


bool ConcurrencyContext::setupGpu(size2_t amax_tile)
{
	/* Setup OpenCL environment. */
	platform = nullptr;

	cl_platform_id platforms[4];
	cl_uint num_platforms;
	clerr = clGetPlatformIDs(4, platforms, &num_platforms);
	for (int plf = 0; plf < num_platforms; ++plf)
	{
		clerr = clGetDeviceIDs(platforms[plf], CL_DEVICE_TYPE_GPU, 1, &device, NULL);
		if (clerr != CL_SUCCESS) continue;
			
		char name[1024];
		char version[1024];
		char profile[1024];
		char extensions[1024];
		clerr = clGetPlatformInfo(platforms[plf], CL_PLATFORM_NAME, 1024, name, NULL);
		clerr = clGetPlatformInfo(platforms[plf], CL_PLATFORM_VERSION, 1024, version, NULL);
		clerr = clGetPlatformInfo(platforms[plf], CL_PLATFORM_PROFILE, 1024, profile, NULL);
		clerr = clGetPlatformInfo(platforms[plf], CL_PLATFORM_EXTENSIONS, 1024, extensions, NULL);

/*		std::cout << "Platform " << plf << std::endl
			<< platforms[plf] << " = " << name << " " << version << ", "
			<< profile << ","
			<< extensions << std::endl << std::endl;
*/
		platform = platforms[plf];
		cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
		props[1] = (cl_context_properties)platform;

		ctx = clCreateContext(props, 1, &device, NULL, NULL, &clerr);
		queue = clCreateCommandQueue(ctx, device, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &clerr);
        //  Important: Out-of-Order queue
        //  to imposed a specific order on scheduled kernels, use cl_events and barriers
		clGetDeviceInfo(device, CL_DEVICE_NAME, 1024, &name, NULL);
		clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &gpu_units, NULL);
		device_name = name;

		//  Load Kernels
#ifndef UNIT_TEST
		const char* kernelDir = findKernelDirectory("clm4rm");
#else
		//	Unit Test environment w/out QT
		const char* kernelDir = "src/clm4rm";
#endif
		clerr = clm4rm_setup(kernelDir, ctx, device);
		assert(clerr == CL_SUCCESS);

		//	max. tile size fo cubic multiplication
		// tile_n = number of compute units; limited by max. units per group
		// tile_m = number of row/cols per compute unit; limited by available shared memory
		//		for best performance, we should allow >= 2 groups per shared memory area
		// may be overridden by command line
		// @see clcubic_mul()
		if (amax_tile[0]<=0)
		    max_tile[0] = sqrt(max_group_size/32);
		else
		    max_tile[0] = amax_tile[0];
		if (amax_tile[1]<=0)
		    max_tile[1] = (sqrt(1 + 17*shared_mem_words)-1)/(68*max_tile[0]);
		else
		    max_tile[1] = amax_tile[1];
		max_tile[1] = std::min((size_t)MAX_TILE_M,max_tile[1]);
    }
	return hasGpuSupport();
}

#ifndef UNIT_TEST
QByteArray kernelDirectory;

const char* ConcurrencyContext::findKernelDirectory(const char* dirname)
{
	QStringList prefixes = { "Resources","src","rsrc" };
	QString dirstring(dirname);
	//	Look for folder "Resources/clm4rm" or "src/clm4rm" or "rsrc/clm4rm"
	QDir dir = QCoreApplication::applicationDirPath();
	for(;; dir.cdUp()) {
		for(QString prefix : prefixes)
		{
		    QString subdir = prefix+"/"+dirstring;
		    if (dir.exists(subdir)) {
				kernelDirectory = dir.absoluteFilePath(subdir).toLocal8Bit();
				return kernelDirectory.constData();
			}
		}
	}
}
#endif // QT

void ConcurrencyContext::close()
{
	delete tbb_context;
	tbb_context=nullptr;
	//	release OpenCL stuff
	if (queue)
		clReleaseCommandQueue(queue);
	if (hasGpuSupport())
		clm4rm_tear_down(ctx,device);
}

void ConcurrencyContext::cpuid(int leaf, int level, unsigned int regs[4])
{
#ifdef _MSC_VER
	__cpuidex((int*)regs, leaf, level);
#endif
#ifdef __GNUC__
	__cpuid_count(leaf, level, regs[0], regs[1], regs[2], regs[3]);
#endif
}

void ConcurrencyContext::cpuid(int leaf, unsigned int regs[4])
{
#ifdef _MSC_VER
	__cpuid((int*)regs, leaf);
#endif
#ifdef __GNUC__
	__cpuid(leaf, regs[0], regs[1], regs[2], regs[3]);
#endif
}

int ConcurrencyContext::cacheSize(int level)
{
	unsigned int E[4];
	cpuid(4,level,E);

	// = (Ways + 1) * (Partitions + 1) * (Line_Size + 1) * (Sets + 1)
	// = (EBX[31:22] + 1) * (EBX[21:12] + 1) * (EBX[11:0] + 1) * (ECX + 1)

	unsigned int EBX = E[1];
	unsigned int ECX = E[2];
	unsigned int Ways = (EBX >> 22) & 0x3ff;
	unsigned int Partitions = (EBX >> 12) & 0x3ff;
	unsigned int Line_Size = EBX & 0x0fff;
	unsigned int Sets = ECX;
	return (Ways + 1) * (Partitions + 1) * (Line_Size + 1) * (Sets + 1);
}

std::stack<time_point> time_stack;


void frechet::app::pushTimer()
{
	time_stack.push(Clock::now());
}

time_point frechet::app::printTimer(std::string label, bool do_print)
{
	long microsecs;
	time_point t1;
	time_point t2 = Clock::now();

	if (time_stack.empty()) {
		pushTimer();
	}
	else {
		t1 = time_stack.top();
	}

	microsecs = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

	if (do_print) {
		std::string spaces(std::max<int>(0, 12 - label.length()), ' ');
		std::cout << std::fixed << std::setprecision(3)
			<< "    " << label << ": "<< spaces
							   //<< microsecs << " µs" << " = "
			<< (((double)microsecs) / 1e3) << " ms" << " = "
			<< (((double)microsecs) / 1e6) << " s"
			<< std::endl;
	}
	return t2;
}

#ifdef Q_DEBUG
# define DO_DEBUG 1
#else
# define DO_DEBUG 0
#endif

time_point frechet::app::printDebugTimer(std::string label)
{
	return printTimer(label, DO_DEBUG);
}

time_point frechet::app::popTimer(std::string label, bool do_print)
{
	time_point t = printTimer(label,do_print);
	time_stack.pop();
	return t;
}

time_point frechet::app::popDebugTimer(std::string label)
{
	return popTimer(label, DO_DEBUG);
}
