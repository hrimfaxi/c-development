
#include <app/workerthread.h>

using namespace frechet;
using namespace app;

/**
    Note: ownership of job object passes to QThreadPool.
    Do not delete the object youself!
 */
void WorkerJobHandle::startJob(WorkerJob* new_job)
{
    QMutexLocker guard(&mut);
    if (job) job->cancelRequested=true;
    job = new_job;
    job->handle = this;
    job->cancelRequested = false;
    QThreadPool::globalInstance()->start(job);    
}


void WorkerJobHandle::invalidate(WorkerJob *ajob)
{
    QMutexLocker guard(&mut);
    if (ajob==job) job = NULL;
}
/**
 * Note that this method does not stop the long-running tasks immediately.
 * It only raises the canclJob flag. Long-running tasks are expected to
 * poll the flag in regular intervals, and throw an InterruptedException.
 * The exception is caught by the run() method.
 */
void WorkerJobHandle::cancelJob()
{
    QMutexLocker guard(&mut);
    if (job) job->cancelRequested=true;
}

void WorkerJobHandle::shutDown() {
    cancelJob();
    QThreadPool::globalInstance()->waitForDone();
}


void WorkerJob::run() {
    try {
        runJob();
    } catch(InterruptedException&) {
        afterInterrupted();
    } catch(...) {
        std::cerr << "An Error occured" << std::endl;
    }
    handle->invalidate(this);
    //  Note: deletion is delegated to QThreadPool
}
