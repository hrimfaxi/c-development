
#include <frechetviewapplication.h>
#include <concurrency.h>
#include <iostream>
#include <QScreen>
#include <QProcess>
#include <QCommandLineParser>

#define QT_SCALE_FACTOR                 "QT_SCALE_FACTOR"
#define QT_AUTO_SCREEN_SCALE_FACTOR     "QT_AUTO_SCREEN_SCALE_FACTOR"
#define QT_SCREEN_SCALE_FACTORS         "QT_SCREEN_SCALE_FACTORS"

#ifdef Q_OS_WIN
void m4ri_init();
#endif

using namespace frechet;
using namespace app;

QStringList argsList(int argc, char* argv[]);
bool calculateScaleFactors(qreal minDpi, qreal targetDpi, qreal maxDpi, QByteArray& factors);

#ifdef HAS_OPENCL
int printCLInfo();
#endif

bool restart(QStringList args, QByteArray scaleFactors)
{
    QString program = args[0];
    args.removeAt(0);
    args.append("--scale");
    args.append(scaleFactors);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
    QProcess proc;
    proc.setProgram(args[0]);
    proc.setArguments(args);
    return proc.startDetached();
#else
    //  backward compatibility with Qt 5.9...
    return QProcess::startDetached(program,args);
#endif
}

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN) && defined(QT_DEBUG)
    // Get current flag
    int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );

    // Turn on leak-checking bit.
    tmpFlag |= _CRTDBG_ALLOC_MEM_DF;
    tmpFlag |= _CRTDBG_LEAK_CHECK_DF;
    tmpFlag |= _CRTDBG_CHECK_ALWAYS_DF;
    tmpFlag &= ~_CRTDBG_REPORT_FLAG;

    // Set flag to the new value.
    _CrtSetDbgFlag( tmpFlag );

//    m4ri_init();
#endif
//	std::cout << " L1 Cache Size = " << frechet::ConcurrencyContext::cacheSize(1) << std::endl;
//	std::cout << " L2 Cache Size = " << frechet::ConcurrencyContext::cacheSize(2) << std::endl;
//	std::cout << " L3 Cache Size = " << frechet::ConcurrencyContext::cacheSize(3) << std::endl;

    QCoreApplication::setOrganizationName("ti.fu-hagen.de");
    QCoreApplication::setApplicationName("Frechet View");
    //  TODO Gnome does not recognize UTF-8 characters correctly. Is there a work-around?
    //  this is also the key for QSettings. Don't change it.
    QCoreApplication::setApplicationVersion("1.6.0");

    //QCoreApplication::setAttribute(Qt::AA_Use96Dpi);    // influences Font scaling
    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    //  works in a way, but scaling factors are too large (why?)

    //QCoreApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

    //QCoreApplication::setAttribute(Qt::AA_UseDesktopOpenGL);
    //QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);

    QCommandLineParser parser;
    parser.setApplicationDescription("Fréchet Distance Free-Space Utility");
    QCommandLineOption optVersion = parser.addVersionOption();
	QCommandLineOption optHelp = parser.addHelpOption();

    QCommandLineOption screenScale("scale","gui scale", "scale-value");
    parser.addPositionalArgument("input","input file (optional)", "[*.svg,ipe,js,jscript]");

    //<file> (-curve|-polygon|-kfrechet) (-optimize|-decide=<eps>) [-cores=1,2,4,...] [-gpu|-nogpu]
    /*  Command Line Interface */
    QCommandLineOption optCurve({"curve","c"},          "Fréchet distance for polygonal curves");
    QCommandLineOption optPoly({"polygon","p"},         "Fréchet distance for simple polygons");
    QCommandLineOption optKFrechet({"kfrechet","k"},    "k-Fréchet distance\n");

    QCommandLineOption optOptimize({"optimize","opt"},   "calculate Fréchet distance (exact)");
    QCommandLineOption optDecide({"decide","dec"},      "decide whether Fréchet distance <= epsilon", "epsilon");
    QCommandLineOption optApprox({"approx","app"},      "approximate to limited accuracy\n", "accuracy");

    QCommandLineOption optCores({"cores","cpu"},        "run parallel on multiple cores", "cores");
    QCommandLineOption optGpu("gpu",                    "GPU support");
    QCommandLineOption optTile("tile",                  "matrix tile size (n,m)", "tile");
	QCommandLineOption optTopo("large",                "large polygons\n");

    //  Algorithm
    parser.addOption(optCurve);
    parser.addOption(optPoly);
    parser.addOption(optKFrechet);
    //  Variant
    parser.addOption(optDecide);
    parser.addOption(optOptimize);
    parser.addOption(optApprox);
    //  Concurrency
    parser.addOption(optCores);
    parser.addOption(optGpu);
    parser.addOption(optTile);
    parser.addOption(optTopo);
    parser.addOption(screenScale);
	
    QStringList args = argsList(argc,argv);
    bool optOk = parser.parse(args);

	if (!optOk) {
		QCoreApplication qtApp(argc, argv);
		std::cout << parser.errorText().toStdString() << std::endl << std::endl;
		parser.showHelp();
		return -1;
	}

	if (parser.isSet(optVersion)) {
		QCoreApplication qtApp(argc, argv);
		parser.showVersion();
		return -1;
	}

	if (parser.isSet(optHelp)) {
		QCoreApplication qtApp(argc, argv);
		parser.showHelp();
		return -1;
	}

    //  Concurrency parameters
    int cores=0;
    if (parser.isSet("cores")) {
        bool intok;
        cores = parser.value(optCores).toInt(&intok);
        if (!intok) throw std::runtime_error("Number of cores expected.");
    }

    ConcurrencyContext::instance.setup(cores);

	bool gpu = parser.isSet(optGpu);
	bool topo = parser.isSet(optTopo); // only headless!

    //  Input file parameter
    QString inputFile = "";
    if (! parser.positionalArguments().isEmpty())
        inputFile = parser.positionalArguments().first();

    //  Algorithm parameters
    if ( parser.isSet(optCurve) || parser.isSet(optKFrechet) || parser.isSet(optPoly)
         || parser.isSet(optOptimize) || parser.isSet(optDecide))
    {
        // run headless
        QCoreApplication qtApp(argc,argv);
        FrechetViewApplication frApp(argv[0],false);
        parser.process(args);

        if (gpu) {
            size2_t max_tile = { 0,0 };
            if (parser.isSet(optTile)) {
                QString tile_str = parser.value(optTile);
                QStringList tile_strs = tile_str.split(QRegExp("\\D+"), QString::SkipEmptyParts);
                if (tile_strs.size() >= 1) max_tile[0] = tile_strs[0].toUInt();
                if (tile_strs.size() >= 2) max_tile[1] = tile_strs[1].toUInt();
            }
            ConcurrencyContext::instance.setupGpu(max_tile);
        }

        try {
            if (inputFile.isEmpty()) throw std::runtime_error("Input file expected.");
            if (! QFileInfo::exists(inputFile)) throw std::runtime_error("File "+inputFile.toStdString()+" not found.");

            FrechetViewApplication::Algorithm algo;

            if ((parser.isSet(optCurve) + parser.isSet(optPoly) + parser.isSet(optKFrechet)) > 1)
                throw std::runtime_error("Please choose only one algorithm.");

            if (parser.isSet(optCurve))     algo = FrechetViewApplication::Algorithm::ALGORITHM_CURVE;
            if (parser.isSet(optPoly))      algo = FrechetViewApplication::Algorithm::ALGORITHM_POLYGON;
            if (parser.isSet(optKFrechet))  algo = FrechetViewApplication::Algorithm::ALGORITHM_K_FRECHET;
            if (algo==0) algo = FrechetViewApplication::Algorithm::ALGORITHM_CURVE;

			if (topo && (algo != FrechetViewApplication::Algorithm::ALGORITHM_POLYGON))
				throw std::runtime_error("Option --large is only applicable to -p.");

            double accuracy=NAN; // == decide
            double epsilon;
            bool ok=true;

            if (gpu && !ConcurrencyContext::hasGpuSupport())
				throw std::runtime_error("No compatible GPU found. Please install an OpenCL driver.");

            if ( (parser.isSet(optDecide)+parser.isSet(optOptimize)+parser.isSet(optApprox)) >= 2 )
                throw std::runtime_error("Please choose either -decide or -optimize or -approx, not both.");
            if (parser.isSet(optDecide)) {
                accuracy=NAN;
                epsilon = parser.value(optDecide).toDouble(&ok);
                if (!ok) throw std::runtime_error("Epsilon expected for decision variant.");
            }
            if (parser.isSet(optOptimize)) accuracy=0.0; // == critical values
            if (parser.isSet(optApprox)) accuracy = parser.value(optApprox).toDouble(&ok);
            if (!ok || (!std::isnan(accuracy) && (accuracy < 0.0)))
                throw std::runtime_error("Accuracy expected for approximation variant.");

            //  Set up concurrency. Applies only to Poly algorithm.
            if (gpu==FrechetViewApplication::TriState::YES && !ConcurrencyContext::hasGpuSupport())
                throw std::runtime_error(">>> ERROR: no viable GPU device found. Use -nogpu instead.");

            if ((cores > 0) && (gpu != FrechetViewApplication::TriState::DEFAULT)
                && (algo == FrechetViewApplication::ALGORITHM_K_FRECHET))
                std::cout << ">>> Info:         core and gpu settings do not apply for this algorithm. Running on one core instead." << std::endl;
            else if (ConcurrencyContext::countThreads() > 1)
                std::cout << "    Info:         using "
                        <<ConcurrencyContext::countThreads()<<" threads on "
                        <<ConcurrencyContext::countCores()<<" CPU cores."<<std::endl;

            if (ConcurrencyContext::hasGpuSupport()) {
                size2_t max_tile;
                ConcurrencyContext::maxMaxtrixTile(max_tile);
                std::cout << "    Info:         using "
                          << ConcurrencyContext::gpuName()
                          << " with "
                          << ConcurrencyContext::countGpuUnits()
                          << " units." << std::endl
                          << "                  "
                          << "matrix tile = " << max_tile[0] << "," << max_tile[1]
                          << std::endl;
            }

            return frApp.commandLineInterface(inputFile, algo, topo, accuracy, epsilon);

        } catch (std::exception& error) {
            std::cout << ">>> ERROR: " << error.what() << std::endl << std::endl << std::endl << std::endl;
            parser.showHelp();
            return -1;
        }
    }

    //  Run with GUI. Adjust scale factor as necessary.

    /*
     * Hi-DPI scaling
     *
     *  ===Linux===
     *  the following environment variables work well:
     *
     *  export QT_SCALE_FACTOR=1
     *  export QT_AUTO_SCREEN_SCALE_FACTOR=0
     *  export QT_SCREEN_SCALE_FACTORS=2
     *
     *  other mechanisms (like Qt::AA_EnableHiDPIScaling, etc.) are sub-optimal
     *  The tricky part comes now:
     *  - environment variables must be set before QApplication is constructed
     *  - actual pixel ratios can only be calculated *after* QApplication is constructed
     *  ... a dilemma. We solve it by storing the scale factors in preferences. If stored
     *  preferences do not match actual scale factors, the application is *restarted* with
     *  adjusted settings (but this will only happen once after the screen resolution changed).
     *
     *  The actual scale factors are retrieved from three locations
     *  (1) process Environment variables
     *  (2) application preferences (QSettings)
     *  (3) command line arguments (overrides settings)
     * */
#ifndef Q_OS_MAC
    //  (1) process Environment variables
    QByteArray scaleFactors;
    {
        QSettings settings;
        if (parser.isSet(screenScale)) {
            //  (3) command line arguments (override settings)
            scaleFactors = parser.value("scale").toLocal8Bit();
        }
        else
        {   //  (1) Environment variable
            scaleFactors = qgetenv(QT_SCREEN_SCALE_FACTORS);

            if (scaleFactors.isEmpty()) //  fall back to (2) application preferences (QSettings)
                scaleFactors = settings.value(QT_SCREEN_SCALE_FACTORS).toByteArray();
        }
        if (!scaleFactors.isEmpty())
            settings.setValue(QT_SCREEN_SCALE_FACTORS,scaleFactors);
    }

    // qputenv MUST be done BEFORE QApplication is constructed
    if (!scaleFactors.isEmpty()) {
        qputenv(QT_SCALE_FACTOR,"1");
        qputenv(QT_AUTO_SCREEN_SCALE_FACTOR,"0");
        qputenv(QT_SCREEN_SCALE_FACTORS,scaleFactors);
        //std::cout << "QT_SCREEN_SCALE_FACTORS=" << scaleFactors.constData() << std::endl;
    }

    //std::cout << "Hello from " << QCoreApplication::applicationName().toStdString() << std::endl;
    QApplication qtApp(argc,argv);
    

    //  re-assess scale factors
    //  scale factors should be chosen so that the logical resolution
    //  is equal (or close) to 96 dpi. If not, we re-start the whole application
    //  with corrected settings
    if(! calculateScaleFactors(90,96,120, scaleFactors)
            && !parser.isSet("scale"))
    {
        //  relaunch application with corrected scale factors

        if (restart(args,scaleFactors)) {
            std::cout << "Relaunch with --scale " << scaleFactors.toStdString() << std::endl;
            return 0;
        }
        else {
            std::cout << "Relaunch failed " << std::endl;
        }
    }    
#endif // Q_OS_MAC
#ifdef Q_OS_MAC
    QApplication qtApp(argc,argv);
    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
	if (topo)
		std::cout << "WARNING: option --large is ignored. Use it only with command line option -p." << std::endl;
    if (gpu)
        std::cout << "WARNING: option --gpu is ignored. Use it only with command line option -p." << std::endl;

	FrechetViewApplication frApp(argv[0], true);
    frApp.init(inputFile);
    return qtApp.exec();
}

QStringList argsList(int argc, char* argv[])
{
    QStringList result;
    for(int i=0; i < argc; ++i)
        result += QString::fromLocal8Bit(argv[i]);
    return result;
}



bool calculateScaleFactors(qreal minDpi, qreal targetDpi, qreal maxDpi, QByteArray& factors)
{
    bool ok=true;
    QList<QScreen*> screens = QGuiApplication::screens();
    foreach(QScreen* screen, screens)
    {
/*      std::cout << "screen="<<screen->name().toStdString() << std::endl
                  << "size="<<screen->size().width()<<" x " << screen->size().height() << std::endl
                  << "virtual size="<<screen->virtualSize().width()<<" x " << screen->virtualSize().height() << std::endl
                  << "physical size="<<screen->physicalSize().width()<<" x " << screen->physicalSize().height() << std::endl
                  << "pixel ratio="<<screen->devicePixelRatio() << std::endl
                  << "logical dpi="<<screen->logicalDotsPerInch() << std::endl
                  << "physical dpi="<<screen->physicalDotsPerInch() << std::endl;
*/
        qreal screenDpi = screen->logicalDotsPerInch();
        if ((screenDpi < minDpi) || (screenDpi > maxDpi))
            ok = false;

        if (!factors.isEmpty()) factors += ";";
        factors += QByteArray::number(screen->devicePixelRatio() * screenDpi/targetDpi,'g',2);
    }
    return ok;
}
