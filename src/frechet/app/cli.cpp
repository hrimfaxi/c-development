
#include <frechetviewapplication.h>
#include <concurrency.h>
#include <fs_path.h>

using namespace frechet;
using namespace poly;
using namespace k;
using namespace reach;
using namespace app;

void FrechetViewApplication::cli_setup(QString inputFile, Algorithm algo)
{
#ifdef QT_DEBUG
    std::cout << ">>> Debug Build (don't use for serious benchmarks)." << std::endl;
#endif

    //  read input file
	QFileInfo file(inputFile);
	reader.readAll(file.absoluteFilePath());
	if (!reader.errorMessage().isEmpty()) {
		std::string message = reader.errorMessage().toStdString();
		throw std::runtime_error(message);
	}
	if (reader.getResults().size() < 2)
		throw std::runtime_error("Input file with two curves expected.");

	P = reader.getResults()[0].toCurve();
	Q = reader.getResults()[1].toCurve();

	auto fstring = inputFile.toLocal8Bit();
	std::cout	<< "    Input:        " << fstring.data() << std::endl
				<< "    Data:         P=" << P.size() << " vertices. Q=" << Q.size() << " vertices. " << std::endl;
}



int FrechetViewApplication::cli_decideKFrechet(double epsilon)
{
    freeSpace = frechet::FreeSpace::ptr(new frechet::FreeSpace(P,Q));
    kAlgorithm.reset(new frechet::k::kAlgorithm(freeSpace));

    freeSpace->calculateFreeSpace(epsilon);
    freeSpace->calculateBounds(epsilon);
    freeSpace->components().calculateComponents(*freeSpace);

    kAlgorithm->runGreedy();
    /*  The results of the Greedy algorithm
     *  serve as lower and upper bounds for the Brute Force algorithm
     * */
    //bf.k_min = greedy.lowerBound();
    //bf.k_max = greedy.upperBound();

    if (kAlgorithm->optimalResult().k_max == kAlgorithm::NO_SOLUTION)
        return kAlgorithm::NO_SOLUTION;

    if (kAlgorithm->optimalResult().k_optimal != kAlgorithm::UNKNOWN)
        //  the Greedy result is already optimal. Fine.
        return kAlgorithm->optimalResult().k_optimal;

    std::cout << "    Greedy Result: "
              <<kAlgorithm->optimalResult().k_min<<" <= k <= "<<kAlgorithm->optimalResult().k_max
              <<std::endl;

    return kAlgorithm->runBruteForce();
}

int FrechetViewApplication::commandLineInterface(QString inputFile,
                                                 Algorithm algo, bool topo,
                                                 double accuracy, double epsilon) throw(std::exception)
{

    cli_setup(inputFile, algo);

    bool yes;
    double d_F;

    pushTimer();
    FSPath::ptr noPath;

    switch(algo)
    {
    case ALGORITHM_CURVE:
        std::cout << "    Algorithm:    Fréchet Distance for Curves. " <<std::endl;

        polyAlgorithm = newPolyAlgorithm(topo);
        polyAlgorithm->setup(P,Q,true);

        if (accuracy==0.0) {
            std::cout << "                  "<<"Optimisation variant. "<<std::endl;
            d_F = polyAlgorithm->optimiseCurve(0.0);
        }
        else if (accuracy > 0.0) {
            std::cout << "                  "<<"Approximation to "<<accuracy<<std::endl;
            d_F = polyAlgorithm->optimiseCurve(accuracy);
        }
        else if (std::isnan(accuracy)) {
            std::cout << "                  "<<"Decision variant. Is d_F <= "<<epsilon<<" ?"<<std::endl;
            freeSpace = frechet::FreeSpace::ptr(new frechet::FreeSpace(P,Q));
            freeSpace->calculateFreeSpace(epsilon);
            yes = (bool)polyAlgorithm->decideCurve(freeSpace,noPath,NAN,true);
        }
        else
            throw std::runtime_error("unexpected accuracy");
        break;

    case ALGORITHM_POLYGON:

		std::cout << "    Algorithm:    Fréchet Distance for Simple Polygons. "<<std::endl;
		if (accuracy == 0.0)
			std::cout << "                  "<<"Optimisation variant." << std::endl;
		else if (accuracy > 0.0)
			std::cout << "                  "<<"Approximation to " << accuracy << std::endl;
		else if (std::isnan(accuracy))
			std::cout << "                  "<<"Decision variant. Is d_F <= " << epsilon << " ?" << std::endl;

        setupCurves(topo);  //  also instantiates PolyAlgorithm and triangulates, etc.
		printTimer("Setup Curves");

		if (sizeof(accurate_float) > sizeof(double)) {
			std::cout << "    Info:         Using extended floating point arithmetic "
						<<"("<< std::numeric_limits<accurate_float>::digits <<" digits mantissa) for intermediate results." 
						<< std::endl;
		}

        switch(polyAlgorithm->status())
        {
        case poly::Algorithm::SET_UP:            
            break;
        default:
            throw std::runtime_error("unexpected status "+polyAlgorithm->status());
        //  die Kurven müssen mind. 2 Punkte enthalten
        case poly::Algorithm::P_EMPTY:
        case poly::Algorithm::Q_EMPTY:
            throw std::runtime_error("Polygons must not be empty.");
        //  beide Kurven müssen geschlossen sein
        case poly::Algorithm::P_NOT_CLOSED:
        case poly::Algorithm::Q_NOT_CLOSED:
            throw std::runtime_error("Polygons must be closed. Use -curve instead.");
        //  beide Kurven müssen einfache Polygone seine
        case poly::Algorithm::P_NOT_SIMPLE:
        case poly::Algorithm::Q_NOT_SIMPLE:
            throw std::runtime_error("Polygons are not simple. Use -curve instead.");
        //  beide Polygone müssen gegen den Uhrzeigersinn orientiert sein
        case poly::Algorithm::P_NOT_COUNTER_CLOCKWISE:
        case poly::Algorithm::Q_NOT_COUNTER_CLOCKWISE:
            throw std::runtime_error("Polygons are not counter-clockwise. This is a bug !");
        //  für konvexe Polygone könenn wir einen einfacheren Algorithmus verwenden
        case poly::Algorithm::P_CONVEX:
        case poly::Algorithm::Q_CONVEX:
            throw std::runtime_error("One of the polyons is convex. Use -curve instead.");
        }

        if (accuracy==0.0) {
            d_F = polyAlgorithm->optimisePolygon(noPath);
        }
        else if (accuracy > 0.0) {
            d_F = polyAlgorithm->optimisePolygon(noPath,accuracy);
        }
        else if (std::isnan(accuracy)) {
            freeSpace = frechet::FreeSpace::ptr(new frechet::FreeSpace(P,Q));
            freeSpace->calculateFreeSpace(epsilon);
            yes = (bool)polyAlgorithm->decidePolygon(freeSpace,noPath,epsilon,true);
        }
        else
            throw std::runtime_error("Unexpected accuracy");
        break;

    case ALGORITHM_K_FRECHET:
        if (!std::isnan(accuracy))
            throw std::runtime_error("Optimisation variant not implemented for k-Fréchet algorithm.");

        std::cout << "    Algorithm:    k-Fréchet Distance."<<std::endl
                  << "                  Calculate k for epsilon="<<epsilon << std::endl;

        int k = cli_decideKFrechet(epsilon);
        std::cout << "    Result:       ";
        if (k >= 0)
            std::cout <<"k = "<<k<<std::endl;
        else
            std::cout <<"no solution."<<std::endl;
        break;
    }

    Clock::time_point t2 = Clock::now();

    if (algo != ALGORITHM_K_FRECHET)
    {
        std::cout << std::fixed << std::setprecision(std::numeric_limits<double>::digits10);
        if (!std::isnan(accuracy)) {
            std::cout <<  "    Result:       d_F = " << d_F << std::endl;
        }
        else if (yes) {
            std::cout <<  "    Decision:     YES. d_F <= " << epsilon << std::endl;
        }
        else {
            std::cout <<  "    Decision:     NO.  d_F >  " << epsilon << std::endl;
        }
    }

    popTimer("Elapsed time");
    return 0;
}
