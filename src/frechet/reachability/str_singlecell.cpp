
#include <structure.h>


using namespace frechet;
using namespace data;
using namespace reach;


void Structure::singleCell(int i, int j)
{
    //  Free-Space intervals:
    SingleCellAuxData aux;
    aux.F[VERTICAL][FIRST] = fs->cell(i,j).L + (double)j;        //  left
    aux.F[HORIZONTAL][FIRST] = fs->cell(i,j).B + (double)i;      //  bottom
    aux.F[VERTICAL][SECOND] = fs->cell(i+1,j).L + (double)j;     //  right
    aux.F[HORIZONTAL][SECOND] = fs->cell(i,j+1).B + (double)i;   //  top

    //  create segments
    for(Orientation ori=HORIZONTAL; ori <= VERTICAL; ++ori)
        aux.arr[ori] = freeSpaceArrangement(aux, ori);

    /**
        Creation of segments and linking of l,h pointers
        must be done in two steps
        (the segments must exists before we can link the l,h pointers)
      */

    //  create segments, store in *seg
    memset(aux.seg, 0, 2*2*5*sizeof(Pointer));

    createSingleCellSegments(aux, HORIZONTAL, i);
    createSingleCellSegments(aux, VERTICAL, j);

    //  calculate lowest,highest segment on each edge
    for(Orientation ori=HORIZONTAL; ori <= VERTICAL; ++ori)
        for(Direction dir=FIRST; dir <= SECOND; ++dir)
            aux.P[ori][dir] = reachableInterval(aux.seg[ori][dir]);

    //  link l,h pointers
    for(Orientation ori=HORIZONTAL; ori <= VERTICAL; ++ori)
        linkSingleCellSegments(aux, ori);

    //  copy arrays to linked list
    for(Orientation ori=HORIZONTAL; ori <= VERTICAL; ++ori)
        for(Direction dir=FIRST; dir <= SECOND; ++dir)
            copySegments(boundary(ori,dir), aux.seg[ori][dir]);

#ifdef QT_DEBUG
    frechet::Rect r (i,j, i+1,j+1);
    Q_ASSERT(!after_single_cell || (after_single_cell(this, &r,
                                                     nullptr, nullptr, HORIZONTAL),true));
#endif
}

int Structure::freeSpaceArrangement(SingleCellAuxData& aux, Orientation ori)
{
    Interval LF = aux.F[ori][FIRST];
    Interval RF = aux.F[ori][SECOND];

    return freeSpaceArrangement(LF,RF);
}

int Structure::freeSpaceArrangement(Interval LF, Interval RF)
{
    if (!LF && !RF)     // (0) completely empty
        return 0;

    if (LF && !RF)     //  (1) left reachable, right not
        return 1;

    if (!LF && RF)     //  (2) right reachable
        return 2;

    Q_ASSERT(LF && RF);

    if (LF.lower() < RF.lower())
    {
        //  (I)
        //          |
        //   |     ---
        //  ---

        if (LF.upper() < RF.lower())
        //         ---
        //          |
        //         ---
        //
        //  ---
        //   |
        //  ---
            return 3;

        if (LF.upper() == RF.lower())
        //         ---
        //          |
        //  ---    ---
        //   |
        //  ---
            return (RF.upper() > LF.upper()) ? 4:6;

        Q_ASSERT(LF.upper() > RF.lower());
        //  ---     |
        //   |     ---
        //  ---

        if (RF.upper() < LF.upper())
        //  ---
        //   |     ---
        //   |      |
        //   |     ---
        //  ---
            return 5;

        if (RF.upper() == LF.upper())
        //  ---    ---
        //   |      |
        //   |     ---
        //   |
        //  ---
            return 6;

        Q_ASSERT(RF.upper() > LF.upper());
        //         ---
        //          |
        //  ---     |
        //   |      |
        //   |     ---
        //   |
        //  ---
        return 7;
    }
    if (LF.lower() == RF.lower())
    {
        //  (II)
        //   |      |
        //  ---    ---

        if (LF.upper() < RF.upper())
        //         ---
        //          |
        //  ---     |
        //   |      |
        //  ---    ---
            return 8;

        if (LF.upper() == RF.upper())
        //  ---    ---
        //   |      |
        //  ---    ---
            return 9;

        Q_ASSERT(LF.upper() > RF.upper());
        //  ---
        //   |
        //   |     ---
        //   |      |
        //  ---    ---
        return 10;
    }
    Q_ASSERT(LF.lower() > RF.lower());
    //  (III)
    //   |      |
    //  ---     |
    //         ---
    {
        if (RF.upper() < LF.lower())
        //  ---
        //   |
        //  ---
        //
        //         ---
        //          |
        //         ---
            return 11;

        if (RF.upper() == LF.lower())
        //  ---
        //   |
        //  ---    ---
        //          |
        //         ---
            return 12;

        Q_ASSERT(RF.upper() > LF.lower());
        //   |     ---
        //  ---     |
        //          |
        //         ---
        {
            if (LF.upper() < RF.upper())
            //         ---
            //          |
            //  ---     |
            //   |      |
            //  ---     |
            //          |
            //         ---
                return 13;
            if (LF.upper() == RF.upper())
            //  ---    ---
            //   |      |
            //  ---     |
            //          |
            //         ---
                return 14;

            Q_ASSERT(LF.upper() > RF.upper());
            //  ---
            //   |
            //   |     ---
            //   |      |
            //  ---     |
            //          |
            //         ---
            return 15;
        }
    }

    Q_ASSERT(false);    //  never come here
}


void Structure::createSingleCellSegments(SingleCellAuxData& aux, Orientation ori, double y0)
{
    Interval LF = aux.F[ori][FIRST];
    Interval RF = aux.F[ori][SECOND];
    bool BF = aux.F[opposite(ori)][FIRST];
    bool TF = aux.F[opposite(ori)][SECOND];

    Pointer *left = aux.seg[ori][FIRST];
    Pointer *right = aux.seg[ori][SECOND];

    Interval all (y0, y0+1);
    Interval set ( // LF u RF
        numeric::min(LF.lower(),RF.lower()),
        numeric::max(LF.upper(),RF.upper()) );

    //  handle all cases of overlapping intervals
    switch(aux.arr[ori])
    {
    case 0:     // (0) completely empty
        set.clear();
        break;
    case 1:     // (1) left reachable, right not
        if (TF)
            create2Segments(left[2],right[2], LF, ori, REACHABLE, NON_ACCESSIBLE);
        else
            set.clear();
        break;
    case 2:     // (2) right reachable
        if (BF)
            create2Segments(left[2],right[2], RF, ori, NON_ACCESSIBLE, REACHABLE);
        else
            set.clear();
        break;
    case 3:
        create2Segments(left[2],right[2], {LF.upper(),RF.lower()}, ori, NON_ACCESSIBLE, NON_ACCESSIBLE);
        //  fall-through intended
    case 4:
        //          ---
        //           |      [3] = RP
        //          ---
        //                  ([2] = n)
        //   ---
        //    |             [1] = LP
        //   ---
        create2Segments(left[3],right[3], RF, ori, NON_ACCESSIBLE, REACHABLE);
        create2Segments(left[1],right[1], LF, ori, REACHABLE, NON_ACCESSIBLE);
        break;
    case 5:
        //   ---
        //    |             [3] = LP
        //    |     ---
        //    |      |      [2] = RP
        //    |     ---
        //    |             [1] = LP
        //   ---
        if (TF)
            create2Segments(left[3],right[3], {RF.upper(),LF.upper()}, ori, REACHABLE, NON_ACCESSIBLE);
        else
            set.upper() = RF.upper();
        create2Segments(left[2],right[2], RF, ori, SEE_THROUGH,SEE_THROUGH);
        create2Segments(left[1],right[1], {LF.lower(),RF.lower()}, ori, REACHABLE, NON_ACCESSIBLE);
        break;
    case 6:
        //  ---    ---
        //   |      |       [2] = LP,RP
        //   |     ---
        //   |              [1] = LP
        //  ---
        create2Segments(left[2],right[2], RF, ori, SEE_THROUGH,SEE_THROUGH);
        create2Segments(left[1],right[1], {LF.lower(),RF.lower()}, ori, REACHABLE, NON_ACCESSIBLE);
        break;
    case 7:
        //          ---
        //           |      [3] = RP
        //   ---     |
        //    |      |      [2] = LP,RP
        //    |     ---
        //    |             [1] = LP
        //   ---
        create2Segments(left[3],right[3], {LF.upper(),RF.upper()}, ori, NON_ACCESSIBLE, REACHABLE);
        create2Segments(left[2],right[2], {RF.lower(),LF.upper()}, ori, SEE_THROUGH, SEE_THROUGH);
        create2Segments(left[1],right[1], {LF.lower(),RF.lower()}, ori, REACHABLE, NON_ACCESSIBLE);
        break;
    case 8:
        //         ---
        //          |       [3] = RP
        //  ---     |
        //   |      |       [2] = RP,LP
        //  ---    ---
        create2Segments(left[3],right[3], {LF.upper(),RF.upper()}, ori, NON_ACCESSIBLE, REACHABLE);
        create2Segments(left[2],right[2], LF, ori, SEE_THROUGH,SEE_THROUGH);
        break;
    case 9:
        Q_ASSERT(LF==RF);
        //  ---    ---
        //   |      |       [2]
        //  ---    ---
        create2Segments(left[2],right[2], LF, ori, SEE_THROUGH,SEE_THROUGH);
        break;
    case 10:
        //  ---
        //   |              [3]
        //   |     ---
        //   |      |       [2]
        //  ---    ---
        if (TF)
            create2Segments(left[3],right[3], {RF.upper(),LF.upper()}, ori, REACHABLE, NON_ACCESSIBLE);
        else
            set.upper() = RF.upper();
        create2Segments(left[2],right[2], RF, ori, SEE_THROUGH,SEE_THROUGH);
        break;
    case 11:
        if (TF && BF)
            create2Segments(left[2],right[2], {RF.upper(),LF.lower()}, ori, NON_ACCESSIBLE,NON_ACCESSIBLE);
        //  fall-through intended
    case 12:
        //   ---
        //    |             [3] = LP
        //   ---
        //                  ([2] = n)
        //          ---
        //           |      [1] = RP
        //          ---
        //  the right edge can only be seen from the bottom edge
        if (!TF && !BF)
            set.clear();
        else {
            if (TF)
                create2Segments(left[3],right[3], LF, ori, REACHABLE, NON_ACCESSIBLE);
            else
                set.upper() = RF.upper();
            if (BF)
                create2Segments(left[1],right[1], RF, ori, NON_ACCESSIBLE, REACHABLE);
            else
                set.lower() = LF.lower();
        }
        break;
    case 13:
        //        ---
        //         |            [3] = RP
        //  ---    |
        //   |     |            [2] = LP
        //  ---    |
        //         |            [1] = RP
        //        ---
        create2Segments(left[3],right[3], {LF.upper(),RF.upper()}, ori, NON_ACCESSIBLE, REACHABLE);
        create2Segments(left[2],right[2], LF, ori, SEE_THROUGH,SEE_THROUGH);
        if (BF)
            create2Segments(left[1],right[1], {RF.lower(),LF.lower()}, ori, NON_ACCESSIBLE, REACHABLE);
        else
            set.lower() = LF.lower();
        break;
    case 14:
        //  ---    ---
        //   |      |           [2]
        //  ---     |
        //          |           [1]
        //         ---
        create2Segments(left[2],right[2], LF, ori, SEE_THROUGH,SEE_THROUGH);
        if (BF)
            create2Segments(left[1],right[1], {RF.lower(),LF.lower()}, ori, NON_ACCESSIBLE, REACHABLE);
        else
            set.lower() = LF.lower();
        break;
    case 15:
        //  ---
        //   |              [3] = LP
        //   |    ---
        //   |     |        [2] = LP,RP
        //  ---    |
        //         |        [1] = LP
        //        ---
        if (TF)
            create2Segments(left[3],right[3], {RF.upper(),LF.upper()}, ori, REACHABLE, NON_ACCESSIBLE);
        else
            set.upper() = RF.upper();
        if (BF)
            create2Segments(left[1],right[1], {RF.lower(),LF.lower()}, ori, NON_ACCESSIBLE, REACHABLE);
        else
            set.lower() = LF.lower();
        create2Segments(left[2],right[2], {LF.lower(),RF.upper()}, ori, SEE_THROUGH,SEE_THROUGH);
        break;
    default:
        Q_ASSERT(false);    //  don't come here
    }

    //  *seg[0] and *seg[4] are the empty segments below and above
    if (set)
    {
        if (set.lower() > all.lower())
            create2Segments(left[0],right[0], {all.lower(),set.lower()}, ori, NON_ACCESSIBLE,NON_ACCESSIBLE);
        if (set.upper() < all.upper())
            create2Segments(left[4],right[4], {set.upper(),all.upper()}, ori, NON_ACCESSIBLE,NON_ACCESSIBLE);
    }
    else
    {   //  completely empty
        create2Segments(left[0],right[0], all, ori, NON_ACCESSIBLE,NON_ACCESSIBLE);
    }
}

void Structure::linkSingleCellSegments(
        SingleCellAuxData& aux, Orientation ori)
{
    PointerInterval LP = aux.P[ori][FIRST];
    PointerInterval RP = aux.P[ori][SECOND];
    PointerInterval BP = aux.P[opposite(ori)][FIRST].normalized();
    PointerInterval TP = aux.P[opposite(ori)][SECOND].normalized();

    Pointer *left = aux.seg[ori][FIRST];
    Pointer *right = aux.seg[ori][SECOND];

    //  LP,RP may be reversed. We make NO assumption about the correct orientation of PointerIntervals.
    //  adjust() and combine() must take care of it!

    //  handle all cases of overlapping intervals
    switch(aux.arr[ori])
    {
    case 0:     // (0) completely empty
        // nothing to see, nothing to be seen
        break;
    case 1:     // (1) left reachable, right not
        //  we can see the top edge
        if (left[2]) *left[2] = TP;
        break;
    case 2:     // (2) right reachable
        //  can be seen from bottom edge
        if (right[2]) *right[2] = BP;
        break;
    case 3:
    case 4:
        //          ---
        //           |      [3]
        //          ---
        //
        //   ---
        //    |             [1]
        //   ---
        *left[1] = RP+TP;
        *right[3] = BP+LP;
        break;
    case 5:
        //   ---
        //    |             [3]
        //    |     ---
        //    ...
        LP = {left[1],left[2]};
        if (left[3]) {
            *left[3] = TP;
            //  the right edge can not be seen from uppermost left interval (*seg[3])
            Q_ASSERT(! LP.contains(left[3]));    //  *seg[3] can not see *seg[2]
        }
        //  fall-through intended
    case 6:
        //  ---    ---
        //   |      |       [2]
        //   |     ---
        //   |              [1]
        //  ---
        *left[1] = RP+TP;
        //  see-through: only one pointer is needed
        *left[2] = RP+TP;
        *right[2] = BP+LP;
        break;
    case 7:
        //          ---
        //           |      [3]
        //   ---     |
        //    |      |      [2]
        //    |     ---
        //    |             [1]
        //   ---
        //  right and top are visible from bottom and left
        *left[1] = RP+TP;
        *left[2] = RP+TP;
        *right[2] = BP+LP;
        *right[3] = BP+LP;
        break;
    case 8:
        //         ---
        //          |       [3]
        //  ---     |
        //   |      |       [2]
        //  ---    ---
        *left[2] = RP+TP;
        *right[2] = BP+LP;
        *right[3] = BP+LP;
        break;
    case 9:
        //  ---    ---
        //   |      |       [2]
        //  ---    ---
        *left[2] = RP+TP;
        *right[2] = BP+LP;
        break;
    case 10:
        //  ---
        //   |              [3]
        //   |     ---
        //   |      |       [2]
        //  ---    ---
        //  right[2] can not be seen from left[3]
        LP = left[2];
        if (left[3]) {
            *left[3] = TP;
            Q_ASSERT(! LP.contains(left[3]));
        }
        *left[2] = RP+TP;
        *right[2] = BP+LP;
        break;
    case 11:
    case 12:
        //   ---
        //    |             [3]
        //   ---
        //
        //          ---
        //           |      [1]
        //          ---
        //  the right edge can only be seen from the bottom edge
        if (right[1])
            *right[1] = BP;
        //  we can only see the top edge
        if (left[3])
            *left[3] = TP;
        break;
    case 13:
        //        ---
        //         |        [3]
        //  ---    |
        //   |     |        [2]
        //  ---    |
        //         |        [1]
        //        ---
        RP = {right[2],right[3]};
        if (right[1]) {
            *right[1] = BP;
            Q_ASSERT(!RP.contains(right[1])); //  right[1] can not be seen
        }
        *left[2] = RP+TP;
        *right[2] = BP+LP;
        *right[3] = BP+LP;
        break;
    case 15:
        //  ---
        //   |              ([3])
        //   |    ---
        //   |     |
        //   ...
        LP = left[2];
        if (left[3]) {
            *left[3] = TP;
            Q_ASSERT(! LP.contains(left[3])); //  *seg[3] can not be seen
        }
        //  fall-through intended
    case 14:
        //  ---   ---
        //   |     |        [2]
        //  ---    |
        //         |        [1]
        //        ---
        //  this interval can not be seen from the left edge, only from the bottom edge
        RP = right[2];    //  right[1] can not be seen
        if (right[1]) {
            *right[1] = BP;
            Q_ASSERT(! RP.contains(right[1]));
        }
        *left[2] = RP+TP;
        //  the right edge can not be seen from the upper left interval
        *right[2] = BP+LP;
        break;
    default:
        Q_ASSERT(false);    //  don't come here
    }

    //  SEE-THROUGH: one pointer is actually redundant
    clipSeeThroughPointer(left[2]);
    clipSeeThroughPointer(right[2]);
}

void Structure::clipSeeThroughPointer(Pointer p)
{
    if (p && p->type==SEE_THROUGH)
    {
        if ((p->ori==VERTICAL && p->dir==SECOND)
                || (p->ori==HORIZONTAL && p->dir==FIRST))
            p->h = nullptr;
        else
            p->l = nullptr;
    }
}

PointerInterval Structure::reachableInterval(Pointer seg[5])
{
    PointerInterval result;
    for(int i=0; i < 5; ++i)
        if (seg[i] && seg[i]->type!=NON_ACCESSIBLE) {
            result.h = seg[i];
            if (!result.l)
                result.l = seg[i];
        }
    return result;
}

void Structure::create2Segments(Pointer& first, Pointer& second, Interval ival, Orientation ori, Type t1, Type t2)
{
    first = new BoundarySegment(ival,ori,FIRST,t1,this);
    second = new BoundarySegment(ival,ori,SECOND,t2,this);
}


void Structure::copySegments(BoundaryList& list, Pointer seg[5])
{
    for(int i=0; i < 5; ++i)
        if (seg[i])
        {
            Pointer p = seg[i];
            Q_ASSERT(p);
            Q_ASSERT(p->type!=NON_ACCESSIBLE || (p->l==NULL && p->h==NULL));

            list.insert_last(p);
        }
}



