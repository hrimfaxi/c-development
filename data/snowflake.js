/*
	The wellknown Snowflake curve.

	It is not very interesting with regard to the Frechet distance,
	but serves as stress test for the graphics system.
	Besides, it produces decorative free-space diagrams...
*/

function segment(turtle,len,order)
{
	if (order==0) {
		turtle.forward(len);
	}
	else {
		segment(turtle,len/3,order-1);
		turtle.left(60);
		segment(turtle,len/3,order-1);
		turtle.right(120);
		segment(turtle,len/3,order-1);
		turtle.left(60);
		segment(turtle,len/3,order-1);
	}
}

function snowflake(turtle,len,order)
{
	segment(turtle,len,order);
	turtle.right(120);
	segment(turtle,len,order);
	turtle.right(120);
	segment(turtle,len,order);
}

var order=3;
//	be careful: order=4 will take Qt graphics to its limit..

P.M(0,0);
Q.M(0,0);

snowflake(P,1000,order);
snowflake(Q,1000,order-1);




