/*
	Example for a scripted Curve file.
	We create two variables P and Q.

	Basic functions for path construction are:
		append("m 1 2, 3 4")
		constructs a path from an SVG path description

		M,m, H,h, V,v 
		are equivalents to the respective SVG commands

		scale,rotate,translate,map 
		apply transformations
*/


var pp = new Path("m 10 20 30 40");
pp.append("m 50 60 70 90");

var pq = new Path(pp);

//Q=pq;	
//P=pp;
//Q.appendPath(pp);	//	does nothing
P.append(pp.scale(0.1));
P.append("M 10 20, m 30 40, 50 60 M 200 -100");
Q.append("M 20 10, m 40 30, 60 50 M 100 200");

P.m(10,10).scale(0.5);
P.m(20,20).v(14).h(20).append("15 15, 10 10").h(2.0);

P.v(100).m(10,20);
Q.h(20).rotate(30);


