/*
	The wellknown Snowflake curve.

	It is not very interesting with regard to the Frechet distance,
	but serves as stress test for the graphics system.
	Besides, it produces decorative free-space diagrams...
*/


function helix1(turtle, len, step, n)
{
	while(n-- > 0)
	{
		turtle.forward(len).left(90).forward(len).left(90);
		len += step;
	}
}

function helix2(turtle, len, step, n)
{
	while(n-- > 0)
	{
		turtle.forward(len).right(90).forward(len).right(90);
		len += step;
	}
}

P.M(0,0);
Q.M(0,0).left(45);

helix1(P,1,1,10);
P.forward(5);

helix1(Q,1,1,10);
Q.forward(5);

P.scale(10);
Q.scale(10);




