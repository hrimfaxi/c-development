/*
	The wellknown Snowflake curve.

	It is not very interesting with regard to the Frechet distance,
	but serves as stress test for the graphics system.
	Besides, it produces decorative free-space diagrams...
*/


function zigzag(turtle,len,angle,n)
{
	while(n-- > 0) {
		turtle.forward(len).right(180-angle).forward(len).left(180-angle);
	}
}

var n=4;
var len=n*10;
var angle=3.5*n;

P.M(0,0).left((180-angle)/2);
Q.M(0,-len).right(angle/2);

zigzag(P,len,angle,n);

zigzag(Q,len,angle,n);




