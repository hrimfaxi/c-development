/*
	The wellknown Snowflake curve.

	It is not very interesting with regard to the Frechet distance,
	but serves as stress test for the graphics system.
	Besides, it produces decorative free-space diagrams...
*/


function loop8(C,d,sx,sy)
{
	C.M(d+sx,d+sy).M(3*d-sx,-d-sy).M(4*d,0);
	C.M(3*d-sx,d-sy).M(d+sx,-d+sy).M(0,0);
}

P.M(0,0);
Q.M(0,0);

loop8(P,10,0.5,0.5);

loop8(Q,10,0.2,0.2);




