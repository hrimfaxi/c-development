## Fréchet View 1.6.0

### 1. Installation Notes

All binaries are 64-bit. If you desperately need a 32-bit version, let me know.

#### Windows Installation
Unzip downloaded package and run `frechet-view.exe`

#### Linux Installation
Make the downloaded file **executable**:

`chmod +x Frechet_View_for_Linux`

and run it by double clicking, or from a terminal:

`./Frechet_View_for_Linux`

Use "Help/Install Desktop Icon" to place a link onto your desktop 
(you may need to edit that desktop file, but it's easy).

#### macOS Instalation
Open the downloaded disk image and double click the Fréchet View application icon.


### 2. User Guide
See brief [User Guide](https://bitbucket.org/hrimfaxi/c-development/wiki/Home).

Command line options are desribed [here](https://bitbucket.org/hrimfaxi/c-development/wiki/CommandLineInterface.md).

Or type `frechet-view --help`


### 3. Input Data
Fréchet View expects two polygonal curves as input.
`*.svg` and `*.ipe` files are accepted.

Vector graphics files can be edited with a number of applications, like
[InkScape](https://inkscape.org) or [Ipe](http://ipe.otfried.org/).

The Curve algorithm accepts any pair of straight polygon paths (not Beziér Curves).
The Polygon algorithm expects closed, simple polygons (no intersections). 

Script files in a JavaScript-like language are accepted. 
See [here](https://bitbucket.org/hrimfaxi/c-development/wiki/ScriptFiles.md) for more.

### 4. Multi-Core Support

Use command line option frechet-view ... `--cores=1,2,4,...`

Best results are achieved _without_ Hyperthreading. E.g. if you have a Quad-Core processor, use `--cores=4`; `--cores=8` has little effect.


### 3. GPGPU Support

GPGPU support is optional. You need a graphics card driver that supports **OpenCL 1.2** or later.
Best results are achieved if the graphics card has high memory bandwidth.
Graphics cards that share memory with the CPU (like most Notebook cards) are usually less effective.


### 4. Building from Source

#### Clone repository with Fréchet View sources

`git clone https://bitbucket.org/hrimfaxi/c-development.git`

#### Download Third-Party Libraries

- **Qt 5.9** or later is required. Qt comes pre-installed with many Linux system, or can be downloaded [here](https://www.qt.io/download-qt-installer).

Check installed version with `qmake --version`.

- **Boost 1.53** or later (maybe earlier) is required.
  Boost comes pre-installed with most Linux systems, or can be downloaded from [boost.org](https://www.boost.org).

Binary libraries are not needed, Boost header files are sufficient.

- **CGAL** Computational Geometry Algorithms Library
  `git clone https://github.com/CGAL/cgal.git`

Note that we do not need CGAL binaries, header files are sufficient.

- **TBB** Intel Threading Building Blocks

Download a package for your system from [here](https://github.com/01org/tbb/releases).

Unpack it. If you need to build TBB libraries from source, refer to their documentation. It is not very difficult, actually. The hardest part of TBB is finding the right documentation.

- **M4RI** Boolean Matrix Multiplication
`git clone https://bitbucket.org/malb/m4ri.git`
`cd m4ri`
`autoreconf --install`
`./configure --enable-thread-safe --disable-png`
`make`

Next, open `m4ri/m4ri_config.h` and make sure that
the following options are set correctly: 

```
#define __M4RI_HAVE_MM_MALLOC		0
#define __M4RI_HAVE_POSIX_MEMALIGN	0
#define __M4RI_HAVE_SSE2		1

...

#define __M4RI_ENABLE_MZD_CACHE         0
#define __M4RI_ENABLE_MMC               0
```

This is very important, because some of these options are _not safe_ for multi-threading!


- **OpenCL**

OpenCL SDKs are available from Intel, AMD, and (afaik) as part of Nvidia CUDA toolkit.
OpenCL is readily installed on macOS and many Linuxes.


#### Build Project

Adjust include and library paths as necessary in `frechet-view.pro`.
Then run the build script:

```
cd projects/frechet-view
qmake -config release
make
```

(resp. `nmake` on Windows)


Alternatively, a **cmake** project script is available. 
MSVC project files are available, but I won't promise to keep them up-to-date.

#### 32-bit version?

I've never tried to build a 32-bit version.
It could be a bit of a challenge, but I *assume* it's possible.

- our source code is most likely 32-bit compatible
- M4RI is supposed to be 32-bit compatible
- so are Boost and CGAL
- you can get hold of 32-bit versions of Qt and TBB

### Windows Build Notes
Both **Microsoft Visual C++** and **Intel C++** compilers are supported. 
We recommend the latter because it allows for better numerical results.
Using "qmake -spec win32-icc" should do the trick.

Building M4RI from sources may not work on Windows. However, it is sufficient to edit the file `m4ri\m4ri_config.h`, as described above. 

### Linux Build Notes
**gcc 5.4** or later is recommended. 

### macOS Build Notes
**XCode Command Line Tools** are required. Install with
``xcode-select --install``. A full-fledged XCode installation is not needed.