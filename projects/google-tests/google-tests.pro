
win32:GOOGLETEST_DIR=C:\git-repos\googletest
linux:GOOGLETEST_DIR=/home/nightrider/Dokumente/googletest

include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG += qt

QT += core gui xmlpatterns qml svg printsupport


win32: {
QMAKE_CFLAGS += /TP
# use C++ compiler, even for .c files (M4RI only)
BOOST_INCL=C:\boost_1_66_0
CGAL=C:\cgal-releases-CGAL-4.12
CGAL_INCL = $$CGAL\include $$CGAL\Polygon\include $$CGAL\Circulator\include \
 $$CGAL\Installation\include $$CGAL\STL_Extension\include \
 $$CGAL\Number_types\include $$CGAL\Kernel_23\include $$CGAL\Profiling_tools\include \
 $$CGAL\Stream_support\include $$CGAL\Algebraic_foundations\include \
 $$CGAL\Interval_support\include $$CGAL\Modular_arithmetic\include \
 $$CGAL\Cartesian_kernel\include $$CGAL\Distance_2\include $$CGAL\Distance_3\include \
 $$CGAL\Intersections_2\include $$CGAL\Intersections_3\include \
 $$CGAL\Filtered_kernel\include $$CGAL\Homogeneous_kernel\include \
 $$CGAL\Kernel_d\include $$CGAL\Arithmetic_kernel\include \
 $$CGAL\Partition_2\include $$CGAL\Convex_hull_2\include $$CGAL\Triangulation_2\include \
 $$CGAL\TDS_2\include $$CGAL\Hash_map\include $$CGAL\Spatial_sorting\include \
 $$CGAL\Property_map\include
M4RI_INCL=C:\git-repos\m4ri
}

linux:{
BOOST_INCL=/home/nightrider/boost_1.58/include
M4RI_INCL=/home/nightrider/Dokumente/m4ri
QMAKE_CXXFLAGS += -Wno-sign-compare -Wno-reorder
}

macx:BOOST_INCL=/Users/hrimfaxi/boost_1_66_0


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

top_srcdir=../..

SRC=$$top_srcdir/src
INCL=$$top_srcdir/include
TEST=$$top_srcdir/test


INCLUDEPATH += $$INCL $$INCL/data $$INCL/frechet
INCLUDEPATH += $$INCL/frechet/freespace $$INCL/frechet/reachability $$INCL/frechet/input $$INCL/frechet/poly
#INCLUDEPATH += $$INCL/frechet/app
INCLUDEPATH += $$BOOST_INCL $$CGAL_INCL $$M4RI_INCL
# $$CGAL_INCL

SOURCES += \
        $$SRC/data/interval.cpp \
        $$SRC/data/array2d.cpp \
        $$SRC/data/bitset.cpp \
        $$SRC/data/linkedlist.cpp \
        $$SRC/frechet/k_frechet/kalgorithm.cpp \
        $$SRC/frechet/freespace/freespace.cpp \
 #       $$SRC/frechet/app/main.cpp \
 #       $$SRC/frechet/app/frechetviewapplication.cpp \
 #       $$SRC/frechet/app/filehistory.cpp \
        $$SRC/frechet/input/inputreader.cpp \
        $$SRC/frechet/input/datapath.cpp \
 #       $$SRC/frechet/view/mainwindow.cpp \
 #       $$SRC/frechet/view/curveview.cpp \
 #       $$SRC/frechet/view/baseview.cpp \
 #       $$SRC/frechet/view/freespaceview.cpp \
 #       $$SRC/frechet/view/intervalview.cpp \
 #       $$SRC/frechet/view/controlpanel.cpp \
        $$SRC/frechet/freespace/grid.cpp \
        $$SRC/frechet/freespace/components.cpp \
        $$SRC/frechet/reachability/fs_path.cpp \
 #       $$SRC/frechet/view/palette.cpp \
        $$SRC/frechet/input/path.cpp \
        $$SRC/frechet/input/scriptinput.cpp \
        $$SRC/frechet/app/workerthread.cpp \
        $$SRC/frechet/reachability/boundary.cpp \
        $$SRC/frechet/reachability/structure.cpp \
        $$SRC/frechet/reachability/str_singlecell.cpp \
        $$SRC/frechet/reachability/graph_m4ri.cpp \
        $$SRC/frechet/reachability/graph_model.cpp \
        $$SRC/frechet/poly/poly_utils.cpp \
        $$SRC/frechet/poly/algorithm.cpp \
        $$SRC/frechet/poly/shortest_paths.cpp \
        $$SRC/frechet/poly/triangulation.cpp \
        $$SRC/frechet/poly/poly_path.cpp \
        $$SRC/m4ri/mzd_bool.c \
        $$SRC/m4ri/russian_bool.c


HEADERS += \
        $$INCL/data/types.h \
        $$INCL/data/dset.h \
        $$INCL/data/interval.h \
        $$INCL/data/array2d.h \
        $$INCL/data/array2d_impl.h \
        $$INCL/data/bitset.h \
        $$INCL/data/linkedlist.h \
        $$INCL/frechet/k_frechet/kalgorithm.h \
        $$INCL/frechet/freespace/freespace.h \
#        $$INCL/frechet/app/frechetviewapplication.h \
#        $$INCL/frechet/app/filehistory.h \
        $$INCL/frechet/input/inputreader.h \
        $$INCL/frechet/input/datapath.h \
#        $$INCL/frechet/view/mainwindow.h \
#        $$INCL/frechet/view/curveview.h \
#        $$INCL/frechet/view/baseview.h \
#        $$INCL/frechet/view/freespaceview.h \
#        $$INCL/frechet/view/controlpanel.h \
#        $$INCL/frechet/view/palette.h \
#        $$INCL/frechet/view/intervalview.h \
        $$INCL/frechet/freespace/grid.h \
        $$INCL/frechet/freespace/components.h \
        $$INCL/frechet/reachability/fs_path.h \
        $$INCL/frechet/input/path.h \
        $$INCL/frechet/app/workerthread.h \
        $$INCL/frechet/reachability/boundary.h \
        $$INCL/frechet/reachability/structure.h \
        $$INCL/frechet/reachability/graph_model.h \
        $$INCL/frechet/reachability/graph_m4ri.h \
        $$INCL/frechet/poly/poly_utils.h \
        $$INCL/frechet/poly/types.h \
        $$INCL/frechet/poly/algorithm.h \
        $$INCL/frechet/poly/double_queue.h \
        $$INCL/frechet/poly/double_queue_impl.h \
        $$INCL/frechet/poly/shortest_paths.h \
        $$INCL/frechet/poly/triangulation.h \
        $$INCL/frechet/poly/poly_path.h \
        $$INCL/m4ri/mzd_bool.h \
        $$INCL/m4ri/russian_bool.h \
        $$INCL/m4ri/or.h


# Test Cases
INCLUDEPATH += $$TEST

HEADERS += \
        $$TEST/reachability_test_suite.h \
        $$TEST/triangulation_test_suite.h \
        $$TEST/graph_test_suite.h

SOURCES += \
        $$TEST/main.cpp \
        $$TEST/reachability_test_suite.cpp \
        $$TEST/reachability_single_cell_test.cpp \
        $$TEST/reachability_merge_test.cpp \
        $$TEST/triangulation_test_suite.cpp \
        $$TEST/graph_test_suite.cpp


DEFINES += SRCDIR=\\\"$$TEST/\\\"

win32 {
    # LIBS += $$PWD/../../lib/m4ri.lib
    #   Static linking doesn't work for one reason or another.
    SOURCES += $$M4RI_INCL/m4ri/mzd.c \
            $$M4RI_INCL/m4ri/brilliantrussian.c \
           $$M4RI_INCL/m4ri/echelonform.c \
            $$M4RI_INCL/m4ri/misc.c \
            $$M4RI_INCL/m4ri/mp.c \
           $$M4RI_INCL/m4ri/mzp.c \
           $$M4RI_INCL/m4ri/ple.c \
            $$M4RI_INCL/m4ri/mmc.c \
            $$M4RI_INCL/m4ri/graycode.c \
            $$M4RI_INCL/m4ri/ple_russian.c \
           $$M4RI_INCL/m4ri/triangular_russian.c \
           $$M4RI_INCL/m4ri/triangular.c \
           $$M4RI_INCL/m4ri/strassen.c \
           $$M4RI_INCL/m4ri/solve.c
}

linux {
    LIBS += -L$$PWD/../../lib/
    LIBS += -lOpenCL
    LIBS += -lCGAL -lgmp -lm4ri
    # don't:  -L/usr/lib/x86_64-linux-gnu  this would pull the wrong Qt libraries
    # -lCGAL_Core
#    DEFINES *= HAS_OPENCL
#    HEADERS += $$INCL/frechet/app/clpp.h
#    SOURCES += $$SRC/frechet/app/clinfo.cpp \
}
