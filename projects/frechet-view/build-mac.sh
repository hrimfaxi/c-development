# clean build
QT=~/Qt/5.10.1/clang_64/
QTBIN=$QT/bin

# 
# get latest sources
#
cd ~/c-development
git pull

#
# build
#
cd projects/frechet-view
make clean
$QTBIN/qmake -config release
make

#
# Deploy
#
NAME="Fréchet View"
$QTBIN/macdeployqt "$NAME".app
cp ../../rsrc/paseando-al-perro.icns "$NAME".app/Contents/Resources
cp ../../rsrc/Info.plist "$NAME".app/Contents 
#mv frechet-view.app "$NAME".app

#
# Create .dmg
#
#hdiutil create -fs HFS+ -volname "$NAME" -srcfolder "$NAME".app -srcfolder ../../"Demo Data" "$NAME".dmg
# does not work :-(

hdiutil create -fs HFS+ -volname "$NAME" -size 35m "$NAME".dmg
hdiutil attach "$NAME".dmg

cp -r "$NAME".app /Volumes/"$NAME"/"$NAME".app
cp -r ../../"Demo Data" /Volumes/"$NAME"/"Demo Data"

hdiutil detach /Volumes/"$NAME"
