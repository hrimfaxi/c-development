#-------------------------------------------------
#
# Project created by QtCreator 2018-03-22T17:32:21
#
#-------------------------------------------------

QT       += core gui xmlpatterns opengl printsupport qml svg
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

macx:TARGET = "Fréchet View"
linux:TARGET = frechet-view
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

top_srcdir=../..

SRC=$$top_srcdir/src
INCL=$$top_srcdir/include
RSCR=$$top_srcdir/rsrc

win32: {
CONFIG += console
CONFIG -= windows
#   link with /SUBSYSTEM:CONSOLE
QMAKE_CFLAGS += /TP
# use C++ compiler, even for .c files (M4RI only)
BOOST_INCL=C:\boost_1_66_0
CGAL=C:\cgal-releases-CGAL-4.12
M4RI_DIR=C:\git-repos\m4ri
TBB_DIR="C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2019\windows\tbb"
OPENCL_INCL = C:\Intel\OpenCL\sdk\include
DEFINES += NOMINMAX
# for tbb
# long double (Intel compiler only)
QMAKE_CXXFLAGS += -Qlong-double
}

linux: {
BOOST_INCL=/home/nightrider/boost_1.58/include
CGAL=/home/nightrider/Dokumente/cgal
# use latest version of CGAL. Should be HEADER ONLY.
# DON'T confuse with /usr/include/CGAL and don't link against shared library
M4RI_DIR=/home/nightrider/Dokumente/m4ri
TBB_DIR=/opt/intel2/tbb
}

macx: {
BOOST_INCL=/Users/hrimfaxi/boost_1_66_0
CGAL=/Users/hrimfaxi/CGAL-4.13
M4RI_DIR=/Users/hrimfaxi/m4ri
#TBB_DIR=/opt/intel/tbb
TBB_DIR=/Users/hrimfaxi/c-development/tbb
}

#
# Build CGAL from headers
#
CGAL_INCL = $$CGAL/include $$CGAL/Polygon/include $$CGAL/Circulator/include \
 $$CGAL/Installation/include $$CGAL/STL_Extension/include \
 $$CGAL/Number_types/include $$CGAL/Kernel_23/include $$CGAL/Profiling_tools/include \
 $$CGAL/Stream_support/include $$CGAL/Algebraic_foundations/include \
 $$CGAL/Interval_support/include $$CGAL/Modular_arithmetic/include \
 $$CGAL/Cartesian_kernel/include $$CGAL/Distance_2/include $$CGAL/Distance_3/include \
 $$CGAL/Intersections_2/include $$CGAL/Intersections_3/include \
 $$CGAL/Filtered_kernel/include $$CGAL/Homogeneous_kernel/include \
 $$CGAL/Kernel_d/include $$CGAL/Arithmetic_kernel/include \
 $$CGAL/Partition_2/include $$CGAL/Convex_hull_2/include $$CGAL/Triangulation_2/include \
 $$CGAL/TDS_2/include $$CGAL/Hash_map/include $$CGAL/Spatial_sorting/include \
 $$CGAL/Property_map/include

INCLUDEPATH += $$INCL $$INCL/data $$INCL/frechet
INCLUDEPATH += $$INCL/frechet/freespace $$INCL/frechet/reachability $$INCL/frechet/poly
INCLUDEPATH += $$INCL/frechet/input $$INCL/frechet/view $$INCL/frechet/app
INCLUDEPATH += $$INCL/m4ri $$INCL/clm4rm
INCLUDEPATH += $$BOOST_INCL $$CGAL_INCL $$M4RI_DIR $$TBB_DIR/include $$OPENCL_INCL

#
# Build M4RI from sources
#
SOURCES += \
        $$M4RI_DIR/m4ri/misc.c \
        $$M4RI_DIR/m4ri/mzd.c \
        $$M4RI_DIR/m4ri/mmc.c \
        $$M4RI_DIR/m4ri/brilliantrussian.c \
        $$M4RI_DIR/m4ri/debug_dump.c \
#        $$M4RI_DIR/m4ri/djb.c \
        $$M4RI_DIR/m4ri/echelonform.c \
        $$M4RI_DIR/m4ri/graycode.c \
        $$M4RI_DIR/m4ri/mzp.c \
        $$M4RI_DIR/m4ri/ple.c \
        $$M4RI_DIR/m4ri/ple_russian.c \
        $$M4RI_DIR/m4ri/solve.c \
        $$M4RI_DIR/m4ri/strassen.c \
        $$M4RI_DIR/m4ri/triangular.c \
        $$M4RI_DIR/m4ri/triangular_russian.c

SOURCES += \
        $$SRC/data/interval.cpp \
        $$SRC/data/array2d.cpp \
        $$SRC/data/bitset.cpp \
        $$SRC/data/linkedlist.cpp \
        $$SRC/data/matrix_pool.cpp \
        $$SRC/frechet/k_frechet/kalgorithm.cpp \
        $$SRC/frechet/freespace/freespace.cpp \
        $$SRC/frechet/app/main.cpp \
        $$SRC/frechet/app/frechetviewapplication.cpp \
        $$SRC/frechet/app/filehistory.cpp \
        $$SRC/frechet/app/cli.cpp \
        $$SRC/frechet/app/concurrency.cpp \
        $$SRC/frechet/input/inputreader.cpp \
        $$SRC/frechet/input/datapath.cpp \
        $$SRC/frechet/view/mainwindow.cpp \
        $$SRC/frechet/view/animation.cpp \
        $$SRC/frechet/view/curveview.cpp \
        $$SRC/frechet/view/baseview.cpp \
        $$SRC/frechet/view/freespaceview.cpp \
        $$SRC/frechet/view/intervalview.cpp \
        $$SRC/frechet/view/controlpanel.cpp \
        $$SRC/frechet/freespace/grid.cpp \
        $$SRC/frechet/freespace/components.cpp \
        $$SRC/frechet/reachability/fs_path.cpp \
        $$SRC/frechet/view/palette.cpp \
        $$SRC/frechet/input/path.cpp \
        $$SRC/frechet/input/scriptinput.cpp \
        $$SRC/frechet/app/workerthread.cpp \
        $$SRC/frechet/reachability/boundary.cpp \
        $$SRC/frechet/reachability/structure.cpp \
        $$SRC/frechet/reachability/str_singlecell.cpp \
        $$SRC/frechet/reachability/graph_m4ri.cpp \
        $$SRC/frechet/reachability/graph_cl.cpp \
        $$SRC/frechet/reachability/graph_model.cpp \
        $$SRC/frechet/poly/poly_utils.cpp \
        $$SRC/frechet/poly/algorithm.cpp \
        $$SRC/frechet/poly/shortest_paths.cpp \
        $$SRC/frechet/poly/triangulation.cpp \
        $$SRC/frechet/poly/poly_path.cpp \
        $$SRC/frechet/poly/optimise.cpp \
        $$SRC/frechet/poly/parallel.cpp \
        $$SRC/m4ri/mzd_bool.cpp \
        $$SRC/m4ri/russian_bool.cpp \
        $$SRC/clm4rm/clm4rm.cpp \
        $$SRC/clm4rm/clm4rm_bitwise.cpp \
        $$SRC/clm4rm/clm4rm_multiplication.cpp

HEADERS += \
        $$INCL/data/types.h \
        $$INCL/data/dset.h \
        $$INCL/data/interval.h \
        $$INCL/data/array2d.h \
        $$INCL/data/array2d_impl.h \
        $$INCL/data/bitset.h \
        $$INCL/data/linkedlist.h \
        $$INCL/data/matrix_pool.h \
        $$INCL/data/numeric.h \
        $$INCL/data/spirolator.h \
        $$INCL/frechet/k_frechet/kalgorithm.h \
        $$INCL/frechet/freespace/freespace.h \
        $$INCL/frechet/app/frechetviewapplication.h \
        $$INCL/frechet/app/filehistory.h \
        $$INCL/frechet/app/concurrency.h \
        $$INCL/frechet/input/inputreader.h \
        $$INCL/frechet/input/datapath.h \
        $$INCL/frechet/view/mainwindow.h \
        $$INCL/frechet/view/curveview.h \
        $$INCL/frechet/view/baseview.h \
        $$INCL/frechet/view/freespaceview.h \
        $$INCL/frechet/view/controlpanel.h \
        $$INCL/frechet/view/palette.h \
        $$INCL/frechet/view/intervalview.h \
        $$INCL/frechet/view/animation.h \
        $$INCL/frechet/freespace/grid.h \
        $$INCL/frechet/freespace/components.h \
        $$INCL/frechet/reachability/fs_path.h \
        $$INCL/frechet/input/path.h \
        $$INCL/frechet/app/workerthread.h \
        $$INCL/frechet/app/filehistory.h \
        $$INCL/frechet/reachability/boundary.h \
        $$INCL/frechet/reachability/structure.h \
        $$INCL/frechet/reachability/graph_m4ri.h \
        $$INCL/frechet/reachability/graph_cl.h \
        $$INCL/frechet/reachability/graph_model.h \
        $$INCL/frechet/poly/poly_utils.h \
        $$INCL/frechet/poly/types.h \
        $$INCL/frechet/poly/algorithm.h \
        $$INCL/frechet/poly/jobs.h \
        $$INCL/frechet/poly/parallel.h \
        $$INCL/frechet/poly/double_queue.h \
        $$INCL/frechet/poly/double_queue_impl.h \
        $$INCL/frechet/poly/shortest_paths.h \
        $$INCL/frechet/poly/triangulation.h \
        $$INCL/frechet/poly/poly_path.h \
        $$INCL/frechet/poly/optimise_impl.h \
        $$INCL/frechet/poly/parallel.h \
        $$INCL/m4ri/mzd_bool.h \
        $$INCL/m4ri/russian_bool.h \
        $$INCL/m4ri/or.h \
        $$INCL/frechet/freespace/freespace_impl.h \
        $$INCL/frechet/poly/jobs.h \
        $$INCL/frechet/poly/parallel.h \
        $$INCL/clm4rm/clm4rm.h

FORMS += \
        $$RSCR/view/mainwindow.ui \
        $$RSCR/view/controlpanel.ui

RESOURCES += ../../rsrc/images.qrc

linux {
   # LIBS += -L$$PWD/../../lib/
    LIBS += -lOpenCL
   # -lCGAL -lgmp
    LIBS += -L$$TBB_DIR/lib/intel64/gcc4.7 -ltbb -ltbbmalloc -ltbbmalloc_proxy
    RESOURCES += ../../rsrc/linux.qrc
}

macx  {
#    LIBS += -L$$PWD/../../lib/
    LIBS += -framework OpenCL
#    LIBS += -lCGAL -lgmp -lm4ri
#    LIBS += -L/usr/local/lib -lm4ri
    LIBS += -L$$TBB_DIR/lib -ltbb
# -ltbbmalloc -ltbbmalloc_proxy
}

win32 {
    # LIBS += $$PWD/../../lib/m4ri.lib
    LIBS += C:\Intel\OpenCL\sdk\lib\x64\OpenCL.lib
    LIBS += /LIBPATH:"$$TBB_DIR\lib\intel64_win\vc14"
    LIBS += tbb.lib
    RC_FILE = ../../rsrc/win.rc
}
