# clean build
QT=~/Qt5.10.1/5.10.1/gcc_64/
QTBIN=$QT/bin

#
# deployment folder
#
NAME="Fréchet View"
APP=frechet-view.app
#mkdir $APP
#mkdir $APP/usr
#mkdir $APP/usr/bin
mv frechet-view "$APP"/usr/bin
#mkdir $APP/usr/lib
#cp /usr/lib/x86_64-linux-gnu/libOpenCL.so* "$APP"/usr/lib
# don't use /opt/intel instead !!
cp /opt/intel/opencl/libOpenCL.so* "$APP"/usr/lib

TBBDIR=/opt/intel2/tbb/lib/intel64/gcc4.7
cp $TBBDIR/libtbb.* "$APP"/usr/lib
cp $TBBDIR/libtbbmalloc.* "$APP"/usr/lib
cp $TBBDIR/libtbbmalloc_proxy.* "$APP"/usr/lib
#cp /usr/lib/x86_64-linux-gnu/libCGAL.so "$APP"/usr/lib
#cp /usr/lib/x86_64-linux-gnu/libgmp.so "$APP"/usr/lib

# OpenCL Kernels
cp ../../src/clm4rm/*.cl "$APP"/usr/src/clm4rm

# copy dependencies to AppDir
../linuxdeployqt-continuous-x86_64.AppImage $APP/usr/share/applications/frechet-view.desktop -no-translations -qmake=$QTBIN/qmake 
# create AppImage
../appimagetool-x86_64.AppImage  $APP
# compression: --comp xz   or  --comp gzip
mv Fréchet_View-x86_64.AppImage "$NAME"

	
#
# wrap it up
#
#tar -czf frechet-view.tar.gz $OUT

# clean up
#make clean




 
