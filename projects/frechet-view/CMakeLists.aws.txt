project(frechet_view) # Your project name
cmake_minimum_required(VERSION 3.10)

set(CMAKE_PREFIX_PATH /usr/bin/cmake)
set(CMAKE_CXX_STANDARD 11) # This is equal to QMAKE_CXX_FLAGS += -std=c++0x

set ( COMMON_C_FLAGS "-Wno-deprecated-declarations -mmmx -msse -msse2 -msse3 -DHAVE_CONFIG_H=1")
set ( CMAKE_C_FLAGS_DEBUG "${COMMON_C_FLAGS} -g" )
set ( CMAKE_CXX_FLAGS_DEBUG "${COMMON_C_FLAGS} -g" )

set ( CMAKE_C_FLAGS_RELEASE "${COMMON_C_FLAGS} -O3" )
set ( CMAKE_CXX_FLAGS_RELEASE "${COMMON_C_FLAGS} -O3" )

# Find includes in corresponding build directories
#set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
# Instruct CMake to run uic automatically when needed.
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

# This will find the Qt5 files. You will need a QT5_DIR env variable
find_package(Qt5Core REQUIRED) # Equivalent of QT += widgets
find_package(Qt5Gui REQUIRED) # Equivalent of QT += widgets
find_package(Qt5Widgets REQUIRED) # Equivalent of QT += widgets
find_package(Qt5Xml REQUIRED)
find_package(Qt5XmlPatterns REQUIRED)
find_package(Qt5OpenGL REQUIRED) # Equivalent of QT += widgets
find_package(Qt5PrintSupport REQUIRED) # Equivalent of QT += widgets
find_package(Qt5Qml REQUIRED) # Equivalent of QT += widgets
find_package(Qt5Svg REQUIRED) # Equivalent of QT += widgets

find_package(Boost REQUIRED)
#find_package(CGAL REQUIRED)

set(M4RI_DIR       /home/ec2-user/m4ri)
set(TBB_DIR        /opt/intel2/tbb)

set( CGAL /home/ec2-user/cgal )
set( CGAL_INCL 
        ${CGAL}/include 
        ${CGAL}/Polygon/include 
        ${CGAL}/Circulator/include 
        ${CGAL}/Installation/include 
        ${CGAL}/STL_Extension/include 
        ${CGAL}/Number_types/include 
        ${CGAL}/Kernel_23/include 
        ${CGAL}/Profiling_tools/include 
        ${CGAL}/Stream_support/include 
        ${CGAL}/Algebraic_foundations/include 
        ${CGAL}/Interval_support/include 
        ${CGAL}/Modular_arithmetic/include 
        ${CGAL}/Cartesian_kernel/include 
        ${CGAL}/Distance_2/include 
        ${CGAL}/Distance_3/include 
        ${CGAL}/Intersections_2/include 
        ${CGAL}/Intersections_3/include 
        ${CGAL}/Filtered_kernel/include 
        ${CGAL}/Homogeneous_kernel/include 
        ${CGAL}/Kernel_d/include 
        ${CGAL}/Arithmetic_kernel/include 
        ${CGAL}/Partition_2/include 
        ${CGAL}/Convex_hull_2/include 
        ${CGAL}/Triangulation_2/include 
        ${CGAL}/TDS_2/include 
        ${CGAL}/Hash_map/include 
        ${CGAL}/Spatial_sorting/include 
        ${CGAL}/Property_map/include )       

set(SOURCES
        ../../src/data/array2d.cpp
        ../../src/data/bitset.cpp
        ../../src/data/interval.cpp
        ../../src/data/linkedlist.cpp
        ../../src/frechet/app/frechetviewapplication.cpp
        ../../src/frechet/app/filehistory.cpp
        ../../src/frechet/app/cli.cpp
        ../../src/frechet/app/concurrency.cpp
        ../../src/frechet/freespace/components.cpp
        ../../src/frechet/freespace/freespace.cpp
        ../../src/frechet/freespace/grid.cpp
        ../../src/frechet/reachability/boundary.cpp
        ../../src/frechet/reachability/fs_path.cpp
        ../../src/frechet/reachability/str_singlecell.cpp
        ../../src/frechet/reachability/graph_m4ri.cpp
        ../../src/frechet/reachability/graph_cl.cpp
        ../../src/frechet/reachability/graph_model.cpp
        ../../src/frechet/reachability/structure.cpp
        ../../src/frechet/input/datapath.cpp
        ../../src/frechet/input/inputreader.cpp
        ../../src/frechet/input/path.cpp
        ../../src/frechet/input/scriptinput.cpp
        ../../src/frechet/k_frechet/kalgorithm.cpp
        ../../src/frechet/app/workerthread.cpp
        ../../src/frechet/view/baseview.cpp
        ../../src/frechet/view/controlpanel.cpp
        ../../src/frechet/view/curveview.cpp
        ../../src/frechet/view/freespaceview.cpp
        ../../src/frechet/view/intervalview.cpp
        ../../src/frechet/view/mainwindow.cpp
        ../../src/frechet/view/palette.cpp
        ../../src/frechet/poly/algorithm.cpp
        ../../src/frechet/poly/poly_utils.cpp
        ../../src/frechet/poly/shortest_paths.cpp
        ../../src/frechet/poly/triangulation.cpp
        ../../src/frechet/poly/poly_path.cpp
        ../../src/frechet/poly/optimise.cpp
        ../../src/frechet/poly/parallel.cpp
        ../../src/m4ri/mzd_bool.cpp
        ../../src/m4ri/russian_bool.cpp
        ../../src/clm4rm/clm4rm.c
        ../../src/clm4rm/clm4rm_bitwise.c
        ../../src/clm4rm/clm4rm_multiplication.c
        )

set(M4RI_SOURCES
        ${M4RI_DIR}/m4ri/misc.c
        ${M4RI_DIR}/m4ri/mzd.c
        ${M4RI_DIR}/m4ri/mmc.c
        ${M4RI_DIR}/m4ri/brilliantrussian.c
        ${M4RI_DIR}/m4ri/debug_dump.c
        ${M4RI_DIR}/m4ri/djb.c
        ${M4RI_DIR}/m4ri/echelonform.c
        ${M4RI_DIR}/m4ri/graycode.c
        #${M4RI_DIR}/m4ri/io.c
        ${M4RI_DIR}/m4ri/mzp.c
        ${M4RI_DIR}/m4ri/ple.c
        ${M4RI_DIR}/m4ri/ple_russian.c
        ${M4RI_DIR}/m4ri/solve.c
        ${M4RI_DIR}/m4ri/strassen.c
        ${M4RI_DIR}/m4ri/triangular.c
        ${M4RI_DIR}/m4ri/triangular_russian.c)

include_directories(
        ../../include
        ../../include/data
        ../../include/frechet
        ../../include/frechet/app
        ../../include/frechet/freespace
        ../../include/frechet/reachability
        ../../include/frechet/input
        ../../include/frechet/k_frechet
        ../../include/frechet/view
        ../../include/frechet/poly
        ../../include/m4ri
        ../../include/m4ri/m4ri
        ../../include/clm4rm
        ${M4RI_DIR}
        ${TBB_DIR}/include
        ${CGAL_INCL} )

set(HEADERS
        ../../include/data/array2d.h
        ../../include/data/array2d_impl.h
        ../../include/data/bitset.h
        ../../include/data/dset.h
        ../../include/data/interval.h
        ../../include/data/types.h
        ../../include/data/linkedlist.h
        ../../include/data/spirolator.h
        ../../include/data/numeric.h
        ../../include/frechet/app/frechetviewapplication.h
        ../../include/frechet/app/filehistory.h
        ../../include/frechet/app/clpp.h
        ../../include/frechet/app/workerthread.h
        ../../include/frechet/app/concurrency.h
        ../../include/frechet/freespace/components.h
        ../../include/frechet/freespace/freespace.h
        ../../include/frechet/freespace/grid.h
        ../../include/frechet/reachability/boundary.h
        ../../include/frechet/reachability/fs_path.h
        ../../include/frechet/reachability/graph_m4ri.h
        ../../include/frechet/reachability/graph_cl.h
        ../../include/frechet/reachability/graph_model.h
        ../../include/frechet/reachability/structure.h
        ../../include/frechet/input/datapath.h
        ../../include/frechet/input/inputreader.h
        ../../include/frechet/input/path.h
        ../../include/frechet/k_frechet/kalgorithm.h
        ../../include/frechet/view/baseview.h
        ../../include/frechet/view/controlpanel.h
        ../../include/frechet/view/curveview.h
        ../../include/frechet/view/freespaceview.h
        ../../include/frechet/view/intervalview.h
        ../../include/frechet/view/mainwindow.h
        ../../include/frechet/view/palette.h
        ../../include/frechet/poly/types.h
        ../../include/frechet/poly/algorithm.h
        ../../include/frechet/poly/optimise_impl.h
        ../../include/frechet/poly/poly_utils.h
        ../../include/frechet/poly/double_queue.h
        ../../include/frechet/poly/double_queue_impl.h
        ../../include/frechet/poly/shortest_paths.h
        ../../include/frechet/poly/triangulation.h
        ../../include/frechet/poly/poly_path.h
        ../../include/frechet/poly/parallel.h
        ../../include/m4ri/mzd_bool.h
        ../../include/m4ri/russian_bool.h
        ../../include/clm4rm/clm4rm.h
        )

#set(TEST_SOURCES
#        ../../test/reachability_single_cell_test.cpp)

set(CMAKE_AUTOUIC_SEARCH_PATHS
        ../../rsrc/view)
set(UI
        ../../rsrc/view/controlpanel.ui
        ../../rsrc/view/mainwindow.ui
        )
set(QRC
        ../../rsrc/images.qrc)

#find_library(TBB NAMES tbb tbbmalloc HINTS /opt/intel2/tbb/lib/intel64/gcc4.7)
find_library(TBB_debug  NAMES tbb_debug tbbmalloc_debug tbbmalloc_proxy_debug HINTS /opt/intel2/tbb/lib/intel64/gcc4.7)
find_library(TBB        NAMES tbb tbbmalloc tbbmalloc_proxy HINTS /opt/intel2/tbb/lib/intel64/gcc4.7)

# This will create you executable
add_executable(frechet_view ${SOURCES} ${M4RI_SOURCES} ${HEADERS} ${UI} ${QRC} ../../src/frechet/app/main.cpp)
# This will link necessary Qt5 libraries to your project
target_link_libraries(frechet_view debug
        Qt5::Core
        Qt5::Gui
        Qt5::Widgets
        Qt5::OpenGL
        Qt5::PrintSupport
        Qt5::XmlPatterns
        Qt5::Qml
        Qt5::Svg
        ${TBB_debug}
        OpenCL)
#        CGAL 
#        gmp
#        quadmath
#        boost_system boost_thread)
#        m4ri png)

target_link_libraries(frechet_view optimized
        Qt5::Core
        Qt5::Gui
        Qt5::Widgets
        Qt5::OpenGL
        Qt5::PrintSupport
        Qt5::XmlPatterns
        Qt5::Qml
        Qt5::Svg
        ${TBB}
        OpenCL)
#        CGAL 
#        gmp
#        quadmath
#        boost_system boost_thread)
#        m4ri png)


# Unit Test Target
#add_executable(frechet_view_unit_tests ${SOURCES} ${TEST_SOURCES} ${HEADERS})
