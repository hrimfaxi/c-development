rem clean build
set QT=C:/Qt/Qt5.10.1/5.10.1/msvc2017_64
set QTBIN=%QT%/bin
set PATH=%QTBIN%;%PATH%

rem 
rem get latest sources
rem
cd C:/git-repos/c-development
git pull

rem
rem build
rem
rem set up tool chain:
set VC="C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\VC\Auxiliary\Build"
call %VC%/vcvarsall.bat amd64
cd projects/frechet-view
nmake clean
%QTBIN%/qmake -spec win32-msvc -config release
nmake

rem
rem Deploy
rem
set NAME="Frechet_View"
mkdir %NAME%
move release\frechet-view.exe %NAME%
%QTBIN%/windeployqt --dir %NAME% --release --no-translations --no-compiler-runtime %NAME%/frechet-view.exe 

rem C++ runtime
set RT="C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\redist\x64\Microsoft.VC140.CRT"
copy %RT%\vcruntime140.dll %NAME%
copy %RT%\msvcp140.dll %NAME%
copy %RT%\vccorlib140.dll %NAME%

rem libmmd.dll
copy ..\..\lib\libmmd.dll %NAME%

rem OpenCL.dll
copy ..\..\lib\OpenCL_x64.dll %NAME%\OpenCL.dll

rem tbb.dll
copy ..\..\lib\tbb.dll %NAME%

rem Desktop files
copy ..\..\rsrc\desktop.ini %NAME%
copy ..\..\rsrc\paseando-al-perro.ico %NAME%
attrib +s +h %NAME%/desktop.ini
attrib +s +h %NAME%/paseando-al-perro.ico

rem wrap it up
zip -r -S Frechet_View_for_Windows.zip %NAME%



 