# clean build
QT=~/Qt5.10.1/5.10.1/gcc_64/
QTBIN=$QT/bin

# 
# get latest sources
#
cd ~/Dokumente/c-development
git pull

#
# build
#
cd projects/frechet-view
make clean
$QTBIN/qmake -config release
make
