
win32:GOOGLETEST_DIR=C:\git-repos\googletest
linux:GOOGLETEST_DIR=/home/nightrider/Dokumente/googletest

include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG += qt

win32: {
QMAKE_CFLAGS += /TP
# use C++ compiler, even for .c files (M4RI only)
BOOST_INCL=C:\boost_1_66_0
M4RI_INCL=C:\git-repos\m4ri
TBB_DIR="C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2019\windows\tbb"
}

linux:{
BOOST_INCL=/home/nightrider/boost_1.58/include
M4RI_INCL=/home/nightrider/Dokumente/m4ri
QMAKE_CXXFLAGS += -Wno-sign-compare -Wno-reorder
QMAKE_CXXFLAGS += -mmmx -msse -msse2 -msse3 -O2
}

top_srcdir=../..

SRC=$$top_srcdir/src
INCL=$$top_srcdir/include
TEST=$$top_srcdir/test

INCLUDEPATH += $$TEST/matrix $$INCL $$M4RI_INCL $$GOOGLETEST_DIR/include $$GOOGLETEST_DIR $$TBB_DIR/include

SOURCES += \
        $$TEST/matrix/main.cpp \
        $$SRC/frechet/app/concurrency.cpp \
        $$SRC/m4ri/mzd_bool.c \
        $$SRC/m4ri/russian_bool.c \
        $$GOOGLETEST_DIR/googletest/src/gtest-all.cc \
    ../../src/m4ri/ocl_prototype.c

HEADERS += \
        $$TEST/matrix/matrix_benchmark.h \
 #       $$TEST/matrix/ublas_benchmark.h \
 #       $$TEST/matrix/mkl_benchmark.h \
 #       $$TEST/matrix/bitserial_benchmark.h \
        $$TEST/matrix/m4ri_benchmark.h \
        $$INCL/m4ri/mzd_bool.h \
        $$INCL/m4ri/russian_bool.h \
        $$INCL/m4ri/or.h \
    ../../include/m4ri/ocl_prototype.h

DEFINES += SRCDIR=\\\"$$TEST/matrix\\\"

win32 {
    # LIBS += $$PWD/../../lib/m4ri.lib
    LIBS += $$TBB_DIR/lib/intel64_win/vc14/tbb_debug.lib
    #   Static linking doesn't work for one reason or another.
    SOURCES += $$M4RI_INCL/m4ri/mzd.c \
            $$M4RI_INCL/m4ri/brilliantrussian.c \
            $$M4RI_INCL/m4ri/echelonform.c \
            $$M4RI_INCL/m4ri/misc.c \
            $$M4RI_INCL/m4ri/mp.c \
            $$M4RI_INCL/m4ri/mzp.c \
            $$M4RI_INCL/m4ri/ple.c \
            $$M4RI_INCL/m4ri/mmc.c \
            $$M4RI_INCL/m4ri/graycode.c \
            $$M4RI_INCL/m4ri/ple_russian.c \
            $$M4RI_INCL/m4ri/triangular_russian.c \
            $$M4RI_INCL/m4ri/triangular.c \
            $$M4RI_INCL/m4ri/strassen.c \
            $$M4RI_INCL/m4ri/solve.c
}
