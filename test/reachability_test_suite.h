#ifndef REACHABILITY_TEST_DATA_H
#define REACHABILITY_TEST_DATA_H

#include <interval.h>
#include <freespace.h>
#include <structure.h>

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>
//#include <reachability_test_suite.h>

/**
 * Overlapping Interval arrangements (see Structure::singleCell)
 */
typedef std::pair<frechet::Interval,frechet::Interval> TwoIntervals;

inline TwoIntervals swapped(const TwoIntervals& a) {
    return TwoIntervals(a.second,a.first);
}

using namespace frechet;
using namespace reach;
using namespace testing;

class ReachabilityTestSuite : public testing::Test
{
private:    
    static ReachabilityTestSuite* suite;

    FreeSpace::ptr fs=nullptr;
    Structure* str=nullptr;
    //  Test: segments outside [l,h] are unreachable (only in single cell test)
    bool test_outside_pointers;

    friend class TestStructure;

    static TwoIntervals arrangements [50];
    static int count;

protected:
    virtual void SetUp() override {
        suite = this;
    }

public:
    void testSingleCell();
    void testMergeGeneric();
    void testMergeCurves();
    //void testAllocators();

    void testMergeColumns(QString filename, double eps, int i1, int i2);

private:
    FreeSpace::ptr createEmptyFreeSpace(int n, int m);

    void testMergeCurve(QString filename, double eps);
    void testMergeCurve(Curve& P, Curve& Q, double eps);

    void testSingleCell(int i, int j, TwoIntervals vert, TwoIntervals horiz);
    void assertPointerInterval(Pointer p, PointerInterval ival);

    void assertComplexStructure(Structure*, const Rect& r, bool with_fs=true);
    void assertStructure(int i0, int j0, int i1, int j1);

    void assert2Segments(Pointer i1, Pointer i2, Orientation ori);
    void assert1Segment(Pointer i, Pointer j, Interval F);
    void assertNeighbors(Pointer a, Pointer b, Pointer c, Pointer d);

    void assertLHContingency(const BoundaryList& l, const BoundaryList& r);

    void readFile(QString filename, Curve& P, Curve& Q, bool closep, bool closeq);
    static void closeCurve(Curve& P);
    static void openCurve(Curve& P);

    static void before_merge(Structure*, const Rect*, Structure*, const Rect*, Orientation);
    static void after_merge(Structure*, const Rect*, Structure*, const Rect*, Orientation);
    static void after_single_cell(Structure*, const Rect*, Structure*, const Rect*, Orientation);

//    void testAllocator(BElementAllocator*);
};

#endif // REACHABILITY_TEST_DATA_H
