#include <reachability_test_suite.h>
#include <triangulation_test_suite.h>
#include <graph_test_suite.h>

#include <gtest/gtest.h>
#include <QApplication>
#include <poly_path.h>
#include <numeric.h>

TEST_F(ReachabilityTestSuite, SingleCell)
{
    testSingleCell();
}

TEST_F(ReachabilityTestSuite, MergeGeneric)
{
   testMergeGeneric();
}

TEST_F(ReachabilityTestSuite, MergeCurves)
{
    testMergeCurves();
}

TEST_F(ReachabilityTestSuite, MergeColumns)
{
    testMergeColumns("data/ipe-closed.ipe", 266, 6,10);
}

TEST_F(TriangulationTestSuite, Decomposition)
{
    testDecomposition("data/hilbert-4.js");
    testDecomposition("data/hilbert-4.svg");
    //testDecomposition("data/hilbert-4-precision.svg");
    //testDecomposition("data/hilbert-5.svg");
}

TEST_F(TriangulationTestSuite, FanTriangulation)
{
    //  Googletest, see https://github.com/google/googletest/blob/master/googletest/docs/advanced.md
    testFanTriangulation("data/ipe-polygon.ipe");
    testFanTriangulation("data/ipe-closed.ipe");

    testFanTriangulation("data/hilbert.js");
}

TEST_F(TriangulationTestSuite, Guibas)
{
//    testGuibas("data/ipe-closed.ipe",false,false,false);

    testGuibas("data/hilbert-4.jscript",false,true,false);
//    testGuibas("data/hilbert-4.js",false,true,false);
    //testGuibas("data/hilbert-5.svg",false,false,false);
//    testGuibas("data/hilbert-5.svg",false,true,false);
//    testGuibas("data/snowflake.js",false,true,false);
}

TEST_F(TriangulationTestSuite, ExtraPoints)
{
    testGuibas("data/ipe-closed.ipe",false,false,true);
}

TEST_F(TriangulationTestSuite, GuibasFS)
{
//    testGuibasFS("data/ipe-closed.ipe", 400.0,true);
//    testGuibasFS("data/snowflake.js", 400.0,true);
//    testGuibasFS("data/hilbert.js", 300.0,false);
    testGuibasFS("Demo Data/trousers.jscript",35,false);
    testGuibasFS("Demo Data/trousers.jscript",36,true);
}

TEST_F(TriangulationTestSuite, AlgorithmEpsilon)
{
    testAlgorithm("data/ipe-closed.ipe", 257.381);
    testAlgorithm("Demo Data/trousers.svg", 35.4);
}

TEST_F(TriangulationTestSuite, BinarySimple)
{
    testApproximation("data/ipe-closed.ipe", 0.000001);
    testApproximation("Demo Data/trousers.jscript", 0.000001);
}

void assertDMapping(const Curve& P, double p1, double p2,
                   const Curve& Q, double q1, double q2,
                   double epsilon)
{
    QPointF P1 = Grid::mapToPoint(P, p1);
    QPointF P2 = Grid::mapToPoint(P, p2);
    QPointF Q1 = Grid::mapToPoint(Q, q1);
    QPointF Q2 = Grid::mapToPoint(Q, q2);

    ASSERT_LE(frechet::euclidean_distance<double>(P1,Q1), epsilon) << p1 << " " << q1;
    ASSERT_LE(frechet::euclidean_distance<double>(P2,Q2), epsilon) << p2 << " " << q2;
}

void assertMapping(const Curve& P, double p1, double p2,
                   const Curve& Q, double q1, double q2,
                   double epsilon)
{
    //  ranges should fit into one free-space cell
    ASSERT_EQ(floor(p1) + 1, ceil(p2)) << p1 << " " << p2;
    ASSERT_EQ(floor(q1) + 1, ceil(q2)) << q1 << " " << q2;
    assertDMapping(P,p1,p2, Q,q1,q2, epsilon);
}

TEST_F(TriangulationTestSuite, Trouser)
{
    //testCurvePolyApproximation("Demo Data/trousers.jscript", 0.000001);
    double epsilon = 35.4;
    testGuibasFS("Demo Data/trousers.jscript", epsilon, epsilon >= 35.4);
    //
    if (!res) return;

    const Curve& P = algo->Pcurve();
    const Curve& Q = algo->Qcurve();
/*
    double p1=0, p2=1, p3=2, p4=3, p5=0.381, p6=0.39;
    double q1=1, q2=0, q3=0.381, q4=0.39, q5=2, q6=3;

    QPointF Q3= Grid::mapToPoint(Q, q3);
    std::cout <<"("<<Q3.x()<<","<<Q3.y()<<")"<< std::endl;

    //  Die Diagonale p1-p3 mappt auf den shortest path q1-q3
    assertDMapping(P,p1,p3, Q,q1,q3, epsilon);
    //  das rechte Hosenbein mappt ebenfalls auf dieses Segment
    assertMapping(P,p3,p4, Q,q3,q4, epsilon);
    assertMapping(P,p4,p1+4, Q,q4,q1, epsilon);
    //  das linke Hosenbein mappt auf den Rest
    assertMapping(P,p1,p5, Q,q1,q5, epsilon);
    assertMapping(P,p5,p6, Q,q5,q6, epsilon);
    assertMapping(P,p6,p2, Q,q6,q2+4, epsilon);
    assertMapping(P,p2,p3, Q,q2,q3, epsilon);
*/
    frechet::poly::PolygonFSPath pfs(fs);
    pfs.update(res,epsilon);
    //  validate P to Q mappings
    //  Start point is mapped to Q[0]
    double pw = pfs.startPoint().x();
    Q_ASSERT(pfs.startPoint().y()==0.0);
    QPointF p = Grid::mapToPoint(P,pw);
    QPointF q = Q[0];
    double d = frechet::euclidean_distance<double>(p,q);
    ASSERT_LE(d, epsilon + 1e-12) << d << " <= " << epsilon;
/*  @deprecated
    for(int i=0; i < P.size(); ++i)
    {
        p = P[i];
        if (pfs.isMappedQ(i)) {
            double qw = pfs.mapQ(i);
            q = Grid::mapToPoint(Q, qw);

            std::cout<< "P["<<i<<"] ("<<p.x()<<","<<p.y()<<") "
                    << " = "
                    << "Q["<<qw<<"] ("<<q.x()<<","<<q.y()<<")"
                    << std::endl;
            ASSERT_LE(frechet::euclidean_distance(p,q), epsilon+1e-12);
        }
    }
*/
    //  map diagonal(0,2) to shortest path
/*    double w0 = pfs.mapQ(0);
    double w2 = pfs.mapQ(2);
    QLineF d (P[0],P[2]);

    Triangulation& Qtri = algo->triangulation(false);
    frechet::poly::PolygonShortestPaths psp(Qtri);
    Curve sp02 = psp.find1ShortestPath(w0,w2);
    for(int j=0; j < sp02.size(); ++j) {
        QPointF q = sp02[j];
        std::cout << "("<<q.x()<<","<<q.y()<<") "
                <<" d=" << frechet::euclidean_distance(d,q)
                << std::endl;
        ASSERT_LE(frechet::euclidean_distance(d,q), epsilon);
    }
*/
}

TEST_F(TriangulationTestSuite, IpeClosed266)
{
    testIpeClosed266();
}

TEST_F(GraphTestSuite, Copy)
{
    createRandomModel(30);
    testCopy();
}

TEST_F(GraphTestSuite, EdgeQueries)
{
    testMatrixBasics();
    createRandomModel(30);
    testEdgeQueries();
}

TEST_F(GraphTestSuite, BitwiseAnd)
{
    createRandomModel(30);
    testBitwiseAnd();
}

TEST_F(GraphTestSuite, ReachabilityGraph)
{
    createRandomModel(30);
    createReachabilityGraphs();

    readFile("data/ipe-closed.ipe",400.0);
    createReachabilityGraphs();
}

TEST_F(GraphTestSuite, TransitiveClosure)
{
    createRandomModel(30);
    testTransitiveClosure();

    readFile("data/ipe-closed.ipe",400.0);
    testTransitiveClosure();
}

TEST_F(GraphTestSuite, Spirolator)
{
    testSpirolator(1,1);
    testSpirolator(2,3);
    testSpirolator(2,4);
    testSpirolator(5,8);
    testSpirolator(5,9);
    testSpirolator(5,10);
    testSpirolator(50,89);
    testSpirolator(50,90);
    testSpirolator(50,91);
}



int main(int argc, char *argv[])
{
    //  Some Qt function require an QApplication instance.
    //  (but not necessarily a FrechetViewApplication...)
    QApplication app(argc, argv); \
    app.setAttribute(Qt::AA_Use96Dpi, true);

    ::testing::InitGoogleTest(&argc, argv);
    int result = RUN_ALL_TESTS();

    //app.exec();
    return result;
}
