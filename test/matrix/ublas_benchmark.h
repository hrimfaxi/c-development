#ifndef UBLAS_BENCHMARK_H
#define UBLAS_BENCHMARK_H

#include <matrix_benchmark.h>

#ifndef NDEBUG
# define NDEBUG
//  important for release build !!
#endif

#include <boost/numeric/ublas/matrix.hpp>

/**
 * Benchmark for Boost uBLAS
 *
 */
class UBlasBenchmark : public MatrixBenchmark
{
public:
    UBlasBenchmark() : MatrixBenchmark(FLOAT32) { }

    void multiply() override
    {
        typedef boost::numeric::ublas::matrix<short> smatrix_t;
        typedef boost::numeric::ublas::matrix<float> fmatrix_t;

        switch(etype) {
            case INT16:     (*(smatrix_t*)C) = boost::numeric::ublas::prod( *(smatrix_t*)A, *(smatrix_t*)B ); return;
            case FLOAT32:   (*(fmatrix_t*)C) = boost::numeric::ublas::prod( *(fmatrix_t*)A, *(fmatrix_t*)B ); return;
        }
    }

    void allocMatrixes() override
    {
        switch(etype) {
            case INT16:     allocMatrixes<short>(); return;
            case FLOAT32:   allocMatrixes<float>(); return;
        }
        assert(false);
    }

    template<typename T>
    void allocMatrixes()
    {
        typedef boost::numeric::ublas::matrix<T> matrix_t;
        A = new matrix_t(n,n);
        B = new matrix_t(n,n);
        C = new matrix_t(n,n);

        for(int i=0; i < n; ++i)
            for(int j=0; j < n; ++j)
            {
                (*(matrix_t*)A) (i,j) = rbool();
                (*(matrix_t*)B) (i,j) = rbool();
                (*(matrix_t*)C) (i,j) = 0;
            }
    }

    void freeMatrixes() override
    {
        switch(etype) {
            case INT16:     freeMatrixes<short>(); return;
            case FLOAT32:   freeMatrixes<float>(); return;
        }
        assert(false);
    }

    template<typename T>
    void freeMatrixes()
    {
        typedef boost::numeric::ublas::matrix<T> matrix_t;
        delete (matrix_t*)A;
        delete (matrix_t*)B;
        delete (matrix_t*)C;
    }
};

//  Test Cases

TEST_F(UBlasBenchmark, Multiplys16)
{
    etype = INT16;
    benchmarkMultiply("ublas-s16",256);
}

TEST_F(UBlasBenchmark, Multiplyf32)
{
    etype = FLOAT32;
    benchmarkMultiply("ublas-f32",256);
}


#endif // UBLAS_BENCHMARK_H
