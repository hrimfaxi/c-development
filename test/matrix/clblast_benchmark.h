//
// Created by nightrider on 15.09.18.
//

#ifndef MATRIX_BENCHMARKS_CLBLAST_BENCHMARK_H
#define MATRIX_BENCHMARKS_CLBLAST_BENCHMARK_H

#include <matrix_benchmark.h>

extern "C" {
#include <clblast_c.h>
};

/**
 * Benchmark for clBLAS
 *
 *
 */
class clBlastBenchmark : public MatrixBenchmark
{
protected:
    cl_context ctx;
    cl_int err;
    cl_platform_id platforms[4];
    cl_platform_id platform;
    cl_uint num_platforms;
    cl_device_id device = 0;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
    cl_command_queue queue = 0;
    cl_event event = NULL;

    cl_mem bufA, bufB, bufC;

public:

    clBlastBenchmark() : MatrixBenchmark(FLOAT32)
    {
        transposeB=true;
        int ret = 0;

        /* Setup OpenCL environment. */
        err = clGetPlatformIDs( 4, platforms, &num_platforms );
        for(int plf=0; plf < num_platforms; ++plf)
        {
            char name[1024];
            char version[1024];
            char profile[1024];
            char extensions[1024];
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_NAME, 1024,name, NULL);
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_VERSION, 1024,version, NULL);
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_PROFILE, 1024,profile, NULL);
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_EXTENSIONS, 1024,extensions, NULL);
            std::cout   << platforms[plf] << " = " << name << " " << version << ", "
                        << profile << ","
                        << extensions << std::endl << std::endl;
        }

        //  choose platform 1
        platform = platforms[1];
        err = clGetDeviceIDs( platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL );
        EXPECT_EQ(err,CL_SUCCESS);

        props[1] = (cl_context_properties)platform;
        ctx = clCreateContext( props, 1, &device, NULL, NULL, &err );
        queue = clCreateCommandQueue( ctx, device, 0, &err );
    }

    ~clBlastBenchmark()
    {
        /* Release OpenCL working objects. */
        clReleaseCommandQueue( queue );
        clReleaseContext( ctx );
    }


    void multiply(bool uptri) override
    {
		ASSERT_FALSE(uptri);
        /* Call clBLAS extended function. Perform gemm for the lower right sub-matrices */
        err = CLBlastSgemm(
                CLBlastLayoutRowMajor,
                CLBlastTransposeNo, CLBlastTransposeYes,
                n,n,n,
                1.0, bufA, 0, n,
                bufB, 0, n, 0.0,
                bufC, 0, n,
                &queue, &event);

        //  float16 is not supported by our hardward

        ASSERT_EQ(err,CL_SUCCESS);

        /* Wait for calculations to be finished. */
        err = clWaitForEvents( 1, &event );
        ASSERT_EQ(err,CL_SUCCESS);

        /* Fetch results of calculations from GPU memory. */
        size_t size = n*n*sizeof(cl_float);
        err = clEnqueueReadBuffer( queue, bufC, CL_TRUE, 0, size, C, 0, NULL, NULL );
        ASSERT_EQ(err,CL_SUCCESS);
    }


    void allocMatrixes(bool uptri) override
    {
		ASSERT_FALSE(uptri);
        allocFloatMatrixes();

        /* Prepare OpenCL memory objects and place matrices inside them. */
        size_t size = n*n*sizeof(cl_float);
        bufA = clCreateBuffer( ctx, CL_MEM_READ_ONLY, size, NULL, &err );               ASSERT_EQ(err,CL_SUCCESS);
        bufB = clCreateBuffer( ctx, CL_MEM_READ_ONLY, size, NULL, &err );               ASSERT_EQ(err,CL_SUCCESS);
        bufC = clCreateBuffer( ctx, CL_MEM_READ_WRITE, size, NULL, &err );              ASSERT_EQ(err,CL_SUCCESS);

        err = clEnqueueWriteBuffer( queue, bufA, CL_TRUE, 0, size, A, 0, NULL, NULL );  ASSERT_EQ(err,CL_SUCCESS);
        err = clEnqueueWriteBuffer( queue, bufB, CL_TRUE, 0, size, B, 0, NULL, NULL );  ASSERT_EQ(err,CL_SUCCESS);
        err = clEnqueueWriteBuffer( queue, bufC, CL_TRUE, 0, size, C, 0, NULL, NULL );  ASSERT_EQ(err,CL_SUCCESS);
    }

    void allocFloatMatrixes()
    {
        A = new cl_float[n*n];
        B = new cl_float[n*n];
        C = new cl_float[n*n];

        //  B is transposed ("column-major")
        for(int i=0; i < n; ++i)
            for(int j=0; i < n; ++i)
            {
                ((cl_float*)A) [i*n+j] = rbool();
                ((cl_float*)B) [i+j*n] = rbool();
                ((cl_float*)C) [i*n+j] = 0;
            }
    }

    void freeMatrixes() override
    {
        /* Release OpenCL memory objects. */
        clReleaseMemObject( bufC );
        clReleaseMemObject( bufB );
        clReleaseMemObject( bufA );

        delete (cl_float*)A;
        delete (cl_float*)B;
        delete (cl_float*)C;
    }

};

TEST_F(clBlastBenchmark, Multiplyf32)
{
    etype = FLOAT32;
    benchmarkMultiply("clblast-f32",4*1024);
}



#endif //MATRIX_BENCHMARKS_CLBLAST_BENCHMARK_H
