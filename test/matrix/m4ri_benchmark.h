#ifndef M4RI_BENCHMARK_H
#define M4RI_BENCHMARK_H

#include <matrix_benchmark.h>
#include <tbb/tbb.h>
#include <frechet/app/concurrency.h>


/**
 * Benchmark for M4RI
 *
 */
class M4riBenchmark : public MatrixBenchmark
{
protected:	
	mzd_t* Cref;
	typedef mzd_t* (*MatrixOp3) (mzd_t*,const mzd_t*,const mzd_t*,int);
    typedef mzd_t* (*MatrixOp4) (mzd_t*,const mzd_t*,const mzd_t*,int,int,int);

    int cores,threads, L2,L3;
public:
    M4riBenchmark() : MatrixBenchmark(BIT)
    { }

    virtual void TearDown() override
    {
        frechet::ConcurrencyContext::instance.close();
    }

    void init(bool parallel)
    {
        //m4ri_init();

        cores = frechet::ConcurrencyContext::instance.countCores();
        threads = parallel ? cores/2 : 1;
        frechet::ConcurrencyContext::instance.setup(threads);
        ASSERT_EQ(threads,frechet::ConcurrencyContext::instance.countThreads());

        L2 = frechet::ConcurrencyContext::cacheSize(2);
        L3 = frechet::ConcurrencyContext::cacheSize(3);

        std::cout   << "L2 cache = " << L2 << std::endl
                    << "L3 cache = " << L3 << std::endl
                    << threads << " threads on " << cores << " cores"
                                              << std::endl << std::endl;
    }

    void multiply(bool uptri) override
    {
		int k, blocksize;
		mzd_optimal_parameters((mzd_t*)A, (mzd_t*)B,
                L2, L3,
		        &k, &blocksize);

        if (uptri) {
            mzd_bool_mul_uptri((mzd_t*)C, (mzd_t*)A, (mzd_t*)B, k, blocksize, threads);
        }
        else {
			mzd_bool_mul_m4rm((mzd_t*)C, (mzd_t*)A, (mzd_t*)B, k, blocksize, threads);
        }
        //mzd_bool_mul_m4rm((mzd_t*)C, (mzd_t*)A, (mzd_t*)B, 0);
        //mzd_bool_mul_naive((mzd_t*)C, (mzd_t*)A, (mzd_t*)B);
    }

    void assertMultiply() override
    {
        mzd_bool_mul_m4rm(Cref, (mzd_t*)A, (mzd_t*)B, 0,0, 2);
        ASSERT_TRUE(mzd_equal(Cref,(mzd_t*)C));
    }

    void allocMatrixes(bool uptri) override
    {
    	allocMatrixes2(n,n,uptri);
    }

    void allocMatrixes3(int m, int l, int n)
    {
        A = rand_matrix(m,l,sparsity);
        B = rand_matrix(l,n,sparsity);
        C = rand_matrix(m,n,sparsity);
        Cref = mzd_copy(nullptr,(mzd_t*)C);
    }

    void allocUptriMatrixes3(int m)
    {
        A = rand_uptri_matrix(m,sparsity);
        B = rand_uptri_matrix(m,sparsity);
        C = rand_uptri_matrix(m,sparsity);
        Cref = mzd_copy(nullptr,(mzd_t*)C);
    }

    void allocMatrixes2(int m, int n, bool uptri)
    {
        if (uptri) {
            ASSERT_EQ(m,n);
            A = rand_uptri_matrix(n,sparsity);
            B = rand_uptri_matrix(n,sparsity);
            C = rand_uptri_matrix(n,sparsity);
        }
        else {
            A = rand_matrix(m,n,sparsity);
            B = rand_matrix(m,n,sparsity);
            C = rand_matrix(m,n,sparsity);
        }
        Cref = mzd_copy(nullptr,(mzd_t*)C);
    }

    void freeMatrixes() override
    {
        mzd_free((mzd_t*)A);
        mzd_free((mzd_t*)B);
        mzd_free((mzd_t*)C);
        mzd_free(Cref);
		A = B = C = NULL;
		Cref = NULL;
    }

    static constexpr int SET_COUNT = 20;
    static const int TEST_SETS [SET_COUNT][4];

    void useTestSet(int i)
    {
        this->m = TEST_SETS[i][0];
        this->l = TEST_SETS[i][1];
        this->n = TEST_SETS[i][2];
        this->sparsity = (double)TEST_SETS[i][3]/100.0;

        std::cout << "Set "<<i<< " = "<< m<<","<<l<<","<<n<<std::endl;
    }

    void testOperator2(MatrixOp3 op, MatrixOp3 opref)
    {
    	for(int set=0; set < SET_COUNT; ++set)
    	{
            useTestSet(set);

			allocMatrixes2(m,n,false);

			opref(Cref, (mzd_t*)A, (mzd_t*)B,0);	// reference implementation
			op((mzd_t*)C, (mzd_t*)A, (mzd_t*)B,2);	// my implementation

            if (!mzd_equal((mzd_t*)C,Cref)) {
                std::cout << "failure" << std::endl;
                print((mzd_t*) A);
                print((mzd_t*) B);
                print(Cref);
                print((mzd_t *) C);
            }
			// results must be equal
			ASSERT_TRUE(mzd_equal((mzd_t*)C, Cref));

			freeMatrixes();
    	}
    }

    void testOperator3(MatrixOp3 op, MatrixOp3 opref)
    {
    	for(int set=0; set < SET_COUNT; ++set)
    	{
            useTestSet(set);
	
			allocMatrixes3(m,l,n);		

			opref(Cref, (mzd_t*)A, (mzd_t*)B,0);	// reference implementation
            op((mzd_t*)C, (mzd_t*)A, (mzd_t*)B,0);	// my implementation

            if (!mzd_equal((mzd_t*)C,Cref)) {
                std::cout << "failure" << std::endl;
                //print((mzd_t*) A);
                //print((mzd_t*) B);
                print(Cref);
                print((mzd_t *) C);
            }
			// results must be equal
			ASSERT_TRUE(mzd_equal((mzd_t*)C, Cref));

			freeMatrixes();
    	}
    }

    void testOperator4(MatrixOp4 op, MatrixOp3 opref)
    {
        for(int set=0; set < SET_COUNT; ++set)
        {
            useTestSet(set);

            allocMatrixes3(m,l,n);

            opref(Cref, (mzd_t*)A, (mzd_t*)B,0);	// reference implementation
            op((mzd_t*)C, (mzd_t*)A, (mzd_t*)B,0,0,4);	// my implementation

            if (!mzd_equal((mzd_t*)C,Cref)) {
                std::cout << "failure" << std::endl;
            	//print((mzd_t*) A);
				//print((mzd_t*) B);
				print(Cref);
				print((mzd_t *) C);
			}
            // results must be equal
            ASSERT_TRUE(mzd_equal((mzd_t*)C,Cref));

            freeMatrixes();
        }
    }


protected:	

	mzd_t* rand_matrix(int r, int c, double sp) {
		mzd_t* M = mzd_init(r,c);
		for(int i=0; i < r; ++i)
            for(int j=0; j < c; ++j)
            		mzd_write_bit((mzd_t*)M, i,j, rbool(sp));
        return M;
	}

    mzd_t* rand_uptri_matrix(int rc, double sp) {
        mzd_t* M = mzd_init(rc,rc);
        for(int i=0; i < rc; ++i)
            for(int j=i; j < rc; ++j)
                    mzd_write_bit((mzd_t*)M, i,j, rbool(sp));
        return M;
    }

	static mzd_t* ref_add(mzd_t* C, const mzd_t* A, const mzd_t* B, int)
	{	//	refereence implementation for Addition
        EXPECT_EQ(A->nrows,B->nrows);
        EXPECT_EQ(A->ncols,B->ncols);
        bool a,b;
		for(int i=0; i < A->nrows; ++i)
			for(int j=0; j < A->ncols; ++j)
			{
				a = (bool)mzd_read_bit(A,i,j);
				b = (bool)mzd_read_bit(B,i,j);
				mzd_write_bit(C, i,j, a | b);
			}
		return C;
	}

    static mzd_t* ref_and(mzd_t* C, const mzd_t* A, const mzd_t* B, int)
    {   //  refereence implementation for Addition
        EXPECT_EQ(A->nrows,B->nrows);
        EXPECT_EQ(A->ncols,B->ncols);
        bool a,b;
        for(int i=0; i < A->nrows; ++i)
            for(int j=0; j < A->ncols; ++j)
            {
                a = (bool)mzd_read_bit(A,i,j);
                b = (bool)mzd_read_bit(B,i,j);
                mzd_write_bit(C, i,j, a & b);
            }
        return C;
    }

	static mzd_t* ref_addmul(mzd_t* C, const mzd_t* A, const mzd_t* B, int)
	{	//	refereence implementation for Addition-Multiplication
        EXPECT_EQ(A->ncols,B->nrows);
        EXPECT_EQ(A->nrows,C->nrows);
        EXPECT_EQ(B->ncols,C->ncols);
        bool a,b,c;
		for(int i=0; i < A->nrows; ++i)
			for(int j=0; j < B->ncols; ++j)
			{
				c = (bool)mzd_read_bit(C,i,j);
				if (c) continue;

				for(int h=0; h < A->ncols; ++h) {
					a = (bool)mzd_read_bit(A,i,h);
					b = (bool)mzd_read_bit(B,h,j);
					if (a & b) { c=true; break; }
				}
				mzd_write_bit(C, i,j, c);
			}
		return C;
	}
};

const int M4riBenchmark::TEST_SETS [M4riBenchmark::SET_COUNT][4] =
{
		1,    1,    1,  50,
		1,  128,  128,   1,
		1,    1,    1,  50,
		2,   64,   64,   1,
		64,  64,   64,   1,
		128,  128,  128, 1,
        21,  171,   31, 1,
        21,  171,   31, 1,
        193,   65,   65, 1,
        256, 4096, 4096, 1,
        257, 4096, 4096, 1,
        1025, 1025, 1025, 1,
        2048, 2048, 4096, 1,
        4096, 3528, 4096, 1,
        1024, 1025,    1, 1,
        1000, 1000, 1000, 1,
        1000,   10,   20, 1,
        1710, 1290, 1000, 1,
        1290, 1710,  200, 1,
        1290, 1710, 2000, 1,
        //1290, 1290, 2000, 1,
        //1000,  210,  200, 1
};


TEST_F(M4riBenchmark, Prototype)
{
    //  Test OpenCL prototype
    init(false);
    testOperator3( proto_bool_mul_m4rm, M4riBenchmark::ref_mul );
}


TEST_F(M4riBenchmark, PrototypeCubic)
{
    //  Test OpenCL prototype
    init(false);
    testOperator3( proto_bool_mul_cubic, M4riBenchmark::ref_mul );
}


TEST_F(M4riBenchmark, Multiply_full)
{
    init(false);
    benchmarkMultiply("m4ri full sequential",false,8*1024);
}

TEST_F(M4riBenchmark, Multiply_full_parallel)
{
    init(true);
    benchmarkMultiply("m4ri full parallel",false,8*1024);
}

TEST_F(M4riBenchmark, Multiply_uptri)
{
    init(false);
    benchmarkMultiply("m4ri uptri sequential",true,8*1024);
}

TEST_F(M4riBenchmark, Multiply_uptri_parallel)
{
    init(true);
    benchmarkMultiply("m4ri uptri parallel",true,8*1024);
}

TEST_F(M4riBenchmark, mzd_bool_mul_uptri)
{
    init(false);
    for(int set=0; set < SET_COUNT; ++set)
    {
        int m = this->m = this->l = this->n = TEST_SETS[set][1];
        this->sparsity = (double)TEST_SETS[set][3]/100.0;

        std::cout << "Set "<<set<< " = "<< m <<std::endl;

        allocUptriMatrixes3(m);

        mzd_bool_mul_m4rm(Cref, (mzd_t*)A, (mzd_t*)B, 0,0,0);	// reference implementation
        mzd_bool_mul_uptri((mzd_t*)C, (mzd_t*)A, (mzd_t*)B, 0,0,0);	// my implementation

        if (!mzd_equal((mzd_t*)C,Cref)) {
            std::cout << "failure" << std::endl;
            print((mzd_t*) A);
            print((mzd_t*) B);
            print(Cref);
            print((mzd_t *) C);
        }
        // results must be equal
        ASSERT_TRUE(mzd_equal((mzd_t*)C,Cref));

        freeMatrixes();
    }
}

TEST_F(M4riBenchmark, mzd_bool_add)
{
    init(false);
    testOperator2( mzd_bool_add, M4riBenchmark::ref_add );
}

TEST_F(M4riBenchmark, mzd_bool_and)
{
    init(false);
    testOperator2( mzd_bool_and, M4riBenchmark::ref_and );
}

TEST_F(M4riBenchmark, mzd_bool_and_uptri)
{
    init(false);
    for(int set=0; set < SET_COUNT; ++set)
    {
        int m = this->m = this->l = this->n = TEST_SETS[set][1];
        this->sparsity = (double)TEST_SETS[set][3]/100.0;

        allocUptriMatrixes3(m);

        mzd_bool_and(Cref, (mzd_t*)A, (mzd_t*)B, 0);	// reference implementation
        mzd_bool_and_uptri((mzd_t*)C, (mzd_t*)A, (mzd_t*)B, 2);	// my implementation

        if (!mzd_equal((mzd_t*)C,Cref)) {
            std::cout << "failure" << std::endl;
            print((mzd_t*) A);
            print((mzd_t*) B);
            print(Cref);
            print((mzd_t *) C);
        }
        // results must be equal
        ASSERT_TRUE(mzd_equal((mzd_t*)C, Cref));

        freeMatrixes();
    }
}

TEST_F(M4riBenchmark, mzd_bool_mul_m4rm)
{
    init(false);
    testOperator4( mzd_bool_mul_m4rm, MatrixBenchmark::ref_mul );
}

TEST_F(M4riBenchmark, mzd_bool_mul_navie)
{
    init(false);
	testOperator3( mzd_bool_mul_naive, MatrixBenchmark::ref_mul );
}

TEST_F(M4riBenchmark, mzd_bool_addmul_m4rm)
{
    init(false);
	testOperator4( mzd_bool_addmul_m4rm, M4riBenchmark::ref_addmul );
}

TEST_F(M4riBenchmark, mzd_bool_addmul_naive)
{
    init(false);
	testOperator3( mzd_bool_addmul_naive, M4riBenchmark::ref_addmul );
}

#endif // M4RI_BENCHMARK_H

