#ifndef BITSERIAL_BENCHMARK_H
#define BITSERIAL_BENCHMARK_H

#include <matrix_benchmark.h>

#include <gemmbitserial.hpp>
#include <arch-generic.hpp>

/**
 * Benchmark for gemmbitserial
 *
 */
class BitserialBenchmark : public MatrixBenchmark
{
private:
    gemmbitserial::GEMMContext ctx;
public:
    BitserialBenchmark() : MatrixBenchmark(BIT) { transposeB=true; }

    void multiply() override
    {
        gemmBitSerial(ctx);
    }

    void allocMatrixes() override
    {
        ctx = gemmbitserial::allocGEMMContext(
                n, n/*depth?*/, n,
                1, 1,
                false, false);

        //  B is consired to be transposed
        for(int i=0; i < n; ++i)
            for(int j=0; j < n; ++j)
            {
                if (rbool()) ctx.lhs.set(0,i,j);
                if (rbool()) ctx.rhs.set(0,j,i);
            }
    }

    void freeMatrixes() override
    {
        deallocGEMMContext(ctx);
    }
};


TEST_F(BitserialBenchmark, Multiply)
{
    benchmarkMultiply("bitserial",8192);
}

#endif // BITSERIAL_BENCHMARK_H
