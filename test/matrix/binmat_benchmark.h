#ifndef BINMAT_BENCHMARK_H
#define BINMAT_BENCHMARK_H

#include <matrix_benchmark.h>

extern "C" {
#include <libbinmat.h>
}

/**
 * Benchmark for binmat
 *
 */
class BinmatBenchmark : public MatrixBenchmark
{
public:
    BinmatBenchmark() : MatrixBenchmark(BIT) { transposeB=true; }

    void multiply() override
    {
        binmat_multiply((binmat_data_t*)C, (binmat_data_t*)A, (binmat_data_t*)B, n);
    }

    void allocMatrixes() override
    {
        A = binmat_alloc((binmat_index_t )n);
        B = binmat_alloc((binmat_index_t )n);
        C = binmat_alloc((binmat_index_t )n);

        //  B is consired to be transposed
        for(binmat_index_t i=0; i < n; ++i)
            for(binmat_index_t j=0; j < n; ++j)
            {
                binmat_setbit((binmat_data_t*)A, (binmat_index_t )n, i,j, rbool());
                binmat_setbit((binmat_data_t*)B, (binmat_index_t )n, j,i, rbool());
            }
    }

    void freeMatrixes() override
    {
        binmat_free((binmat_data_t*)A);
        binmat_free((binmat_data_t*)B);
        binmat_free((binmat_data_t*)C);
    }
};


TEST_F(BinmatBenchmark, Multiply)
{
    benchmarkMultiply("binmat",4096);
}

#endif // BINMAT_BENCHMARK_H
