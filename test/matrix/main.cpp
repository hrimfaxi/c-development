
#include <gtest/gtest.h>
/*
#include <matrix_benchmark.h>
#include <ublas_benchmark.h>
*/
#include <m4ri_benchmark.h>
/*
#include <mkl_benchmark.h>
#include <binmat_benchmark.h>
#include <bitserial_benchmark.h>
#include <clblas_benchmark.h>
#include <clblast_benchmark.h>
*/
#include <clm4rm_benchmark.h>

// ...

/*  Benchmark for various Matrix Multiplication Libraries
 *
 *  (1)     Boost uBLAS
 *          Data Type               16bit integer, 32bit float
 *          Algorithm               ?
 *          Asymptotic Complexity   ?
 *          Vectorization Support   ?
 *
 *  (2)     M4RI (außer Konkurrenz, da XOR-Arithmetik !!)
 *          Data Type               1bit
 *          Algorithm               4-Russians
 *          Asymptotic Complexity   n³ / log^? n
 *
 *  (3)     Intel MKL
 *          Data Type               32bit float, 8/32bit integer
 *          Algorithm               Strassen or better
 *          Asymptotic Complexity   n^2.x
 *          Vectorization Support   yes
 *
 *  (4)     binmat
 *          Data Type               1bit
 *          Algorithm               naive
 *          Asymptotic Complexity   n³   (ns + n² ?)
 *          Vectorization Support   no
 *
 *          gemmlowp
 *          Data Type               8bit integer
 *          Algorithm               naive
 *          Asymptotic Complexity   n³
 *          Vectorization Support   ?
 *
 *          gemmbitserial
 *          Data Type               1bit
 *          Algorithm               naive
 *          Asymptotic Complexity   n³
 *          Vectorization Support   ?
 *
*
 *          OpenBLAS
 *          MTL4
 *          Armadillo
 *          Eigen
 *
 *          clBLAS
 *          ClBlast
 *          cuBLAS (?)
 *
 */


int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    int result = RUN_ALL_TESTS();

    return result;
}
