#ifndef MATRIX_BENCHMARK_H
#define MATRIX_BENCHMARK_H

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>

#include <random>
#include <chrono>
#include <fstream>
#include <ostream>

#ifndef _WIN32
extern "C" {
#endif
#include <m4ri/m4ri.h>
#include <m4ri/russian_bool.h>
#include <m4ri/mzd_bool.h>
#include <clm4rm/ocl_prototype.h>
#ifndef _WIN32
}
#endif

class MatrixBenchmark : public testing::Test
{
public:
    enum ElementType {
        BIT,
        INT8, INT16, INT32, INT64,
        FLOAT16, FLOAT32, FLOAT64
    } etype;

    int n,l,m;
    void *A,*B,*C;
    //  C := A*B

    bool transposeB;
    double sparsity;

protected:
    //  Matrix Sizes
    static const int N_MIN = 64;         // 2**6
    static const int N_MAX = 8*1024;    // 2**20
    //static const int N_MAX = 1048576;    // 2**20

    //  random generator
    std::random_device rdevice;
    std::mt19937 rgen;
    std::uniform_real_distribution<double> rdis;

public:
    MatrixBenchmark(ElementType type)
        :   n(0),m(0),
        A(nullptr),B(nullptr),C(nullptr),
        transposeB(false), sparsity(0.2),
        rdevice(),
        rgen(rdevice()),
        rdis(0.0,1.0),
        etype(type)
    {
        rgen.seed(0);
    }


    void benchmarkMultiply(std::string filename, bool uptri, int nmax=N_MAX)
    {
        typedef std::chrono::high_resolution_clock Clock;

        std::fstream out;
        out.open(filename+".csv",std::fstream::out);

        std::cout << filename << std::endl;
        out << filename << std::endl
            << "n; " << "loop; " << "µs;" << std::endl;

        for(this->n=N_MIN; n <= nmax; n = 2*n)
        {
            m = l = n;
            int loop = nmax/n;
            allocMatrixes(uptri);

            std::cout << n << " x " << loop << std::endl;
            auto t1 = Clock::now();

            for(int i=0; i < loop; ++i)
                multiply(uptri);

            finishMultiply();

            auto t2 = Clock::now();
            long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

            out << n << ";" << loop << ";" << microsecs << ";" << std::endl;
            std::cout << microsecs << " µs"
                << " / " << (((double)microsecs)/1000/loop) << " ms"
                << std::endl;

            assertMultiply();

            freeMatrixes();
        }

        out.close();
    }


    static mzd_t* ref_mul(mzd_t* C, const mzd_t* A, const mzd_t* B, int)
    {	//	refereence implementation for Multiplication
        EXPECT_EQ(A->ncols,B->nrows);
        EXPECT_EQ(A->nrows,C->nrows);
        EXPECT_EQ(B->ncols,C->ncols);
/*        bool a,b,c;
		for(int i=0; i < A->nrows; ++i)
			for(int j=0; j < B->ncols; ++j)
			{
				c=false;
				for(int h=0; h < A->ncols; ++h) {
					a = (bool)mzd_read_bit(A,i,h);
					b = (bool)mzd_read_bit(B,h,j);
					if (a & b) { c=true; break; }
				}
				mzd_write_bit(C, i,j, c);
			}*/
        mzd_bool_mul_m4rm(C, A, B, 0, 0, 1);
        return C;
    }

    long bytesRequired(int n) const
    {
        long nn = (long)n*(long)n;
        switch(etype)
        {
            case BIT:       return nn/8;
            case INT8:      return nn;
            case INT16:     return 2*nn;
            case INT32:
            case FLOAT32:   return 4*nn;
            case INT64:
            case FLOAT64:   return 8*nn;
        }
    }

    virtual void allocMatrixes(bool uptri) =0;
    virtual void multiply(bool uptri) =0;
    virtual void finishMultiply() {}
    virtual void freeMatrixes() =0;

    virtual void assertMultiply() { }

    void print(mzd_t* M)
    {
        for(int row=0; row < M->nrows; ++row) {
            for (int col = 0; col < M->ncols; col += 64) {
                word w = mzd_read_bits(M, row, col, 64);
                std::cout << std::hex << std::setw(16) << w << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

protected:
    bool rbool()  {
        return rbool(sparsity);
    }
    bool rbool(double sp) {
        return rdis(rgen) <= sp;
    }
};

#endif // MATRIX_BENCHMARK_H
