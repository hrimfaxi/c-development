//
// Created by nightrider on 21.09.18.
//

#ifndef MATRIX_BENCHMARKS_CLM4RM_BENCHMARK_H
#define MATRIX_BENCHMARKS_CLM4RM_BENCHMARK_H

#include <matrix_benchmark.h>
#include <m4ri_benchmark.h>
#include <clm4rm.h>

#if defined(__cplusplus) && !defined(_MSC_VER)
extern "C" {
#endif
#include <m4ri/mzd_bool.h>
#include <m4ri/russian_bool.h>
//#include <m4ri/debug_dump.h>
#if defined(__cplusplus) && !defined(_MSC_VER)
}
#endif

/**
 * Benchmark for M4RM with OpenCL
 *
 *
 */
class clM4RMBenchmark : public M4riBenchmark
{
protected:
    cl_context ctx;
    cl_int err;
    cl_platform_id platforms[4];
    cl_platform_id platform;
    cl_uint num_platforms;
    cl_device_id device = 0;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
    cl_command_queue queue = 0;
    cl_event event = NULL;
    bool use_m4rm=true;
	int rowpadding = 0;

    clmatrix_t *Ag, *Bg, *Cg;

    static constexpr const char* KERNEL_DIR = "src/clm4rm";
public:

    clM4RMBenchmark() : M4riBenchmark()
    {
        transposeB=true;
        int ret = 0;

        /* Setup OpenCL environment. */
        err = clGetPlatformIDs( 4, platforms, &num_platforms );
        for(int plf=0; plf < num_platforms; ++plf)
        {
            char name[1024];
            char version[1024];
            char profile[1024];
            char extensions[1024];
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_NAME, 1024,name, NULL);
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_VERSION, 1024,version, NULL);
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_PROFILE, 1024,profile, NULL);
            err = clGetPlatformInfo(platforms[plf], CL_PLATFORM_EXTENSIONS, 1024,extensions, NULL);
            std::cout   << "Platform "<<plf<<std::endl
						<< platforms[plf] << " = " << name << " " << version << ", "
                        << profile << ","
                        << extensions << std::endl << std::endl;
        }

        //  choose a GPU platform
		int i;
		for (i = 0; i < num_platforms; ++i)
		{
			err = clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_GPU, 1, &device, NULL);
			if (err == CL_SUCCESS) break;
		}
		//	fall-back to CPU platform
		if (err != CL_SUCCESS) {
			for (i = 0; i < num_platforms; ++i)
			{
				err = clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_CPU, 1, &device, NULL);
				if (err == CL_SUCCESS) break;
			}
		}
		EXPECT_EQ(err, CL_SUCCESS);
		std::cout << "Choosing Platform " << i << std::endl;
		platform = platforms[i];

        props[1] = (cl_context_properties)platform;
        ctx = clCreateContext( props, 1, &device, NULL, NULL, &err );
        queue = clCreateCommandQueue( ctx, device, CL_QUEUE_PROFILING_ENABLE, &err );

        //  Load Kernels
        err = clm4rm_setup(KERNEL_DIR, ctx, device);
        EXPECT_EQ(err,CL_SUCCESS);
    }

    ~clM4RMBenchmark()
    {
        /* Release OpenCL working objects. */
        clReleaseCommandQueue( queue );
        clReleaseContext( ctx );
    }


    void allocMatrixes(bool uptri) override
    {
        M4riBenchmark::allocMatrixes(uptri);
		allocGMatrixes();
    }

	void allocGMatrixes() {
		ASSERT_NE(A, nullptr);
		ASSERT_NE(B, nullptr);
		ASSERT_NE(C, nullptr);
		Ag = clm4rm_copy((mzd_t*)A, rowpadding, true, ctx);
		Bg = clm4rm_copy((mzd_t*)B, rowpadding, true, ctx);
		Cg = clm4rm_create(((mzd_t*)C)->nrows, ((mzd_t*)C)->ncols, rowpadding, false, ctx);

		ASSERT_EQ(Ag->nrows,Cg->nrows);
		ASSERT_EQ(Ag->padded_rows,Cg->padded_rows);

		ASSERT_EQ(Ag->ncols,Bg->nrows);
		ASSERT_EQ(Ag->width*32,Bg->padded_rows);

		ASSERT_EQ(Bg->ncols,Cg->ncols);
		ASSERT_EQ(Bg->width,Cg->width);
	}

    void freeMatrixes() override
    {
        M4riBenchmark::freeMatrixes();
        clm4rm_free(Ag);
        clm4rm_free(Bg);
        clm4rm_free(Cg);
		Ag = Bg = Cg = nullptr;
    }


    void multiply(bool uptri) override
    {
        size2_t max_tile = {2,2};
		if (use_m4rm) {
			ASSERT_FALSE(uptri);
			event = clm4rm_mul(Cg, Ag, Bg, queue, false);
		}
		else if (uptri)
			event = clutri_mul(Cg,Ag,Bg,queue,max_tile,false);
		else
            event = clcubic_mul(Cg,Ag,Bg,queue,max_tile,false);
    }

    virtual void finishMultiply() {
        clWaitForEvents(1,&event);
        clm4rm_read((mzd_t*)C,Cg,queue);

        M4riBenchmark::finishMultiply();
    }


    void testReadWrite(int rows=128, int cols=128) {
        //  Init - Write - Read
        mzd_t* M1 = rand_matrix(rows,cols,0.5);
        mzd_t* M2 = mzd_init(rows,cols);

        clmatrix_t* Mg = clm4rm_create(rows,cols, rowpadding, false,ctx);
        ASSERT_NE(Mg,nullptr);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        clm4rm_write(Mg, M1, queue);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        clm4rm_read(M2,Mg, queue);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        ASSERT_TRUE(mzd_equal(M1,M2));

        clm4rm_free(Mg);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);


        //  Copy - Read
        Mg = clm4rm_copy(M1,rowpadding,false,ctx);
        ASSERT_NE(Mg,nullptr);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        clm4rm_read(M2,Mg, queue);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        ASSERT_TRUE(mzd_equal(M1,M2));

        clm4rm_free(Mg);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_free(M1);
        mzd_free(M2);
    }

    void testStack(int rows1=128, int rows2=133, int cols=128) {
        mzd_t* A = rand_matrix(rows1,cols,0.5);
        mzd_t* B = rand_matrix(rows1,cols,0.5);

        Ag = clm4rm_copy(A, rowpadding, true,ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);
        Bg = clm4rm_copy(B, rowpadding, true,ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* C1 = mzd_stack(nullptr,A,B);
        ASSERT_EQ(C1->nrows, A->nrows+B->nrows);

        Cg = clm4rm_create(Ag->nrows+Bg->nrows, Ag->ncols, rowpadding, false, ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);
        ASSERT_EQ(Cg->nrows, Ag->nrows+Bg->nrows);

        clm4rm_stack(Cg,Ag,Bg,queue);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* C2 = clm4rm_read(nullptr,Cg,queue);

        ASSERT_TRUE(mzd_equal(C1,C2));

        clm4rm_free(Ag);
        clm4rm_free(Bg);
        clm4rm_free(Cg);

        mzd_free(A);
        mzd_free(B);
        mzd_free(C1);
        mzd_free(C2);
    }

    void testConcat(int rows=128, int cols1=133, int cols2=128) {
        mzd_t* A = rand_matrix(rows,cols1,0.5);
        mzd_t* B = rand_matrix(rows,cols2,0.5);

        Ag = clm4rm_copy(A, rowpadding, true,ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);
        Bg = clm4rm_copy(B, rowpadding, true,ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* C1 = mzd_concat(nullptr,A,B);
        ASSERT_EQ(C1->ncols, A->ncols+B->ncols);

        Cg = clm4rm_create(Ag->nrows, Ag->ncols+Bg->ncols, rowpadding, false, ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);
        ASSERT_EQ(Cg->ncols, Ag->ncols+Bg->ncols);

        clm4rm_concat(Cg,Ag,Bg,queue);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* C2 = clm4rm_read(nullptr,Cg,queue);

        ASSERT_TRUE(mzd_equal(C1,C2));

        clm4rm_free(Ag);
        clm4rm_free(Bg);
        clm4rm_free(Cg);

        mzd_free(A);
        mzd_free(B);
        mzd_free(C1);
        mzd_free(C2);
    }

    void testAnd(int rows=128, int cols=133)
    {
        mzd_t* A = rand_matrix(rows,cols,0.5);
        mzd_t* B = rand_matrix(rows,cols,0.5);

        Ag = clm4rm_copy(A, rowpadding, true,ctx);
        Bg = clm4rm_copy(B, rowpadding, true,ctx);

        mzd_t* C1 = mzd_bool_and(nullptr,A,B,1);

        Cg = clm4rm_create(A->nrows,A->ncols, rowpadding, false,ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        clm4rm_and(Cg,Ag,Bg,queue,true);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* C2 = clm4rm_read(nullptr,Cg,queue);
        ASSERT_TRUE(mzd_equal(C1,C2));

        //  test AND in place
#if ! IMAGE2D
        clm4rm_and(Ag,Ag,Bg,queue,true);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* A2 = clm4rm_read(nullptr,Ag,queue);
        ASSERT_TRUE(mzd_equal(C1,A2));
        mzd_free(A2);
#endif

        clm4rm_free(Ag);
        clm4rm_free(Bg);
        clm4rm_free(Cg);

        mzd_free(A);
        mzd_free(B);
        mzd_free(C1);
        mzd_free(C2);
    }

    void testOr(int rows=128, int cols=133)
    {
        mzd_t* A = rand_matrix(rows,cols,0.5);
        mzd_t* B = rand_matrix(rows,cols,0.5);

        Ag = clm4rm_copy(A, rowpadding, true,ctx);
        Bg = clm4rm_copy(B, rowpadding, true,ctx);

        mzd_t* C1 = mzd_bool_add(nullptr,A,B,1);

        Cg = clm4rm_create(A->nrows,A->ncols, rowpadding, false,ctx);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        clm4rm_or(Cg,Ag,Bg,queue);
        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* C2 = clm4rm_read(nullptr,Cg,queue);

        ASSERT_TRUE(mzd_equal(C1,C2));

        clm4rm_free(Ag);
        clm4rm_free(Bg);
        clm4rm_free(Cg);

        mzd_free(A);
        mzd_free(B);
        mzd_free(C1);
        mzd_free(C2);
    }

    void testMul(int rows=128, int depth=128, int cols=128, double density=0.4)
    {
        mzd_t* A = rand_matrix(rows,depth, density);
        mzd_t* B = rand_matrix(depth,cols, density);

        mzd_t* C1 = mzd_bool_mul_m4rm(nullptr,A,B,0,0,1);
        mzd_t* C2 = mzd_bool_mul_naive(nullptr,A,B,1);

        ASSERT_TRUE(mzd_equal(C1,C2));

        Ag = clm4rm_copy(A, rowpadding, true,ctx);
        Bg = clm4rm_copy(B, rowpadding, true,ctx);

        Cg = clm4rm_create(rows,cols, rowpadding, false,ctx);

        clm4rm_mul(Cg,Ag,Bg,queue,true);

        ASSERT_EQ(clm4rm_error,CL_SUCCESS);

        mzd_t* C3 = clm4rm_read(nullptr,Cg,queue);

        if (!mzd_equal(C1,C3)) {
            std::cout<<"--A--"<<std::endl;
            print(A);
            std::cout<<"--B--"<<std::endl;
            print(B);
            std::cout<<"--C reference--"<<std::endl;
            print(C1);
            std::cout<<"--C cl--"<<std::endl;
            print(C3);
        }

        ASSERT_TRUE(mzd_equal(C1,C3));

        clm4rm_free(Ag); clm4rm_free(Bg); clm4rm_free(Cg);

        mzd_free(A); mzd_free(B); mzd_free(C1); mzd_free(C2); mzd_free(C3);
    }

    void testMulAll()
    {
        ASSERT_GT(max_group_size,0);
        for(int set=0; set < SET_COUNT; ++set)
        {
            useTestSet(set);

            allocMatrixes3(m,l,n);
			allocGMatrixes();

            MatrixBenchmark::ref_mul(Cref, (mzd_t*)A, (mzd_t*)B,0);	// reference implementation

            size2_t max_tile = {2,2};
            if (use_m4rm)
                clm4rm_mul(Cg,Ag,Bg,queue,true);
            else
                clcubic_mul(Cg,Ag,Bg,queue,max_tile,true);

            ASSERT_EQ(clm4rm_error,CL_SUCCESS);

            mzd_t* C3 = clm4rm_read(nullptr,Cg,queue);
/*
        if (!mzd_equal(Cref,C3)) {
            std::cout<<"--A--"<<std::endl;
            print((mzd_t*)A);
            std::cout<<"--B--"<<std::endl;
            print((mzd_t*)B);
            std::cout<<"--C reference--"<<std::endl;
            print(Cref);
            std::cout<<"--C cl--"<<std::endl;
            print(C3);
        }
*/
            ASSERT_TRUE(mzd_equal(Cref,C3));
			
            freeMatrixes();

			ASSERT_EQ(Ag, nullptr);
			ASSERT_EQ(Bg, nullptr);
			ASSERT_EQ(Cg, nullptr);
        }
    }
};

TEST_F(clM4RMBenchmark, DeviceInfo)
{
    //  Which endianness ?
    cl_uint units;
    cl_uint clock;
    cl_bool endianness;
    cl_ulong globalMem, globalCache, localMem;
    char name[1024];
    size_t workGroupSize;
    cl_uint dimensions;
    size_t itemSizes[4];

    clGetDeviceInfo(device,CL_DEVICE_MAX_COMPUTE_UNITS, 4,&units, NULL);
    clGetDeviceInfo(device,CL_DEVICE_MAX_CLOCK_FREQUENCY, 4,&clock, NULL);
    clGetDeviceInfo(device,CL_DEVICE_ENDIAN_LITTLE, 1,&endianness, NULL);

    clGetDeviceInfo(device,CL_DEVICE_NAME, 1024,name,NULL);
    clGetDeviceInfo(device,CL_DEVICE_GLOBAL_MEM_SIZE, 8,&globalMem, NULL);
    clGetDeviceInfo(device,CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, 8,&globalCache, NULL);
    clGetDeviceInfo(device,CL_DEVICE_LOCAL_MEM_SIZE, 8,&localMem,NULL);

    clGetDeviceInfo(device,CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t),&workGroupSize, NULL);
    clGetDeviceInfo(device,CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &dimensions, NULL);
    clGetDeviceInfo(device,CL_DEVICE_MAX_WORK_ITEM_SIZES, 4*sizeof(size_t), &itemSizes, NULL);

    //	TODO size of local memory
	//	TODO size of texture memory

	std::cout << name <<std::endl;
    std::cout << "Max. Compute Units="<<units<<std::endl;
    std::cout << "Clock Frequency="<<clock<<std::endl;
    std::cout << "Global Memory="<<globalMem<<std::endl;
    std::cout << "Global Cache="<<globalCache<<std::endl;
    std::cout << "Local Memory="<<localMem<<std::endl;
    std::cout << "Device is "<<(endianness==CL_TRUE?"little":"big") << "-endian" << std::endl;
    std::cout << "Max. Work Group Size="<<workGroupSize<<std::endl;
    std::cout << "Max. Item Size="<<itemSizes[0]<<","<<itemSizes[1]<<","<<itemSizes[2]<<std::endl;
}

TEST_F(clM4RMBenchmark, ReadWrite)
{
    testReadWrite();
    testStack();
    testConcat(128,128,128);
    testConcat(128,371,419);
    testConcat(128,3697,14358);
}

TEST_F(clM4RMBenchmark, Bitwise)
{
    testAnd(963,4326);
    testAnd(358,358);
    testOr(3697,14358);
}

TEST_F(clM4RMBenchmark, FindDiagonalElement)
{
	mzd_t* M = mzd_init(137, 245);
	int el = 61;
	mzd_write_bit(M, el,el, 1);
	
	mzd_write_bit(M, el+1, el, 1);
	mzd_write_bit(M, el-1, el, 1);
	mzd_write_bit(M, el, el+1, 1);
	mzd_write_bit(M, el, el-1, 1);
	mzd_write_bit(M, el+1, el-1, 1);
	mzd_write_bit(M, el-1, el+1, 1);

	clmatrix_t* G = clm4rm_copy(M, 32, 1, ctx);
	int result = clm4rm_query_diagonal(G, ctx, queue);
	ASSERT_EQ(result, el);

	clm4rm_read(M, G, queue);
	ASSERT_EQ(mzd_read_bit(M, el, el), 1);

	mzd_write_bit(M, el,el, 0);
	copy_matrix_data(G->local_data,M,32);
	clm4rm_write(G,M,queue);

	result = clm4rm_query_diagonal(G, ctx, queue);
	ASSERT_EQ(result, -1);

	clm4rm_free(G);
	mzd_free(M);
}

TEST_F(clM4RMBenchmark, Benchmark_full)
{
    std::cout << "-- max. group size = "<<max_group_size<<std::endl;
    benchmarkMultiply("OpenCL m4rm full",false,8*1024);
}

TEST_F(clM4RMBenchmark, Benchmark_cubic_full)
{
    use_m4rm=false;
    rowpadding=32;
    benchmarkMultiply("OpenCL cubic full",false,8*1024);
}

TEST_F(clM4RMBenchmark, Benchmark_cubic_utri)
{
	use_m4rm = false;
	rowpadding = 32;
	benchmarkMultiply("OpenCL cubic upper-triangle", true, 8 * 1024);
}

TEST_F(clM4RMBenchmark, Multiply_full)
{
    use_m4rm=true;
    testMulAll();
}

TEST_F(clM4RMBenchmark, Multiply_cubic_full)
{
    use_m4rm=false;
    rowpadding=32;
    testMulAll();
}

TEST_F(clM4RMBenchmark, Multiply_cubic_utri)
{
	use_m4rm = false;
	rowpadding = 32;
	init(false);
	for (int set = 0; set < SET_COUNT; ++set)
	{
		int m = this->m = this->l = this->n = TEST_SETS[set][1];
		this->sparsity = (double)TEST_SETS[set][3] / 100.0;

		std::cout << "Set " << set << " = " << m << std::endl;

		allocUptriMatrixes3(m);
		allocGMatrixes();

		size2_t max_tile = {2,2};
		mzd_bool_mul_uptri((mzd_t*)Cref, (mzd_t*)A, (mzd_t*)B, 0, 0, 0);	// reference implementation
		clutri_mul (Cg, Ag, Bg, queue,max_tile,true);	// my implementation

		ASSERT_EQ(clm4rm_error, CL_SUCCESS);

		mzd_t* C3 = clm4rm_read(nullptr, Cg, queue);
		/*
		if (!mzd_equal(Cref,C3)) {
		std::cout<<"--A--"<<std::endl;
		print((mzd_t*)A);
		std::cout<<"--B--"<<std::endl;
		print((mzd_t*)B);
		std::cout<<"--C reference--"<<std::endl;
		print(Cref);
		std::cout<<"--C cl--"<<std::endl;
		print(C3);
		}
		*/
		ASSERT_TRUE(mzd_equal(Cref, C3));

		freeMatrixes();

		ASSERT_EQ(Ag, nullptr);
		ASSERT_EQ(Bg, nullptr);
		ASSERT_EQ(Cg, nullptr);
	}
}


#endif //MATRIX_BENCHMARKS_CLM4RM_BENCHMARK_H
