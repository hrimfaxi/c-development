#ifndef MKL_BENCHMARK_H
#define MKL_BENCHMARK_H

#include <matrix_benchmark.h>

#define MKL_ILP64
#include <mkl.h>

/**
 * Benchmark for Intel MKL
 *
 */
class MklBenchmark : public MatrixBenchmark
{
public:

    MklBenchmark() : MatrixBenchmark(FLOAT32) { transposeB=true; }

    void multiply() override
    {
        switch(etype) {
            case INT8: {
/*
 *  input: 8bit integer, output 32 bit integer
 *
 *  lda,ldb,ldc "leading dimension" = n, right ?
 */
            int c_offset = 0;
            cblas_gemm_s8u8s32(CblasRowMajor,
                    CblasNoTrans, CblasTrans,
                    CblasFixOffset, // TODO what's this ??
                    n,n,n,
                    1.0, A, n, 0,
                    B, n, 0,
                    0.0, (int32_t*)C, n, &c_offset);
            return; }

            case INT16:
                assert(false);
/*
 *  input: 16bit integer, output 32bit integer
 *
            void cblas_gemm_s16s16s32(const CBLAS_LAYOUT Layout, const CBLAS_TRANSPOSE TransA,
                                      const CBLAS_TRANSPOSE TransB, const CBLAS_OFFSET OffsetC,
                                      const MKL_INT M, const MKL_INT N, const MKL_INT K,
                                      const float alpha, const MKL_INT16 *A, const MKL_INT lda, const MKL_INT16 ao,
                                      const MKL_INT16 *B, const MKL_INT ldb, const MKL_INT16 bo, const float beta,
                                      MKL_INT32 *C, const MKL_INT ldc, const MKL_INT32 *cb);
*/
            case FLOAT32:
                cblas_sgemm(CblasRowMajor,CblasNoTrans,CblasTrans,
                            n,n,n, 1.0, (float*)A,n, (float*)B,n, 0.0, (float*)C,n);
                return;
        }
    }


    void allocMatrixes() override {
        //  TODO what is the optimal ALIGNMENT ?
        switch(etype) {
            case INT8:      allocMatrixes<int8_t,int32_t>(64,64); return;
            case INT16:     allocMatrixes<int16_t,int32_t>(64,64); return;
            case FLOAT32:   allocMatrixes<float,float>(64,64); return;
        }
        assert(false);
    }

    template<typename AT, typename CT>
    void allocMatrixes(int align, int calign) {
        A = mkl_malloc(n*n*sizeof(AT), align);
        B = mkl_malloc(n*n*sizeof(AT), align);
        C = mkl_malloc(n*n*sizeof(CT), calign);

        //  B is transposed ("column-major")
        for(int i=0; i < n; ++i)
            for(int j=0; i < n; ++i)
            {
                ((AT*)A) [i*n+j] = rbool();
                ((AT*)B) [i+j*n] = rbool();
                ((CT*)C) [i*n+j] = 0;
            }
    }

    void freeMatrixes() override
    {
        mkl_free(A);
        mkl_free(B);
        mkl_free(C);
    }
};

TEST_F(MklBenchmark, Multiplyf32)
{
    etype = FLOAT32;
    benchmarkMultiply("mkl-f32",4*1024);
}

TEST_F(MklBenchmark, Multiplyi8)
{
    etype = INT8;
    benchmarkMultiply("mkl-i8",4*1024);
}

#endif // MKL_BENCHMARK_H
