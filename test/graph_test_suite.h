#ifndef GRAPH_TEST_SUITE_H
#define GRAPH_TEST_SUITE_H

#include <gtest/gtest.h>
#include <graph_m4ri.h>

class GraphTestSuite : public testing::Test
{
protected:
    frechet::FreeSpace::ptr fs;
    frechet::reach::GraphModel::ptr model;
    std::vector<int> dpoints;
    std::list<frechet::reach::Graph::ptr> rg;

public:
    void testCopy();
    void testMatrixBasics();
    void testEdgeQueries();
    void testBitwiseAnd();
    void testSpirolator(int i, int j);

    void createReachabilityGraphs();
    void testTransitiveClosure();
    void benchMarkDensity();

public:
    void createRandomModel(int n);
    void readFile(QString filename, double epsilon);
    void printDiagnostics(frechet::reach::Graph::ptr);
    static void printDiagnostics(frechet::reach::Graph::ptr, std::vector<int>* dpoints);

private:
    void init(const frechet::Curve& P, const frechet::Curve& Q, double epsilon);
    frechet::reach::Graph::ptr createEmptyGraph(int di, int dj);
    frechet::reach::Graph::ptr createEmptyGraph(frechet::reach::IndexRange range);
    frechet::reach::Graph::ptr createRandomGraph(int di, int dj, double density);
    void createReachabilityGraph(int di, int dj);
    void testMatrixBits(mzd_t* M, int row, int i, int j, bool print);
    void assertNeighbors(frechet::reach::Graph::ptr g, frechet::reach::Graph::ptr h);
    void testTransitiveClosure(frechet::reach::Graph::ptr A, frechet::reach::Graph::ptr B);
    void testEdgeQueries(frechet::reach::Graph::ptr g, frechet::reach::IndexRange irange1, frechet::reach::IndexRange irange2);
};

#endif // GRAPH_TEST_SUITE_H
