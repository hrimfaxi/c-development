
#include <QPrinter>
#include <QSvgGenerator>
#include <QGraphicsTextItem>


#include <poly_utils.h>
#include <inputreader.h>
#include <ostream>
#include <shortest_paths.h>
#include <poly_path.h>
#include <random>

#include <triangulation_test_suite.h>
#include <graph_test_suite.h>

void TriangulationTestSuite::SetUp()
{
    algo.reset(new AlgorithmSingleCore());
    guibas = nullptr;
}

int irand(int n)
{
    double x = (double)rand()/RAND_MAX;
    return (int)n*x;
}

double drand(int i)
{
    double x = (double)rand()/RAND_MAX;
    return i+0.1+0.8*x;
}

void TriangulationTestSuite::testGuibas(QString filename, bool primary, bool swap, bool extra_points)
{
    algo.reset(new AlgorithmSingleCore());

    readFile(filename);

    algo->decompose(false);
    if (swap) algo->swap();
    algo->triangulate();

    Triangulation& tri = primary ? algo->PTriangulation() : algo->QTriangulation();
    const Curve& c = primary ? algo->Pcurve() : algo->Qcurve();
    int size_before = tri.size();
    if (extra_points)
    {
        //  add random extra points on the polygon edge [0..n]
        int n = c.size();
        double x;
        for(int i=0; i < 10; ++i) {
            x = drand(irand(n-1));
            tri.insertVertexOnEdge(x);
        }
        tri.insertVertexOnEdge(drand(n-2));
        tri.insertVertexOnEdge(drand(n-2));

        ASSERT_TRUE(tri.isComplete());
        ASSERT_EQ(tri.size(),size_before+12);
    }

    guibas = new PolygonShortestPaths(tri);

    for (int i=0; i < tri.size(); ++i) {
        if (tri[i]==0) continue;
        try {
            guibas->findShortestPaths(tri[i]);
        } catch(std::exception& ex) {
            saveTriangulation(QString("%1-Failed").arg(filename), c, tri);
        }
        if (i==0)
            saveTriangulation(
                    QString("%1-%2-Guibas").arg(filename).arg((primary!=swap) ? "P":"Q"),
                    c, tri);
    }

    if (extra_points) {
        tri.resize(size_before);

        tri.clearPredecessors();
//        saveTriangulation(
//                    QString("%1-Guibas-after").arg(filename),
//                    algo->curve(primary), tri);

        ASSERT_TRUE(tri.isComplete());
        ASSERT_EQ(tri.size(),size_before);
    }
}

void TriangulationTestSuite::initializeAlgorithm(QString filename)
{

    algo.reset(new AlgorithmSingleCore());

    readFile(filename);

    bool p_is_primary = algo->decompose(false);
    if (!p_is_primary) //  swap P and Q !!
        algo->swap();
    algo->triangulate();

    fs.reset(new FreeSpace(algo->Pcurve(), algo->Qcurve()));
}

void TriangulationTestSuite::testGuibasFS(QString filename, double epsilon, bool expected=true)
{
    PolygonShortestPathsFS::after_target_found = TriangulationTestSuite::after_target_found;
    PolygonShortestPathsFS::after_placement_added = TriangulationTestSuite::after_placement_added;

    initializeAlgorithm(filename);

    fs->calculateFreeSpace(epsilon);

    res = algo->decidePolygon(fs,frechet::poly::PolygonFSPath::ptr(),epsilon,true);

    if (res) {

        //GraphTestSuite::printDiagnostics(G.first, nullptr);
//        prettyPrint(G.first);
/*
        prettyPrint(algo->RG(0,1));
        prettyPrint(algo->RG(1,2));
        prettyPrint(algo->CRG(0,1,Triangulation::Edge()));
        prettyPrint(algo->MERGE(algo->RG(1,2), algo->CRG(0,1,Triangulation::Edge())));

        //prettyPrint(G.first);
        constructHomeomorphism(G);
*/
    }

    ASSERT_EQ(expected, res.get() != nullptr);
    //ASSERT_EQ(expected, res.i>=0);
    ASSERT_EQ(algo->status(), expected ?
                  Algorithm::Status::YES :
                  Algorithm::Status::NO);
}

void TriangulationTestSuite::plotRects(QString filename, const std::vector<QRectF>& rects)
{
    QGraphicsView* gview = new QGraphicsView;
    QGraphicsScene* scene = new QGraphicsScene;

    gview->setRenderHint(QPainter::Antialiasing, true);
    gview->setDragMode(QGraphicsView::ScrollHandDrag);
    gview->setOptimizationFlags(QGraphicsView::DontSavePainterState);
    gview->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
    gview->setScene(scene);
    gview->setInteractive(false);

    QPen pen(QColor(0,0,255,180), 0.1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    QBrush brush(QColor(0,0,255,60));

    scene->addEllipse(-2,-2,4,4, pen);

    for(const QRectF& r : rects) {
        scene->addRect(r, pen, brush);
        QGraphicsTextItem * text = scene->addText( QString("%1-%2\n%3-%4").arg(r.top()).arg(r.bottom()).arg(r.left()).arg(r.right()));
        text->setScale(0.02);
        text->setPos(r.topLeft());
    }

    //scene->setSceneRect(P.boundingRect());
    //gview->setSceneRect(P.boundingRect());
    gview->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    //gview->rotate(90);
    //gview->scale(0.1,0.1);

    //gview->show();
    //saveAsSvg(filename+".svg",gview);
    saveAsPdf(filename+".pdf",gview);

    delete scene;
    delete gview;
}

void TriangulationTestSuite::prettyPrintRow(Graph::ptr g, Orientation ori, int row)
{
//    int h1 = g->graphModel()->count1(HORIZONTAL);
    int h2 = g->graphModel()->count2(HORIZONTAL);
    int v2 = g->graphModel()->count2(VERTICAL);

//    for(int col=0; col < h1; ++col)
//        std::cout << prettyPrintChar(g,ori,row,HORIZONTAL,col);
    std::cout << ((row>0 && row%10==0) ? "+":"|");
//    for(int col=h1; col < h2; ++col)
//        std::cout << prettyPrintChar(g,ori,row,HORIZONTAL,col);
    std::cout << ((row>0 && row%10==0) ? "++":"||");
    for(int col=0; col < v2; ++col)
        std::cout << prettyPrintChar(g,ori,row,VERTICAL,col);
    std::cout << std::endl;
}

char TriangulationTestSuite::prettyPrintChar(Graph::ptr g, Orientation o1, int row, Orientation o2, int col)
{
    return g->contains_edge(o1,row,o2,col) ? '1':'.';
}

void TriangulationTestSuite::prettyPrintSep(Graph::ptr g, char sep, char sepd)
{
//    int h1 = g->graphModel()->count1(HORIZONTAL);
    int h2 = g->graphModel()->count2(HORIZONTAL);
    int v2 = g->graphModel()->count2(VERTICAL);

//    printn(std::cout,h1, sep,sepd) << "+";
//    printn(std::cout,h2-h1, sep,sepd) << "++";
    printn(std::cout,v2, sep,sepd) << std::endl;
}

std::ostream& TriangulationTestSuite::printn(std::ostream& out, int n, char c, char cd) {
    for(int i=0; i < n; ++i)
        if (i>0 && i%10==0)
            out << cd;
        else
            out << c;
    return out;
}

void TriangulationTestSuite::prettyPrint(Graph::ptr g)
{
 //   int h1 = g->graphModel()->count1(HORIZONTAL);
    int h2 = g->graphModel()->count2(HORIZONTAL);
    int v2 = g->graphModel()->count2(VERTICAL);

    g->printOrigin(std::cout);
    std::cout << std::endl;

//    for(int row=0; row < h1; ++row)
//        prettyPrintRow(g,HORIZONTAL,row);
    prettyPrintSep(g,'-','+');
//    for(int row=h1; row < h2; ++row)
//        prettyPrintRow(g,HORIZONTAL,row);
    prettyPrintSep(g,'=','+');
    for(int row=0; row < v2; ++row)
        prettyPrintRow(g,VERTICAL,row);
}
/*
void TriangulationTestSuite::constructHomeomorphism(Algorithm::Result res)
{
/*    Algorithm::GraphPtr G = res.G;
    int i = res.i;

    int n = fs->n-1;
    int di = algo->d(i) % n;
    int di1 = algo->d(i-1) % n;
    if (di < di1) di += n;
    Q_ASSERT(di1 < di);

    std::cout   << algo->graphModel->count2(HORIZONTAL)
                << " x "
                << algo->graphModel->count2(VERTICAL)
                << std::endl;

    Interval i1 (di1, di);
    IndexRange r1 = algo->graphModel->map(HORIZONTAL,i1,false);
    IndexRange r2 = algo->graphModel->map(HORIZONTAL,i1+(double)n,false);

    ASSERT_TRUE(G->contains_edge(r1,r2));

    //  (1) find the solution
    //  all values are indices into GraphModel
    int p1,p2;
    std::list<int> Pi, Qi;

    bool found=false;
    for(p1=r1.lower; p1 < r1.upper; ++p1)
        for(p2=r2.lower; p2 < r2.upper; ++p2)
            if (G->contains_edge(HORIZONTAL,p1,HORIZONTAL,p2)) {
                found=true;
                goto outerloop;
            }
outerloop:
    ASSERT_TRUE(found) << " edge not found";
* /
    int h1 = res.G->graphModel()->count1(HORIZONTAL);
    int p1 = algo->findSolutionStartPoint(res);
    int p2 = p1+h1;
    std::list<int> Pi, Qi;

    //  start point is mapped to Q(0),
    Pi.push_front(p1);
    Qi.push_back(0);

    constructPath(res.G, HORIZONTAL,p1, HORIZONTAL,p2, Pi,Qi);

    //  end point is mapped to Q(m-1)
    Pi.push_back(p2);
    Qi.push_back(algo->graphModel->count2(VERTICAL)-1);

    //  (3) map GraphModel indices to Intervals, then to Curve points
    std::vector<Interval> hmap,vmap;
    algo->graphModel->createReverseMap(HORIZONTAL,hmap);
    algo->graphModel->createReverseMap(VERTICAL,vmap);

    std::list<double> Pd,Qd;
    Curve Ph,Qh;
    for(int i : Pi) {
        double d = hmap[i].lower();
        Pd.push_back(d);
        Ph.push_back(Grid::mapToPoint(algo->P->curve, d));
    }
    for(int j : Qi) {
        double d = vmap[j].lower();
        Qd.push_front(d);
        Qh.push_back(Grid::mapToPoint(algo->Q->curve, d));
    }
}
*/
/*
void TriangulationTestSuite::constructPath(Graph::ptr G,
       Orientation o1, int from,
       Orientation o2, int to,
       std::list<int>& P,
       std::list<int>& Q)
{
    ASSERT_TRUE(G->contains_edge(o1,from,o2,to));

    Graph::ptr a = G->origin.a;
    Graph::ptr b = G->origin.b;
    int h1 = G->graphModel()->count1(HORIZONTAL);
    int h2 = G->graphModel()->count2(HORIZONTAL);
    int v2 = G->graphModel()->count2(VERTICAL);

    //  (2) follow the construction of G
    switch(G->origin.operation) {
        case Graph::Origin::COMBINE:
            //  follow...
            constructPath(a, o1,from, o2,to, P,Q);
            break;

        case Graph::Origin::MERGE:
            //  find intermediate edge

            bool acontains = a->contains_edge(o1, from, o2, to);
            bool bcontains = b->contains_edge(o1, from, o2, to);

            if (o1==HORIZONTAL && o2==HORIZONTAL) {
                //  this is not supposed to happen
                //  the solution must pass through a vertical edge
                ASSERT_FALSE(acontains);
                ASSERT_FALSE(bcontains);
            }
            else if (acontains || bcontains) {
                //  found it.
                return;
            }

            int k;
            for(k=0; k < v2; ++k)
            {
                acontains = a->contains_edge(o1, from, VERTICAL,k);
                bcontains = b->contains_edge(VERTICAL,k, o2,to);

                //  RGs live inf the left half of the Free-Space but (implicitly) cover the right half, too
                //  (note that this does not hold for merged graphs)
                if (o1==HORIZONTAL
                    && a->origin.operation==Graph::Origin::RG)
                    acontains |= a->contains_edge(o1,from-h1, VERTICAL,k);

                if (o2==HORIZONTAL
                    && b->origin.operation==Graph::Origin::RG)
                    bcontains |= b->contains_edge(VERTICAL,k, o2,to-h1);

                if (acontains && bcontains)
                    break;
            }

            ASSERT_LT(k,G->graphModel()->count2(VERTICAL)) << "no intermediate edge found";

            //  recurse
            constructPath(G->origin.a, o1,from, VERTICAL,k, P,Q);

            P.push_back(G->origin.b->hmask().lower);
            Q.push_back(k);

            constructPath(G->origin.b, VERTICAL,k, o2,to, P,Q);
            break;
    }
}
*/
void TriangulationTestSuite::testAlgorithm(QString filename, double eps_ok)
{
    PolygonShortestPathsFS::after_target_found = TriangulationTestSuite::after_target_found;
    PolygonShortestPathsFS::after_placement_added = TriangulationTestSuite::after_placement_added;

    double epsilons[] = {
            0.0, 1.0, 24, 25, 26, 36, 38, 40, 44, 45, 46, 224.0, 227.5, 256, 257, 258, 308.0, 441.0, 553.0, 644.98, 644.985, 1000, 1e6
    };
    int COUNT=23;

    initializeAlgorithm(filename);

    FreeSpace::ptr old_fs;
    AlgorithmSingleCore::GraphMap  CRG_map;
    //Algorithm::GraphVector RG_map;
    Algorithm::PlacementVector placements;
    AlgorithmSingleCore::GraphMap validPlacements;

    for(int i=0; i < COUNT; ++i)
    {
        double eps = epsilons[i];
        std::cout << "eps=" << eps << std::endl;

        /*
         * collect valid placements
         */
        pl_intervals.clear();
        pl_gnodes.clear();

        fs->calculateFreeSpace(eps);
        Graph::ptr res = algo->decidePolygon(fs,FSPath::ptr(),eps,true);

        bool expected = (eps >= eps_ok);
        //std::cout << "i="<<res.i<< std::endl;

        EXPECT_EQ(expected, res.get() != nullptr);
        //EXPECT_EQ(expected, res.i>=0);
        EXPECT_EQ(algo->status(), expected ?
                                  Algorithm::Status::YES :
                                  Algorithm::Status::NO);

        if (res) {
            //G.first->printOrigin(std::cout);
            //GraphTestSuite::printDiagnostics(G.first, nullptr);
            /*
             * Reconstruct homeomorphism
             * as two sequences of intervals
             */
            // (1) search path through reachability graphs
//            prettyPrint(G.first);
//            constructHomeomorphism(G);
        }
        //plotRects(QString("%1-pli-%2").arg(filename).arg(eps), pl_intervals);
        //plotRects(QString("%1-plg-%2").arg(filename).arg(eps), pl_gnodes);

        if (i >= 3) {
            //  compare Graphs from smaller epsilon
/*
            std::cout   << " a placements " << std::endl;
            validPlacements->print(std::cout, VERTICAL,VERTICAL);
            std::cout   << std::endl
                        << " b placements " << std::endl;
            algo->validPlacements->print(std::cout, VERTICAL,VERTICAL);
*/


            assertSubFreeSpace(old_fs,fs);
            assertPlacements(algo->_placements, *algo->graphModel);
            assertSubPlacements(placements,algo->_placements);

            assertSubGraphs("placements ", validPlacements, algo->_validPlacements);
//            assertSubGraphs("RG", RG_map, algo->_RG_map);
//            assertSubGraphs("CRG", CRG_map, algo->_CRG_map);
        }

        //  backup Graph data
        old_fs.reset(new FreeSpace(*fs));
//        CRG_map = algo->_CRG_map;
//        RG_map = algo->_RG_map;
        placements = algo->_placements;
        validPlacements = algo->_validPlacements;
    }
}

void TriangulationTestSuite::testApproximation(QString filename, double precision)
{
    /*  binäre Suche */
    double low = 0.0;
    double high = 1e6;

    Graph::ptr res;
    initializeAlgorithm(filename);

    std::cout << "binary search" << std::endl;
    while((high-low) > precision) {
        double mid = (low+high)/2;

        std::cout << "  .." << mid << std::endl;
        fs->calculateFreeSpace(mid);
        res = algo->decidePolygon(fs,FSPath::ptr(),mid,true);

        if (res)
            high=mid;
        else
            low=mid;
    }
    std::cout <<"["<< low << ".." << high <<"]" << std::endl;
}

void TriangulationTestSuite::testCurvePolyApproximation(QString filename, double precision)
{
    /*  Die Trouser-Kurve ist das Musterbeispiel dafür, dass sich
     *  d_F auf einer Kurve und auf einem Polygon unterscheiden.
     *  */
    /*  binäre Suchen */
    double low, high;
    initializeAlgorithm(filename);

    //std::cout << "binary search curve" << std::endl;
    low = 0.0; high=1e6;
    while((high-low) > precision) {
        double mid = (low+high)/2;

        //std::cout << "  .." << mid << std::endl;
        fs->calculateFreeSpace(mid);
        bool ok = algo->decideCurve(fs,FSPath::ptr(),0,false);

        if (ok)
            high=mid;
        else
            low=mid;
    }
    double dC = (low+high)/2;
    std::cout << "d_C = "<< dC << std::endl;

    //std::cout << "binary search polygon" << std::endl;
    Graph::ptr res;
    low = 0.0; high=1e6;
    while((high-low) > precision) {
        double mid = (low+high)/2;

        //std::cout << "  .." << mid << std::endl;
        fs->calculateFreeSpace(mid);
        Graph::ptr Gmid = algo->decidePolygon(fs,FSPath::ptr(),mid,true);

        if (Gmid) {
            high = mid;
            res = Gmid;
        }
        else
            low=mid;
    }

    if (res) {
        prettyPrint(res->origin.b);                       //  RG(1,2)
        prettyPrint(res->origin.a->origin.b->origin.a);   //  RG(0,1)
        prettyPrint(res->origin.a->origin.b);             //  CRG(0,1)

        prettyPrint(res);               //  MERGE(RG(1,2), CRG(0,1), RG(1,2))
        prettyPrint(res->origin.a);     //  MERGE(RG(1,2), CRG(0,1))
    }

    double dP = (low+high)/2;
    std::cout << "d_P = "<< dP << std::endl;

    ASSERT_GE(dP,45);
    ASSERT_NE(dC,dP);
}

Pointer findBoundarySegment(const BoundaryList& list, double x, frechet::reach::Type type) {
    Pointer p = list.first();
    for( ; p; p = p->next())
        if (p->contains(x) && p->type==type)
            return p;
    return nullptr;
}

void TriangulationTestSuite::testIpeClosed266()
{
    double eps = 266.0;
    initializeAlgorithm("data/ipe-closed.ipe");

    fs->calculateFreeSpace(eps);
    PolygonFSPath::ptr path(new PolygonFSPath(fs));

    Graph::ptr G = algo->decidePolygon(fs,path,eps,true);

    Curve fix = path->fixPoints();
    std::cout << "fix points: "<<std::endl;
    for(int i=0; i < fix.size(); ++i)
        std::cout << "["<<fix[i].x()<<","<<fix[i].y()<<"]"<<std::endl;

    //  RG(6,10) seems to be wrong
    //  Fix points [6,9.32886] and [10,14.1286] are NOT reachable

    std::cout <<std::endl << "d-points:"<<std::endl;
    int l = algo->l();
    for(int i=0; i < l; ++i) {
        std::cout << "d("<<i<<")" << algo->d(i) << std::endl;
    }

    Graph::ptr RG;// = algo->RG(4,5);
    const GraphModel::ptr model = RG->graphModel();

    Point p1 = fix[6];
    Point p2 = fix[7];

    double v1 = p1.y(); // 9.328858757189515;
    double v2 = p2.y(); // 14.12858765175692;

    IndexRange r1 = model->mapClosest(VERTICAL,Interval(v1,v1));
    IndexRange r2 = model->mapClosest(VERTICAL,Interval(v2,v2));

    std::cout << "RG(4,5)" << std::endl;
    std::cout << "("<<p1.x()<<","<<p1.y() << ") maps to [" << r1.lower<<","<<r1.upper<<"]"<<std::endl;
    std::cout << "("<<p2.x()<<","<<p2.y() << ") maps to [" << r2.lower<<","<<r2.upper<<"]"<<std::endl;

    ASSERT_TRUE(RG->contains_edge(r1,r2));

    //  Compare to Reachability structure
    frechet::reach::Structure str(fs);
    str.calculateColumns(6,10);
    //  find boundary segment
    Pointer p = findBoundarySegment(str.left(),v1,REACHABLE);
    Pointer q1 = findBoundarySegment(str.right(),v2,REACHABLE);
    Pointer q2 = findBoundarySegment(str.right(),v2,NON_ACCESSIBLE);
    Pointer q3 = findBoundarySegment(str.right(),v2,SEE_THROUGH);

    std::cout << "Reachability Structure (6,10)" << std::endl;
    std::cout << "("<<p->lower()<<","<<p->upper()<<") "
              << " maps to " << (p->l->ori==VERTICAL ? "V":"H") << p->l->lower()
              << ".." << (p->h->ori==VERTICAL ? "V":"H") << p->h->upper() << std::endl;
}

void TriangulationTestSuite::assertSubFreeSpace(FreeSpace::ptr a, FreeSpace::ptr b)
{
    ASSERT_EQ(a->n,b->n);
    ASSERT_EQ(a->m,b->m);

    for(int i=0; i < a->n; ++i)
        for(int j=0; j < a->m; ++j)
        {
            Cell c1 = a->cell(i,j);
            Cell c2 = b->cell(i,j);

            if (c1.L) {
                ASSERT_LE(c2.L.lower(), c1.L.lower());
                ASSERT_GE(c2.L.upper(), c1.L.upper());
            }
            if (c1.B) {
                ASSERT_LE(c2.B.lower(), c1.B.lower());
                ASSERT_GE(c2.B.upper(), c1.B.upper());
            }

            assertBorderCase(
                    (j < a->m-1) ? &c1.L : nullptr,
                    (i < a->n-1) ? &c1.B : nullptr,
                    (j>0) ? &a->cell(i,j-1).L : nullptr,
                    (i>0) ? &a->cell(i-1,j).B : nullptr);
            assertBorderCase(
                    (j < a->m-1) ? &c2.L : nullptr,
                    (i < a->n-1) ? &c2.B : nullptr,
                    (j>0) ? &b->cell(i,j-1).L : nullptr,
                    (i>0) ? &b->cell(i-1,j).B : nullptr);
        }
}

void TriangulationTestSuite::assertPlacements(const Algorithm::PlacementVector& a, const GraphModel& model)
{
    for(const Placements& ps : a)
        for(const Placement& p : ps)
        {   //  Placements must be consistent with GraphModel
            IndexRange rng = model.map(VERTICAL,p.fs,true);
            ASSERT_EQ(rng,p.gn);
        }
}




void TriangulationTestSuite::assertSubPlacements(const Algorithm::PlacementVector& a, const Algorithm::PlacementVector& b)
{
    //  all Placements from a must be contained in b.
    ASSERT_EQ(a.size(),b.size());
    for(int i=0; i < a.size(); ++i)
        assertSubPlacements(a[i],b[i]);
}

void TriangulationTestSuite::assertSubPlacements(const Placements& a, const Placements& b)
{
    //  Placements must grow, may grow together.
    for(const Placement& p : a)
    {
        bool ok=false;
        for(const Placement& q : b)
            if (q.fs.contains(p.fs)) {
                ok=true;
                break;
            }
        ASSERT_TRUE(ok) << p.fs;
    }
}

void TriangulationTestSuite::assertBorderCase(Interval* N, Interval* E, Interval* S, Interval* W)
{
    bool is_border_case =
               (N && N->lower()==0.0)
            || (E && E->lower()==0.0)
            || (S && S->upper()==1.0)
            || (W && W->upper()==1.0);

    if (is_border_case) {
        if (N) { ASSERT_EQ(N->lower(),0.0); ASSERT_FALSE(std::isnan(N->upper())); }
        if (E) { ASSERT_EQ(E->lower(),0.0); ASSERT_FALSE(std::isnan(E->upper())); }
        if (S) { ASSERT_EQ(S->upper(),1.0); ASSERT_FALSE(std::isnan(S->lower())); }
        if (W) { ASSERT_EQ(W->upper(),1.0); ASSERT_FALSE(std::isnan(W->lower())); }
    }
}

void TriangulationTestSuite::assertSubGraphs(
        QString label,
        AlgorithmSingleCore::GraphMap a,
        AlgorithmSingleCore::GraphMap b)
{
    ASSERT_EQ(a.size(),b.size());
    AlgorithmSingleCore::GraphMap::iterator i = a.begin();
    for( ; i != a.end(); ++i)
    {
        int hkey = i->first;
        Graph::ptr g = i->second;

        AlgorithmSingleCore::GraphMap::iterator j = b.find(hkey);

        ASSERT_NE(j,b.end());
        Graph::ptr h = j->second;
        assertSubGraph(label+hkey, g,h);
    }
}
/*
void TriangulationTestSuite::assertSubGraphs(
        QString label,
        Algorithm::PlacementMap a,
        Algorithm::PlacementMap b)
{
    ASSERT_EQ(a.size(),b.size());
    for(Algorithm::PlacementMap::iterator i = a.begin(); i != a.end(); ++i)
    {
        Algorithm::PlacementMap::iterator j = b.find(i->first);
        ASSERT_NE(j,b.end());
        ASSERT_EQ(i->first,j->first);

        assertSubGraph(
                QString("%1-%2-%3").arg(label).arg(i->first.first).arg(i->first.second),
                i->second, j->second);
    }
}
*/
void TriangulationTestSuite::assertSubGraphs(
        QString label,
        std::vector<Graph::ptr> a,
        std::vector<Graph::ptr> b)
{
    ASSERT_EQ(a.size(),b.size());
    for(int i=0; i < a.size(); ++i)
        assertSubGraph(QString("%1-%2").arg(label).arg(i), a[i],b[i]);
}

#define ASSERT_BETWEEN(a,b,c)   ASSERT_LE(a,b); ASSERT_LT(b,c);

void TriangulationTestSuite::assertSubGraph (
        QString label,
        Graph::ptr a,
        Graph::ptr b)
{
     std::vector<Interval> reverse_map[2] = {
             a->graphModel()->reverseMap(HORIZONTAL),
             a->graphModel()->reverseMap(VERTICAL)
     };

    for(Orientation o1=HORIZONTAL; o1 <= VERTICAL; ++o1)
        for(Orientation o2=HORIZONTAL; o2 <= VERTICAL; ++o2)
        {
            Rect r = a->rect(o1,o2);
            for(int i=r.i0; i < r.i1; ++i)
                for(int j=r.j0; j < r.j1; ++j)
                    if (a->contains_edge(o1,i, o2,j))
                    {
                        ASSERT_LT(i,reverse_map[o1].size());
                        ASSERT_LT(j,reverse_map[o2].size());

                        Interval ii = reverse_map[o1][i];
                        Interval jj = reverse_map[o2][j];

                        //ASSERT_TRUE(a->contains_edge(o1,ii,o2,jj));

                        IndexRange ri = b->graphModel()->mapClosest(o1,ii);
                        IndexRange rj = b->graphModel()->mapClosest(o2,jj);

                        //  ASSERT_TRUE(b->contains_edge(o1, ii, o2, jj));  will not work, since b uses a different GraphModel and
                        //  can not look up Interval from a
                        EXPECT_TRUE(b->contains_edge(ri,rj)) << label.toStdString()
                        << ii << jj << " " << i << "x" << j;
                    }
        }
}

std::vector<QRectF> TriangulationTestSuite::pl_intervals;
std::vector<QRectF> TriangulationTestSuite::pl_gnodes;

void TriangulationTestSuite::after_target_found(
        PolygonShortestPathsFS* sp,
        Triangulation::Vertex_handle v,
        bool vreachable)
{
    //std::cout << "updated point: " << v->index << std::endl;
    bool is_target = (v->target!=nullptr);
    if (is_target) {
        ASSERT_EQ(v->target->w,v->offset);
        ASSERT_TRUE(v->target->fs.contains(v->offset));
    }
    //  TOP intervals are not really needed, but useful for test comparison
    Interval T, TR, TR2;
    FreeSpace::calculateFreeSpaceSegment<double>(*v, sp->d.p1(), sp->d.p2(), sp->epsilon, T);
    FreeSpace::fixBorderCase(nullptr,&T,&v->L,nullptr);
    FreeSpace::fixBorderCase(nullptr,nullptr,&v->R,&T);

    if (v->LR || !T)
        TR = T;
    else if (v->BR)
        TR = FSPath::opposite(v->BR,T);

    //  TODO TR2

    //ASSERT_EQ(TR.contains(1.0), v->RR.contains(1.0));
    //  There is one border case, where
    //  v->RR = empty, TR = [x..1]
    //  So 1.0 is only reachable through 1 single point.

    bool reachable = v->RR.contains(1.0) || TR.contains(1.0);
    ASSERT_EQ(vreachable, is_target && reachable);

    /*  Does the incremental Free-Space resemble a proper Free-Space ?
     *
     *  Calculate Free-Space from scratch.
     * */
    Curve P,Q;
    P = createCurve(sp->d);
    Q = createCurve(v);

    FreeSpace::ptr fs(new FreeSpace(P,Q));
    fs->calculateFreeSpace(sp->epsilon);

    FSPath path(fs);
    //path.update();

    //  Compare to incremental free-space
    const Cell& ct = fs->cell(0,fs->m-1);
    ASSERT_EQ(ct.B, T);
    ASSERT_EQ(path.bottom(0,fs->m-1), TR);

    Triangulation::Vertex* p = &*v;
    for(int i=fs->m-2; i >= 0; p = p->prev, --i) {
        ASSERT_TRUE(p && p->prev);
        const Cell& cc = fs->cell(0,i);
        const Cell& cr = fs->cell(1,i);

        ASSERT_EQ(p->L, cc.L);
        ASSERT_EQ(p->B, cc.B);
        ASSERT_EQ(p->R, cr.L);

        ASSERT_NEAR(p->LR, path.left(0,i), 1e-12);
        ASSERT_NEAR(p->BR, path.bottom(0,i), 1e-12);
        ASSERT_NEAR(p->RR, path.left(1,i), 1e-12);
    }
    ASSERT_NE(p,nullptr);
    ASSERT_EQ(p->prev,nullptr);

    ASSERT_EQ(reachable,path.isReachable(1,fs->m-1,0.0));
    ASSERT_EQ(reachable,path.isReachable(QPointF(1.0,fs->m-1),0.0));

    if (reachable)
    {
        Curve ppath[2];
        ppath[0] = path.getPath(0);
        ppath[1] = path.getPath(1);
        ASSERT_EQ(ppath[0].first(),QPointF(0.0,0.0));
        ASSERT_EQ(ppath[1].last(),QPointF(1.0,fs->m-1));
        ASSERT_TRUE(ppath[1].isEmpty());
        //  Map to free-space view coordinates.
        //  Note: we need this Curve later, for visualisation
        Grid grid;
        grid.setCurves(P,Q);
        ppath[0] = grid.mapCurve(ppath[0]);

        QPointF g1 (grid.hor().length(),grid.vert().length());
        ASSERT_EQ(ppath[0].first(),QPointF(0.0,0.0));
        ASSERT_EQ(ppath[0].last(),g1);
    }
}

void TriangulationTestSuite::after_placement_added(
        PolygonShortestPathsFS*,
        const Placement* a,
        const Placement* b)
{
    //std::cout << a->fs << b->fs << std::endl;
    pl_intervals.push_back(QRectF(
            a->fs.lower(),
            b->fs.lower(),
            a->fs.size(),
            b->fs.size()));
    pl_gnodes.push_back(QRectF(
            a->gn.lower,
            b->gn.lower,
            a->gn.len(),
            b->gn.len()));
}

void TriangulationTestSuite::testDecomposition(QString filename)
{
    algo.reset(new AlgorithmSingleCore());

    readFile(filename);

    //  dump raw data (for comparison)
    dumpPolygon("../../" +filename+"P.txt",algo->Pcurve());
    dumpPolygon("../../" +filename+"Q.txt",algo->Qcurve());

    /*  CGAL decomposition algorithms   */
    testDecomposition(filename+"-P", algo->Pcurve());
    testDecomposition(filename+"-Q", algo->Qcurve());
}

static QString NAME[3] = {
    "Greene","Hertel","Optimal"
};

void TriangulationTestSuite::testDecomposition(QString filename, const Curve& P)
{
    //  test three algorithms; with and w/out verification
//    testDecomposition(filename,P, PolygonUtilities::APPROX_GREENE, true);
//    testDecomposition(filename,P, PolygonUtilities::APPROX_GREENE, false);

    testDecomposition(filename,P, PolygonUtilities::APPROX_HERTEL, true);
//    testDecomposition(filename,P, PolygonUtilities::APPROX_HERTEL, false);

//    testDecomposition(filename,P, PolygonUtilities::OPTIMAL_GREENE, true);
//    testDecomposition(filename,P, PolygonUtilities::OPTIMAL_GREENE, false);
}

void TriangulationTestSuite::testDecomposition(QString filename,
                                               const Curve& P,
                                               PolygonUtilities::DecompositionAlgorithm algo,
                                               bool verify)
{
    PolygonUtilities utils(P);
    ASSERT_TRUE(utils.is_counter_clockwise());
    ASSERT_TRUE(utils.is_simple());

    int result = utils.decompose(algo,verify);
    EXPECT_GT(result,0);
    if (result <= 0) return;

    Partition parts;
    utils.partition(parts);

    std::cout << "Decompose " << filename.toStdString() << " ("<<P.size()<<" vertices)"
                 << ": " << NAME[algo].toStdString()
                 << " " << parts.size() << " convex parts. " << std::endl;

    QString dumpFile = QString("%1-decompose-%2-%3").arg(filename).arg(NAME[algo]).arg(verify);
    savePartition(dumpFile,P,parts);
}

void TriangulationTestSuite::testFanTriangulation(QString filename)
{
    algo.reset(new AlgorithmSingleCore());

    readFile(filename);

    algo->decompose(false);
    algo->triangulate();

    //  save as PDF
    saveTriangulation(filename+"-P", algo->Pcurve(), algo->PTriangulation());
    assertTriangulation(algo->PTriangulation());

    saveTriangulation(filename+"-Q", algo->Qcurve(), algo->QTriangulation());
    assertTriangulation(algo->QTriangulation());

    //algo.decidePolygon();
}


void TriangulationTestSuite::readFile(QString filename)
{
    //QWARN(QDir().absolutePath().toLatin1());
    QFileInfo file ("../../" + filename);
    //QWARN(file.absoluteFilePath().toLatin1());
    InputReader reader;
    reader.readAll(file.absoluteFilePath());
    ASSERT_EQ(reader.getResults().size(),2);

    int precision = 0;//reader.isScript() ? 8:0;

    Curve P = reader.getResults()[0].toCurve(precision);
    Curve Q = reader.getResults()[1].toCurve(precision);

    ASSERT_TRUE(P.isClosed());
    ASSERT_TRUE(Q.isClosed());

    Algorithm::Status error = algo->setup(P,Q,true);

    ASSERT_EQ(error,Algorithm::SET_UP);
}

void TriangulationTestSuite::saveTriangulation(QString filename,
                                               const Curve& P,
                                               const Triangulation& tri)
{
    QGraphicsView* gview = new QGraphicsView;
    QGraphicsScene* scene = new QGraphicsScene;

    gview->setRenderHint(QPainter::Antialiasing, true);
    gview->setDragMode(QGraphicsView::ScrollHandDrag);
    gview->setOptimizationFlags(QGraphicsView::DontSavePainterState);
    gview->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
    gview->setScene(scene);
    gview->setInteractive(false);

    QPen path_pen(QColor(0,0,255,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    QPen diagonal_pen(QColor(180,180,180,180), 2.0,   Qt::DashLine, Qt::RoundCap, Qt::RoundJoin);
    QPen tree_pen(QColor(255,0,0,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    QPen funnel_pen(QColor(0,255,0,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);

    QPainterPath path;
    path.addPolygon(P);
    scene->addPath(path,path_pen);
    //  For some unkonwn reason, QSvgGenerator crops the scene rect
    //  somehow ...
    //scene->setSceneRect(-1000,-2000,1000,2000);
    //gview->setSceneRect(0,0,1000,2000);

    //  Polygon
    for(int i=1; i < P.size(); ++i)
    {
        QString label = QString::number(i);
        QGraphicsTextItem* item = scene->addText(label);
        QPointF p = P[i];
        item->setPos(p);

        scene->addEllipse(p.x()-2,p.y()-2,4,4, path_pen);
    }
    //  Extra triangulation points
    for (int i=P.size(); i < tri.size(); ++i)
    {
        QPointF p = *tri[i];
        scene->addEllipse(p.x()-2,p.y()-2,4,4, path_pen);
    }
    //  Triangulation Edges
    for(auto d = tri.edges_begin(); d != tri.edges_end(); ++d)
    {
        if (d.is_outer_edge()) continue;    //  always ignore
        //if (d.is_polygon_edge()) continue;

        QLineF line = d.line();
        scene->addLine(line,diagonal_pen);
    }
    //  Shortest Paths
    for(int i=0; i < tri.size(); ++i)
    {
        Triangulation::Vertex_handle v = tri[i];
        if (v!=0) {
            QLineF line = v->treeEdge();
            if (!line.isNull()) {
                QGraphicsLineItem* item = scene->addLine(line,tree_pen);
                item->setZValue(0.8);
            }
        }
    }
    //  Guibas Funnel
    if (guibas) {
       PolygonShortestPaths::Funnel funnel = guibas->getFunnel();
       for(int t=1; t <= funnel.rightSize(); ++t) {
           QLineF line (*funnel[t-1],*funnel[t]);
           QGraphicsLineItem* item = scene->addLine(line,funnel_pen);
           item->setZValue(+1);
       }
       for(int t=1; t <= funnel.leftSize(); ++t) {
           QLineF line (*funnel[-t+1],*funnel[-t]);
           QGraphicsLineItem* item = scene->addLine(line,funnel_pen);
           item->setZValue(+1);
       }
    }

    scene->setSceneRect(P.boundingRect());
    gview->setSceneRect(P.boundingRect());
    gview->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    //gview->rotate(90);
    gview->scale(0.1,0.1);

    //gview->show();
    //saveAsSvg(filename+".svg",gview);
    saveAsPdf(filename+".pdf",gview);

    delete scene;
    delete gview;
}


void TriangulationTestSuite::savePartition(QString filename,
                                           const Curve& P,
                                           Partition& parts)
{
    QGraphicsView* gview = new QGraphicsView;
    QGraphicsScene* scene = new QGraphicsScene;

    gview->setRenderHint(QPainter::Antialiasing, true);
    gview->setDragMode(QGraphicsView::ScrollHandDrag);
    gview->setOptimizationFlags(QGraphicsView::DontSavePainterState);
    gview->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
    gview->setScene(scene);
    gview->setInteractive(false);

    QPen path_pen(QColor(0,0,255,180), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    QPen diagonal_pen(QColor(255,255,0,180), 2.0,   Qt::DashLine, Qt::RoundCap, Qt::RoundJoin);

    QPainterPath path;
    path.addPolygon(P);
    scene->addPath(path,path_pen);
    //  For some unkonwn reason, QSvgGenerator crops the scene rect
    //  somehow ...
    //scene->setSceneRect(-1000,-2000,1000,2000);
    //gview->setSceneRect(0,0,1000,2000);


    //  Decomposition Edges
    for(auto j = parts.begin(); j != parts.end(); ++j)
    {
        const Polygon& p = *j;
        for(int i=0; i < p.size()-1; ++i)
        {
            QLineF line(P[p[i]],P[p[i+1]]);
            scene->addLine(line,diagonal_pen);
        }
        QLineF line(P[p.back()],P[p.front()]);
        scene->addLine(line,diagonal_pen);
    }

    scene->setSceneRect(P.boundingRect());
    gview->setSceneRect(P.boundingRect());
    gview->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    //gview->rotate(90);
    gview->scale(0.1,0.1);

    //gview->show();
    //saveAsSvg(filename+".svg",gview);
    saveAsPdf(filename+".pdf",gview);

    delete scene;
    delete gview;
}

void TriangulationTestSuite::saveAsPdf(QString filename, QGraphicsView* gview)
{
    QPrinter printer;
    //  Note: default ctor crashes on Windows in debug mode.
    //  Release mode seems to work.
    printer.setOutputFormat(QPrinter::PdfFormat);
    //printer.setPaperSize(QPrinter::A4); //   ?
    printer.setOutputFileName("../../" + filename);
    std::cout << "Writing to " << printer.outputFileName().toStdString() << std::endl;

    QPainter painter(&printer);
    gview->render(&painter);
}


void TriangulationTestSuite::saveAsSvg(QString filename, QGraphicsView* gview)
{
    /*  Note: sadly, clipping seems not to work with QSvgGenerator.
     *  Also, the scene size is not accurate.
     * */
    QSvgGenerator generator;
    QSizeF size = gview->sceneRect().size();
    generator.setFileName("../../" + filename);
    std::cerr << "create file " << filename.toLocal8Bit().data() << std::endl;
    generator.setSize(QSize(size.width(),size.height()));
    //generator.setViewBox(gview->sceneRect());
    //generator.setTitle(windowTitle());
    //generator.setDescription("Created by "
    //                         +QApplication::applicationDisplayName()+" "
    //                         +QApplication::applicationVersion());

    QPainter painter;
    painter.begin(&generator);
    gview->render(&painter,
           QRectF(QPointF(0,0),size));
}

void TriangulationTestSuite::assertTriangulation(const Triangulation &tri)
{
    //std::cout << std::endl << tri << std::endl;

    auto tbegin = tri.edges_begin();
    auto tend = tri.edges_end();
    for(auto t=tbegin; t!=tend; ++t) {
        ASSERT_TRUE(
            t.is_polygon_edge()
            || t.is_outer_edge()
            || t.is_diagonal());

        Segment s = t.segment();
    }
    //   no infinite face loops
    for(int i=0; i < tri.size(); ++i)
    {
        Triangulation::Vertex_handle v = tri[i];
        if (v != Triangulation::Vertex_handle())
            assertVertex(v);
    }
}

void TriangulationTestSuite::assertVertex(Triangulation::Vertex_handle v)
{
    //  v->face() must be set
    ASSERT_NE(v->face(),Triangulation::Face_handle());

    //  faces around v must be oriented counter-clockwise
    //  otherwise, the FirceCirculator will get stuck
    Triangulation::Face_circulator f0 (v,v->face());
    Triangulation::Face_circulator f = f0;
    for(int loop=0; ++f != f0; ++loop) {
        if (loop > 1000) {
            std::cout << "infinite face loop at " << v->pindex << std::endl;
            printFaces(v,5);
            ASSERT_TRUE(false);
        }
    }
}


void TriangulationTestSuite::printFaces(Triangulation::Vertex_handle v, int max)
{
    Triangulation::Face_circulator f0 (v,v->face());
    Triangulation::Face_circulator f = f0;
    do {
        std::cout << "["
                      << f->vertex(0)->pindex << ".."
                      << f->vertex(1)->pindex << ".."
                      << f->vertex(2)->pindex << "]"
                      << std::endl;
    } while(max-- > 0 && ++f != f0);
}

void TriangulationTestSuite::dumpPolygon(QString fileName, const Curve &P)
{
    QFile file(fileName);
    file.open(QFile::ReadWrite);
    QTextStream out(&file);
    out.setRealNumberNotation(QTextStream::ScientificNotation);
    out.setRealNumberPrecision(80); //  maximum precision!!

    for(int i=0; i < P.size(); ++i)
        out << P[i].x() <<","<<P[i].y()<< " ";
}

Curve TriangulationTestSuite::createCurve(QLineF line)
{
    Curve P;
    P.push_back(line.p1());
    P.push_back(line.p2());
    return P;
}

Curve TriangulationTestSuite::createCurve(Triangulation::Vertex_handle v)
{
    Curve P;
    Triangulation::Vertex* p = &*v;
    for( ; p; p = p->prev)
        P.push_front(*p);
    return P;
}

