/**
 * Test suite for Reachability Structure: creating a single cell.
 *
 * There are several ways the free-space intervals can be arranged.
 * Test all combinations.
 *
 *  We set up an synthetic Free-Space
 */
#include <QString>

#include <freespace.h>
#include <structure.h>
#include <iostream>
#include <set>

#include <reachability_test_suite.h>
#include "reachability_test_suite.h"


// do not use Q_ASSERT, use testing::ASSERT instead
#undef Q_ASSERT

using namespace frechet;
using namespace reach;


void ReachabilityTestSuite::testSingleCell()
{
    /**
     * What is sizeof(BoundarySegment) ?
     */
    std::cout << "sizeof(BoundarySegment) = " << sizeof(BoundarySegment) << std::endl;
    std::cout << "sizeof(Type) = " << sizeof(Type) << std::endl;
    std::cout << "sizeof(Orientation) = " << sizeof(Orientation) << std::endl;
    std::cout << "sizeof(Direction) = " << sizeof(Direction) << std::endl;

#ifdef QT_DEBUG
    EXPECT_LE(sizeof(BoundarySegment),72);
#else
    EXPECT_LE(sizeof(BoundarySegment),64);
#endif
    
    test_outside_pointers = true;
    Structure::after_single_cell = ReachabilityTestSuite::after_single_cell;

    fs = createEmptyFreeSpace(20,20);
    std::set<int> all_arrangements, h_arrangements, v_arrangements;
    for (int i=0; i < count; ++i)
        for(int j=0; j < count; ++j)
        {
            //  all possible arrangements must be covered
            int a1 = Structure::freeSpaceArrangement(arrangements[i].first, arrangements[i].second);
            int a2 = Structure::freeSpaceArrangement(swapped(arrangements[i]).first, swapped(arrangements[i]).second);
            int a3 = Structure::freeSpaceArrangement(arrangements[j].first, arrangements[j].second);
            int a4 = Structure::freeSpaceArrangement(swapped(arrangements[j]).first, swapped(arrangements[j]).second);

            h_arrangements.insert(a1);
            h_arrangements.insert(a2);

            v_arrangements.insert(a3);
            v_arrangements.insert(a4);

            all_arrangements.insert(a1 + a3*16);
            all_arrangements.insert(a1 + a4*16);
            all_arrangements.insert(a2 + a3*16);
            all_arrangements.insert(a2 + a4*16);

            testSingleCell(i%10, j%10, arrangements[i], arrangements[j]);
            testSingleCell((i+1)%10, (j+1)%10, arrangements[i], swapped(arrangements[j]));
            testSingleCell((i+1)%10, (j+2)%10, swapped(arrangements[i]), arrangements[j]);
            testSingleCell((i+2)%10, (j+1)%10, swapped(arrangements[i]), swapped(arrangements[j]));
        }

    ASSERT_EQ(h_arrangements.size(), 16);
    ASSERT_EQ(v_arrangements.size(), 16);
    ASSERT_EQ(all_arrangements.size(), 16*16);
}



void ReachabilityTestSuite::assertPointerInterval(Pointer p, PointerInterval ival)
{
    ASSERT_NE(ival.l,nullptr);
    ASSERT_NE(ival.h,nullptr);

    //  l,h pointers must point UP or DOWN
    Structure::assertPointerInterval(p,ival);

    //  all segments outside of (l,h) are unreachable
    if (test_outside_pointers)
    {
        if (p->dir==FIRST) {
            //  l,h pointing UP
            //  segments strictly SMALLER than origin are unreachable by definition
            for(Pointer i = str->next(ival.h); i; i = str->next(i))
                ASSERT_TRUE((i->type == NON_ACCESSIBLE) || (compare_pointers(i,p) < 0) );
            for(Pointer i = str->prev(ival.l); i; i = str->prev(i))
                ASSERT_TRUE((i->type == NON_ACCESSIBLE) || (compare_pointers(i,p) < 0) );
        }
        else {
            //  l,h pointing DOWN
            //  segments strictly BIGGER than origin are unreachable by definition
            for(Pointer i = str->next(ival.h); i; i = str->next(i))
                ASSERT_TRUE((i->type == NON_ACCESSIBLE) || (compare_pointers(i,p) > 0) );
            for(Pointer i = str->prev(ival.l); i; i = str->prev(i))
                ASSERT_TRUE((i->type == NON_ACCESSIBLE) || (compare_pointers(i,p) > 0) );
        }
    }
}

void ReachabilityTestSuite::assert2Segments(
        Pointer i1, Pointer i2,
        Orientation ori)
{
    //  consistent interval
    ASSERT_EQ(i1->lower(),i2->lower());
    ASSERT_EQ(i1->upper(),i2->upper());

    //  interval must not be empty, or can it ??
#ifdef NO_EMPTY_REACH_INTERVALS
    ASSERT_GT(i1->upper(), i1->lower());
    ASSERT_GT(i2->upper(), i2->lower());
#else
    ASSERT_GE(i1->upper(), i1->lower());
    ASSERT_GE(i2->upper(), i2->lower());
#endif

    //  consistent ori, dir
    ASSERT_EQ(i1->ori,ori);
    ASSERT_EQ(i2->ori,ori);
    //  opposite segments
    ASSERT_EQ(i1->dir,FIRST);
    ASSERT_EQ(i2->dir,SECOND);

#ifdef QT_DEBUG
    //  all segments must belong to us
    ASSERT_EQ(i1->owner(),str);
    ASSERT_EQ(i2->owner(),str);
#endif
    //  single cell: consecutive n's are undesirable
    if (test_outside_pointers)
    {
        if (i1->next())
        {
            ASSERT_TRUE(i2->next());
            ASSERT_FALSE( i1->type==NON_ACCESSIBLE
                       && i2->type==NON_ACCESSIBLE
                       && i1->next()->type==NON_ACCESSIBLE
                       && i2->next()->type==NON_ACCESSIBLE);
        }
    }
}

void ReachabilityTestSuite::assert1Segment(Pointer i, Pointer j, Interval F)
{
    PointerInterval ival = *i;
    switch(i->type) {
        case NON_ACCESSIBLE:
            //  N must be opposite to N,R (but not S)
            ASSERT_NE(j->type,SEE_THROUGH);

            // N corresponds to a blocked segment
            //  or a Free segment without an exit (TODO how to assert this?)
            //Q_ASSERT(!F || !F.intersects_proper(*p));
            //  N intervals have no pointers
            ASSERT_EQ(i->l,nullptr);
            ASSERT_EQ(i->h,nullptr);
            break;

         case SEE_THROUGH:
            //  S must be symmetrical
            ASSERT_EQ(j->type,SEE_THROUGH);
            //  one pointer is actually redundant
            ASSERT_NE((i->l!=nullptr), (i->h!=nullptr));
            if ((i->ori==VERTICAL && i->dir==SECOND)
                    || (i->ori==HORIZONTAL && i->dir==FIRST))
            {
                ASSERT_EQ(i->h,nullptr);
                ival.h = j;
            }
            else {
                ASSERT_TRUE((i->ori==HORIZONTAL && i->dir==SECOND)
                         || (i->ori==VERTICAL && i->dir==FIRST));
                ASSERT_EQ(i->l,nullptr);
                ival.l = j;
            }

            //  S corresponds to a free segment
            ASSERT_TRUE(F.contains(*i));

            assertPointerInterval(i,ival);
            break;

        case REACHABLE:
            if (test_outside_pointers) {
                //  single cell: R must be opposite to N (R-S and R-R would become S-S)
                ASSERT_EQ(j->type,NON_ACCESSIBLE);
            }
            //  Interval must not be identical to origin (otherwise it would be a see-through)
            ASSERT_TRUE( (i->l != j) || (i->h != j) );
            //  R corresponds to a free segment
            ASSERT_TRUE(!F || F.contains(*i));

            assertPointerInterval(i,ival);
            break;
    }

    //  l,h segments must be accessible
    ASSERT_TRUE(! i->l || i->l->type!=NON_ACCESSIBLE);
    ASSERT_TRUE(! i->h || i->h->type!=NON_ACCESSIBLE);

    ASSERT_EQ((ival.l!=nullptr), (ival.h!=nullptr));

    if (ival.l)
    {
        //  Interval must not be empty
        ASSERT_TRUE(ival.l && ival.h);
        ASSERT_FALSE( empty_interval(ival) );
        ASSERT_EQ( ival.l->dir, ival.h->dir );

        //  l,h in opposite direction
        ASSERT_EQ( ival.l->dir, opposite(i->dir) );
        ASSERT_EQ(ival.h->dir, opposite(i->dir));
#ifdef QT_DEBUG
        ASSERT_EQ( ival.l->owner(), i->owner());
        ASSERT_EQ(ival.h->owner(), i->owner());
#endif

        //  iterate from l to h
        StructureIterator q (*str,ival);
        for( ; q; ++q)
        {   //  q must be reachable from p
            ASSERT_EQ(q->dir, opposite(i->dir));
            switch(q->type)
            {
            case REACHABLE:
                ASSERT_TRUE(q->l && q->h);
                ASSERT_EQ(q->l->dir, i->dir);
                ASSERT_EQ(q->h->dir, i->dir);
                ASSERT_TRUE(q->contains((Pointer)i));
                break;
            case SEE_THROUGH:
                ASSERT_NE((q->l==nullptr), (q->h==nullptr));
                if (q->l) {
                    ASSERT_EQ(q->l->dir, i->dir);
                    ASSERT_GE( compare_interval((Pointer)i, q->l), 0 );
                }
                if (q->h) {
                    ASSERT_EQ(q->h->dir, i->dir);
                    ASSERT_LE( compare_interval((Pointer)i, q->h), 0);
                }
                break;
            }
        }
    }
}

void ReachabilityTestSuite::assertNeighbors(
        Pointer a, Pointer b, Pointer c, Pointer d)
{
    if (a) {
        ASSERT_EQ( str->next(a), b );
    }
    if (b) {
        ASSERT_EQ( str->prev(b), a );
        ASSERT_EQ( str->next(b), c );
    }
    if (c) {
        ASSERT_EQ( str->prev(c), b );
        ASSERT_EQ( str->next(c), d );
    }
    if (d) {
        ASSERT_EQ( str->prev(d), c );
    }
}

int single_cell_iteration=0;

void ReachabilityTestSuite::testSingleCell(int i, int j, TwoIntervals vert, TwoIntervals horiz)
{
    ++single_cell_iteration;
    /*
     * create an artificial Free-Space cell
     */
    fs->cell(i,j).L = vert.first;
    fs->cell(i,j).B = horiz.first;
    fs->cell(i+1,j).L = vert.second;
    fs->cell(i,j+1).B = horiz.second;

    /*
     * Calculate 1 Reachability Cell
     */
    str = new Structure(fs);
    str->singleCell(i,j);

    //  basic assertions (like contiguous intervals), corners
    assertStructure( i,j, i+1,j+1 );

    Pointer i1 = str->first(VERTICAL).first();
    Pointer i2 = str->second(VERTICAL).first();

    for( ; i1; i1=i1->next(), i2=i2->next())
    {
        assert2Segments(i1,i2, VERTICAL);
        assert1Segment(i1,i2, vert.first+(double)j);
        assert1Segment(i2,i1, vert.second+(double)j);
    }

    ASSERT_TRUE(!i1 && !i2);

    i1 = str->first(HORIZONTAL).first();
    i2 = str->second(HORIZONTAL).first();

    for( ; i1; i1=i1->next(), i2=i2->next())
    {
        assert2Segments(i1,i2, HORIZONTAL);
        assert1Segment(i1,i2, horiz.first+(double)i);
        assert1Segment(i2,i1, horiz.second+(double)i);
    }

    ASSERT_TRUE(!i1 && !i2);
    delete str;
}

void ReachabilityTestSuite::assertStructure(int i0, int j0, int i1, int j1)
{
    /*
     * Consistency Checks
     */
    const BoundaryList& l = str->left();
    const BoundaryList& r = str->right();
    const BoundaryList& b = str->bottom();
    const BoundaryList& t = str->top();

    //  Traversal (including corner cases)
    if (l.size()>=2 && b.size()>=2) {
        assertNeighbors(NULL, NULL, r[0], r[1]);  //  bottom-right corner
        assertNeighbors(r[-2], r[-1], t[-1], t[-2]);  //  top-right corner with bend(!)
        assertNeighbors(t[1], t[0], NULL, NULL); //   top-left corner
        // bottom-left corner can not be traversed counter-clockwise

        assertNeighbors(NULL,NULL, b[-1],b[-2]);  //  bottom-right corner
        //  top-right corner can not be traversed clockwise
        assertNeighbors(l[-2], l[-1], NULL, NULL); //   top-left corner
        assertNeighbors(b[1],b[0],l[0],l[1]); // bottom-left corner with bend(!)
    }


    ASSERT_FALSE(l.empty());
    ASSERT_FALSE(r.empty());
    ASSERT_FALSE(b.empty());
    ASSERT_FALSE(t.empty());

    ASSERT_EQ(l.size(),r.size());
    ASSERT_EQ(b.size(),t.size());

    //  covered range is [i..i+1] x [j..j+1]
    ASSERT_EQ(b.first()->lower(), i0);
    ASSERT_EQ(b.last()->upper(), i1);

    ASSERT_EQ(t.first()->lower(), i0);
    ASSERT_EQ(t.last()->upper(), i1);

    ASSERT_EQ(l.first()->lower(), j0);
    ASSERT_EQ(l.last()->upper(), j1);

    ASSERT_EQ(r.first()->lower(), j0);
    ASSERT_EQ(r.last()->upper(), j1);

    //  intervals must be contiguous
    for(Pointer p=l[1]; p; p=p->next()) ASSERT_EQ(p->prev()->upper(),p->lower());
    for(Pointer p=r[1]; p; p=p->next()) ASSERT_EQ(p->prev()->upper(),p->lower());
    for(Pointer p=b[1]; p; p=p->next()) ASSERT_EQ(p->prev()->upper(),p->lower());
    for(Pointer p=t[1]; p; p=p->next()) ASSERT_EQ(p->prev()->upper(),p->lower());

    //  and not empty, OR CAN THEY ??
#ifdef NO_EMPTY_REACH_INTERVALS
    for(Pointer p=l[0]; p; p=p->next()) ASSERT_GT(p->upper(), p->lower());
    for(Pointer p=r[0]; p; p=p->next()) ASSERT_GT(p->upper(), p->lower());
    for(Pointer p=b[0]; p; p=p->next()) ASSERT_GT(p->upper(), p->lower());
    for(Pointer p=t[0]; p; p=p->next()) ASSERT_GT(p->upper(), p->lower());
#else
    for(Pointer p=l[0]; p; p=p->next()) ASSERT_GE(p->upper(), p->lower());
    for(Pointer p=r[0]; p; p=p->next()) ASSERT_GE(p->upper(), p->lower());
    for(Pointer p=b[0]; p; p=p->next()) ASSERT_GE(p->upper(), p->lower());
    for(Pointer p=t[0]; p; p=p->next()) ASSERT_GE(p->upper(), p->lower());
#endif
 }


// @Override
void ReachabilityTestSuite::after_single_cell(
        Structure* single,  const Rect* r,
        Structure*,  const Rect*,
        Orientation)
{
    ReachabilityTestSuite* suite = ReachabilityTestSuite::suite;

    Structure* recover = suite->str;
    suite->str = single;

    suite->assertStructure(r->i0,r->j0,r->i1,r->j1);

    suite->str = recover;
}



