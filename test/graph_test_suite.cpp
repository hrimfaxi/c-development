

#include <graph_test_suite.h>
#include <data/spirolator.h>

#if defined(__cplusplus) && ! defined(_MSC_VER)
extern "C" {
# include <m4ri/m4ri.h>
# include <m4ri/mzd_bool.h>
# include <m4ri/russian_bool.h>
}
#else
# include <m4ri/m4ri.h>
# include <m4ri/mzd_bool.h>
# include <m4ri/russian_bool.h>
#endif

#include <QRandomGenerator>
#include <QFileInfo>
#include <inputreader.h>

using namespace frechet;
using namespace reach;

QRandomGenerator qrg;

void GraphTestSuite::createRandomModel(int n)
{
    //  Note: we don't care about simple polygons.
    //  We just need a random FreeSpace.
    Curve P,Q;
    for(int i=0; i < n; ++i)
    {
        P.push_back(QPointF(qrg.bounded(0,1000),qrg.bounded(0,1000)));
        Q.push_back(QPointF(qrg.bounded(0,1000),qrg.bounded(0,1000)));
    }
    P.push_back(P.front());
    Q.push_back(Q.front());
    //  n=30 generates a GraphModel containing about 450 nodes per dimension,
    //  1300 nodes in total, > 1M bits in matrix
    init(P,Q,500.0);
}

void GraphTestSuite::readFile(QString filename, double epsilon)
{
    QFileInfo file ("../../" + filename);

    InputReader reader;
    reader.readAll(file.absoluteFilePath());

    Curve P,Q;
    P = reader.getResults()[0].toCurve();
    Q = reader.getResults()[1].toCurve();

    init(P,Q,epsilon);
}

void GraphTestSuite::init(const Curve& P, const Curve& Q, double epsilon)
{
    Q_ASSERT(P.isClosed());
    Q_ASSERT(Q.isClosed());

    fs.reset(new FreeSpace(P,Q));
    fs->calculateFreeSpace(epsilon);
    dpoints.clear();
    for(int i=0; i < fs->n-1; )
    {
        i += qrg.bounded(1,4);
        if (i < fs->n)
            dpoints.push_back(i);
    }

    model.reset(new GraphModel());
    model->init(fs);

    int n = fs->n-1;
    int m = fs->m-1;

//    ASSERT_EQ(model->count2(HORIZONTAL), 2*model->count1(HORIZONTAL));

    ASSERT_EQ(0, model->map_lower(VERTICAL,0.0));
    ASSERT_EQ(0, model->map_lower(HORIZONTAL,0.0));

    ASSERT_EQ(model->map_upper(VERTICAL,m,false)+1, model->count2(VERTICAL));

//    ASSERT_EQ(model->map_lower(HORIZONTAL,n), model->count1(HORIZONTAL));
//    ASSERT_EQ(model->map_upper(HORIZONTAL,n,false), model->count1(HORIZONTAL));
//    ASSERT_EQ(model->map_upper(HORIZONTAL,n,true), model->count1(HORIZONTAL)+1);

    ASSERT_EQ(model->map_upper(HORIZONTAL,2*n,false), model->count2(HORIZONTAL));
}

Graph::ptr GraphTestSuite::createEmptyGraph(int di, int dj)
{
    IndexRange range = model->map(HORIZONTAL,Interval(di,dj),false);
    return createEmptyGraph(range);
}

Graph::ptr GraphTestSuite::createEmptyGraph(IndexRange range)
{
    Graph::ptr g (new Graph(model,range));
    g->allocateAll();
    return g;
}

Graph::ptr GraphTestSuite::createRandomGraph(int di, int dj, double density)
{
    Graph::ptr g = createEmptyGraph(di,dj);
    for(Orientation o1=HORIZONTAL; o1 <= VERTICAL; ++o1)
        for(Orientation o2=HORIZONTAL; o2 <= VERTICAL; ++o2)
        {
            Rect bounds = g->rect(o1,o2);
            for(int i=bounds.i0; i < bounds.i1; ++i)
                for(int j=bounds.j0; j < bounds.j1; ++j)
                    if (qrg.bounded(1.0) < density)
                        g->add_edge(o1,i, o2,j);
        }
    return g;
}

void GraphTestSuite::createReachabilityGraph(int i, int j)
{
    //  TODO the same happens within poly::Algorithm
    Q_ASSERT((i+1)==j);
    int l = dpoints.size();
    int n = fs->n-1;

    int di = dpoints[i] % n;
    int dj = dpoints[j % l] % n;
    if (dj < di) dj += n;

    Structure str(fs);
    str.calculateColumns(di, dj);

    Graph::ptr g (new  Graph(model, str));
    g->setOriginRG(i % l);

    printDiagnostics(g);

    Rect r = g->rect(HORIZONTAL,HORIZONTAL);
    ASSERT_EQ(r.i0, model->map_lower(HORIZONTAL, (double)di));
    ASSERT_EQ(r.i1, model->map_lower(HORIZONTAL, (double)dj));
    ASSERT_EQ(r.j0, r.i0);
    ASSERT_EQ(r.j1, r.i1);

    r = g->rect(HORIZONTAL,VERTICAL);
    ASSERT_EQ(r.i0, model->map_lower(HORIZONTAL, (double)di));
    ASSERT_EQ(r.i1, model->map_lower(HORIZONTAL, (double)dj));
    ASSERT_EQ(r.j0, 0);
    ASSERT_EQ(r.j1, model->count2(VERTICAL));

    r = g->rect(VERTICAL,HORIZONTAL);
    ASSERT_EQ(r.i0, 0);
    ASSERT_EQ(r.i1, model->count2(VERTICAL));
    ASSERT_EQ(r.j0, model->map_lower(HORIZONTAL, (double)di));
    ASSERT_EQ(r.j1, model->map_lower(HORIZONTAL, (double)dj));

    r = g->rect(VERTICAL,VERTICAL);
    ASSERT_EQ(r.i0, 0);
    ASSERT_EQ(r.i1, model->count2(VERTICAL));
    ASSERT_EQ(r.j0, r.i0);
    ASSERT_EQ(r.j1, r.i1);

    if (!rg.empty())
        assertNeighbors(rg.back(),g);
    rg.push_back(g);
}

void GraphTestSuite::assertNeighbors(Graph::ptr g, Graph::ptr h)
{
//    Rect r1 = g->rect(HORIZONTAL,HORIZONTAL);
//    Rect r2 = h->rect(HORIZONTAL,HORIZONTAL);
//    int m = model->count1(HORIZONTAL);

    ASSERT_TRUE(Graph::is_adjacent_to(*g,*h));
    ASSERT_TRUE((g->origin.j == h->origin.i) || (h->origin.j == g->origin.i));
}

void GraphTestSuite::testCopy()
{
    Graph::ptr g = createRandomGraph(0,1, 0.5);
    Graph::ptr h (new Graph(*g));
    ASSERT_EQ(*g,*h);

    h = Graph::ptr(new Graph(model,g->hmask()));
    h->allocateAll();

    *h = *g;
    ASSERT_EQ(*g,*h);
}


void print_matrix(mzd_t* M)
{
    for(int row=0; row < M->nrows; ++row) {
        for (int col = 0; col < M->ncols; col += 64) {
            word w = mzd_read_bits(M, row, col, 64);
            std::cout << std::hex << std::setw(16) << w << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


void GraphTestSuite::testMatrixBasics()
{
/*
    static void set_bits(uint64_t* row, int i, int j);
    static bool has_bits(uint64_t* row, int i, int j);
*/
    //  Border cases 0,1, .. 32 .. 63,64 .. nrows, ncols
    int rows = qrg.bounded(8,13);
    int cols = qrg.bounded(128,371);
    mzd_t* M = mzd_init(rows,cols);

    int sets[][3] = {
        0,      0,  0,
        0,     63, 63,
        0,     64, 64,
        0,     65, 65,

        0,       0,  1,
        rows-1,  0, 64,
        1,       1, 63,
        2,       2, 65,

        3,          5,  127,
        4,         63,  128,
        rows-1,    64,  127,
        2,    cols-64, cols,
        2,    cols-63, cols,
        2,          0, cols
    };

    for(int s=0; s<14; s++)
        testMatrixBits(M, sets[s][0], sets[s][1],sets[s][2],true);

    for(int k=0; k < 1000; ++k) {
        int r = qrg.bounded(0,M->nrows);
        int i = qrg.bounded(0,M->ncols);
        int j = qrg.bounded(i,M->ncols+1);
        testMatrixBits(M,r,i,j,false);
    }
}

void GraphTestSuite::testMatrixBits(mzd_t* M, int row, int i, int j, bool print)
{
    ASSERT_LT(row,M->nrows);
    ASSERT_GE(i,0);
    ASSERT_LE(i,j);
    ASSERT_LE(j,M->ncols);

    mzd_set_ui(M,0);
    ASSERT_TRUE(mzd_is_zero(M));
    ASSERT_EQ(mzd_row(M,row), M->rows[row]);

    Graph::set_bits(M,row, i,j);

    if (i > 0) {
        ASSERT_FALSE(Graph::has_bits(M,row,0,i));
        ASSERT_EQ(mzd_read_bit(M,row,i-1),0);
    }

    if (j < M->ncols) {
        ASSERT_FALSE(Graph::has_bits(M,row,j,M->ncols));
        ASSERT_EQ(mzd_read_bit(M,row,j),0);
    }

    if (i >= j) {
        ASSERT_FALSE(Graph::has_bits(M,row,i,j));
        ASSERT_TRUE(mzd_is_zero(M));
        print=false;
    }
    else {
        ASSERT_TRUE(Graph::has_bits(M,row,i,j));
    }

    if (print) {
        std::cout << "set row="<<row<<" (i,j) = 0x"<<i<<",0x"<<j <<std::endl;
        print_matrix(M);
        std::cout << std::endl;
    }

    for(int k=i; k < j; ++k) {
        ASSERT_TRUE(Graph::has_bits(M,row,k,k+1));
        ASSERT_EQ(mzd_read_bit(M,row,k), 1);

        if (row > 0) {
            ASSERT_EQ(mzd_read_bit(M,row-1,k), 0);
            ASSERT_FALSE(Graph::has_bits(M,row-1,0,M->ncols));
        }
        if (row+1 < M->nrows) {
            ASSERT_EQ(mzd_read_bit(M,row+1,k), 0);
            ASSERT_FALSE(Graph::has_bits(M,row+1,0,M->ncols));
        }
    }
}

void GraphTestSuite::testEdgeQueries()
{
//    int m = model->count1(HORIZONTAL);
    int M = model->count2(HORIZONTAL);
    int V = model->count2(VERTICAL);

    /*  valid IndexRanges start at [0..m1)
     *  have an extend up to (and including) [m2]
     *
     *  if the H/H matrix grows into [2m..3m],
     *  queries need to be mapped to [0..2m]
     *  which is a bit tricky (therefore this test)
     * */
    IndexRange matrix_ranges[] = {
/*      { HORIZONTAL, 0, 1 },
        { HORIZONTAL, 0, m },
        { HORIZONTAL, 1, 2 },
        { HORIZONTAL, 1, m-1 },
        { HORIZONTAL, 2, m+1 },
        { HORIZONTAL, m-1, m },
        { HORIZONTAL, m-1, m+1 },

        { HORIZONTAL, 0, M },
        { HORIZONTAL, 1, M+1 },
        { HORIZONTAL, m-2, M+m-2 },
        { HORIZONTAL, m-1, M+m-1 }*/
    };

    /*  Insert ranges must be subsets of the above
     *  but in the domain [0..2m)
     */
    IndexRange insert_ranges[][2] = {
/*      { { HORIZONTAL, 0, 1 }, { VERTICAL, 0, V } },
        { { HORIZONTAL, 0, m }, { VERTICAL, 0, V } },
        { { HORIZONTAL, 1, 2 }, { VERTICAL, 0, V } },
        { { HORIZONTAL, 1, m-1 }, { VERTICAL, 0, V } },
        { { HORIZONTAL, 2, m+1 }, { VERTICAL, 0, V } },
        { { HORIZONTAL, m-1, m }, { VERTICAL, 0, V } },
        { { HORIZONTAL, m-1, m+1 }, { VERTICAL, 0, V } },

        { { HORIZONTAL, 0, M }, { VERTICAL, 0, V } },
        { { HORIZONTAL, 1, M+1 }, { VERTICAL, 0, V } },
        { { HORIZONTAL, m-2, M+m-2 }, { VERTICAL, 0, V } },
        { { HORIZONTAL, m-1, M+m-1 }, { VERTICAL, 0, V } }
*/
    };
    int COUNT=11;

    for(int s=0; s < COUNT; ++s)
    {
        IndexRange mrange = matrix_ranges[s];
        IndexRange irange1 = insert_ranges[s][0];
        IndexRange irange2 = insert_ranges[s][1];

        Graph::ptr g = createEmptyGraph(mrange);

        testEdgeQueries(g, irange1,irange1);
        testEdgeQueries(g, irange1,irange2);
        testEdgeQueries(g, irange2,irange1);
        testEdgeQueries(g, irange2,irange2);
    }
}

void GraphTestSuite::testEdgeQueries(Graph::ptr g,
                                     IndexRange irange1,
                                     IndexRange irange2)
{
    /*  Queries range from [0..2m)
     */
    int H = model->count2(HORIZONTAL);
    int V = model->count2(VERTICAL);

    IndexRange query_ranges[] = {
        { HORIZONTAL, 0, 1 },
        { HORIZONTAL, 1, H },
        { HORIZONTAL, 2, H },
        { HORIZONTAL, 2, H-1 },
        { HORIZONTAL, H-1, H },
        { HORIZONTAL, 0, std::min(H,64) },

        { VERTICAL, 0, 1 },
        { VERTICAL, 1, V },
        { VERTICAL, 2, V },
        { VERTICAL, 2, V-1 },
        { VERTICAL, V-1, V },
        { VERTICAL, 0, std::min(V,64) },
    };
    int COUNT=12;

    g->clear();
    g->add_range(irange1,irange2);

    for(int i=0; i < COUNT; ++i)
        for(int j=0; j < COUNT; ++j) {
            IndexRange qrange1 = query_ranges[i];
            IndexRange qrange2 = query_ranges[j];
            int M1 = model->count2(qrange1.ori);
            int M2 = model->count2(qrange2.ori);

            bool contains = g->contains_edge(qrange1,qrange2);
            bool intersects = (irange1.intersects(qrange1) || (irange1-M1).intersects(qrange1))
                           && (irange2.intersects(qrange2) || (irange2-M2).intersects(qrange2));
            ASSERT_EQ(contains,intersects);
        }
}

void GraphTestSuite::testBitwiseAnd()
{
    Graph::ptr g1 = createRandomGraph(0,29, 0.5);
    Graph::ptr g2 = createRandomGraph(0,29, 0.5);
    ASSERT_TRUE(false) << "rewrite this case";
/*
    Graph::ptr h (new Graph(*g1));
    h->bitwise_and(*g2);

    for(Orientation o1=HORIZONTAL; o1 <= VERTICAL; ++o1)
        for(Orientation o2=HORIZONTAL; o2 <= VERTICAL; ++o2)
        {
            Rect bounds = g1->rect(o1,o2);
            Q_ASSERT(g1->rect(o1,o2) == h->rect(o1,o2));
            Q_ASSERT(g2->rect(o1,o2) == h->rect(o1,o2));
            for(int i=bounds.i0; i < bounds.i1; ++i)
                for(int j=bounds.j0; j < bounds.j1; ++j)
                    ASSERT_EQ(h->contains_edge(o1,i,o2,j),
                              g1->contains_edge(o1,i,o2,j) && g2->contains_edge(o1,i,o2,j));
        }
    printDiagnostics(h);
*/
}

void GraphTestSuite::createReachabilityGraphs()
{
    rg.clear();
    for(int i=0; i < dpoints.size(); ++i)
        createReachabilityGraph(i,i+1);
}

void GraphTestSuite::testTransitiveClosure()
{  
    typedef std::list<Graph::ptr>::iterator iterator;
    createReachabilityGraphs();

    for(iterator i = rg.begin(); i != rg.end(); )
    {
        Graph::ptr A = *i++;
        Graph::ptr B = *i++;

        testTransitiveClosure(A,B);
    }
}

#define ASSERT_IMPLIES(A,B) ASSERT_TRUE(!(A) || (B))

void GraphTestSuite::testTransitiveClosure(Graph::ptr A, Graph::ptr B)
{
    ASSERT_TRUE(false) << "rewrite this case";
/*
    int offset = A->hmask().upper - B->hmask().lower;
    Graph::ptr C = A->transitiveClosure(*B);
    C->setOriginMerge(A,B);

    printDiagnostics(C);

    for(Orientation o1=HORIZONTAL; o1 <= VERTICAL; ++o1)
        for(Orientation o2=HORIZONTAL; o2 <= VERTICAL; ++o2)
        {
            Rect r = A->rect(o1,o2);
            for(int i=r.i0; i < r.i1; ++i)
                for(int j=r.j0; j < r.j1; ++j)
                {
                    if (o2==VERTICAL)
                    {
                        if (A->contains_edge(o1,i, VERTICAL,j))
                        {
                            for(Orientation o3=HORIZONTAL; o3 <= VERTICAL; ++o3)
                            {
                                Rect q = B->rect(VERTICAL,o3);
                                for(int k=q.j0; k < q.j1; ++k)
                                {
                                    if (B->contains_edge(VERTICAL,j, o3,k))
                                    {   //  Transitive Closure
                                        if (o3==HORIZONTAL)
                                            ASSERT_TRUE(C->contains_edge(o1,i, o3,k+offset));
                                        else
                                            ASSERT_TRUE(C->contains_edge(o1,i, o3,k));
                                    }
                                }
                            }
                        }
                    }
                    if (o2==HORIZONTAL)
                    {
                        // H/H V/H is copied from A to C
                        ASSERT_IMPLIES(
                                A->contains_edge(o1,i,o2,j),
                                C->contains_edge(o1,i,o2,j));
                    }
                }

            r = B->rect(o1,o2);
            for(int i=r.i0; i < r.i1; ++i)
                for(int j=r.j0; j < r.j1; ++j)
                {
                    if (o1==HORIZONTAL)
                    {
                        //  H/H H/V is copied from B to C
                        //  (maybe with an offset)
                        bool b = B->contains_edge(o1,i,o2,j);
                        bool c;
                        if (o2==HORIZONTAL)
                            c = C->contains_edge(o1,i+offset,o2,j+offset);
                        else
                            c = C->contains_edge(o1,i+offset,o2,j);
                        ASSERT_IMPLIES(b,c);
                    }
                }
        }

    rg.push_back(C);
*/
}

void GraphTestSuite::testSpirolator(int min, int max)
{
    std::set<int> values;
    for(int i=min; i < max; ++i) values.insert(i);

    Spirolator s(min,max);
    for( ; (bool)s; ++s) {
        int i = (int)s;
        ASSERT_NE(values.find(i), values.end()) << "duplicate" << (int)s;
        values.erase(i);
    }
    ASSERT_TRUE(values.empty()) <<min<<".."<<max<< " missing "<< *values.begin();
}


void GraphTestSuite::benchMarkDensity()
{

}

std::string label[2] = { "H","V" };

void GraphTestSuite::printDiagnostics(frechet::reach::Graph::ptr g)
{
    printDiagnostics(g,&dpoints);
}

void GraphTestSuite::printDiagnostics(frechet::reach::Graph::ptr g, std::vector<int>* dpoints=nullptr)
{
    Rect hbounds = g->rect(HORIZONTAL,HORIZONTAL);
    g->printOrigin(std::cout);
    if (g->origin.i >= 0 && g->origin.j >= 0 && dpoints)
    {
        int l = dpoints->size();
        std::cout << std::endl
                  << "i="<<g->origin.i
                  << ", j="<<g->origin.j
                  << std::endl
                  << "di=" << (*dpoints)[g->origin.i % l]
                  << ", dj=" << (*dpoints)[g->origin.j % l];
    }
    std::cout << std::endl
              << "h-bounds "
              << hbounds.i0 << ".."
              << hbounds.i1 << std::endl;

    for(Orientation o1=HORIZONTAL; o1 <= VERTICAL; ++o1)
        for(Orientation o2=HORIZONTAL; o2 <= VERTICAL; ++o2)
        {
            Rect bounds = g->rect(o1,o2);
            std::cout << label[o1]<<label[o2]
                      << " "
                      << g->memory(o1,o2) << " Bytes, "
                      << " density " << g->density(o1,o2);
            if (g->is_upper_triangular(o1,o2))
                std::cout << " tri";
            std::cout << std::endl;
        }
    std::cout << "total "
              << g->memory() << " Bytes, "
              << " density " << g->density()
              << std::endl << std::endl;

    //g->print(std::cout, VERTICAL,VERTICAL);
}

