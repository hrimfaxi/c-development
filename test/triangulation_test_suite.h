#ifndef TRIANGULATION_TEST_SUITE_H
#define TRIANGULATION_TEST_SUITE_H

#include <QObject>
#include <QGraphicsView>
#include <QString>

#include <poly/algorithm.h>
#include <poly/parallel.h>
#include <poly_utils.h>
#include <shortest_paths.h>

#include <gtest/gtest.h>
//#include <gmock/gmock-matchers.h>
//#include <reachability_test_suite.h>

using namespace frechet;
using namespace poly;
using namespace reach;

class TriangulationTestSuite : public testing::Test {
protected:
    Algorithm::ptr algo;
    Graph::ptr res;
    FreeSpace::ptr fs;
    PolygonShortestPaths *guibas;

    //  collect valid placements
    static std::vector<QRectF> pl_intervals, pl_gnodes;

protected:
    virtual void SetUp() override;

public:
    void testDecomposition(QString filename);
    void testFanTriangulation(QString filename);
    void testGuibas(QString filename, bool primary, bool swap, bool extra_points);
    void testGuibasFS(QString filename, double epsilon, bool expected);
    void testAlgorithm(QString filename, double eps_ok);
    void testApproximation(QString filename, double precsion);
    void testCurvePolyApproximation(QString filename, double precsion);
    void testIpeClosed266();

protected:
    void initializeAlgorithm(QString filename);
    void readFile(QString filename);
    void testDecomposition(QString filename, const Curve &P);
    void testDecomposition(QString filename,
                           const Curve &P,
                           PolygonUtilities::DecompositionAlgorithm algo,
                           bool verify);

    void saveTriangulation(QString filename,
                           const Curve &P,
                           const Triangulation &tri);
    void savePartition(QString filename,
                       const Curve &P,
                       Partition &parts);

    static void saveAsPdf(QString filename, QGraphicsView *gview);
    static void saveAsSvg(QString filename, QGraphicsView *gview);

    void assertTriangulation(const Triangulation &tri);
    void assertVertex(Triangulation::Vertex_handle v);

    void printFaces(Triangulation::Vertex_handle v, int max = 1000);
    void dumpPolygon(QString fileName, const Curve &P);
    static Curve createCurve(QLineF line);
    //  Note: we need this method later, for visualisation
    static Curve createCurve(Triangulation::Vertex_handle v);

    void assertSubFreeSpace(FreeSpace::ptr a, FreeSpace::ptr b);
    void assertPlacements(const std::vector<Placements> &a, const GraphModel &model);
    void assertSubPlacements(const std::vector<Placements> &a,
                             const std::vector<Placements> &b);
    void assertSubPlacements(const Placements &a, const Placements &b);
    void assertBorderCase(Interval *N, Interval *E, Interval *S, Interval *W);
    void assertSubGraphs(QString label, AlgorithmSingleCore::GraphMap a, AlgorithmSingleCore::GraphMap b);
    void assertSubGraphs(QString label, std::vector<Graph::ptr> a, std::vector<Graph::ptr> b);
    void assertSubGraph(QString label, Graph::ptr a, Graph::ptr b);

    static void prettyPrint(Graph::ptr g);
    static void prettyPrintRow(Graph::ptr g, Orientation ori, int row);
    static void prettyPrintSep(Graph::ptr g, char sep, char sepd);
    static char prettyPrintChar(Graph::ptr g, Orientation o1, int row, Orientation o2, int col);
    static std::ostream& printn(std::ostream& out, int n, char c, char cd);

    static void plotRects(QString filename, const std::vector<QRectF> &);

    //  @deprecated use PolygonFSPath::update() instead
/*  void constructHomeomorphism(Graph::ptr G);
    void constructPath(Graph::ptr G,
                       Orientation o1, int from,
                       Orientation i2, int to,
                       std::list<int> &P,
                       std::list<int> &Q);
*/
    //  debug hook for Shortest Paths
    static void after_target_found(
            PolygonShortestPathsFS *,
            Triangulation::Vertex_handle v,
            bool reachable);

    static void after_placement_added(
            PolygonShortestPathsFS *,
            const Placement *,
            const Placement *);
};

#endif // TRIANGULATION_TEST_SUITE_H
