/**
 * Test suite for Reachability Structure: Merge two cells
 */
#include <QString>


#include <freespace.h>
#include <structure.h>
#include <inputreader.h>
#include <fs_path.h>

#include <reachability_test_suite.h>

#include <QRandomGenerator>
#include <chrono>

// do not use Q_ASSERT, use testing::ASSERT instead
#undef Q_ASSERT

using namespace frechet;
using namespace reach;

void ReachabilityTestSuite::testMergeGeneric()
{
    test_outside_pointers = false;
    //  assert hooks
    Structure::before_merge = ReachabilityTestSuite::before_merge;
    Structure::after_merge = ReachabilityTestSuite::after_merge;
    Structure::after_single_cell = ReachabilityTestSuite::after_single_cell;

    fs = createEmptyFreeSpace(2*count,2*count);

    //   create a free-space with all combined intervals
    for (int i=0; i < count; ++i)
        for(int j=0; j < 2*count; ++j) {
            fs->cell(2*i,j).L = arrangements[i].first;
            fs->cell(2*i+1,j).L = arrangements[i].second;
        }

    for(int j=0; j < count; ++j)
        for(int i=0; i < 2*count; ++i){
            fs->cell(i,2*j).B = arrangements[j].first;
            fs->cell(i,2*j+1).B = arrangements[j].second;
        }    

    //  Make sure that Free-Space crossings are consistent
    for (int i=0; i < fs->n; ++i)
        for(int j=0; j < fs->m; ++j)
        {
            Interval S0 (0,1);
            Interval W0 (0,1);
            Interval& S = (j > 0) ? fs->cell(i,j-1).L : S0;
            Interval& N = fs->cell(i,j).L;
            Interval& E = fs->cell(i,j).B;
            Interval& W = (i > 0) ? fs->cell(i-1,j).B : W0;

            if (S.contains(1.0)
                    && (!N.contains(0.0) || !E.contains(0.0) || !W.contains(1.0)))
            {
                S.upper() = 0.99;
                S.lower() = min(S.lower(),S.upper());
            }
            if (W.contains(1.0)
                    && (!E.contains(0.0) || !N.contains(0.0) || !S.contains(1.0)))
            {
                W.upper() = 0.99;
                W.lower() = min(W.lower(),W.upper());
            }
            if (N.contains(0.0)
                    && (!E.contains(0.0) || !S.contains(1.0) || !W.contains(1.0)))
            {
                N.lower() = 0.01;
                N.upper() = max(N.lower(),N.upper());
            }
            if (E.contains(0.0)
                    && (!N.contains(0.0) || !S.contains(1.0) || !W.contains(1.0)))
            {
                E.lower() = 0.01;
                E.upper() = max(E.lower(),E.upper());
            }
        }

    str = new Structure(fs);
    //  calculate SINGLE structure
    str->calculateSingle();
    delete str;
}

void ReachabilityTestSuite::testMergeCurves()
{
    testMergeCurve("Demo Data/kamm.jscript", 0.5);

    testMergeCurve("data/diamond.svg", 33); // {0.012122442629351285,0.13081314980328637});
    testMergeCurve("data/closed-curves.svg", 33);
                   // wrap-top {3.3104948762451047,3.405972118380186});
                   // wrap-right {7.435038199229136,7.918396076040903});

    testMergeCurve("Demo Data/Overlap.svg", 15); // {});
    testMergeCurve("Demo Data/Overlap.svg", 15.5); // {2.035293991627282,2.037796169717276});
    testMergeCurve("Demo Data/Overlap.svg", 16.5); // {});
}

void ReachabilityTestSuite::testMergeCurve(QString filename, double eps)
{
    //  Test all three combinations of open/closed curves
    Curve P,Q;

    readFile(filename,P,Q, false,false);
    testMergeCurve(P,Q,eps);

    readFile(filename,P,Q, true,false);
    testMergeCurve(P,Q,eps);

    readFile(filename,P,Q, false,true);
    testMergeCurve(Q,P,eps);    // note: closed curve passed as first argument

    readFile(filename,P,Q, true,true);
    testMergeCurve(P,Q,eps);
}


void ReachabilityTestSuite::testMergeColumns(QString filename, double eps, int i1, int i2)
{
    //  assert hooks
    test_outside_pointers=false; // ?
    Structure::before_merge = ReachabilityTestSuite::before_merge;
    Structure::after_merge = ReachabilityTestSuite::after_merge;
    Structure::after_single_cell = ReachabilityTestSuite::after_single_cell;

    Curve P,Q;
    readFile(filename,P,Q,false,false);

    fs = FreeSpace::ptr(new FreeSpace(P,Q));
    fs->calculateFreeSpace(eps);

    ASSERT_EQ(P.size(),fs->n);
    ASSERT_EQ(Q.size(),fs->m);

    str = new Structure(fs);
    str->calculateColumns(i1,i2);

    delete str;

}

void ReachabilityTestSuite::readFile(QString filename, Curve& P, Curve& Q, bool closep, bool closeq)
{
    //QWARN(QDir().absolutePath().toLatin1());
    QFileInfo file ("../../" + filename);
    //QWARN(file.absoluteFilePath().toLatin1());
    InputReader reader;
    reader.readAll(file.absoluteFilePath());

    P = reader.getResults()[0].toCurve();
    Q = reader.getResults()[1].toCurve();

    if (closep)    closeCurve(P);
    if (!closep)   openCurve(P);
    if (closeq)    closeCurve(Q);
    if (!closeq)   openCurve(Q);

    ASSERT_EQ(closep,P.isClosed());
    ASSERT_EQ(closeq,Q.isClosed());
}

void ReachabilityTestSuite::closeCurve(Curve& P)
{
    if (!P.isClosed())
        P.last() = P.first();
}

void ReachabilityTestSuite::openCurve(Curve& P)
{
    if (P.isClosed())
        P.last() += QPointF(0.1,0.1);
}

void ReachabilityTestSuite::testMergeCurve(Curve& P, Curve& Q, double eps) {
    test_outside_pointers = false;
    int concurrency = 4;
    //  assert hooks are not thread safe, sorry...
    if (concurrency <= 1) {
        Structure::before_merge = ReachabilityTestSuite::before_merge;
        Structure::after_merge = ReachabilityTestSuite::after_merge;
        Structure::after_single_cell = ReachabilityTestSuite::after_single_cell;
    }
    else {
        Structure::before_merge = nullptr;
        Structure::after_merge = nullptr;
        Structure::after_single_cell = nullptr;
    }
    //  TODO run tests with P,Q open/closed

    fs = FreeSpace::ptr(new FreeSpace(P,Q));
    fs->calculateFreeSpace(eps);

    ASSERT_EQ(P.size(),fs->n);
    ASSERT_EQ(Q.size(),fs->m);

    str = new Structure(fs);
    Pointer start = str->calculate();

    if (start)
    {   //  calculate feasible path
        FSPath path (fs);
        path.update(path.toPoint(start),eps);
        Curve curves[2];
        curves[0] = path.getPath(0);
        curves[1] = path.getPath(1);
    }

//    Q_ASSERT((!start && !expected_start) ||
//            start->equals(expected_start,1e-9));

    delete str;
}

void ReachabilityTestSuite::assertComplexStructure(Structure* that, const Rect& rect, bool with_fs)
{
    Structure* recover_str = str;
    str = that;

    if (!that->freeSpace()) with_fs = false;

    //  basic assertions (like contiguous intervals), corners
    const BoundaryList& l = str->left();
    const BoundaryList& r = str->right();
    const BoundaryList& b = str->bottom();
    const BoundaryList& t = str->top();

    assertStructure(b.first()->lower(), l.first()->lower(),
                   b.last()->upper(), l.last()->upper());

    //  assertions for each segments & corresponding free-space
    ASSERT_EQ((int) b.first()->lower(), rect.i0);
    ASSERT_EQ((int) b.last()->upper(), rect.i1);
    ASSERT_EQ((int) t.first()->lower(), rect.i0);
    ASSERT_EQ((int) t.last()->upper(), rect.i1);

    ASSERT_EQ((int) l.first()->lower(), rect.j0);
    ASSERT_EQ((int) l.last()->upper(), rect.j1);
    ASSERT_EQ((int) r.first()->lower(), rect.j0);
    ASSERT_EQ((int) r.last()->upper(), rect.j1);

    Pointer i1=l.first();
    Pointer i2=r.first();
    for( ; i1; i1=i1->next(), i2=i2->next()) {
        int j = (int) i1->lower();
        // FreeSpace intervals are [0..1]
        // Reachability intervals are [j..j+1]
        assert2Segments(i1,i2, VERTICAL);

        Interval F1,F2;
        if (with_fs) {
            F1 = (fs->cell(rect.i0, j).L + (double)j);
            F2 = (fs->cell(rect.i1, j).L + (double)j);
            if (j > 0) {
                F1 += (fs->cell(rect.i0, j-1).L + (double)(j-1));
                F2 += (fs->cell(rect.i1, j-1).L + (double)(j-1));
            }
            //  also compare against neighboring F's
            //  for cases like [1,1] overlapping into the next F segment
        }
        assert1Segment(i1,i2, F1);
        assert1Segment(i2,i1, F2);
    }

    i1=b.first();
    i2=t.first();
    for( ; i1; i1=i1->next(), i2=i2->next()) {
        int i = (int) i1->lower();
        assert2Segments(i1,i2, HORIZONTAL);

        Interval F1,F2;
        if (with_fs) {
            F1 = (fs->cell(i, rect.j0).B + (double)i);
            F2 = (fs->cell(i, rect.j1).B + (double)i);
            if (i > 0) {
                F1 += (fs->cell(i-1, rect.j0).B + (double)(i-1));
                F2 += (fs->cell(i-1, rect.j1).B + (double)(i-1));
            }
        }
        assert1Segment(i1,i2, F1);
        assert1Segment(i2,i1, F2);
    }

    //  l,h ranges NEED NOT be continously reachable
/*
    assertLHContingency(l,r);
    assertLHContingency(r,l);
    assertLHContingency(b,t);
    assertLHContingency(t,b);
*/
    str = recover_str;
}

void ReachabilityTestSuite::assertLHContingency(const BoundaryList& l, const BoundaryList& r)
{
    Pointer i1 = l.first();
    Pointer i2 = r.first();
    for( ; i1; i1=i1->next(), i2=i2->next())
    {
        ASSERT_EQ((Interval)*i1, (Interval)*i2);
        if (i1->type!=NON_ACCESSIBLE)
        {
            StructureIterator lh (*str, *i1, i2);
            for( ; lh; ++lh)
                ASSERT_NE(lh->type,NON_ACCESSIBLE);
        }
    }
}


// @Override
void ReachabilityTestSuite::before_merge(
        Structure* a, const Rect* ra,
        Structure* b, const Rect* rb,
        Orientation ori)
{
    ReachabilityTestSuite* suite = ReachabilityTestSuite::suite;
    suite->assertComplexStructure(a,*ra);
    suite->assertComplexStructure(b,*rb);
}

// @Override
void ReachabilityTestSuite::after_merge(
        Structure* a,  const Rect* ra,
        Structure* b,  const Rect* rb,
        Orientation ori)
{
    ReachabilityTestSuite* suite = ReachabilityTestSuite::suite;

    Rect r (ra->i0,ra->j0, rb->i1,rb->j1);
    // Reachabiltiy Struct may be larger than FreeSpace
    //  no use testings against it
    bool with_fs = (a->freeSpace() != nullptr);
    if ((r.i1 > suite->fs->n-1) || (r.j1 > suite->fs->m-1))
        with_fs = false;

    ReachabilityTestSuite::suite->assertComplexStructure(a,r,with_fs);

    //  'b' is not used anymore
    ASSERT_TRUE(b->first(opposite(ori)).empty());
    ASSERT_TRUE(b->second(opposite(ori)).empty());
}
/*
void ReachabilityTestSuite::testAllocators()
{
    /** Compare memory allocators for BoundarySegment
     *
     *  (1) Heap (new)
     *  (2) Pool
     *  TODO (3) thread-local Pool
     * /
    Curve P,Q;
    readFile("data/hilbert.js", P,Q, false,false);

    fs = FreeSpace::ptr(new FreeSpace(P,Q));
    fs->calculateFreeSpace(2200);

    std::cout << "sizeof(BoundarySegment)=" << sizeof(BoundarySegment) << std::endl;
    std::cout << P.size() << " * " << Q.size() << " = " << P.size()*Q.size() << " cells" << std::endl << std::endl;;

    {
        BElementAllocator heap_allocator(sizeof(BoundarySegment));
        std::cout << "=== Heap Allocator ===" << std::endl;
        testAllocator(&heap_allocator);
    }
    {
        BElementPoolAllocator pool_allocator(sizeof(BoundarySegment));
        std::cout << "=== Pool Allocator ===" << std::endl;
        testAllocator(&pool_allocator);
    }

    fs.reset();
}


void ReachabilityTestSuite::testAllocator(BElementAllocator* alloc)
{
    typedef std::chrono::high_resolution_clock Clock;

    auto t1 = Clock::now();

    str = new Structure(fs,alloc);
    Pointer start = str->calculate();
    int segments = str->bottom().size() + str->top().size() + str->left().size() + str->right().size();
    int h_segments = str->bottom().size();
    int v_segments = str->left().size();

    delete str;

    auto t2 = Clock::now();
    long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

    std::cout << (((double)microsecs)/1000) << " ms"
              << std::endl;
    std::cout << segments << " segments."
            <<" RG graph = " << h_segments << " * " << v_segments << std::endl;
}
*/
